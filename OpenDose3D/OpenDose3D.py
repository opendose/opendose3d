import importlib
import json
import os
from pathlib import Path
from datetime import datetime

import ctk
import qt
import slicer
from slicer.ScriptedLoadableModule import (
    ScriptedLoadableModule,
    ScriptedLoadableModuleWidget,
)
from Logic.vtkmrmlutils import *

from Logic.OpenDose3DLogic import OpenDose3DLogic
from Logic.OpenDose3DTest import OpenDose3DTest
from Logic.vtkmrmlutilsTest import vtkmrmlutilsTest

# from Logic.xmlutilsTest import xmlutilsTest
from Logic.nodesTest import nodesTest
from Logic import utils, vtkmrmlutils, dbutils, jsonutils, sentry  # , xmlexport
from Logic.config import readSPECTSensitivity, readOptions, writeOptions, readCalibration
from Logic.gate import Gate
from Logic.constants import Constants
from Logic.nodes import Node
from Logic.logging import getLogger, CRITICAL, ERROR, INFO, WARNING, DEBUG, NOTSET
from Logic.attributes import Attributes

__submoduleNames__ = [
    "OpenDose3DLogic",
    "OpenDose3DTest",
    "PhysicalUnits",
    "constants",
    "vtkmrmlutils",
    "vtkutils",
    "vtkmrmlutilsTest",
    "testbuilder",
    "testutils",
    "errors",
    "Process",
    "nodes",
    "nodesTest",
    "NodeDICOM",
    "NodeMRB",
    "fit_values",
    "fitutils",
    "fit_values_NucmedUI",
    "fitutils_NucmedUI",
    #'xmlutilsTest', 'xmlutils', 'xmlexport','xmlconstants',
    "attributes",
    "config",
    "sentry",
    "logging",
    "utils",
    "dbutils",
    "gate",
    "jsonutils",
]
__package__ = "OpenDose3D"
mod = importlib.import_module("Logic", __name__)
importlib.reload(mod)
__all__ = ["OpenDose3D", "OpenDose3DWidget", "OpenDose3DLogic", "OpenDose3DTest"]


#######################
#                     #
# OpenDose3D         #
#                     #
#######################


class OpenDose3D(ScriptedLoadableModule):
    """Uses ScriptedLoadableModule base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def __init__(self, parent):
        super().__init__(parent)
        self.script_path = utils.getScriptPath()
        # Load the logger
        self.logger = getLogger("OpenDose3D")
        self.logger.setLevel(INFO)
        # TODO make this more human readable by adding spaces
        self.parent.title = "OpenDose3D"
        self.parent.categories = ["Radiotherapy"]
        self.parent.dependencies = []
        # replace with "Firstname Lastname (Organization)"
        self.parent.contributors = [
            "Alex Vergara Gil (INSERM, France)",
            "Jose Alejandro Fragoso Negrin (INSERM, France)",
            "Janick Rueegger (KSA, Switzerland)",
            "Ludovic Ferrer (NANTES, France)",
        ]
        self.parent.helpText = """
            This module implements the full 3D Dosimetry for molecular radiotherapy on multiple time points.\n
            Naming convention: \n
              Volumes must contain the time in hours like 4HR\n
              Volumes must contain the identifier "CTCT" for CT and "ACSC" for SPECT\n
              By pressing the Clean Scene button all files that does not follow this convention will be removed from scene!!\n
            WARNING: This module is currently in beta stage and it CONTAINS BUGS. DO NOT use it for clinical purposes!!!
        """
        self.parent.helpText += '<p>For more information see the <a href="https://gitlab.com/opendose/opendose3d/-/blob/develop/User_Manual.md">online documentation</a>.</p>'
        self.parent.acknowledgementText = """
            This software was originally developed by Alex Vergara Gil (INSERM, France) and Janick Rueegger (KSA, Switzerland),
            in collaboration with Ludovic Ferrer (NANTES, France) and Thiago NM Lima (KSA, Switzerland).
        """  # replace with organization, grant and thanks.

        sentry.init()

        # Add this test to the SelfTest module's list for discovery when the module
        # is created.  Since this module may be discovered before SelfTests itself,
        # create the list if it doesn't already exist.
        try:
            slicer.selfTests
        except AttributeError:
            slicer.selfTests = {}
        slicer.selfTests["OpenDose3DTest"] = self.runTest

    def runTest(self, msec=100, **kwargs):
        """
        :param msec: delay to associate with :func:`ScriptedLoadableModuleTest.delayDisplay()`.
        """
        from coverage import Coverage

        # Setup test execution recording. An sqlite database file will be created here
        cov = Coverage(data_file=str(self.script_path / "Resources" / "coverage.db"))
        cov.exclude("from")
        cov.exclude("import")
        cov.exclude("@(abc\.)?abstractmethod")
        # Record what lines of code are executed during self test run
        cov.start()
        self.logger.info("\n******* Starting Tests of OpenDose3D **********\n")

        # test vtkmrmlutilsTest
        testCase = vtkmrmlutilsTest()
        testCase.messageDelay = msec
        testCase.runTest(**kwargs)

        # test nodesTest
        testCase = nodesTest()
        testCase.messageDelay = msec
        testCase.runTest(**kwargs)

        # test xmlutilsTest
        # testCase = xmlutilsTest()
        # testCase.messageDelay = msec
        # testCase.runTest(**kwargs)

        # test jsonutils
        testCase = jsonutils.jsonutilsTest()
        testCase.messageDelay = msec
        testCase.runTest(**kwargs)

        # test OpenDose3DTest
        # name of the test case class is expected to be <ModuleName>Test
        module = importlib.import_module(self.__module__)
        className = self.moduleName + "Test"
        try:
            TestCaseClass = getattr(module, className)
        except AttributeError:
            # Treat missing test case class as a failure; provide useful error message
            raise AssertionError(
                f"Test case class not found: {self.__module__}.{className} "
            )

        testCase = TestCaseClass()
        testCase.messageDelay = msec
        testCase.runTest(**kwargs)

        self.logger.info("\n******* All tests passed **********\n")
        cov.stop()

        # List python files that should be mentioned in the report
        extensionPythonFiles = []
        excludedFiles = [
            "OpenDose3D.py",
            "Gamma.py",
            "Calibration.py",
            "sentry.py",
            "nonDicomFileSetDescriptor.py",
        ]
        for d, _, fileNames in os.walk(self.script_path):
            fileNames = [
                f for f in fileNames if f.endswith(".py") and not f.startswith("__")
            ]
            fileNames = [
                f for f in fileNames if not f in excludedFiles and not "CLIScripts" in d
            ]
            fileNames = [f for f in fileNames if not f.endswith("Test.py")]
            for f in fileNames:
                extensionPythonFiles.append(os.path.join(d, f))

        # Generate report to a text file
        with open(str(self.script_path / "coverage.txt"), "w") as rf:
            cov.report(morfs=extensionPythonFiles, file=rf, show_missing=True)


#######################
#                     #
# OpenDose3DWidget   #
#                     #
#######################


class OpenDose3DWidget(ScriptedLoadableModuleWidget):
    """Uses ScriptedLoadableModuleWidget base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """
    def saveOptions(self):
        self.options = {
            "Parameters":{ 
                "Center": self.ClinicalCenter.currentText,
                "Isotope": self.isotopsList.currentIndex,
                "InjectionActivity": self.InjectionActivity.text,
                "InjectionTime": self.qDateTimeEdit.dateTime.toString(
                    self.dateTimeDisplayFormat
                ),
                #'usecolors': 1,
                "usePath": str(self.usePath)
            },
            "Registration": {
                "Preset" : "Rigid" if self.Rigid.checked else "Elastix"
            },
            "ADR Calculation": {
                "Calculation Material": self.kernelMaterial.currentText,
                "Density correction": int(self.applyDensityCorrection.isChecked()),
                "Algorithm" : self.doseRateAlgorithm.currentText,
                "Activity threshold": self.activityThreshold,
                "Density threshold": self.densityThreshold / Constants().units["kg_m3"],
                "Materials": {
                    "water":round(Constants().materials["water"]["density"] / Constants().units["kg_m3"],2),
                    "soft":round(Constants().materials["soft"]["density"] / Constants().units["kg_m3"],2),
                    "bone":round(Constants().materials["bone"]["density"] / Constants().units["kg_m3"],2),
                    "lung":round(Constants().materials["lung"]["density"] / Constants().units["kg_m3"],2),
                    "adipose":round(Constants().materials["adipose"]["density"] / Constants().units["kg_m3"],2)
                }
            },
            "Segmentation": {
                "bone marrow extraction": {
                    "enabled": int(self.boneMarrowCheckBox.checked),
                    "minHU" : self.boneMarrowMinHU.text,
                    "maxHU" : self.boneMarrowMaxHU.text
                }
            },
            "Time Integration": {
                "use_fit_functions_from": self.fittingClass,
                "Model Fitting": self.logic.defaultFit
            }
        }
        writeOptions(self.options)

    def setup(self):
        """Creation of the Module Widgets
        This procedure set up all widgets and define all conections
        TODO: Add CT calibration curve
        """
        self.script_path = utils.getScriptPath()
        # self.developerMode = False # Comment to show developer mode
        self.options = readOptions()
        self.calibration = readCalibration()
        self.fittingClass = self.options["Time Integration"]["use_fit_functions_from"]
        self.usePath = Path(self.options["Parameters"]["usePath"])
        self.activityThreshold = float(self.options["ADR Calculation"]["Activity threshold"])
        self.densityThreshold = float(self.options["ADR Calculation"]["Density threshold"]) *\
                                        Constants().units["kg_m3"]
        self.logger = getLogger("OpenDose3D")
        self.logger.setLevel(WARNING)
        self.ADRMPlotsMade = False
        self.ACTMPlotsMade = False
        self.SegmentationPropagated = False
        self.db = self.script_path / "Resources" / "OpenDose3D.db3"
        self.dateTimeDisplayFormat = "yyyy-MM-dd HH:mm:ss"
        self.acqTime = datetime.now()
        self.version = '    v1.0'
        self.study_version = self.version.strip()
        self.InfoVersionMessage = False
        # self.enableColors = bool(self.options['usecolors'])
        self.setupWidgets()
        self.setupConnections()
        # Initial setup of the isotope
        isotopeText = self.isotopsList.currentText
        self.logic = OpenDose3DLogic(parent=self, isotopeText=isotopeText)
        self.test = OpenDose3DTest()
        self.logic.defaultFit = self.options["Time Integration"]["Model Fitting"]
        algoritm = self.options["ADR Calculation"]["Algorithm"]
        doseRateIndex = self.doseRateAlgorithm.findText(algoritm, qt.Qt.MatchFixedString)
        self.doseRateAlgorithm.setCurrentIndex(doseRateIndex)
        self.onIsotopeChange()
        self.logic.setFittingClass(self.fittingClass)
        self.logic.OD3D_version = self.version

        # Refresh Apply button state
        self.checkAvailability()

    def setupWidgets(self):
        # Instantiate widgets ...
        utils.clearLayout(self.layout)
        super().setup()

        #
        # Start of Parameters Area
        #
        """ This section is for calibration data (CT, SPECT) and for general data input like isotope.
        """

        ParametersCollapsibleButton = ctk.ctkCollapsibleButton()
        ParametersCollapsibleButton.text = "Parameters"
        self.layout.addWidget(ParametersCollapsibleButton)
        ParameterLayout = qt.QHBoxLayout(ParametersCollapsibleButton)
        ParametersWidget = qt.QWidget()
        ParameterLayout.addWidget(ParametersWidget)
        ParametersCollapsibleButton.setStyleSheet(
            Constants().createCSS(Constants().colors["Grays"][0])
        )
        ParametersFormLayout = qt.QFormLayout(ParametersWidget)

        calibrationHContainer = qt.QHBoxLayout()

        label = qt.QLabel("Select clinical center   ")
        calibrationHContainer.addWidget(label)

        self.sensitivity = readSPECTSensitivity()
        centers = list(self.sensitivity.keys())
        self.ClinicalCenter = qt.QComboBox()
        self.ClinicalCenter.setToolTip("Create your center in Calibration module")
        self.ClinicalCenter.addItems(centers)
        self.ClinicalCenter.setCurrentIndex(centers.index(self.options["Parameters"]["Center"]))
        self.ClinicalCenter.setSizePolicy(qt.QSizePolicy.Preferred, qt.QSizePolicy.Fixed)
        self.ClinicalCenter.setMinimumWidth(100)
        calibrationHContainer.addWidget(self.ClinicalCenter)

        calibrationHContainer.addStretch()

        label = qt.QLabel("Select calibration date")
        calibrationHContainer.addWidget(label)

        self.calibrationDate = qt.QComboBox()
        self.calibrationDate.setToolTip("Create calibration data in Calibration module")
        self.calibrationDate.setSizePolicy(qt.QSizePolicy.Expanding, qt.QSizePolicy.Fixed)
        calibrationHContainer.addWidget(self.calibrationDate)

        ParametersFormLayout.addRow(calibrationHContainer)

        # isotopes
        self.isotopsList = qt.QComboBox()
        self.defaultIsotopesList = Constants().isotopes.keys()
        center = self.ClinicalCenter.currentText
        if center == "None":
            self.isotopsList.addItems(sorted(self.defaultIsotopesList))
            self.isotopsList.setCurrentIndex(int(self.options["Parameters"]["Isotope"]))
        else:
            centerIsotopesList = list(self.sensitivity[center].keys())
            self.isotopsList.addItems(sorted(centerIsotopesList))
        self.isotopsList.setCurrentIndex(int(self.options["Parameters"]["Isotope"]))
        ParametersFormLayout.addRow("Radionuclide", self.isotopsList)

        # SPECT sensitivity (MBq/counts)
        sensitivityHContainer = qt.QHBoxLayout()
        self.sensitivityUnits = qt.QComboBox()
        self.sensitivityUnits.addItems(
            [
                "Bq/counts",
                "MBq/counts",
                "counts/Bq",
                "counts/MBq",
                "Bqs/counts",
                "MBqs/counts",
                "counts/Bqs",
                "counts/MBqs",
            ]
        )
        index = self.sensitivityUnits.findText(
            self.calibration['SPECTSensitivity']['Units'], qt.Qt.MatchFixedString)
        if index > 0:
            self.sensitivityUnits.setCurrentIndex(index)

        self.spectSensitivity = qt.QLineEdit()
        self.spectSensitivity.setText(
            f'{self.calibration["SPECTSensitivity"]["Value"]:.2f}'
        )
        self.spectSensitivity.setToolTip("Camera factor, select proper unit.")
        self.spectSensitivity.setValidator(
            qt.QDoubleValidator(0.99, 99.99, 4, self.spectSensitivity)
        )

        if self.ClinicalCenter.currentText != "None":
            self.spectSensitivity.readOnly = True
            # self.sensitivityUnits.enabled = False
        sensitivityHContainer.addWidget(self.spectSensitivity)
        sensitivityHContainer.addWidget(self.sensitivityUnits)
        ParametersFormLayout.addRow("Camera Sensitivity      ", sensitivityHContainer)

        self.calibrationTimeContainer = qt.QFrame()
        layout = qt.QHBoxLayout()
        layout.addStretch()
        label = qt.QLabel("Calibration Time (seconds)")
        layout.addWidget(label)
        self.spectCalibrationTime = qt.QLineEdit()
        self.spectCalibrationTime.setText(
            str(self.calibration["SPECTSensitivity"]["Time"])
        )
        layout.addWidget(self.spectCalibrationTime)
        self.calibrationTimeContainer.setLayout(layout)
        ParametersFormLayout.addRow(self.calibrationTimeContainer)
        self.spectCalibrationTime.setValidator(
            qt.QIntValidator(0, 36000, self.spectCalibrationTime)
        )
        self.setCalibrationTimeVisibility()

        activityHContainer = qt.QHBoxLayout()
        activityLabelContainer = qt.QVBoxLayout()
        injectedActLabel = qt.QLabel("Injected Activity (MBq)")
        activityLabelContainer.addWidget(injectedActLabel)
        injectedTimeLabel = qt.QLabel("Injection Time")
        activityLabelContainer.addWidget(injectedTimeLabel)

        activityHContainer.addLayout(activityLabelContainer)
        activityValueContainer = qt.QVBoxLayout()
        self.InjectionActivity = qt.QLineEdit()
        self.InjectionActivity.setText(str(self.options["Parameters"]["InjectionActivity"]))
        self.InjectionActivity.setValidator(
            qt.QDoubleValidator(0.99, 99.99, 2, self.InjectionActivity)
        )
        activityValueContainer.addWidget(self.InjectionActivity)

        self.qDateTimeEdit = qt.QDateTimeEdit(qt.QDate.currentDate())
        self.qDateTimeEdit.setMaximumDate(qt.QDate.currentDate())
        self.qDateTimeEdit.setCalendarPopup(True)
        self.qDateTimeEdit.setDisplayFormat(self.dateTimeDisplayFormat)
        activityValueContainer.addWidget(self.qDateTimeEdit)
        activityHContainer.addLayout(activityValueContainer)

        activityHContainer.addStretch()

        versionLabelContainer = qt.QVBoxLayout()
        icon = qt.QLabel()
        pixmap = qt.QPixmap(f"{self.script_path}/Resources/Icons/icon.png")
        icon.setPixmap(pixmap)
        icon.setScaledContents(True)
        icon.setMaximumSize(50, 50)
        icon.setAlignment(qt.Qt.AlignRight)
        icon.setToolTip("OpenDose3D: a free, open-source clinical dosimetry software for patient-specific dosimetry")
        versionLabelContainer.addWidget(icon)

        version = qt.QLabel()
        # version.setTextFormat(qt.Qt.RichText)
        # icon_path = Path(self.script_path) / "Resources" / "Icons" / "canadaFlag.png"
        # version.setText(f"{self.version}-(Toronto <img src='{icon_path}' align='middle'>)")
        version.setText(f"{self.version}")
        versionLabelContainer.addWidget(version)

        activityHContainer.addLayout(versionLabelContainer)
        activityHContainer.addStretch()

        ParametersFormLayout.addRow(activityHContainer)

        # # Injection Activity
        # self.InjectionActivity = qt.QLineEdit()
        # self.InjectionActivity.setText(str(self.options["Parameters"]['InjectionActivity']))
        # self.InjectionActivity.setValidator(qt.QDoubleValidator(0.99,99.99,2, self.InjectionActivity))
        # ParametersFormLayout.addRow(
        #     "Injected Activity (MBq)", self.InjectionActivity)

        # # Injection Time
        # dateTimeLayout = qt.QHBoxLayout()
        # self.qDateTimeEdit = qt.QDateTimeEdit(qt.QDate.currentDate())
        # self.qDateTimeEdit.setMaximumDate(qt.QDate.currentDate())
        # self.qDateTimeEdit.setCalendarPopup(True)
        # self.qDateTimeEdit.setDisplayFormat(self.dateTimeDisplayFormat)
        # dateTimeLayout.addWidget(self.qDateTimeEdit)
        # dateTimeLayout.addStretch()
        # ParametersFormLayout.addRow("Injection Time", dateTimeLayout)

        # Workflow session
        workflowLayout = qt.QHBoxLayout()
        label = qt.QLabel("Select workflow    ")
        workflowLayout.addWidget(label)
        workflowLayout.addStretch()

        self.activityWorkflow = qt.QRadioButton()
        self.activityWorkflow.text = "Activity"
        workflowLayout.addWidget(self.activityWorkflow)
        workflowLayout.addStretch()

        self.adrWorkflow = qt.QRadioButton()
        self.adrWorkflow.text = "Absorbed Dose Rate"
        self.adrWorkflow.checked = True
        workflowLayout.addWidget(self.adrWorkflow)
        workflowLayout.addStretch()

        self.autoWorkflow = qt.QCheckBox("auto")
        self.autoWorkflow.setChecked(False)
        emm = slicer.app.extensionsManagerModel()
        if not emm.isExtensionInstalled("TotalSegmentator"):
            self.autoWorkflow.hide()
        workflowLayout.addWidget(self.autoWorkflow)

        ParametersFormLayout.addRow(workflowLayout)
        ParametersWidget.setLayout(ParametersFormLayout)
        
        self.initialButton = qt.QPushButton("Initialization")
        self.initialButton.toolTip = (
            "Rename original DICOM files and sort them by time point. \
             Resample the CTs to SPECT resolution. \
             Rescale the SPECT values and calculate density images."
        )
        # self.initializationButton.hide()
        ParametersFormLayout.addRow("Start-up", self.initialButton)

        #
        # End of Parameters Area
        #

        #
        # Start of Registration Area
        #
        """ This section is for registration of multiple time points.
              1. The registration is made CT to CT, you need to provide the reference CT to start with. Once is executed, a transformation will be applied to all volumes in each time point.
              2. User are expected to check the output of this module, is it is incorrect then go to transform module and manually adjust the transformations to get a closer registration in each time point. The finally run again the registration provided here.
              3. Users are not supposed to provide his own transformations as they will be unreadable by the functionality, follow strictly the steps: execute -> manual check -> execute
        """

        self.registrationCollapsibleButton = ctk.ctkCollapsibleButton()
        self.registrationCollapsibleButton.text = "Registration"
        self.registrationCollapsibleButton.collapsed = True
        self.layout.addWidget(self.registrationCollapsibleButton)
        registrationLayout = qt.QHBoxLayout(self.registrationCollapsibleButton)
        registrationWidget = qt.QWidget()
        registrationLayout.addWidget(registrationWidget)
        self.registrationCollapsibleButton.setStyleSheet(
            Constants().createCSS(Constants().colors["Violets"][0])
        )
        registrationFormLayout = qt.QFormLayout(registrationWidget)

        # select preset
        selectrRegistrationTypeContainer = qt.QHBoxLayout()
        self.Rigid = qt.QRadioButton()
        self.Rigid.checked = True
        self.Rigid.hide()
        self.Rigid.text = "Rigid"
        selectrRegistrationTypeContainer.addWidget(self.Rigid)

        self.Elastix = qt.QRadioButton()
        self.Elastix.text = "Elastic"
        self.Elastix.hide()
        selectrRegistrationTypeContainer.addWidget(self.Elastix)
        registrationFormLayout.addRow(
            "Select preset :", selectrRegistrationTypeContainer
        )
        regType = self.options["Registration"]["Preset"]
        if "Elastix" in regType:
            self.Elastix.checked = True
        else:
            self.Rigid.checked = True

        # input volume selector
        self.initialPoint = slicer.qMRMLNodeComboBox()
        self.initialPoint.nodeTypes = [Attributes().vtkMRMLScalarVolumeNode]
        self.initialPoint.addAttribute(
            Attributes().vtkMRMLScalarVolumeNode, Attributes().modality, "CT"
        )
        self.initialPoint.selectNodeUponCreation = True
        self.initialPoint.addEnabled = False
        self.initialPoint.removeEnabled = False
        self.initialPoint.noneEnabled = False
        self.initialPoint.showHidden = False
        self.initialPoint.showChildNodeTypes = False
        self.initialPoint.setMRMLScene(slicer.mrmlScene)
        self.initialPoint.setToolTip("Select the reference Volume.")
        registrationFormLayout.addRow("Reference Volume: ", self.initialPoint)

        # Execute Button
        transformationHContainer = qt.QHBoxLayout()
        self.registrationButton = qt.QPushButton("Execute")
        self.registrationButton.toolTip = "Execute the CT-CT registration."
        transformationHContainer.addWidget(self.registrationButton)
        self.elastixContainer = qt.QHBoxLayout()
        self.installElastixButton = qt.QPushButton("Install Elastix")
        self.installElastixButton.toolTip = (
            "Installs Slicer Elastix if it is not installed."
        )
        transformationHContainer.addWidget(self.installElastixButton)
        emm = slicer.app.extensionsManagerModel()
        if emm.isExtensionInstalled("SlicerElastix"):
            self.installElastixButton.hide()
            self.Rigid.show()
            self.Elastix.show()
        self.switchtoTransformsButton = qt.QPushButton("Check Transforms")
        self.switchtoTransformsButton.toolTip = (
            "Switches to Transforms Module for checking the transformations."
        )
        transformationHContainer.addWidget(self.switchtoTransformsButton)
        registrationFormLayout.addRow(transformationHContainer)

        registrationWidget.setLayout(registrationFormLayout)

        #
        # End of Registration Area
        #

        #
        # Start of Absorbed Dose Rate Calculation Area
        #
        """ This section is for absorbed dose rate calculation. User has to select the proper method depending on study type. If more isotopes are needed then make a request.
        """
        self.doseCollapsibleButton = ctk.ctkCollapsibleButton()
        self.doseCollapsibleButton.text = "Absorbed Dose Rate calculation"
        self.doseCollapsibleButton.collapsed = True
        self.layout.addWidget(self.doseCollapsibleButton)
        doseLayout = qt.QHBoxLayout(self.doseCollapsibleButton)
        doseWidget = qt.QWidget()
        doseLayout.addWidget(doseWidget)
        self.doseCollapsibleButton.setStyleSheet(
            Constants().createCSS(Constants().colors["Grays"][1])
        )
        doseFormLayout = qt.QFormLayout(doseWidget)

        # Dose Rate calculation algorithm
        self.doseRateAlgorithm = qt.QComboBox()
        Algorithms = ["Local Energy Deposition", "FFT Convolution", "Monte Carlo"]
        self.doseRateAlgorithm.addItems(Algorithms)
        doseFormLayout.addRow("Absorbed dose rate algorithm", self.doseRateAlgorithm)

        self.densityCorrectionContainer = qt.QFrame()
        layout2 = qt.QHBoxLayout()
        self.applyDensityCorrection = qt.QCheckBox("Apply density correction")
        self.kernelMaterialContainer = qt.QFrame()
        layout3 = qt.QHBoxLayout()
        label = qt.QLabel("select calculation material")
        layout3.addWidget(label)
        materials = [
            materialName["name"] for materialName in Constants().materials.values()
        ]
        self.kernelMaterial = qt.QComboBox()
        self.kernelMaterial.addItems(materials)
        layout3.addWidget(self.kernelMaterial)
        self.kernelMaterialContainer.setLayout(layout3)
        layout2.addWidget(self.applyDensityCorrection)
        layout2.addWidget(self.kernelMaterialContainer)
        self.densityCorrectionContainer.setLayout(layout2)
        doseFormLayout.addRow(self.densityCorrectionContainer)
        # self.densityCorrectionContainer.hide()

        # Calculate Dose Rate Button
        self.doseRateButton = qt.QPushButton("Calculate Absorbed Dose Rate")
        self.doseRateButton.toolTip = (
            "Execute the absorbed dose rate algorithm specified."
        )
        doseFormLayout.addRow(self.doseRateButton)

        # Monte Carlo section
        self.MonteCarloContainer = qt.QFrame()
        MonteCarloHContainer = qt.QHBoxLayout()
        self.GenerateGateButton = qt.QPushButton("Generate Gate")
        self.GenerateGateButton.toolTip = "Generates Gate macros to run Monte Carlo."
        MonteCarloHContainer.addWidget(self.GenerateGateButton)
        self.ImportGateButton = qt.QPushButton("Import Gate")
        self.ImportGateButton.toolTip = "Import Gate results."
        MonteCarloHContainer.addWidget(self.ImportGateButton)
        self.MonteCarloContainer.setLayout(MonteCarloHContainer)
        doseFormLayout.addRow(self.MonteCarloContainer)
        self.MonteCarloContainer.hide()
        doseWidget.setLayout(doseFormLayout)

        densCorr = self.options["ADR Calculation"]["Density correction"]
        self.applyDensityCorrection.setChecked(densCorr)
        self.kernelMaterialContainer.setVisible(not densCorr)

        material = self.options["ADR Calculation"]["Calculation Material"]
        kernelMaterialIndex = self.kernelMaterial.findText(material, qt.Qt.MatchFixedString)
        self.kernelMaterial.setCurrentIndex(kernelMaterialIndex)

        #
        # End of Absorbed Dose Rate Calculation Area
        #

        #
        # Start of Segmentation Area
        #
        """ This section is to handle with segmentations. User is supposed to provide a segmentation either by the use of Segment Editor module, or by importing his own segmentation. If RTStructs are imported for this reason they must be converted to slicer segments (they must contain a binary map)
              1. The segmentation is propagated to each time point
              2. The user is expected to modify each time point segmentation after propagation to match the movements of desired structures
        """
        self.segmentationCollapsibleButton = ctk.ctkCollapsibleButton()
        self.segmentationCollapsibleButton.text = "Segmentation"
        self.segmentationCollapsibleButton.collapsed = True
        self.layout.addWidget(self.segmentationCollapsibleButton)
        segmentationLayout = qt.QHBoxLayout(self.segmentationCollapsibleButton)
        segmentationWidget = qt.QWidget()
        segmentationLayout.addWidget(segmentationWidget)
        self.segmentationCollapsibleButton.setStyleSheet(
            Constants().createCSS(Constants().colors["Violets"][1])
        )
        segmentationFormLayout = qt.QFormLayout(segmentationWidget)

        # segmentationInstallHContainer = qt.QHBoxLayout()
        # self.installExtraEffectsButton = qt.QPushButton("Install extra effects")
        # self.installExtraEffectsButton.toolTip = (
        #     "Installs Segmentation Extra Effects if it is not installed."
        # )
        # segmentationInstallHContainer.addWidget(self.installExtraEffectsButton)
        # if emm.isExtensionInstalled("SegmentEditorExtraEffects"):
        #     self.installExtraEffectsButton.hide()
        # segmentationFormLayout.addRow(segmentationInstallHContainer)

        segmentationHContainer = qt.QHBoxLayout()

        self.switchtoSegmentEditorButton = qt.QPushButton("VOI segmentation")
        self.switchtoSegmentEditorButton.toolTip = (
            "Switches to Segment Editor for creating the segmentation."
        )
        segmentationHContainer.addWidget(self.switchtoSegmentEditorButton)
        segmentationFormLayout.addRow(segmentationHContainer)

        self.TotalSegmentatorButton = qt.QPushButton("Install Total Segmentator")
        self.TotalSegmentatorButton.toolTip = (
            "Installs Total Segmentator if it is not installed."
        )
        segmentationHContainer.addWidget(self.TotalSegmentatorButton)
        if emm.isExtensionInstalled("TotalSegmentator"):
            self.TotalSegmentatorButton.enabled = False
            self.TotalSegmentatorButton.text = "OAR auto segmentation"
            self.TotalSegmentatorButton.toolTip = (
                "Use Total Segmentator for AI segmentation at reference time point."
            )
        
        # Segmentation Propagation
            
        # Create a QFrame to hold the horizontal layout
        segmenPropagationFrame = qt.QFrame()
        segmenPropagationFrame.setFrameShape(qt.QFrame.Panel)
        segmenPropagationFrame.setFrameShadow(qt.QFrame.Sunken)

        # Horizontal layout for the QFrame
        segmenPropagationHContainer = qt.QHBoxLayout(segmenPropagationFrame)

        # Segmentation label
        segmenPropagationHContainer.addWidget(qt.QLabel("Reference Segmentation: "))

        # Segmentation node selector
        self.segmentation = slicer.qMRMLNodeComboBox()
        self.segmentation.nodeTypes = ["vtkMRMLSegmentationNode"]
        self.segmentation.selectNodeUponCreation = True
        self.segmentation.addEnabled = False
        self.segmentation.removeEnabled = False
        self.segmentation.noneEnabled = False
        self.segmentation.showHidden = False
        self.segmentation.showChildNodeTypes = False
        self.segmentation.setMRMLScene(slicer.mrmlScene)
        self.segmentation.setToolTip("Select the reference segmentation.")
        segmenPropagationHContainer.addWidget(self.segmentation)

        # Propagate button
        self.propagateButton = qt.QPushButton("Propagate Segmentation")
        self.propagateButton.setToolTip("Propagates the segmentation to all time points.")
        segmenPropagationHContainer.addWidget(self.propagateButton)

        # Add the QFrame to the form layout
        segmentationFormLayout.addRow(segmenPropagationFrame)

        # Set layout to the parent widget
        segmentationWidget.setLayout(segmentationFormLayout)

        #
        # End of Segmentation Area
        #

        #
        # Start Time Integration Area
        #
        self.TimeIntegrationCollapsibleButton = ctk.ctkCollapsibleButton()
        self.TimeIntegrationCollapsibleButton.text = "Time integration"
        self.TimeIntegrationCollapsibleButton.collapsed = True
        self.layout.addWidget(self.TimeIntegrationCollapsibleButton)
        TimeIntegrationLayout = qt.QHBoxLayout(self.TimeIntegrationCollapsibleButton)
        TimeIntegrationWidget = qt.QWidget()
        TimeIntegrationLayout.addWidget(TimeIntegrationWidget)
        self.TimeIntegrationCollapsibleButton.setStyleSheet(
            Constants().createCSS(Constants().colors["Grays"][3])
        )
        TimeIntegrationFormLayout = qt.QFormLayout(TimeIntegrationWidget)

        self.fittingTable = qt.QTableWidget()
        columnHeader = [
            "Volume name",
            " Force Zero ",
            "Model Fitting",
        ]
        self.fittingTable.setRowCount(0)
        self.fittingTable.setColumnCount(len(columnHeader))
        self.fittingTable.setHorizontalHeaderLabels(columnHeader)
        self.fittingTable.setStyleSheet(
            "QWidget { background-color: #CDC6BC;} \
            QHeaderView::section { background-color: #BDB6AB; padding: 4px; border: 1px solid #fffff8; font-size: 10pt;} \
            QTableWidget {gridline-color: #fffff8;font-size: 10pt;} \
            QTableWidget QTableCornerButton::section {background-color: #BDB6AB ;border: 1px solid #fffff8;} "
        )
        header = self.fittingTable.horizontalHeader()
        header.setSectionResizeMode(0, qt.QHeaderView.Stretch)
        header.setSectionResizeMode(1, qt.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(2, qt.QHeaderView.Stretch)
        TimeIntegrationFormLayout.addRow(self.fittingTable)

        activityHContainer = qt.QHBoxLayout()

        self.ACTMTablesButton = qt.QPushButton("Activity Tables")
        self.ACTMTablesButton.toolTip = (
            "Creates the activity tables for all structures in each time point."
        )
        activityHContainer.addWidget(self.ACTMTablesButton)

        self.IntegrateActivityButton = qt.QPushButton("Integrate Activity")
        self.IntegrateActivityButton.toolTip = "Integrates activity in time for all structures. Returns cumulative activity per segment"
        activityHContainer.addWidget(self.IntegrateActivityButton)

        self.ACTMTablesButton.hide()
        self.IntegrateActivityButton.hide()

        TimeIntegrationFormLayout.addRow(activityHContainer)

        absdoserateHContainer = qt.QHBoxLayout()

        self.DOSETablesButton = qt.QPushButton("Absorbed Dose Rate Tables")
        self.DOSETablesButton.toolTip = (
            "Creates absorbed dose rate tables for all structures in each time point."
        )
        absdoserateHContainer.addWidget(self.DOSETablesButton)

        self.IntegrateDoseButton = qt.QPushButton("Integrate Absorbed Dose Rate")
        self.IntegrateDoseButton.toolTip = "Integrates absorbed dose rates in time for all structures. Returns absorbed dose per segment"
        absdoserateHContainer.addWidget(self.IntegrateDoseButton)

        TimeIntegrationFormLayout.addRow(absdoserateHContainer)
        TimeIntegrationWidget.setLayout(TimeIntegrationFormLayout)

        #
        # End Time Integration Area
        #

        # Add vertical spacer
        self.layout.addStretch(1)

        #
        # Start Utilities Area
        #
        """ This section contains two functionalities:
              1. Clean Scene: check naming convention and erase everything that does not follow it, also erases all additional data for reprocessing
        """

        UtilitiesCollapsibleButton = ctk.ctkCollapsibleButton()
        UtilitiesCollapsibleButton.text = "Utilities"
        UtilitiesCollapsibleButton.collapsed = True
        self.layout.addWidget(UtilitiesCollapsibleButton)
        UtilitiesFormLayout = qt.QFormLayout(UtilitiesCollapsibleButton)
        UtilitiesHContainer = qt.QHBoxLayout()

        exportHContainer = qt.QHBoxLayout()

        self.ACTMExportButton = qt.QPushButton("Export XML: ACTM")
        self.ACTMExportButton.toolTip = "Export XML for Cumulated Activity Mode."
        exportHContainer.addWidget(self.ACTMExportButton)

        self.DOSEExportButton = qt.QPushButton("Export XML: ADRM")
        self.DOSEExportButton.toolTip = "Export XML for Absorbed Dose Rate Mode."
        exportHContainer.addWidget(self.DOSEExportButton)

        UtilitiesFormLayout.addRow(exportHContainer)

        # DB Compression
        if dbutils.isCompressed():  # Database is compressed
            self.DBCompressionButton = qt.QPushButton("Decompress Database")
            self.DBCompressionButton.toolTip = "Decompress the database."
        else:
            self.DBCompressionButton = qt.QPushButton("Compress Database")
            self.DBCompressionButton.toolTip = "Compress the database."

        UtilitiesHContainer.addWidget(self.DBCompressionButton)

        # Clean up
        # self.CleanUpButton = qt.QPushButton("Clean Scene")
        # self.CleanUpButton.toolTip = "Cleans Scene from all additional or intermediate results."
        # UtilitiesHContainer.addWidget(self.CleanUpButton)

        # Reload
        self.ReloadButton = qt.QPushButton("Reload")
        self.ReloadButton.toolTip = "Reload OpenDose3D."
        UtilitiesHContainer.addWidget(self.ReloadButton)

        # Run test
        self.RunTestButton = qt.QPushButton("Run Test")
        self.RunTestButton.toolTip = "Test for the scripted module. "
        UtilitiesHContainer.addWidget(self.RunTestButton)

        UtilitiesFormLayout.addRow(UtilitiesHContainer)

        #
        # End Utilities Area
        #

        #
        # Start Options Area
        #

        OptionsCollapsibleButton = ctk.ctkCollapsibleButton()
        OptionsCollapsibleButton.text = "Settings"
        OptionsCollapsibleButton.collapsed = True
        self.layout.addWidget(OptionsCollapsibleButton)
        OptionsFormLayout = qt.QFormLayout(OptionsCollapsibleButton)
        OptionsVContainer = qt.QVBoxLayout()

        OptionsHContainer = qt.QHBoxLayout()
        self.LegendButton = qt.QCheckBox("Show Legend in graphs")
        self.ShowLegend = False
        self.LegendButton.setChecked(self.ShowLegend)
        OptionsHContainer.addWidget(self.LegendButton)
        self.LnyaxisButton = qt.QCheckBox("ln y axis")
        self.Lnyaxis = False
        self.LnyaxisButton.setChecked(self.Lnyaxis)
        OptionsHContainer.addWidget(self.LnyaxisButton)
        OptionsVContainer.addLayout(OptionsHContainer)
        OptionsFormLayout.addRow(OptionsVContainer)

        # OptionsHContainer = qt.QHBoxLayout()
        # self.MTButton = qt.QCheckBox("Use multithreads")
        self.multithreading = True
        # self.MTButton.setChecked(self.multithreading)
        # OptionsHContainer.addWidget(self.MTButton)
        # self.useColorButton = qt.QCheckBox("Use colors")
        # self.useColorButton.setChecked(self.enableColors)
        # OptionsHContainer.addWidget(self.useColorButton)
        # OptionsVContainer.addLayout(OptionsHContainer)

        minUH = float(self.options["Segmentation"]["bone marrow extraction"]["minHU"])
        maxUH = float(self.options["Segmentation"]["bone marrow extraction"]["maxHU"])
        BMenabled = float(self.options["Segmentation"]["bone marrow extraction"]["enabled"])

        BMHContainer = qt.QHBoxLayout()
        self.boneMarrowCheckBox = qt.QCheckBox("bone marrow extraction")
        self.boneMarrowCheckBox.setChecked(BMenabled)
        BMHContainer.addWidget(self.boneMarrowCheckBox)

        minLabel = qt.QLabel("min HU ")
        minLabel.setAlignment(0x0002 | 0x0080)
        BMHContainer.addWidget(minLabel)

        self.boneMarrowMinHU = qt.QSpinBox()
        self.boneMarrowMinHU.setMaximum(1000)
        self.boneMarrowMinHU.setValue(minUH)
        self.boneMarrowMinHU.enabled = BMenabled
        BMHContainer.addWidget(self.boneMarrowMinHU)

        maxLabel = qt.QLabel("max HU ")
        maxLabel.setAlignment(0x0002 | 0x0080)
        BMHContainer.addWidget(maxLabel)

        self.boneMarrowMaxHU = qt.QSpinBox()
        self.boneMarrowMaxHU.setMaximum(1000)
        self.boneMarrowMaxHU.setValue(maxUH)
        self.boneMarrowMaxHU.enabled = BMenabled
        BMHContainer.addWidget(self.boneMarrowMaxHU)

        OptionsFormLayout.addRow(BMHContainer)

        KernelSelectorContainer = qt.QHBoxLayout()
        kernelLabel = qt.QLabel("Select Kernel")
        self.kernelSelector = qt.QComboBox()
        self.kernelSelector.addItems(['OpenDoseDVKData'])
        self.kernelUpdateList = qt.QPushButton("Update kernel list")
        KernelSelectorContainer.addWidget(kernelLabel)
        KernelSelectorContainer.addWidget(self.kernelSelector)
        KernelSelectorContainer.addWidget(self.kernelUpdateList)
        
        OptionsFormLayout.addRow(KernelSelectorContainer)

        self.loggingLevels = {
            "Critical": CRITICAL,
            "Error": ERROR,
            "Warning": WARNING,
            "Info": INFO,
            "Debug": DEBUG,
            "All": NOTSET,
        }
        self.loggingButton = qt.QComboBox()
        self.loggingButton.addItems(list(self.loggingLevels.keys()))
        self.loggingButton.setCurrentIndex(2)
        OptionsFormLayout.addRow("Set logging level", self.loggingButton)

        #
        # End Options Area
        #

    def setupConnections(self):
        # Connect widgets

        # Parameters
        self.isotopsList.currentTextChanged.connect(self.onIsotopeChange)
        self.ClinicalCenter.currentTextChanged.connect(self.onCenterChange)
        self.calibrationDate.currentTextChanged.connect(self.onCalibrationDateChanged)
        self.sensitivityUnits.currentIndexChanged.connect(
            self.setCalibrationTimeVisibility
        )
        self.activityWorkflow.clicked.connect(self.onActivityWorkflow)
        self.adrWorkflow.clicked.connect(self.onADRWorkflow)
        self.autoWorkflow.stateChanged.connect(self.onAutoWorkflow)
        self.qDateTimeEdit.dateTimeChanged.connect(self.onInjectedDateChanged)
        self.initialButton.clicked.connect(self.onInitialButton)
        self.InjectionActivity.textChanged.connect(self.onInjectionActivityChanged)
        self.spectCalibrationTime.textChanged.connect(self.onSpectCalibrationTime)
        
        # Registration
        self.registrationButton.clicked.connect(self.onRegistrationButton)
        self.Rigid.clicked.connect(self.saveOptions)
        self.Elastix.clicked.connect(self.saveOptions)
        self.installElastixButton.clicked.connect(
            lambda: self.onInstallModuleButton("SlicerElastix")
        )
        self.initialPoint.currentNodeChanged.connect(self.onInitialPointChange)
        self.switchtoTransformsButton.clicked.connect(
            lambda: self.switchModule("Transforms")
        )

        # Absorbed Dose Rate
        self.doseRateAlgorithm.currentIndexChanged.connect(self.onDoseRateAlgorithm)
        self.applyDensityCorrection.stateChanged.connect(self.onDensityCorrectionCheck)
        self.doseRateButton.clicked.connect(self.onDoseRateButton)
        self.kernelMaterial.currentIndexChanged.connect(self.onKernelUpdateList)
        self.GenerateGateButton.clicked.connect(self.onGenerateGate)
        self.ImportGateButton.clicked.connect(self.onImportMonteCarlo)

        # Segmentation
        self.propagateButton.clicked.connect(self.onPropagateButton)
        # self.installExtraEffectsButton.clicked.connect(
        #     lambda: self.onInstallModuleButton("SegmentEditorExtraEffects")
        # )
        self.TotalSegmentatorButton.clicked.connect(self.onTotalSegmentator)
        self.segmentation.currentNodeChanged.connect(self.checkAvailability)
        self.switchtoSegmentEditorButton.clicked.connect(
            lambda: self.switchModule("SegmentEditor")
        )

        # Tables and Plots
        self.ACTMTablesButton.clicked.connect(self.onACTMTablesButton)
        self.DOSETablesButton.clicked.connect(self.onDOSETablesButton)

        # Time Integration

        self.IntegrateActivityButton.clicked.connect(self.onIntegrateActivityButton)
        self.IntegrateDoseButton.clicked.connect(self.onIntegrateDoseButton)
        self.fittingTable.itemChanged.connect(self.onTableItemChanged)

        # Utilities
        self.DBCompressionButton.clicked.connect(self.onCompressDB)
        # self.CleanUpButton.clicked.connect(self.onCleanUpButton)
        self.ReloadButton.clicked.connect(self.onReloadButton)
        self.RunTestButton.clicked.connect(self.onRunTestButton)
        # self.ACTMExportButton.clicked.connect(self.onACTMExportButton)
        # self.DOSEExportButton.clicked.connect(self.onDOSEExportButton)

        # Options
        self.LegendButton.stateChanged.connect(self.onLegendButton)
        self.LnyaxisButton.stateChanged.connect(self.onLnyaxisButton)
        # self.MTButton.stateChanged.connect(self.onMTButton)
        # self.useColorButton.stateChanged.connect(self.useColors)
        self.boneMarrowCheckBox.stateChanged.connect(self.onboneMarrowCheckBox)
        self.kernelUpdateList.clicked.connect(self.onKernelUpdateList)
        self.loggingButton.currentIndexChanged.connect(self.onloggingButton)

    def onReload(self):
        """
        Override reload scripted module widget representation.
        """
        self.logger.warning("Reloading OpenDose3D")
        importlib.reload(mod)
        for submoduleName in __submoduleNames__:
            mod1 = importlib.import_module(".".join(["Logic", submoduleName]), __name__)
            importlib.reload(mod1)

        if isinstance(self, ScriptedLoadableModuleWidget):
            ScriptedLoadableModuleWidget.onReload(self)

    def onDensityCorrectionCheck(self):
        if self.applyDensityCorrection.isChecked():
            self.kernelMaterialContainer.hide()
        else:
            self.kernelMaterialContainer.show()
        self.saveOptions()

    def onActivityWorkflow(self):
        self.doseCollapsibleButton.hide()
        self.DOSETablesButton.hide()
        self.IntegrateDoseButton.hide()
        self.ACTMTablesButton.show()
        self.IntegrateActivityButton.show()
        layoutManager = slicer.app.layoutManager()
        if self.ACTMPlotsMade:
            # Show chart in layout if it exists
            chartname = f"T:CHRT Activity"
            plotChartNode = slicer.util.getFirstNodeByClassByName(
                Attributes().vtkMRMLPlotChartNode, chartname
            )
            if plotChartNode:
                showChartinLayout(plotChartNode)
                # Update fitting table
                self.onFillFittingTable(self.segmentation.currentNode())
                # Show integration table if it exists
                tableName = f"T:TABL Activity integrated"
                tableNode = slicer.util.getFirstNodeByClassByName(
                    Attributes().vtkMRMLTableNode, tableName
                )
                if tableNode:
                    showTable(tableNode)
                else:
                    layoutManager.setLayout(
                        slicer.vtkMRMLLayoutNode.SlicerLayoutFourUpPlotView
                    )
        else:
            layoutManager.setLayout(slicer.vtkMRMLLayoutNode.SlicerLayoutFourUpView)
        if self.autoWorkflow.isChecked():
            self.initialButton.text = "Compute Cumulated Activity"
        self.checkAvailability()

    def onADRWorkflow(self):
        if not self.autoWorkflow.isChecked():
            self.doseCollapsibleButton.show()
        self.DOSETablesButton.show()
        self.IntegrateDoseButton.show()
        self.ACTMTablesButton.hide()
        self.IntegrateActivityButton.hide()
        layoutManager = slicer.app.layoutManager()
        if self.ADRMPlotsMade:
            # Show chart in layout if it exists
            chartname = f"T:CHRT {self.doseRateAlgorithm.currentText}"
            plotChartNode = slicer.util.getFirstNodeByClassByName(
                Attributes().vtkMRMLPlotChartNode, chartname
            )
            if plotChartNode:
                showChartinLayout(plotChartNode)
                # Update fitting table
                self.onFillFittingTable(self.segmentation.currentNode())
                # Show integration table if it exists
                tableName = f"T:TABL {self.doseRateAlgorithm.currentText} integrated"
                tableNode = slicer.util.getFirstNodeByClassByName(
                    Attributes().vtkMRMLTableNode, tableName
                )
                if tableNode:
                    showTable(tableNode)
                else:
                    layoutManager.setLayout(
                        slicer.vtkMRMLLayoutNode.SlicerLayoutFourUpPlotView
                    )
        else:
            layoutManager.setLayout(slicer.vtkMRMLLayoutNode.SlicerLayoutFourUpView)
        if self.autoWorkflow.isChecked():
            self.initialButton.text = "Compute Absorbed Dose"
        self.checkAvailability()

    def onAutoWorkflow(self):
        if self.autoWorkflow.isChecked():
            self.registrationCollapsibleButton.hide()
            self.doseCollapsibleButton.hide()
            self.segmentationCollapsibleButton.hide()
            self.TimeIntegrationCollapsibleButton.hide()
            if self.adrWorkflow.isChecked():
                self.initialButton.text = "Compute Absorbed Dose"
            elif self.activityWorkflow.isChecked():
                self.initialButton.text = "Compute Cumulated Activity"
        else:
            self.registrationCollapsibleButton.show()
            if self.adrWorkflow.checked:
                self.doseCollapsibleButton.show()
            self.segmentationCollapsibleButton.show()
            self.TimeIntegrationCollapsibleButton.show()
            self.initialButton.text = "Initialization"

    # def onMTButton(self):
    #     self.multithreading = self.MTButton.isChecked()

    def onLegendButton(self):
        self.ShowLegend = self.LegendButton.isChecked()
        graphnodes = slicer.util.getNodesByClass(Attributes().vtkMRMLPlotChartNode)

        for graphnode in graphnodes:
            graphnode.SetLegendVisibility(self.ShowLegend)

    def onboneMarrowCheckBox(self):
        if self.boneMarrowCheckBox.checked:
            self.boneMarrowMinHU.enabled = True
            self.boneMarrowMaxHU.enabled = True
        else:
            self.boneMarrowMinHU.enabled = False
            self.boneMarrowMaxHU.enabled = False
        self.saveOptions()

    def onLnyaxisButton(self):
        if self.logic:
            self.logic.logyaxis = self.LnyaxisButton.isChecked()
        graphnodes = slicer.util.getNodesByClass(Attributes().vtkMRMLPlotChartNode)

        for graphnode in graphnodes:
            graphnode.SetYAxisLogScale(self.LnyaxisButton.isChecked())

    def onInstallModuleButton(self, module="SlicerElastix"):
        utils.installModule(module)

    def onloggingButton(self):
        levelText = self.loggingButton.currentText
        levelValue = self.loggingLevels[levelText]
        self.logger.setLevel(levelValue)

    def switchModule(self, moduleName):
        # Switch to another module
        pluginHandlerSingleton = slicer.qSlicerSubjectHierarchyPluginHandler.instance()
        pluginHandlerSingleton.pluginByName("Default").switchToModule(moduleName)

    def onCleanUpButton(self):
        self.logic.clean()
        self.ADRMPlotsMade = False
        self.ACTMPlotsMade = False
        self.InfoVersionMessage = False
        self.checkAvailability()

    def onReloadButton(self):
        self.onReload()

    def onRunTestButton(self):
        if self.test:
            self.test.FullTest_positive()

    def onRenameButton(self):
        injectionTime = self.qDateTimeEdit.dateTime.toString(self.dateTimeDisplayFormat)
        self.logic.standardise(self.InjectionActivity.text, injectionTime)
        self.saveOptions()
        self.checkAvailability()

    def onResampleButton(self):
        self.logic.resampleCT()
        self.checkAvailability()

    def onRescaleButton(self):
        self.saveOptions()

        try:
            sensitivity = float(self.spectSensitivity.text)
        except Exception as e:
            self.logger.error(f"Sensitivity not valid, please check\n{e}")

        try:
            calibrationTime = float(self.spectCalibrationTime.text)
        except Exception as e:
            self.logger.error(f"Calibration Time not valid, please check\n{e}")

        self.calibration["SPECTSensitivity"] = {
            "Value": float(sensitivity),
            "Units": self.sensitivityUnits.currentText,
            "Time": int(calibrationTime),
        }

        self.logic.scaleValues(
            calibration=self.calibration,
            injectedActivity=self.InjectionActivity.text,
            injectionTime=self.qDateTimeEdit.dateTime.toString(
                self.dateTimeDisplayFormat
            ),
            clinicalCenter=self.ClinicalCenter.currentText,
        )
        self.checkAvailability()

    def onRegistrationButton(self):
        referenceNode = self.initialPoint.currentNode()
        if self.Elastix.checked:
            self.logic.createTransforms(referenceNode, preset="Elastix")
        else:
            self.logic.createTransforms(referenceNode, preset="Rigid")
        self.checkAvailability()

    def onDoseRateAlgorithm(self):
        algorithm = self.doseRateAlgorithm.currentText
        if "local" in algorithm.lower() or "convolution" in algorithm.lower():
            self.densityCorrectionContainer.show()
        else:
            self.densityCorrectionContainer.hide()

        if "Monte Carlo" in algorithm:
            self.MonteCarloContainer.show()
            self.doseRateButton.hide()
        else:
            self.MonteCarloContainer.hide()
            self.doseRateButton.show()

        chartname = f"T:CHRT {algorithm}"
        plotChartNode = slicer.util.getFirstNodeByClassByName(
            Attributes().vtkMRMLPlotChartNode, chartname
        )

        if plotChartNode and self.ADRMPlotsMade:
            try:
                showChartinLayout(plotChartNode)
                self.onFillFittingTable(self.segmentation.currentNode())
            except:
                layoutManager = slicer.app.layoutManager()
                layoutManager.setLayout(slicer.vtkMRMLLayoutNode.SlicerLayoutFourUpView)
                self.ADRMPlotsMade = False

        tableName = f"T:TABL {algorithm} integrated"
        tableNode = slicer.util.getFirstNodeByClassByName(Attributes().vtkMRMLTableNode, tableName)
        if tableNode:
            showTable(tableNode)
        else:
            layoutManager = slicer.app.layoutManager()
            layoutManager.setLayout(slicer.vtkMRMLLayoutNode.SlicerLayoutFourUpView)

        self.saveOptions()
        self.checkAvailability()

    def setCalibrationTimeVisibility(self):
        if "Bqs" in self.sensitivityUnits.currentText:
            self.calibrationTimeContainer.hide()
        else:
            self.calibrationTimeContainer.show()

    def onCalibrationDateChanged(self):
        center = self.ClinicalCenter.currentText
        isotopeText = self.isotopsList.currentText
        calibrationDate = self.calibrationDate.currentText
        if (
            center in self.sensitivity
            and isotopeText in self.sensitivity[center]
            and calibrationDate in self.sensitivity[center][isotopeText]
        ):
            lsensitivity = self.sensitivity[center][isotopeText][calibrationDate]
            self.spectSensitivity.setText(f'{float(lsensitivity["Sensitivity"]):.2f}')
            index = self.sensitivityUnits.findText(
                lsensitivity["Units"], qt.Qt.MatchFixedString
            )
            if index > 0:
                self.sensitivityUnits.setCurrentIndex(index)
            self.spectCalibrationTime.setText(lsensitivity["Time"])

    def onCenterChange(self):
        self.logger.info(f"Loading data for center {self.ClinicalCenter.currentText}")
        center = self.ClinicalCenter.currentText
        self.isotopsList.clear()
        if center == "None":
            self.isotopsList.addItems(sorted(self.defaultIsotopesList))
            self.isotopsList.setCurrentIndex(int(self.options["Parameters"]["Isotope"]))
            self.spectSensitivity.readOnly = False
            self.sensitivityUnits.enabled = True
            self.calibrationDate.clear()
        else:
            centerIsotopesList = list(self.sensitivity[center].keys())
            self.isotopsList.addItems(sorted(centerIsotopesList))
            self.spectSensitivity.readOnly = True
            self.sensitivityUnits.enabled = False
        self.saveOptions()

    def onIsotopeChange(self):
        isotopeText = self.isotopsList.currentText
        if isotopeText:
            self.logger.info(f"Loading data for isotope {isotopeText}")
            center = self.ClinicalCenter.currentText
            self.logic.Isotope = dbutils.setupIsotope(isotopeText)
            self.calibrationDate.clear()
            if center in self.sensitivity and isotopeText in self.sensitivity[center]:
                lsensitivity = self.sensitivity[center][isotopeText]
                dates = list(lsensitivity.keys())
                self.calibrationDate.addItems(dates)
            self.saveOptions()

    def onInitialPointChange(self):
        lnode = self.initialPoint.currentNode()
        if not lnode:  # The node must has been selected
            self.registrationButton.enabled = False
            return
        if lnode.GetImageData() is None:  # The selected node must contain an image
            self.registrationButton.enabled = False
            return
        lName = lnode.GetName()
        # This node must be a CT
        if "CTCT" in lName:
            self.registrationButton.enabled = True
            self.logger.info(f"Selected {lName} as reference volume")
        else:
            self.registrationButton.enabled = False
            self.logger.error(f"Selected volume {lName} is not a CT node")

    def onCompressDB(self):
        if dbutils.isCompressed():  # Database is compressed
            dbutils.decompressDataBase()
        else:
            dbutils.compressDataBase()
        self.checkAvailability()

    def onDoseRateButton(self):
        # initialize
        mode = self.doseRateAlgorithm.currentText
        self.logic.densityCorrection = self.applyDensityCorrection.isChecked()
        kernelMaterial = self.getKernelMaterial()
        # select method
        if "local" in mode.lower():
            self.logic.LED(
                DVK_material=kernelMaterial, 
                actThreshold=self.activityThreshold,
                densThreshold=self.densityThreshold
            )
        elif "convolution" in mode.lower():
            self.logic.compressed = dbutils.isCompressed()
            self.logic.convolution(
                isotopeText=self.isotopsList.currentText,
                DVK_material=kernelMaterial,
                actThreshold=self.activityThreshold,
                densThreshold=self.densityThreshold,
                mode=mode,
                multithreaded=self.multithreading,
                kernelOrigin=self.kernelSelector.currentText
            )
        elif "monte" in mode.lower():
            self.logic.importMonteCarlo()

        self.checkAvailability()
        self.fittingTable.enabled = False
        self.IntegrateDoseButton.enabled = False

    def onGenerateGate(self):
        directory = qt.QFileDialog.getExistingDirectory(
            self.parent,
            Constants().strings["Choose Directory"],
            self.usePath,
            qt.QFileDialog.ShowDirsOnly | qt.QFileDialog.DontResolveSymlinks,
        )
        if directory:
            self.usePath = Path(directory)
            self.Gate = Gate(
                directory=self.usePath,
                calibration=self.calibration,
                isotope=self.isotopsList.currentText,
            )
            self.Gate.generate()
        self.saveOptions()

    def onImportMonteCarlo(self):
        directory = qt.QFileDialog.getExistingDirectory(
            self.parent,
            Constants().strings["Choose Directory"],
            self.usePath,
            qt.QFileDialog.ShowDirsOnly | qt.QFileDialog.DontResolveSymlinks,
        )
        if directory:
            self.usePath = Path(directory)
            self.logic.importMonteCarlo(directory=self.usePath)
        self.checkAvailability()
        self.fittingTable.enabled = False
        self.IntegrateDoseButton.enabled = False

    def onPropagateButton(self):
        self.logic.Propagate(self.segmentation.currentNode())
        self.logic.ResampleSegmentToSPECT()
        self.SegmentationPropagated = True
        self.checkAvailability()
        self.fittingTable.enabled = False
        self.IntegrateDoseButton.enabled = False
        self.IntegrateActivityButton.enabled = False

    def onTotalSegmentator(self):
        emm = slicer.app.extensionsManagerModel()
        if not emm.isExtensionInstalled("TotalSegmentator"):
            self.onInstallModuleButton("TotalSegmentator")
        else:
            try:
                import TotalSegmentator

                TotalSegmentator.TotalSegmentatorLogic().setupPythonRequirements()
            except Exception as e:
                qt.QMessageBox.critical(
                    slicer.util.mainWindow(),
                    "Error",
                    "Total Segmentor failed during setup",
                )
            try:
                self.logic.autoSegmentation(
                    self.autoWorkflow.isChecked(), self.adrWorkflow.checked
                )
                if self.boneMarrowCheckBox.checked:
                    self.logic.boneMarrowSegmentation(
                        minHU=self.boneMarrowMinHU.value,
                        maxHU=self.boneMarrowMaxHU.value,
                        autoADR=self.autoWorkflow.isChecked(),
                    )
                self.logic.ResampleSegmentToSPECT()
            except Exception as e:
                qt.QMessageBox.critical(slicer.util.mainWindow(), "Error", str(e))
                return

    def onACTMTablesButton(self):
        if self.logic.STPmode:
            self.onPropagateButton()
        self.logic.setColors()
        self.logic.computeNewValues(mode="Activity")
        self.logic.processTimeIntegrationVariables(mode="Activity")
        self.logic.showTableandPlotTimeIntegrationVariables(
            mode="Activity", showlegend=self.ShowLegend
        )
        self.logic.defaultFitting(mode="Activity", algorithm="")
        self.onFillFittingTable(self.segmentation.currentNode())
        self.ACTMPlotsMade = True
        self.checkAvailability()

    def onDOSETablesButton(self):
        if self.logic.STPmode:
            self.onPropagateButton()
        self.logic.setColors()
        self.logic.computeNewValues(
            mode="Absorbed Dose Rate", algorithm=self.doseRateAlgorithm.currentText
        )
        self.logic.processTimeIntegrationVariables(
            mode="Absorbed Dose Rate", algorithm=self.doseRateAlgorithm.currentText
        )
        self.logic.showTableandPlotTimeIntegrationVariables(
            mode="Absorbed Dose Rate",
            algorithm=self.doseRateAlgorithm.currentText,
            showlegend=self.ShowLegend,
        )
        self.logic.defaultFitting(
            mode="Absorbed Dose Rate", algorithm=self.doseRateAlgorithm.currentText
        )
        index = self.kernelMaterial.findText(self.logic.calculationMaterial, 
                                             qt.Qt.MatchFixedString)
        self.kernelMaterial.setCurrentIndex(index)
        self.applyDensityCorrection.setChecked(self.logic.densityCorrection)
        self.onFillFittingTable(self.segmentation.currentNode())
        self.ADRMPlotsMade = True
        self.saveOptions()
        self.checkAvailability()

    def onTableItemChanged(self, sender):
        plotViewNode = slicer.app.layoutManager().plotWidget(0).mrmlPlotViewNode()
        plotChartNode = plotViewNode.GetPlotChartNode()
        seriesNames = []
        plotChartNode.GetPlotSeriesNodeNames(seriesNames)

        refSegmentName = Node.getFolderChildren(self.logic.referenceFolderID)[
            "SEGM"
        ].name
        refSegmentNode = slicer.util.getFirstNodeByClassByName(
            Attributes().vtkMRMLSegmentationNode, refSegmentName
        )
        segmentID = refSegmentNode.GetSegmentation().GetSegmentIdBySegmentName(
            sender.text()
        )

        model = plotChartNode.GetName().split()[1]
        if "activity" in model.lower():
            plotname = f"P:PLOT Activity CA_{sender.text()}"
        else:
            algo = self.doseRateAlgorithm.currentText
            plotname = f"P:PLOT {algo} DR_{sender.text()}"

        plt = slicer.util.getFirstNodeByClassByName(Attributes().vtkMRMLPlotSeriesNode, plotname)

        plt_fit = slicer.util.getFirstNodeByClassByName(
            Attributes().vtkMRMLPlotSeriesNode, f"{plotname}_fit"
        )

        if sender.checkState() and plotname not in seriesNames:
            plotChartNode.AddAndObservePlotSeriesNodeID(plt.GetID())
            plotChartNode.AddAndObservePlotSeriesNodeID(plt_fit.GetID())
            refSegmentNode.GetDisplayNode().SetSegmentVisibility(segmentID, True)
        elif not sender.checkState():
            plotChartNode.RemovePlotSeriesNodeID(plt.GetID())
            plotChartNode.RemovePlotSeriesNodeID(plt_fit.GetID())
            refSegmentNode.GetDisplayNode().SetSegmentVisibility(segmentID, False)

    def onFillFittingTable(self, segmentationNode):
        segmentation = segmentationNode.GetSegmentation()
        self.fittingTable.setRowCount(len(segmentation.GetSegmentIDs()))
        algorithm = self.doseRateAlgorithm.currentText
        segColors = self.logic.segmentsColors

        for i, segmentID in enumerate(segmentation.GetSegmentIDs()):
            segment = segmentation.GetSegment(segmentID).GetName()

            if self.activityWorkflow.checked:
                fit = self.logic.fittingVIOs[f"CA_{segment}_Activity"]
                inc = self.logic.forceZERO[f"CA_{segment}_Activity"]
            else:
                fit = self.logic.fittingVIOs[f"DR_{segment}_{algorithm}"]
                inc = self.logic.forceZERO[f"DR_{segment}_{algorithm}"]

            checkbox = qt.QTableWidgetItem(segment)
            color = list(segColors[segment])
            checkbox.setForeground(qt.QBrush(qt.QColor(color[0], color[1], color[2])))
            checkbox.setCheckState(2)
            self.fittingTable.setItem(i, 0, checkbox)

            if "single point" in fit.fit_type:
                self.fittingTable.setColumnCount(2)
                self.fittingTable.setHorizontalHeaderLabels(
                    ["Volume name", " Tef (h) "]
                )
                if self.activityWorkflow.checked:
                    Tef = self.logic.fittingVIOs[f"CA_{segment}_Activity_Tef"]
                else:
                    Tef = self.logic.fittingVIOs[f"DR_{segment}_{algorithm}_Tef"]
                tefLineEdit = qt.QLineEdit(f'{Tef:.2f}')
                self.fittingTable.setCellWidget(i, 1, tefLineEdit)
                tefLineEdit.setValidator(
                    qt.QDoubleValidator(0.99, 99.99, 2, tefLineEdit)
                )
                tefLineEdit.textChanged.connect(
                    lambda _, lineEdit=tefLineEdit: self.onUpdateSingleTP(lineEdit)
                )
            else:
                forceZeroCheck = qt.QCheckBox()
                forceZeroCheck.setStyleSheet("padding-left: 40px;")
                if inc:
                    forceZeroCheck.setCheckState(2)
                else:
                    forceZeroCheck.setCheckState(0)
                self.fittingTable.setCellWidget(i, 1, forceZeroCheck)
                
                forceZeroCheck.stateChanged.connect(
                    lambda _, checkbox_=forceZeroCheck: self.forceZoreStateChanged(checkbox_)
                )

                fitComboBox = qt.QComboBox()
                fit_functions = self.logic.FIT_FUNCTIONS_DIC["ALL"]
                base_items = ["trapezoid", *fit_functions.keys()]
                # Append "auto-fit" if there are more than one function in the list
                if len(fit_functions) >= 2:
                    base_items.append("auto-fit")
                fitComboBox.addItems(base_items)

                if fit:
                    index = fitComboBox.findText(fit.fit_type, qt.Qt.CaseSensitive)
                    fitComboBox.setCurrentIndex(index)
                else:
                    fitComboBox.setCurrentIndex(1)
                
                self.fittingTable.setCellWidget(i, 2, fitComboBox)
                fitComboBox.currentIndexChanged.connect(
                    lambda _, comboBox=fitComboBox: self.onUpdateFitting(comboBox)
                )    

    def onUpdateSingleTP(self, lineEdit):
        # Get the row and column index of the lineEdit widget
        index = self.fittingTable.indexAt(lineEdit.pos)
        row = index.row()
        col = index.column()

        tefLineEdit_widget = self.fittingTable.cellWidget(row, 1)
        VIOname = self.fittingTable.item(row, 0).text()
        ADRalgorithm = ""

        try:
            value = float(tefLineEdit_widget.text)
            if not (0.01 <= value <= self.logic.Isotope["T_h"]):
                # If not, reset to a default value
                tefLineEdit_widget.setText(f'{self.logic.Isotope["T_h"]:.2f}')
        except ValueError:
            # If conversion to float fails, reset to a min value
            if not tefLineEdit_widget.text:
                tefLineEdit_widget.setText("0.01")

        if self.adrWorkflow.checked:
            workflow = "Absorbed Dose Rate"
            ADRalgorithm = self.doseRateAlgorithm.currentText
            tableName = f"T:TABL {ADRalgorithm} integrated"
            tableNode = slicer.util.getFirstNodeByClassByName(
                Attributes().vtkMRMLTableNode, tableName
            )
        else:
            workflow = "Activity"
            tableName = f"T:TABL Activity integrated"
            tableNode = slicer.util.getFirstNodeByClassByName(
                Attributes().vtkMRMLTableNode, tableName
            )

        try:
            self.logic.updateFittingSingleTP(
                mode=workflow,
                algorithm=ADRalgorithm,
                segmentName=VIOname,
                segmentRow=row,
                Tef=tefLineEdit_widget.text,
            )
        except Exception as e:
            qt.QMessageBox.critical(slicer.util.mainWindow(), "Error", e)

        if tableNode:
            self.logic.integrate(mode=workflow, algorithm=ADRalgorithm)

        self.fittingTable.item(row, 0).setCheckState(2)

    def forceZoreStateChanged(self, checkBox):
        index = self.fittingTable.indexAt(checkBox.pos)
        fitComboBox = self.fittingTable.cellWidget(index.row(), 2)
        previousText = fitComboBox.currentText
        fitComboBox.clear()

        fit_functions_key = "ZERO" if checkBox.isChecked() else "ALL"
        fit_functions = self.logic.FIT_FUNCTIONS_DIC[fit_functions_key]

        base_items = ["trapezoid", *fit_functions.keys()]

        # Append "auto-fit" if there are more than one function in the list
        if len(fit_functions) > 1:
            base_items.append("auto-fit")

        fitComboBox.addItems(base_items)

        if not fit_functions:
            fitComboBox.setCurrentIndex(0)
        elif len(fit_functions) < 2:
            fitComboBox.setCurrentIndex(0 if "trapezoid" in previousText else 1)
        else:
            # Set to "auto-fit" if the previous selection was not "trapezoid"
            if "trapezoid" not in previousText:
                auto_fit_index = fitComboBox.findText("auto-fit", qt.Qt.MatchFixedString)
                fitComboBox.setCurrentIndex(auto_fit_index if auto_fit_index >= 0 else 0)

    def onUpdateFitting(self, comboBox):
        # Get the row and column index of the ComboBox widget
        index = self.fittingTable.indexAt(comboBox.pos)
        row = index.row()
        col = index.column()

        forceZeroCheckBox = self.fittingTable.cellWidget(row, 1)
        fitComboBox_widget = self.fittingTable.cellWidget(row, 2)
        VIOname = self.fittingTable.item(row, 0).text()
        ADRalgorithm = ""

        if not fitComboBox_widget.currentText:
            return

        if self.adrWorkflow.checked:
            workflow = "Absorbed Dose Rate"
            ADRalgorithm = self.doseRateAlgorithm.currentText
            tableName = f"T:TABL {ADRalgorithm} integrated"
            tableNode = slicer.util.getFirstNodeByClassByName(
                Attributes().vtkMRMLTableNode, tableName
            )
        else:
            workflow = "Activity"
            tableName = f"T:TABL Activity integrated"
            tableNode = slicer.util.getFirstNodeByClassByName(
                Attributes().vtkMRMLTableNode, tableName
            )

        try:
            self.logic.updateFitting(
                mode=workflow,
                algorithm=ADRalgorithm,
                segmentName=VIOname,
                segmentRow=row,
                forceZero=forceZeroCheckBox.isChecked(),
                integrationmode=fitComboBox_widget.currentText,
            )
        except Exception as e:
            qt.QMessageBox.critical(slicer.util.mainWindow(), "Error", e)
            if forceZeroCheckBox.isChecked() or "auto-fit" not in fitComboBox_widget.currentText:
                self.fittingTable.cellWidget(row, 1).setCheckState(0)
                # Try to find and set "auto-fit" as the current index
                index = fitComboBox_widget.findText("auto-fit", qt.Qt.MatchFixedString)
                # If "auto-fit" is not found, fall back to "trapezoid"
                if index < 0:
                    index = fitComboBox_widget.findText("trapezoid", qt.Qt.MatchFixedString)
                fitComboBox_widget.setCurrentIndex(index)
            else:
                Index = fitComboBox_widget.findText("trapezoid", qt.Qt.MatchFixedString)
                fitComboBox_widget.setCurrentIndex(Index)

        if tableNode:
            self.logic.integrate(mode=workflow, algorithm=ADRalgorithm)

        self.fittingTable.item(row, 0).setCheckState(2)

    def onIntegrateActivityButton(self):
        self.logic.integrate(mode="Activity", algorithm="")

    def onIntegrateDoseButton(self):
        self.logic.integrate(
            mode="Absorbed Dose Rate", algorithm=self.doseRateAlgorithm.currentText
        )

    def getKernelMaterial(self) -> str:
        kernelMaterial = None
        kernelMaterial = [
                material
                for material, values in Constants().materials.items()
                if values["name"] == self.kernelMaterial.currentText
            ][0]     
        return kernelMaterial

    def onKernelUpdateList(self):
        isotopeText=self.isotopsList.currentText
        kernelMaterial = self.getKernelMaterial()
        kernels = self.logic.listOfKernel(isotopeText, kernelMaterial)
        if not kernels:
            self.saveOptions()
            return
        self.kernelSelector.clear()
        self.kernelSelector.addItem("OpenDoseDVKData")
        for item in kernels.keys():
            if item != "OpenDoseDVKData":
                self.kernelSelector.addItem(item)
        self.kernelSelector.setCurrentIndex(0)
        self.saveOptions()
        
    def onInitialButton(self):
        if not getFolderIDs() and not self.checkDICOM:
            self.getInitialTimeDataFromDICOM()
        if self.checkDICOM and self.initialButton.enabled:
            if getFolderIDs():
                self.logic.clean()
            self.logic.Acronym_DENSITY_MAP = "DENM"
            self.onRenameButton()
            self.onResampleButton()
            self.onRescaleButton()
            if self.autoWorkflow.checked:
                if self.adrWorkflow.checked:
                    self.onTotalSegmentator()
                    self.doseRateAlgorithm.setCurrentIndex(0)
                    self.applyDensityCorrection.setChecked(True)
                    self.kernelMaterial.setCurrentIndex(0)
                    self.onDoseRateButton()
                    self.onDOSETablesButton()
                    self.onIntegrateDoseButton()
                else:
                    referenceFolderID = self.logic.getReferenceFolder()
                    nodes = Node.getFolderChildren(referenceFolderID)
                    referenceNode = nodes["CTCT"]
                    self.initialPoint.setCurrentNodeID(referenceNode.data.GetID())
                    self.Elastix.checked = True
                    self.onRegistrationButton()
                    self.onTotalSegmentator()
                    self.onPropagateButton()
                    self.onACTMTablesButton()
                    self.onIntegrateActivityButton()
            self.saveOptions()

    # def onACTMExportButton(self):
    #     # TODO: Define input for Clinical Study Title and ID
    #     referenceFolder = self.logic.getReferenceFolder()

    #     directory = qt.QFileDialog.getExistingDirectory(
    #         self.parent,
    #         Constants().strings["Choose Directory"],
    #         self.usePath,
    #         qt.QFileDialog.ShowDirsOnly | qt.QFileDialog.DontResolveSymlinks
    #     )
    #     if directory:
    #         self.usePath = Path(directory)
    #         XMLString = xmlexport.XML_MEDIRAD(
    #             referenceFolder,
    #             self.usePath,
    #             'Dosimetry calculation - I-131 ablation of thyroid',
    #             '755523-t33'
    #         )
    #         XMLString.fillACTM_XMLfile()
    #         self.saveOptions()

    # def onDOSEExportButton(self):
    #     # TODO: Define input for Clinical Study Title and ID
    #     referenceFolder = self.logic.getReferenceFolder()

    #     directory = qt.QFileDialog.getExistingDirectory(
    #         self.parent,
    #         Constants().strings["Choose Directory"],
    #         self.usePath,
    #         qt.QFileDialog.ShowDirsOnly | qt.QFileDialog.DontResolveSymlinks
    #     )
    #     if directory:
    #         self.usePath = Path(directory)
    #         XMLString = xmlexport.XML_MEDIRAD(
    #             referenceFolder,
    #             self.usePath,
    #             'Dosimetry calculation - I-131 ablation of thyroid',
    #             '755523-t33'
    #         )
    #         XMLString.fillADRM_XMLfile(
    #             mode='Absorbed Dose Rate',
    #             algorithm=self.doseRateAlgorithm.currentText
    #         )
    #         self.saveOptions()

    # def useColors(self):
    #     self.enableColors = not(self.enableColors)
    #     self.saveOptions()
    #     self.setupWidgets()
    def petModeInterface(self):
        self.spectSensitivity.setText("1")
        self.spectSensitivity.readOnly = True
        self.sensitivityUnits.clear()
        self.sensitivityUnits.addItem("Bq/ml")
        self.calibrationTimeContainer.hide()

    def stpModeInterface(self):
        self.registrationCollapsibleButton.hide() if self.logic.STPmode else self.registrationCollapsibleButton.show()
        self.propagateButton.hide() if self.logic.STPmode else self.propagateButton.show()
    
    def onInjectedDateChanged(self):
        injTimeParameter = self.qDateTimeEdit.dateTime.toString(self.dateTimeDisplayFormat)
        injTimeParameter = datetime.strptime(injTimeParameter, Constants().timeFormat)
        if injTimeParameter < self.acqTime:
            self.qDateTimeEdit.setStyleSheet("")
            self.logic.injectionTime = injTimeParameter
            self.dicomInjTime = True
        
        if self.checkDICOM:
            self.initialButton.enabled = self.dicomInjAct and self.dicomInjTime

    def onInjectionActivityChanged(self):
        self.InjectionActivity.setStyleSheet("")
        self.dicomInjAct = True
        self.logic.injectedActivity = float(self.InjectionActivity.text)
        if self.checkDICOM:
            self.initialButton.enabled = self.dicomInjAct and self.dicomInjTime

    def onSpectCalibrationTime(self):
        self.dicomAcqDuration = True

    def getInitialTimeDataFromDICOM(self):
        volumeNodeIDs = vtkmrmlutils.getScalarVolumeNodes()
        if not volumeNodeIDs:
            return
        from Logic.NodeDICOM import NodeDICOM
        # search for initial acquisition time
        acquisitionTimes = []
        injectionTimes = []
        injectedActivities = []
        acquisitionDuration = []

        injectionTimes.append(datetime.now())
        acquisitionTimes.append(datetime.now())

        for volumeNodeID in volumeNodeIDs:
            node = NodeDICOM(volumeNodeID, default_isotope="", default_activity=-1)
            modality = node.getModality()
            if modality in ["NM", "PT"]:
                self.logic.PETmode = modality == "PT"
                acquisitionTimeStr = node.getAcquisition()
                if acquisitionTimeStr:
                    acquisitionTimes.append(
                        datetime.strptime(acquisitionTimeStr, Constants().timeFormat)
                    )
                injectionTimeStr = node.getInjectionTime()
                if injectionTimeStr:
                    injectionTimes.append(
                        datetime.strptime(injectionTimeStr, Constants().timeFormat)
                    )
                injectedActivity = float(node.getInjectedActivity())
                injectedActivities.append(injectedActivity)
                if modality == "NM" and node.getAcquisitionDuration():
                    acquisitionDuration.append(float(node.getAcquisitionDuration()))

        self.petModeInterface() if self.logic.PETmode else None
        self.acqTime = sorted(acquisitionTimes)[0]
        injTimeDICOM = sorted(injectionTimes)[0] 
        injActDICOM = sorted(injectedActivities)[-1]

        qtInjTimeDICOM = qt.QDateTime.fromString(injTimeDICOM, self.dateTimeDisplayFormat)
        qtAcqTimeDICOM = qt.QDateTime.fromString(self.acqTime, self.dateTimeDisplayFormat)
        qtInjTimeParameter = self.qDateTimeEdit.dateTime
        self.qDateTimeEdit.setMaximumDateTime(qtAcqTimeDICOM)

        if injActDICOM > 0:
            if self.dicomInjAct and injActDICOM != float(self.InjectionActivity.text):
                slicer.util.infoDisplay(text="The activity entered is different from the DICOM tag")
            else:
                self.logic.injectedActivity = injActDICOM
                self.InjectionActivity.setText(f"{self.logic.injectedActivity:.2f}")
                self.InjectionActivity.setToolTip(
                    f"Injected activity {self.logic.injectedActivity} extracted from DICOMheader"
                )
                self.dicomInjAct = True
        
        if injTimeDICOM < self.acqTime:
            if self.dicomInjTime and qtInjTimeDICOM != qtInjTimeParameter:
                slicer.util.infoDisplay(text="The Injection Time entered is different from the DICOM tag")
            else:
                self.qDateTimeEdit.setDateTime(qtInjTimeDICOM)
                self.logic.injectionTime = injTimeDICOM
                self.qDateTimeEdit.setToolTip(
                    f"Injection Time {self.logic.injectionTime} extracted from DICOMheader"
                )
                self.dicomInjTime = True

        if not self.dicomInjTime:
            self.qDateTimeEdit.setDateTime(qtAcqTimeDICOM)
            self.logic.injectionTime = self.acqTime
            self.qDateTimeEdit.setStyleSheet("QWidget { background-color: #f69697;}")
            self.initialButton.enabled = False

        if not self.dicomInjAct:
            self.InjectionActivity.setStyleSheet("QWidget { background-color: #f69697;}")
            self.initialButton.enabled = False

        if not self.dicomAcqDuration and not self.logic.PETmode:
            acqDuration = np.mean(acquisitionDuration)
            self.spectCalibrationTime.setText(f"{acqDuration:.2f}")
            self.dicomAcqDuration = True

        if not self.dicomInjAct or not self.dicomInjTime:
            slicer.util.warningDisplay(text=("No Injected Activity or Injection Time was found in the DICOM header.\n" 
                                            + "Please enter a correct value"))

        self.checkDICOM = True

    def is_version_lower(self, version_1, version_2):
        """
        Check if version_1 is lower than version_2.

        Returns:
            bool: True if version_1 is lower than version_2, False otherwise.
        """
        def version_to_tuple(version):
            try:
                # Return a default tuple (0,) if the version is empty or None
                if not version:
                    return (0,)
                return tuple(map(int, version.lstrip('v').split('.')))
            except ValueError:
                return (0,)  # Default tuple in case of invalid version string

        return version_to_tuple(version_1) < version_to_tuple(version_2)

    def Parameters(self, nodes):
        if "ACSC" in nodes:
            nodeID = nodes["ACSC"].nodeID
            injecTime = getItemDataNodeAttributeValue(
                nodeID, Attributes().injectionTime
            )
            injecActivity = getItemDataNodeAttributeValue(
                nodeID, Attributes().injectedActivity
            )
            self.study_version = getItemDataNodeAttributeValue(
                nodeID, Attributes().version
            )

            qDateTime = qt.QDateTime.fromString(injecTime, self.dateTimeDisplayFormat)
            self.qDateTimeEdit.setDateTime(qDateTime)
            self.InjectionActivity.setText(injecActivity)

            self.logic.injectionTime = injecTime
            self.logic.injectedActivity = float(injecActivity)

        if "ACTM" in nodes:
            nodeID = nodes["ACTM"].nodeID
            calibDuration = getItemDataNodeAttributeValue(
                nodeID, Attributes().calibrationDuration
            )
            sensitivityUnit = getItemDataNodeAttributeValue(
                nodeID, Attributes().sensitivityUnits
            )
            sensitivity = getItemDataNodeAttributeValue(
                nodeID, Attributes().sensitivity
            )
            isotope = getItemDataNodeAttributeValue(nodeID, Attributes().isotope)
            center = getItemDataNodeAttributeValue(nodeID, Attributes().institutionName)

            if center in self.sensitivity:
                centerIndex = self.ClinicalCenter.findText(
                    center, qt.Qt.MatchFixedString
                )
                self.ClinicalCenter.setCurrentIndex(centerIndex)
                indexIsotope = self.isotopsList.findText(
                    isotope, qt.Qt.MatchFixedString
                )
                if indexIsotope < 0:
                    self.ClinicalCenter.setCurrentIndex(0)

            indexIsotope = self.isotopsList.findText(isotope, qt.Qt.MatchFixedString)
            self.isotopsList.setCurrentIndex(indexIsotope)
            if not self.logic.PETmode:
                self.spectCalibrationTime.setText(calibDuration)
            self.spectSensitivity.setText(sensitivity)
            indexSenUnit = self.sensitivityUnits.findText(
                sensitivityUnit, qt.Qt.MatchFixedString
            )
            if not indexSenUnit < 0:
                self.sensitivityUnits.setCurrentIndex(indexSenUnit)

        if not self.InfoVersionMessage:
            self.InfoVersionMessage = True
            current_version = self.version.strip()
            if self.is_version_lower(self.study_version, current_version):
                slicer.util.infoDisplay(
                    text=f"This study was conducted using an earlier version: {self.study_version}"
                )
        
        if self.is_version_lower(self.study_version, "v1.0"):
            self.logic.Acronym_DENSITY_MAP = "DERS"
        else:
            self.logic.Acronym_DENSITY_MAP = "DENM"

        self.petModeInterface() if self.logic.PETmode else None
        self.stpModeInterface() if self.logic.STPmode else None
                
    def checkAvailability(self):
        self.dicomInjTime = False
        self.dicomInjAct = False
        self.dicomAcqDuration = False
        self.checkDICOM = False
        self.initialPoint.enabled = False
        self.onInitialPointChange()
        self.doseRateButton.enabled = False
        self.GenerateGateButton.enabled = False
        self.ImportGateButton.enabled = False
        self.propagateButton.enabled = False
        self.ACTMTablesButton.enabled = False
        self.DOSETablesButton.enabled = False
        self.IntegrateActivityButton.enabled = False
        self.IntegrateDoseButton.enabled = False
        self.fittingTable.enabled = False
        self.kernelUpdateList.enabled = False
        if dbutils.isCompressed():  # Database is compressed
            self.DBCompressionButton.setText("Compress Database")
            self.DBCompressionButton.toolTip = "Compress the database."
        else:
            self.DBCompressionButton.setText("Decompress Database")
            self.DBCompressionButton.toolTip = "Decompress the database."
        algorithm = self.doseRateAlgorithm.currentText
        if getFolderIDs():
            referenceFolder = self.logic.getReferenceFolder()
            nodes = Node.getFolderChildren(referenceFolder)
            if nodes:
                self.dicomInjTime = True
                self.dicomInjAct = True
                self.dicomAcqDuration = True
                self.checkDICOM = True
                self.initialPoint.enabled = "CTCT" in nodes
                self.onInitialPointChange()
                self.doseRateButton.enabled = "ACTM" in nodes
                self.GenerateGateButton.enabled = "ACTM" in nodes
                self.ImportGateButton.enabled = "ACSC" in nodes
                hasSegmentation = vtkmrmlutils.hasSegmentation(
                    self.segmentation.currentNode()
                )
                self.propagateButton.enabled = hasSegmentation
                self.TotalSegmentatorButton.enabled = "CTCT" and "ACTM" in nodes
                self.ACTMTablesButton.enabled = "ACTM" in nodes and hasSegmentation
                self.DOSETablesButton.enabled = (
                    "ADRM" in nodes and algorithm in nodes["ADRM"] and hasSegmentation
                )
                self.IntegrateActivityButton.enabled = (
                    self.ACTMPlotsMade and self.ACTMTablesButton.enabled
                )
                self.IntegrateDoseButton.enabled = (
                    self.ADRMPlotsMade and self.DOSETablesButton.enabled
                )
                self.fittingTable.enabled = (
                    self.activityWorkflow.checked
                    and self.ACTMPlotsMade
                    or self.adrWorkflow.checked
                    and self.ADRMPlotsMade
                    and self.DOSETablesButton.enabled
                )
                self.kernelUpdateList.enabled = "ACSC" in nodes
                self.Parameters(nodes)
