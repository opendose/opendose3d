import logging
import math

import vtk, qt
import numpy as np
from datetime import datetime, timedelta
from Logic.vtkmrmlutils import *

import slicer
from slicer.ScriptedLoadableModule import *
from slicer.util import VTKObservationMixin, findChildren

import SegmentStatistics

from Logic import utils, vtkmrmlutils, errors, dbutils, config  # , xmlexport
from Logic.nodes import Node
from Logic.PhysicalUnits import PhysicalUnits
from Logic.attributes import Attributes
from Logic.constants import Constants
from Logic.errors import ConfigError, DICOMError

from Logic.fitutils import RecoveryFunction

#
# Calibration
#


class Calibration(ScriptedLoadableModule):
    """Uses ScriptedLoadableModule base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def __init__(self, parent):
        ScriptedLoadableModule.__init__(self, parent)
        self.parent.title = "OpenDose Calibration"
        self.parent.categories = ["Radiotherapy"]
        self.parent.dependencies = []
        self.parent.contributors = [
            "José Alejandro Fragoso Negrin (INSERM, France), Alex Vergara Gil (INSERM, France)"
        ]
        self.parent.helpText = """
        This module implements the Calibration steps for molecular radiotherapy.\n
        """
        self.parent.acknowledgementText = """
        This software was originally developed by José Alejandro Fragoso Negrin (INSERM, France)
"""


#######################
#                     #
# CalibrationWidget   #
#                     #
#######################


class CalibrationWidget(ScriptedLoadableModuleWidget, VTKObservationMixin):
    """Uses ScriptedLoadableModuleWidget base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def __init__(self, parent=None):
        """
        Called when the user opens the module the first time and the widget is initialized.
        """
        ScriptedLoadableModuleWidget.__init__(self, parent)
        VTKObservationMixin.__init__(self)  # needed for parameter node observation
        self.logic = CalibrationLogic()
        self._parameterNode = None
        self._updatingGUIFromParameterNode = False
        self.uiWidget = None
        self.actionExport = []
        self.SPECTnode = None
        self.CTnode = None

        self.isCenter = False
        self.volumeLineRC = {}
        self.progressBarRC = {}
        self.markupPlace = {}

    def setup(self):
        """
        Called when the user opens the module the first time and the widget is initialized.
        """
        ScriptedLoadableModuleWidget.setup(self)

        # Load widget from .ui file (created by Qt Designer).
        # Additional widgets can be instantiated manually and added to self.layout.
        self.uiWidget = slicer.util.loadUI(self.resourcePath("UI/Calibration.ui"))
        self.layout.addWidget(self.uiWidget)
        self.ui = slicer.util.childWidgetVariables(self.uiWidget)

        self.ui.newClinicalCenterButton.enabled = True

        # Set scene in MRML widgets. Make sure that in Qt designer the top-level qMRMLWidget's
        # "mrmlSceneChanged(vtkMRMLScene*)" signal in is connected to each MRML widget's.
        # "setMRMLScene(vtkMRMLScene*)" slot.
        self.uiWidget.setMRMLScene(slicer.mrmlScene)

        self.logic = CalibrationLogic()

        self.validation(self.ui.SPECTTankVolume)
        self.validation(self.ui.SPECTInjectedActivity)
        self.validation(self.ui.SPECTResidualActivity)
        self.validation(self.ui.SPECTTankDiameter)

        self.validation(self.ui.RCSensitivitySPECT)
        self.validation(self.ui.RCInjectedActivity)
        self.validation(self.ui.RCResidualActivity)
        self.validation(self.ui.RCVolumeSyringe)
        self.checkAvailability()

        self.ui.ResampleComboBox.addItems(['Lanczos', 
                                          'Linear', 
                                          'ResampleInPlace', 
                                          'NearestNeighbor',
                                          'WindowedSinc',
                                          'Blackman'])

        # Connections

        # These connections ensure that we update parameter node when scene is closed
        self.addObserver(
            slicer.mrmlScene, slicer.mrmlScene.StartCloseEvent, self.onSceneStartClose
        )
        self.addObserver(
            slicer.mrmlScene, slicer.mrmlScene.EndCloseEvent, self.onSceneEndClose
        )

        self.ui.ClinicalCenter.currentIndexChanged.connect(self.onCenterChanged)
        self.ui.SPECTCalibrationCollapsibleButton.clicked.connect(
            self.setupViewSensitivity
        )
        self.ui.RecoveryCoefficientCollapsibleButton.clicked.connect(
            self.setupViewRecovery
        )
        self.ui.newClinicalCenterButton.clicked.connect(self.onNewClinicalCenterButton)
        self.ui.renameButton.clicked.connect(self.onStandardizeButton)
        self.ui.SPECTMeasurementDateTimeEdit.dateTimeChanged.connect(self.onDateChanged)
        self.ui.SPECTCalibrationButton.clicked.connect(self.onSPECTCalibrationButton)
        self.ui.SPECTExportCalibrationButton.clicked.connect(
            self.onExportCalibrationButton
        )

        self.ui.SPECTTankVolume.textChanged.connect(self.onInputParamEdit)
        self.ui.SPECTInjectedActivity.textChanged.connect(self.onInputParamEdit)
        self.ui.SPECTResidualActivity.textChanged.connect(self.onInputParamEdit)
        self.ui.SPECTTankDiameter.textChanged.connect(self.onInputParamEdit)
        self.ui.SPECTResidualDateTime.dateTimeChanged.connect(self.onInputParamEdit)

        self.ui.computeRCButton.clicked.connect(self.onComputeRCButton)
        self.ui.standardizeButtonRC.clicked.connect(self.onStandardizeButtonRC)
        self.ui.RCSensitivitySPECT.textChanged.connect(self.onRCSensitivitySPECT)
        self.ui.RCInjectedActivity.textChanged.connect(self.onActivityinSyringe)
        self.ui.RCResidualActivity.textChanged.connect(self.onActivityinSyringe)
        self.ui.RCVolumeSyringe.textChanged.connect(self.onActivityinSyringe)
        self.ui.RCResidualActDateTime.dateTimeChanged.connect(self.onActivityinSyringe)
        self.ui.RCInitialActDateTime.dateTimeChanged.connect(self.onRCDateChanged)
        self.ui.RCExportButton.clicked.connect(self.onExportRCdataButton)

        self.ui.SPECT_RCsetting_CheckBox.stateChanged.connect(self.checkAvailability)
        self.ui.CT_RCsetting_CheckBox.stateChanged.connect(self.checkAvailability)
        self.ui.CT_SPECTtoCT_opt.toggled.connect(self.checkAvailability)

    def cleanup(self):
        """
        Called when the application closes and the module widget is destroyed.
        """
        self.removeObservers()

    def enter(self):
        """
        Called each time the user opens this module.
        """

        pass

    def exit(self):
        """
        Called each time the user opens a different module.
        """
        pass

    def onSceneStartClose(self, caller, event):
        """
        Called just before the scene is closed.
        """
        pass

    def onSceneEndClose(self, caller, event):
        """
        Called just after the scene is closed.
        """
        pass

    def onNewClinicalCenterButton(self):
        Dialog = qt.QDialog()
        formLayout = qt.QFormLayout()
        CenterCode = qt.QLineEdit()
        CenterCode.setAlignment(qt.Qt.AlignRight)
        formLayout.addRow("Enter new center code", CenterCode)
        CenterDescription = qt.QLineEdit()
        CenterDescription.setAlignment(qt.Qt.AlignRight)
        formLayout.addRow("Enter new center Description", CenterDescription)
        Proceed = qt.QPushButton("Create new center")
        Proceed.toolTip = "Saves new center to database."
        Proceed.enabled = True
        hbox = qt.QHBoxLayout()
        hbox.addWidget(Proceed)
        Cancel = qt.QPushButton("Cancel")
        Cancel.toolTip = "Cancels and return to Slicer."
        Cancel.enabled = True
        hbox.addWidget(Cancel)
        formLayout.addRow(hbox)
        Proceed.clicked.connect(lambda: Dialog.done(1))
        Cancel.clicked.connect(lambda: Dialog.reject())
        Dialog.setLayout(formLayout)
        Dialog.setWindowTitle("Please enter code and description of new center")
        if Dialog.exec_() == qt.QDialog.Accepted:
            code = CenterCode.text
            description = CenterDescription.text
            self.addnewCenter(code, description)
            print("Saved")
        else:
            print("Cancelled")

    def addnewCenter(self, code, description):
        centers = config.readClinicalCenters()
        centers[code] = description
        print(centers)
        config.writeClinicalCenters(centers)
        self.ui.ClinicalCenter.clear()
        self.ui.ClinicalCenter.addItems(list(centers.keys()))
        index = self.ui.ClinicalCenter.findText(code, qt.Qt.MatchFixedString)
        if index > 0:
            self.ui.ClinicalCenter.setCurrentIndex(index)

    def onCenterChanged(self):
        center = self.ui.ClinicalCenter.currentText
        if center:
            if not self.ui.SPECTCalibrationCollapsibleButton.collapsed:
                vtkmrmlutils.setItemDataNodeAttribute(
                    self.SPECTnode.nodeID, Attributes().institutionName, center
                )
                self.ui.SPECTExportCalibrationButton.enabled = False
                self.ui.SPECTExportCalibrationButton.text = "Select Export"

            if not self.ui.RecoveryCoefficientCollapsibleButton.collapsed:
                SensitivityDatabase = config.readSPECTSensitivity()
                centers = list(SensitivityDatabase.keys())
                vtkmrmlutils.setItemDataNodeAttribute(
                    self.SPECTnode.nodeID, Attributes().institutionName, center
                )
                sensitivityRC = vtkmrmlutils.getItemDataNodeAttributeValue(
                    self.SPECTnode.nodeID, "Sensitivity SPECT (counts/MBqs)")
                if not sensitivityRC:
                    if center in centers:
                        isotope = self.SPECTnode.getIsotope()
                        isotopes = list(SensitivityDatabase[center].keys())
                        if isotope in isotopes:
                            acqDateTime = datetime.strptime(
                                self.SPECTnode.getAcquisition(), Constants().timeFormat
                            )
                            str_dates = list(SensitivityDatabase[center][isotope].keys())
                            datesInDataBase = [
                                datetime.strptime(date, Constants().timeFormat)
                                for date in str_dates
                            ]
                            diffInSec = [
                                (acqDateTime - dates).total_seconds()
                                for dates in datesInDataBase
                            ]
                            min_diffInSec = min([diff for diff in diffInSec if diff > 0])
                            index = diffInSec.index(min_diffInSec)
                            select_date = str_dates[index]
                            sensitivityRC = SensitivityDatabase[center][isotope][
                                select_date
                            ]["Sensitivity"]
                            self.ui.RCSensitivitySPECT.setText(sensitivityRC)
                            self.ui.RCSensitivitySPECT.readOnly = True
                        else:
                            self.ui.RCSensitivitySPECT.setText("")
                            self.ui.RCSensitivitySPECT.readOnly = False
                    else:
                        self.ui.RCSensitivitySPECT.setText("")
                        self.ui.RCSensitivitySPECT.readOnly = False

    def validation(self, qtLineEdit):
        val = qt.QDoubleValidator(0.99, 99.99, 2, qtLineEdit)
        qtLineEdit.setValidator(val)

    def convert2QDatetime(
        self, datetime, displayFormat="yyyy-MM-dd HH:mm:ss", date=False, time=False
    ):
        QDateTime = qt.QDateTime.fromString(
            datetime.strftime(Constants().timeFormat), displayFormat
        )
        if time:
            return QDateTime.time()
        elif date:
            return QDateTime.date()
        return QDateTime

    #####################################################
    ######        Sensitivity Calibration          ######
    #####################################################

    def setupViewSensitivity(self):
        if self.ui.SPECTCalibrationCollapsibleButton.collapsed == True:
            self.ui.RecoveryCoefficientCollapsibleButton.collapsed = False
            self.setupRCui()
        else:
            self.ui.RecoveryCoefficientCollapsibleButton.collapsed = True
            self.enableDataCalibration(False)

    def enableDataCalibration(self, activate):
        if activate:
            self.ui.SPECTInjectedActivity.enabled = True
            self.ui.SPECTMeasurementDateTimeEdit.enabled = True
            self.ui.SPECTResidualActivity.enabled = True
            self.ui.SPECTResidualDateTime.enabled = True
            self.ui.SPECTTankVolume.enabled = True
            self.ui.SPECTTankDiameter.enabled = True
        else:
            self.ui.SPECTInjectedActivity.enabled = False
            self.ui.SPECTMeasurementDateTimeEdit.enabled = False
            self.ui.SPECTResidualActivity.enabled = False
            self.ui.SPECTResidualDateTime.enabled = False
            self.ui.SPECTTankVolume.enabled = False
            self.ui.SPECTTankDiameter.enabled = False
            self.ui.SPECTCalibrationButton.enabled = False
            self.ui.SPECTExportCalibrationButton.enabled = False
            self.ui.SPECTExportCalibrationButton.text = "Select Export"
            self.ui.SPECTIsotope.setText("")
            self.ui.SPECTAcquisitionDuration.setText("")
            self.ui.SPECTAcquisitionDate.setText("")
            self.ui.SPECTInjectedActivity.setText("")
            self.ui.SPECTResidualActivity.setText("")
            self.ui.SPECTTankDiameter.setText("")
            self.ui.SPECTTankVolume.setText("")

    def onStandardizeButton(self):
        self.logic.clean()
        nodes = slicer.mrmlScene.GetNodesByClass(Attributes().vtkMRMLScalarVolumeNode)
        for node in nodes:
            nodeCreated = Node.new(vtkmrmlutils.getItemID(node))
            modality = nodeCreated.getModality()
            if modality in ["PT", "NM"]:
                self.SPECTnode = nodeCreated
            elif modality == "CT":
                self.CTnode = nodeCreated

        if not self.SPECTnode or not self.CTnode:
            self.ui.SPECTCalibrationButton.enabled = False
            self.enableDataCalibration(False)
            logging.error(f"The input must be CT image and SPECT image, please check!!")
            return  # TODO show an error popup
        if (
            self.SPECTnode.data.GetImageData() is None
        ):  # The selected node must contain an image
            self.ui.SPECTCalibrationButton.enabled = False
            return  # TODO show an error popup

        try:
            center = self.SPECTnode.getInstitutionName()
            if center or center == "None":
                self.addnewCenter(center, "")
        except:
            self.onNewClinicalCenterButton()

        try:
            isotope = self.SPECTnode.getIsotope()
        except DICOMError as dcmErr:
            return  # TODO show an error popup

        try:
            acqDateTime = datetime.strptime(
                self.SPECTnode.getAcquisition(), Constants().timeFormat
            )
        except DICOMError as dcmErr:
            return  # TODO show an error popup

        try:
            acqDuration = self.SPECTnode.getAcquisitionDuration()
        except DICOMError as dcmErr:
            return  # TODO show an error popup

        try:
            injTime = datetime.strptime(
                self.SPECTnode.getInjectionTime(), Constants().timeFormat
            )
        except:
            injTime = acqDateTime

        try:
            injActivity = self.SPECTnode.getInjectedActivity()
            if injActivity == -1:
                injActivity = ""
        except DICOMError as dcmErr:
            return

        try:
            residualActivity = vtkmrmlutils.getItemDataNodeAttributeValue(
                self.SPECTnode.nodeID, "Residual Activity (MBq)"
            )
            if residualActivity is None:
                residualActivity = ""
        except ConfigError as confErr:
            return

        try:
            tankDiameter = vtkmrmlutils.getItemDataNodeAttributeValue(
                self.SPECTnode.nodeID, "Tank Diameter (cm)"
            )
            if tankDiameter is None:
                tankDiameter = ""
        except ConfigError as confErr:
            return

        try:
            tankVolume = vtkmrmlutils.getItemDataNodeAttributeValue(
                self.SPECTnode.nodeID, "Tank Volume (cm3)"
            )
            if tankVolume is None:
                tankVolume = ""
        except ConfigError as confErr:
            return

        try:
            residualTime = vtkmrmlutils.getItemDataNodeAttributeValue(
                self.SPECTnode.nodeID, "Residual Activity Time"
            )
            if residualTime is None:
                residualTime = injTime
            else:
                residualTime = datetime.strptime(residualTime, Constants().timeFormat)
        except ConfigError as confErr:
            return

        try:
            restTime = vtkmrmlutils.getItemDataNodeAttributeValue(
                self.SPECTnode.nodeID, "Decay Time (sec)"
            )
            if restTime is None:
                restTime = round(
                    (acqDateTime - injTime).total_seconds()
                    / PhysicalUnits().getUnit("Time", "s")
                )
        except ConfigError as confErr:
            return

        self.ui.SPECTIsotope.setText(isotope)
        self.ui.SPECTAcquisitionDate.setText(acqDateTime)
        self.ui.SPECTAcquisitionDuration.setText(
            str(timedelta(seconds=float(acqDuration)))
        )
        self.ui.SPECTMeasurementDateTimeEdit.setDateTime(
            self.convert2QDatetime(injTime)
        )
        self.ui.SPECTMeasurementDateTimeEdit.setMaximumDateTime(
            self.convert2QDatetime(acqDateTime)
        )
        self.ui.SPECTResidualDateTime.setDateTime(self.convert2QDatetime(residualTime))
        self.ui.SPECTResidualDateTime.setMinimumDateTime(
            self.convert2QDatetime(injTime)
        )
        self.ui.SPECTInjectedActivity.setText(injActivity)
        self.ui.SPECTResidualActivity.setText(residualActivity)
        self.ui.SPECTTankDiameter.setText(tankDiameter)
        self.ui.SPECTTankVolume.setText(tankVolume)

        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, Attributes().institutionName, center
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, Attributes().isotope, isotope
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, Attributes().acquisition, acqDateTime
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, Attributes().acquisitionDuration, acqDuration
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, Attributes().injectionTime, injTime
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, Attributes().injectedActivity, injActivity
        )
        #TODO: add to attributes
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, "Residual Activity (MBq)", residualActivity
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, "Tank Diameter (cm)", tankDiameter
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, "Tank Volume (cm3)", tankVolume
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, "Residual Activity Time", str(residualTime)
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, "Decay Time (sec)", restTime
        )

        setSlicerViews(self.CTnode.data.GetID(), self.SPECTnode.data.GetID())
        displayNode = self.SPECTnode.data.GetScalarVolumeDisplayNode()
        colorSpace = slicer.util.getFirstNodeByName("Magma").GetID()
        displayNode.SetAndObserveColorNodeID(colorSpace)

        self.enableDataCalibration(True)
        self.ui.newClinicalCenterButton.enabled = True
        logging.info(f"Selected {self.SPECTnode.name} as reference volume")

    def onInputParamEdit(self):
        values = [
            self.ui.SPECTInjectedActivity.text,
            self.ui.SPECTResidualActivity.text,
            self.ui.SPECTTankVolume.text,
            self.ui.SPECTTankDiameter.text,
        ]

        if not self.ui.SPECTTankVolume.text \
            or float(self.ui.SPECTTankVolume.text) < 100:
                self.ui.SPECTTankDiameter.text = ''

        if "" not in values[:2]:
            vtkmrmlutils.setItemDataNodeAttribute(
                self.SPECTnode.nodeID, Attributes().injectedActivity, values[0]
            )
            vtkmrmlutils.setItemDataNodeAttribute(
                self.SPECTnode.nodeID, "Residual Activity (MBq)", values[1]
            )
            vtkmrmlutils.setItemDataNodeAttribute(
                self.SPECTnode.nodeID, "Tank Volume (cm3)", values[2]
            )
            vtkmrmlutils.setItemDataNodeAttribute(
                self.SPECTnode.nodeID, "Tank Diameter (cm)", values[3]
            )
            dt = self.ui.SPECTResidualDateTime.dateTime
            residualTime = dt.toString(self.ui.SPECTResidualDateTime.displayFormat)
            vtkmrmlutils.setItemDataNodeAttribute(
                self.SPECTnode.nodeID, "Residual Activity Time", residualTime
            )
            self.ui.SPECTCalibrationButton.enabled = True
        else:
            self.ui.SPECTCalibrationButton.enabled = False

        self.ui.SPECTExportCalibrationButton.enabled = False
        self.ui.SPECTExportCalibrationButton.text = "Select Export"

    def onDateChanged(self):
        dt = self.ui.SPECTMeasurementDateTimeEdit.dateTime
        MeasurementDate = dt.toString(
            self.ui.SPECTMeasurementDateTimeEdit.displayFormat
        )
        AcquisitionDate = self.ui.SPECTAcquisitionDate.text

        injTime = datetime.strptime(MeasurementDate, Constants().timeFormat)
        acqTime = datetime.strptime(AcquisitionDate, Constants().timeFormat)
        restTime = (acqTime - injTime).total_seconds() / PhysicalUnits().getUnit(
            "Time", "s"
        )

        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, "Decay Time (sec)", round(restTime)
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, Attributes().injectionTime, MeasurementDate
        )

        self.ui.SPECTResidualDateTime.setMinimumDateTime(dt)
        self.ui.SPECTExportCalibrationButton.enabled = False
        self.ui.SPECTExportCalibrationButton.text = "Select Export"

    def onSPECTCalibrationButton(self):
        self.logic.runSPECTSensitivity(self.SPECTnode)
        self.ui.SPECTExportCalibrationButton.enabled = True

    def onExportChecked(self, sender):
        self.ui.SPECTExportCalibrationButton.text = f"Export ({sender.text}) "
        for act in self.actionExport:
            if sender.text == act.text:
                self.logic.segmentExport[sender.text] = True
                act.setChecked(True)
            else:
                self.logic.segmentExport[act.text] = False
                act.setChecked(False)

    def setupSensitivityExport(self, segmentExport):
        menuExp = qt.QMenu("menuExp", self.ui.SPECTExportCalibrationButton)
        for mtdName, mtdValue in segmentExport.items():
            actBtn = qt.QAction(mtdName)
            actBtn.setCheckable(True)
            actBtn.setChecked(mtdValue)
            menuExp.addAction(actBtn)
            actBtn.connect("triggered()", lambda p=actBtn: self.onExportChecked(p))
            self.actionExport.append(actBtn)
        self.ui.SPECTExportCalibrationButton.setMenu(menuExp)

    def onExportCalibrationButton(self):
        self.logic.exportCalibration(self.SPECTnode)

    def onExportRCdataButton(self):
        self.logic.exportRecoveryCoefficients(self.SPECTnode)

    #####################################################
    ######        Recovery Coefficients            ######
    #####################################################

    def onPlaceButton(self, sender):
        nhide = 0
        for markup in self.markupPlace.values():
            fiducialNode = markup.currentMarkupsFiducialNode()
            if fiducialNode.GetNumberOfControlPoints():
                markup.placeButton().hide()
                nhide += 1
            else:
                markup.placeButton().show()
        self.volumeLineRC[f"theoricalVolumeEdit_{sender.name[-1]}"].readOnly = True
        sender.placeButton().hide()
        if nhide == len(self.markupPlace.values()) - 1:
            self.ui.computeRCButton.enabled = True

    def onDeleteButton(self, sender):
        sender.placeButton().show()
        self.volumeLineRC[f"theoricalVolumeEdit_{sender.name[-1]}"].readOnly = False
        self.ui.computeRCButton.enabled = False
        self.ui.RCExportButton.enabled = False

    def setupRCui(self):
        for markupPlaceWidget in findChildren(
            self.uiWidget, className="qSlicerMarkupsPlaceWidget"
        ):
            markupPlaceWidget.setMRMLScene(slicer.mrmlScene)
            markupPlaceWidget.buttonsVisible = False
            markupPlaceWidget.placeButton().show()
            markupPlaceWidget.deleteButton().show()
            markupPlaceWidget.enabled = False

            placeBtn = markupPlaceWidget.placeButton()
            placeBtn.connect(
                "clicked()", lambda p=markupPlaceWidget: self.onPlaceButton(p)
            )

            deleteBtn = markupPlaceWidget.deleteButton()
            deleteBtn.connect(
                "clicked()", lambda p=markupPlaceWidget: self.onDeleteButton(p)
            )

            self.markupPlace[markupPlaceWidget.name] = markupPlaceWidget

        for lineEdit in findChildren(self.uiWidget, className="QLineEdit"):
            if "theoricalVolume" in lineEdit.name:
                lineEdit.enabled = False
                lineEdit.readOnly = False
                self.validation(lineEdit)
                self.volumeLineRC[lineEdit.name] = lineEdit
                lineEdit.connect(
                    "textChanged(QString)",
                    lambda p=lineEdit: self.checkComputeRCButton(p),
                )

        for progressBar in findChildren(self.uiWidget, className="QProgressBar"):
            progressBar.enabled = False
            progressBar.setValue(0)
            self.progressBarRC[progressBar.name] = progressBar

        # Remove all Markups if they exist
        for node in slicer.mrmlScene.GetNodesByClass(Attributes().vtkMRMLMarkupsFiducialNode):
            if node:
                slicer.mrmlScene.RemoveNode(node)

        self.markupPlace = dict(sorted(self.markupPlace.items()))
        self.volumeLineRC = dict(sorted(self.volumeLineRC.items()))
        self.progressBarRC = dict(sorted(self.progressBarRC.items()))

        self.ui.newClinicalCenterButton.enabled = False
        self.ui.computeRCButton.enabled = False
        self.ui.RCExportButton.enabled = False
        self.ui.RCIsotope.setText("")
        self.ui.RCAcquisitionDate.setText("")
        self.ui.RCAcquisitionDuration.setText("")
        self.ui.RCSensitivitySPECT.setText("")
        self.ui.RCSensitivitySPECT.enabled = False
        self.enableVolumeEdit(False)

    def setupViewRecovery(self):
        if self.ui.RecoveryCoefficientCollapsibleButton.collapsed == True:
            self.ui.SPECTCalibrationCollapsibleButton.collapsed = False
        else:
            self.ui.SPECTCalibrationCollapsibleButton.collapsed = True
            self.setupRCui()

    def onStandardizeButtonRC(self):
        self.logic.clean()
        nodes = slicer.mrmlScene.GetNodesByClass(Attributes().vtkMRMLScalarVolumeNode)
        for node in nodes:
            nodeCreated = Node.new(vtkmrmlutils.getItemID(node))
            modality = nodeCreated.getModality()
            if modality in ["PT", "NM"]:
                self.SPECTnode = nodeCreated
            elif modality == "CT":
                self.CTnode = nodeCreated

        if not self.SPECTnode or not self.CTnode:
            logging.error(f"The input must be CT image and SPECT image, please check!!")
            return  # TODO show an error popup
        if (
            self.SPECTnode.data.GetImageData() is None
        ):  # The selected node must contain an image
            logging.error(f"The selected node {self.SPECTnode.name} must contain an image")
            return  # TODO show an error popup

        try:
            center = self.SPECTnode.getInstitutionName()
            if center or center == "None":
                self.addnewCenter(center, "")
        except:
            self.onNewClinicalCenterButton()

        try:
            isotope = self.SPECTnode.getIsotope()
        except DICOMError as dcmErr:
            return  # TODO show an error popup

        try:
            acqDateTime = datetime.strptime(
                self.SPECTnode.getAcquisition(), Constants().timeFormat
            )
        except DICOMError as dcmErr:
            return  # TODO show an error popup

        try:
            acqDuration = self.SPECTnode.getAcquisitionDuration()
        except DICOMError as dcmErr:
            return  # TODO show an error popup

        try:
            injTime = datetime.strptime(
                self.SPECTnode.getInjectionTime(), Constants().timeFormat
            )
        except:
            injTime = acqDateTime

        try:
            injActivity = self.SPECTnode.getInjectedActivity()
            if injActivity == -1:
                injActivity = ""
        except DICOMError as dcmErr:
            return

        try:
            residualActivity = vtkmrmlutils.getItemDataNodeAttributeValue(
                self.SPECTnode.nodeID, "Residual Activity (MBq)"
            )
            if residualActivity is None:
                residualActivity = ""
        except ConfigError as confErr:
            return

        try:
            volumeSyringeRC = vtkmrmlutils.getItemDataNodeAttributeValue(
                self.SPECTnode.nodeID, "Volunme in Syringe (ml)"
            )
            if volumeSyringeRC is None:
                volumeSyringeRC = ""
        except ConfigError as confErr:
            return

        try:
            sensitivityRC = vtkmrmlutils.getItemDataNodeAttributeValue(
                self.SPECTnode.nodeID, "Sensitivity SPECT (counts/MBqs)"
            )
            if sensitivityRC is None:
                sensitivityRC = ""
        except ConfigError as confErr:
            return

        try:
            residualTime = vtkmrmlutils.getItemDataNodeAttributeValue(
                self.SPECTnode.nodeID, "Residual Activity Time"
            )
            if residualTime is None:
                residualTime = injTime
            else:
                residualTime = datetime.strptime(residualTime, Constants().timeFormat)
        except ConfigError as confErr:
            return

        try:
            restTime = vtkmrmlutils.getItemDataNodeAttributeValue(
                self.SPECTnode.nodeID, "Decay Time (sec)"
            )
            if restTime is None:
                restTime = round(
                    (acqDateTime - injTime).total_seconds()
                    / PhysicalUnits().getUnit("Time", "s")
                )
        except ConfigError as confErr:
            return

        try:
            volumes = vtkmrmlutils.getItemDataNodeAttributeValue(
                self.SPECTnode.nodeID, "volumens (cm3)"
            )
            if volumes is None:
                volumes = ["" for vol in range(len(self.volumeLineRC.keys()))]
            else:
                volumes = self.logic.getListfromAttribute(volumes)
        except ConfigError as confErr:
            return

        self.ui.RCIsotope.setText(isotope)
        self.ui.RCAcquisitionDate.setText(acqDateTime)
        self.ui.RCAcquisitionDuration.setText(
            str(timedelta(seconds=float(acqDuration)))
        )
        self.ui.RCInitialActDateTime.setDateTime(self.convert2QDatetime(injTime))
        self.ui.RCInitialActDateTime.setMaximumDateTime(
            self.convert2QDatetime(acqDateTime)
        )
        self.ui.RCResidualActDateTime.setDateTime(self.convert2QDatetime(residualTime))
        self.ui.RCResidualActDateTime.setMinimumDateTime(
            self.convert2QDatetime(injTime)
        )
        self.ui.RCInjectedActivity.setText(injActivity)
        self.ui.RCResidualActivity.setText(residualActivity)
        self.ui.RCVolumeSyringe.setText(volumeSyringeRC)
        self.ui.RCSensitivitySPECT.setText(sensitivityRC)

        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, Attributes().institutionName, center
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, Attributes().isotope, isotope
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, Attributes().acquisition, acqDateTime
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, Attributes().acquisitionDuration, acqDuration
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, Attributes().injectionTime, injTime
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, Attributes().injectedActivity, injActivity
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, "Residual Activity (MBq)", residualActivity
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, "Volunme in Syringe (ml)", volumeSyringeRC
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, "Sensitivity SPECT (counts/MBqs)", sensitivityRC
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, "Residual Activity Time", str(residualTime)
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, "Decay Time (sec)", restTime
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, "volumens (cm3)", str(volumes)
        )

        self.ui.RCIsotope.enabled = True
        self.ui.RCAcquisitionDate.enabled = True
        self.ui.RCAcquisitionDuration.enabled = True
        self.ui.RCSensitivitySPECT.enabled = True
        self.ui.newClinicalCenterButton.enabled = True
        # self.onActivityinSyringe()
        self.activateMarkups()

        for i, vol in enumerate(volumes):
            self.volumeLineRC[f"theoricalVolumeEdit_{i+1}"].setText(str(vol))

        setSlicerViews(self.CTnode.data.GetID(), self.SPECTnode.data.GetID())
        displayNode = self.SPECTnode.data.GetScalarVolumeDisplayNode()
        colorSpace = slicer.util.getFirstNodeByName("Magma").GetID()
        displayNode.SetAndObserveColorNodeID(colorSpace)

        logging.info(f"Selected {self.SPECTnode.name} as reference volume")

    @staticmethod
    def createFiducialNode(name, color):
        existingFiducialNodes = slicer.util.getNodesByClass(
            Attributes().vtkMRMLMarkupsFiducialNode
        )
        existingFiducialNames = [node.GetName() for node in existingFiducialNodes]

        if name in existingFiducialNames:
            index = existingFiducialNames.index(name)
            fiducialNodeReturned = existingFiducialNodes[index]
            return fiducialNodeReturned

        displayNode = slicer.mrmlScene.AddNewNodeByClass(Attributes().vtkMRMLMarkupsDisplayNode)
        displayNode.SetTextScale(5)
        displayNode.SetSelectedColor(color)

        fiducialNode = slicer.mrmlScene.AddNewNodeByClass(Attributes().vtkMRMLMarkupsFiducialNode)
        fiducialNode.SetName(name)
        fiducialNode.SetAndObserveDisplayNodeID(displayNode.GetID())
        fiducialNode.SetMaximumNumberOfControlPoints(1)

        return fiducialNode

    def activateMarkups(self):
        # Create empty markup fiducial node
        for markupWidget in self.markupPlace.values():
            color = np.random.rand(1, 3)
            name = f"volume_{markupWidget.name[-1]}"
            annotationFiducialNode = self.createFiducialNode(name, color[0])
            markupWidget.setCurrentNode(annotationFiducialNode)
            markupWidget.setPlaceModeEnabled(False)
            markupWidget.deleteLastPoint()
            markupWidget.enabled = False

    def enableVolumeEdit(self, enabled):
        for volLineName, volLineValue in self.volumeLineRC.items():
            volLineValue.enabled = enabled
            if volLineValue.text:
                self.markupPlace[f"fiducialPlaceVolume_{volLineName[-1]}"].enabled = (
                    enabled
                )

    def onActivityinSyringe(self):
        values = [
            self.ui.RCInjectedActivity.text,
            self.ui.RCResidualActivity.text,
            self.ui.RCVolumeSyringe.text,
            self.ui.RCSensitivitySPECT.text,
        ]
        if "" in values:
            self.enableVolumeEdit(False)
        else:
            self.enableVolumeEdit(True)

        dt = self.ui.RCResidualActDateTime.dateTime
        residualTime = dt.toString(self.ui.RCResidualActDateTime.displayFormat)
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, "Residual Activity Time", residualTime
        )

        for i, val in enumerate(values):
            if val and i == 0:
                vtkmrmlutils.setItemDataNodeAttribute(
                    self.SPECTnode.nodeID, Attributes().injectedActivity, values[0]
                )
            elif val and i == 1:
                vtkmrmlutils.setItemDataNodeAttribute(
                    self.SPECTnode.nodeID, "Residual Activity (MBq)", values[1]
                )
            elif val and i == 2:
                vtkmrmlutils.setItemDataNodeAttribute(
                    self.SPECTnode.nodeID, "Volunme in Syringe (ml)", values[2]
                )

        self.ui.RCExportButton.enabled = False

    def onRCSensitivitySPECT(self):
        cameraFactor = self.ui.RCSensitivitySPECT.text
        if cameraFactor:
            self.ui.RCInitialActDateTime.enabled = True
            self.ui.RCResidualActDateTime.enabled = True
            self.ui.RCInjectedActivity.enabled = True
            self.ui.RCResidualActivity.enabled = True
            self.ui.RCVolumeSyringe.enabled = True
            self.onActivityinSyringe()
        else:
            self.ui.RCInitialActDateTime.enabled = False
            self.ui.RCResidualActDateTime.enabled = False
            self.ui.RCInjectedActivity.enabled = False
            self.ui.RCResidualActivity.enabled = False
            self.ui.RCVolumeSyringe.enabled = False
            self.enableVolumeEdit(False)

        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, "Sensitivity SPECT (counts/MBqs)", cameraFactor
        )

    def onRCDateChanged(self):
        dt = self.ui.RCInitialActDateTime.dateTime
        MeasurementDate = dt.toString(self.ui.RCInitialActDateTime.displayFormat)
        AcquisitionDate = self.ui.RCAcquisitionDate.text

        injTime = datetime.strptime(MeasurementDate, Constants().timeFormat)
        acqTime = datetime.strptime(AcquisitionDate, Constants().timeFormat)
        restTime = (acqTime - injTime).total_seconds() / PhysicalUnits().getUnit(
            "Time", "s"
        )

        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, "Decay Time (sec)", round(restTime)
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.SPECTnode.nodeID, Attributes().injectionTime, MeasurementDate
        )

        self.ui.RCResidualActDateTime.setMinimumDateTime(dt)
        self.ui.RCExportButton.enabled = False

    def checkComputeRCButton(self, sender: str) -> None:
        values = [line.text for line in self.volumeLineRC.values()]
        names = [line.name for line in self.volumeLineRC.values()]
        nlineEdited = int(names[values.index(sender)][-1])
        fiducialPlace = self.markupPlace[f"fiducialPlaceVolume_{nlineEdited}"]

        if sender:
            fiducialPlace.enabled = True
            vtkmrmlutils.setItemDataNodeAttribute(
                self.SPECTnode.nodeID, "volumens (cm3)", values
            )
        else:
            fiducialPlace.enabled = False

    def onLogicStartProgress(self):
        for prog in self.progressBarRC.values():
            prog.enabled = False
            prog.setValue(0)

    def onComputeRCButton(self):
        nodes = slicer.util.getNodesByClass(Attributes().vtkMRMLTableNode)
        for node in nodes:
            slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass(Attributes().vtkMRMLSegmentationNode)
        for node in nodes:
            slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass(Attributes().vtkMRMLPlotSeriesNode)
        for node in nodes:
            slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass(Attributes().vtkMRMLPlotChartNode)
        for node in nodes:
            slicer.mrmlScene.RemoveNode(node)

        volumes = {}
        diameter = {}
        for vol in self.volumeLineRC.values():
            volumes[vol.name[-1]] = float(vol.text)
            diameter[vol.name[-1]] = 2 * np.power(
                3 / (4 * np.pi) * float(vol.text) * 1000, 1 / 3
            )
        
        self.logic.calculateActConcentration(self.SPECTnode)

        if self.ui.SPECT_RCsetting_CheckBox.checked:
            self.onLogicStartProgress()
            self.logic.computeRecoveryCoefficient_SPECT(self.SPECTnode, 
                                                volumes, 
                                                diameter)
            
        if self.ui.CT_RCsetting_CheckBox.checked:
            self.onLogicStartProgress()
            self.logic.computeRecoveryCoefficient_CTbase(self.CTnode, 
                                                        self.SPECTnode, 
                                                        volumes, 
                                                        diameter)
            
        if not self.ui.SPECT_RCsetting_CheckBox.checked and not self.ui.CT_RCsetting_CheckBox.checked:
            slicer.util.errorDisplay(text=f"No method has been selected. Please select a method")
            self.logger.error(f"No method has been selected")
            return

        self.ui.RCExportButton.enabled = True

    def onLogicEventProgress(self, i, progress):
        # self.currentStatusLabel.text = "Running ({:3.1f}%)".format(progress)
        self.progressBarRC[f"progressBar_{i}"].enabled = True
        self.progressBarRC[f"progressBar_{i}"].show()
        self.progressBarRC[f"progressBar_{i}"].setValue(progress)

    def checkAvailability(self):
        self.ui.SPECT_SegmentsSetting_Widget.enabled = self.ui.SPECT_RCsetting_CheckBox.checked
        self.ui.CT_ResampleSetting_Widget.enabled = self.ui.CT_RCsetting_CheckBox.checked
        self.ui.CT_SegmentsSetting_Widget.enabled = self.ui.CT_RCsetting_CheckBox.checked
        self.ui.ResampleComboBox.hide() if not self.ui.CT_SPECTtoCT_opt.checked else self.ui.ResampleComboBox.show()

#######################
#                     #
# CalibrationLogic    #
#                     #
#######################


class CalibrationLogic(ScriptedLoadableModuleLogic):
    """This class should implement all the actual
    computation done by your module.  The interface
    should be such that other python code can import
    this class and make use of the functionality without
    requiring an instance of the Widget.
    Uses ScriptedLoadableModuleLogic base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def __init__(self, parent=None):
        """
        Called when the logic class is instantiated. Can be used for initializing member variables.
        """
        ScriptedLoadableModuleLogic.__init__(self)

        super().__init__(parent=parent)
        # Check requirements
        utils.checkRequirements()
        self.logger = logging.getLogger("Calibration.Logic")
        self.CTCalibration = {}
        self.SPECTSensitivity = {}
        self.SPECTRecovery = {}
        self.units = {}
        self.units["cm^3"] = (PhysicalUnits().getUnit("Longitude", "cm")) ** 3
        self.units["cm"] = PhysicalUnits().getUnit("Longitude", "cm")
        self.units["mm"] = PhysicalUnits().getUnit("Longitude", "mm")
        self.units["MBq"] = PhysicalUnits().getUnit("Activity", "MBq")
        self.units["hour"] = PhysicalUnits().getUnit("Time", "h")
        self.units["sec"] = PhysicalUnits().getUnit("Time", "s")
        self.cameraFactor = 1
        self.actConcentration = 1
        self.acquisitionDuration = 1
        self.resampFac = 1
        self.sensitivityResults = {}
        self.segmentExport = {}

    def getStatistics(self, Segmentation, ScalarVolume):
        # Run Segmentation Statistics
        segStatLogic = SegmentStatistics.SegmentStatisticsLogic()
        parameters = segStatLogic.getParameterNode()

        parameters.SetParameter("Segmentation", Segmentation.GetID())
        parameters.SetParameter("ScalarVolume", ScalarVolume.GetID())

        # LabelMap  Plugin
        parameters.SetParameter("LabelmapSegmentStatisticsPlugin.enabled", "True")
        parameters.SetParameter(
            "LabelmapSegmentStatisticsPlugin.centroid_ras.enabled", "True"
        )

        # ScalarVolume Plugin
        parameters.SetParameter("ScalarVolumeSegmentStatisticsPlugin.enabled", "True")
        parameters.SetParameter(
            "ScalarVolumeSegmentStatisticsPlugin.volume_cm3.enabled", "True"
        )
        parameters.SetParameter(
            "ScalarVolumeSegmentStatisticsPlugin.voxel_count.enabled", "True"
        )
        parameters.SetParameter(
            "ScalarVolumeSegmentStatisticsPlugin.mean.enabled", "True"
        )

        segStatLogic.computeStatistics()
        stats = segStatLogic.getStatistics()

        return stats

    def getListfromAttribute(self, attributeList):
        # --------------------------------------------------------------------------------------
        returnList = attributeList.strip("[]").replace("'", "")
        returnList = returnList.split(",")
        returnList = [x.strip() for x in returnList]
        return returnList

    def tableAndPlots(self, nRows, segmentationNode, volumeNode, color):
        # --------------------------------------------------------------------------------------
        resultsTableNode = slicer.mrmlScene.AddNewNodeByClass(Attributes().vtkMRMLTableNode)
        resultsTableNode.RemoveAllColumns()

        modality = segmentationNode.GetName().split('_')[-1]

        resultsTableNode.SetName(f"Table Recovery Coefficients ({modality})")
        table = resultsTableNode.GetTable()

        segmentColumn = vtk.vtkStringArray()
        segmentColumn.SetName("Segment")
        table.AddColumn(segmentColumn)

        volumeMeasuredColumn = vtk.vtkDoubleArray()
        volumeMeasuredColumn.SetName("Volume Measured [ml]")
        table.AddColumn(volumeMeasuredColumn)

        activityColumnValue = vtk.vtkDoubleArray()
        activityColumnValue.SetName("Activity in the volume [MBq]")
        table.AddColumn(activityColumnValue)

        countsNumberColumn = vtk.vtkLongArray()
        countsNumberColumn.SetName("Counts Number")
        table.AddColumn(countsNumberColumn)

        acivityInImageColumn = vtk.vtkDoubleArray()
        acivityInImageColumn.SetName("Activity on the image [MBq]")
        table.AddColumn(acivityInImageColumn)

        recoveryCoefficientColumn = vtk.vtkDoubleArray()
        recoveryCoefficientColumn.SetName(f"Recovery Coefficient ({modality})")
        table.AddColumn(recoveryCoefficientColumn)

        table.SetNumberOfRows(nRows)

        segmentation = segmentationNode.GetSegmentation()

        stats = self.getStatistics(segmentationNode, volumeNode.data)

        # Store tuples of (vol, rc) in a list
        data = []

        for segmentId in segmentation.GetSegmentIDs():
            volume_cm3 = stats[
                segmentId, "ScalarVolumeSegmentStatisticsPlugin.volume_cm3"
            ]
            voxel_count = stats[
                segmentId, "ScalarVolumeSegmentStatisticsPlugin.voxel_count"
            ]
            mean = stats[segmentId, "ScalarVolumeSegmentStatisticsPlugin.mean"]
            activityImage = (
                mean * voxel_count / self.cameraFactor / self.acquisitionDuration * self.resampFac
            )
            # if self.resampFac > 1:
            #     activityImage = activityImage * volume_cm3

            activityMeasured = self.actConcentration * volume_cm3
            rc = activityImage / activityMeasured
            vol = volume_cm3

            # Append tuple (vol, rc) to the list
            data.append((vol, rc))

            # Setting values in a table
            table.SetValue(int(segmentId[-1]) - 1, 0, segmentId)
            table.SetValue(int(segmentId[-1]) - 1, 1, f"{vol:.2f}")
            table.SetValue(int(segmentId[-1]) - 1, 2, f"{activityMeasured:.2f}")
            table.SetValue(int(segmentId[-1]) - 1, 3, int(voxel_count * mean))
            table.SetValue(int(segmentId[-1]) - 1, 4, f"{activityImage:.2f}")
            table.SetValue(int(segmentId[-1]) - 1, 5, f"{rc:.2f}")

        plotSeriesNode = slicer.mrmlScene.AddNewNodeByClass(
            Attributes().vtkMRMLPlotSeriesNode, f"RC_{modality}"
        )
        plotSeriesNode.SetAndObserveTableNodeID(resultsTableNode.GetID())
        plotSeriesNode.SetXColumnName(volumeMeasuredColumn.GetName())
        plotSeriesNode.SetYColumnName(recoveryCoefficientColumn.GetName())
        plotSeriesNode.SetPlotType(slicer.vtkMRMLPlotSeriesNode.PlotTypeScatter)
        plotSeriesNode.SetLineStyle(slicer.vtkMRMLPlotSeriesNode.LineStyleNone)
        plotSeriesNode.SetMarkerStyle(slicer.vtkMRMLPlotSeriesNode.MarkerStyleCircle)
        plotSeriesNode.SetColor(tuple(color))

        # Sort the data list by the first element of each tuple (vol)
        data.sort(key=lambda x: x[0])

        # Separate x, y for fiting
        x, y = np.array(data).T

        # Fit function
        fitter = RecoveryFunction(
            x, y, lamda=1
        )  # lamda parameter required but never used
        fitter.minimize()

        plotTableNode = slicer.mrmlScene.AddNewNodeByClass(Attributes().vtkMRMLTableNode)
        plotTableNode.RemoveAllColumns()

        plotTableNode.SetName(f"Fit Recovery Coefficients ({modality})")
        plotTable = plotTableNode.GetTable()

        xColumn = vtk.vtkDoubleArray()
        xColumn.SetName("x")
        plotTable.AddColumn(xColumn)

        yColumn = vtk.vtkDoubleArray()
        yColumn.SetName("y")
        plotTable.AddColumn(yColumn)

        xdata = np.linspace(x[0], x[-1], 50)
        plotTable.SetNumberOfRows(len(xdata))

        for index, data in enumerate(xdata):
            xColumn.SetValue(index, data)
            yColumn.SetValue(index, fitter.plot(data))

        xColumn.Modified()
        yColumn.Modified()

        plotFitSeriesNode = slicer.mrmlScene.AddNewNodeByClass(
            Attributes().vtkMRMLPlotSeriesNode, f"R_{modality} = {str(fitter).split('=')[-1]}"
        )

        plotFitSeriesNode.SetAndObserveTableNodeID(plotTableNode.GetID())
        plotFitSeriesNode.SetXColumnName(xColumn.GetName())
        plotFitSeriesNode.SetYColumnName(yColumn.GetName())
        plotFitSeriesNode.SetPlotType(slicer.vtkMRMLPlotSeriesNode.PlotTypeScatter)
        plotFitSeriesNode.SetMarkerStyle(slicer.vtkMRMLPlotSeriesNode.MarkerStyleNone)
        plotFitSeriesNode.SetColor(tuple(color))

        plotChartNode = slicer.util.getFirstNodeByClassByName(
             Attributes().vtkMRMLPlotChartNode, "Plot Recovery Coefficients"
        )
        if not plotChartNode:
            plotChartNode = slicer.mrmlScene.AddNewNodeByClass(
                Attributes().vtkMRMLPlotChartNode, "Plot Recovery Coefficients"
            )
        plotChartNode.SetXAxisTitle(" Volume (ml) ")
        plotChartNode.SetYAxisTitle(" RC ")
        plotChartNode.AddAndObservePlotSeriesNodeID(plotFitSeriesNode.GetID())
        plotChartNode.AddAndObservePlotSeriesNodeID(plotSeriesNode.GetID())

        # Show plot in layout
        slicer.modules.plots.logic().ShowChartInLayout(plotChartNode)
        slicer.app.layoutManager().plotWidget(0).plotView().fitToContent()

        vtkmrmlutils.setItemDataNodeAttribute(
            volumeNode.nodeID, "Fitting function", str(fitter)
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            volumeNode.nodeID, "volume on image", np.around(x, decimals=2)
        )
        vtkmrmlutils.setItemDataNodeAttribute(
            volumeNode.nodeID, "Recovery Coefficients", np.around(y, decimals=2)
        )

        showTable(resultsTableNode)

    def getDiameterOffset(self, volume):
        return 0.2/np.exp(volume/4-0.2) + 0.75

    def createMaskedVolume(self, imageNode, pointListNode, diameter, volumeSize):
        # --------------------------------------------------------------------------------------
        ras = vtk.vtkVector3d(0, 0, 0)
        pointListNode.GetNthControlPointPosition(0, ras)
        # color = np.array(pointListNode.GetDisplayNode().GetColor())
        name = pointListNode.GetName()

        # Create new segment node
        segmentationNode = slicer.mrmlScene.AddNewNodeByClass(
            Attributes().vtkMRMLSegmentationNode, f"Segment_{name}"
        )
        segmentationNode.SetReferenceImageGeometryParameterFromVolumeNode(
            imageNode.data
        )

        # Create segment editor to get access to effects
        segmentEditorWidget = slicer.qMRMLSegmentEditorWidget()
        segmentEditorWidget.setMRMLScene(slicer.mrmlScene)
        segmentEditorNode = slicer.vtkMRMLSegmentEditorNode()
        slicer.mrmlScene.AddNode(segmentEditorNode)
        segmentEditorWidget.setMRMLSegmentEditorNode(segmentEditorNode)
        segmentEditorWidget.setSegmentationNode(segmentationNode)
        segmentEditorWidget.setSourceVolumeNode(imageNode.data)

        # Create seed segment inside volume
        diameterOffset = self.getDiameterOffset(volumeSize[name[-1]])
        radius = diameter[name[-1]] * diameterOffset
        sphere = vtk.vtkSphereSource()
        sphere.SetCenter(ras)
        sphere.SetRadius(radius)
        sphere.SetPhiResolution(30)
        sphere.SetThetaResolution(30)
        sphere.Update()
        segmentationNode.AddSegmentFromClosedSurfaceRepresentation(
            sphere.GetOutput(), name, [1.0, 0.0, 0.0]
        )
        segmentEditorWidget.setCurrentSegmentID(name)

        # Set up masking parameters
        segmentEditorWidget.setActiveEffectByName("Mask volume")
        effect = segmentEditorWidget.activeEffect()
        # set fill value to be outside the valid intensity range
        effect.setParameter("FillValue", '0')
        # Blank out voxels that are outside the segment
        effect.setParameter("Operation", "FILL_OUTSIDE")
        # Create a volume that will store temporary masked volumes
        maskedVolume = slicer.mrmlScene.AddNewNodeByClass(
            Attributes().vtkMRMLScalarVolumeNode,
            f"Temporary masked {name}",
        )
        effect.self().outputVolumeSelector.setCurrentNode(maskedVolume)
        effect.self().onApply()

        slicer.mrmlScene.RemoveNode(segmentationNode)

        return maskedVolume
      

    def segmentByThreshold(
        self, segmentNodeName, segmentName, minValue, volumeNode, rmNoise=False
    ):
        # --------------------------------------------------------------------------------------
        segmentationNode = slicer.mrmlScene.GetFirstNodeByName(segmentNodeName)
        segmentationNode.SetReferenceImageGeometryParameterFromVolumeNode(volumeNode)

        # Remove segment if exists
        if segmentationNode.GetSegmentation().GetSegment(segmentName):
            segmentationNode.RemoveSegment(segmentName)

        segmentId = segmentationNode.GetSegmentation().AddEmptySegment(segmentName)

        volumeArray = slicer.util.arrayFromVolume(volumeNode)
        segmentArray = slicer.util.arrayFromSegmentBinaryLabelmap(segmentationNode, segmentId, volumeNode)

        # Modify the segmentation
        # segmentArray[:] = 0  # clear the segmentation
        segmentArray[ volumeArray >= minValue ] = 1  # create segment by simple thresholding of an image
        slicer.util.updateSegmentBinaryLabelmapFromArray(segmentArray, segmentationNode, segmentId, volumeNode)

        voxel_count = len(segmentArray[segmentArray == 1].ravel())
        voxelVolume = (
                    np.prod(np.array(volumeNode.GetSpacing()))
                    * Constants().units["mm3"]
                    / Constants().units["cm3"]
                )

        MinSizeVoxels = 100
        if rmNoise and voxel_count > MinSizeVoxels:
            # Create segment editor to get access to effects
            segmentEditorWidget = slicer.qMRMLSegmentEditorWidget()
            segmentEditorWidget.setMRMLScene(slicer.mrmlScene)
            segmentEditorNode = slicer.vtkMRMLSegmentEditorNode()
            segmentEditorNode.SetOverwriteMode(
                slicer.vtkMRMLSegmentEditorNode.OverwriteNone
            )
            slicer.mrmlScene.AddDefaultNode(segmentEditorNode)
            slicer.mrmlScene.AddNode(segmentEditorNode)
            segmentEditorWidget.setMRMLSegmentEditorNode(segmentEditorNode)
            segmentEditorWidget.setSegmentationNode(segmentationNode)
            segmentEditorWidget.setSourceVolumeNode(volumeNode)
            
            segmentEditorWidget.setActiveEffectByName("Islands")
            effect = segmentEditorWidget.activeEffect()
            effect.setParameter("Operation", "KEEP_LARGEST_ISLAND")
            effect.setParameter("MinimumSize", str(MinSizeVoxels))
            effect.self().onApply()
            segmentEditorWidget.setActiveEffectByName("No editing")
        
        volume_cm3 = voxel_count * voxelVolume
        return volume_cm3

    def createSegmentSen_VolumeThld(self, imageNode, segmentName, tankVolume):
        # --------------------------------------------------------------------------------------
        segmentationNode = slicer.mrmlScene.GetFirstNodeByName(
            "SegmentationSensitivity"
        )
        segmentationNode.SetReferenceImageGeometryParameterFromVolumeNode(
            imageNode.data
        )

        arrayValues = imageNode.getArrayData()
        maxValue = max(arrayValues.ravel())
        minValue = 0

        stats = self.getStatistics(segmentationNode, imageNode.data)
        FOVvolume = stats["Field of View", "ScalarVolumeSegmentStatisticsPlugin.volume_cm3"]

        if tankVolume > FOVvolume:
            raise errors.IOError(
                f"Error in volume entered, \
                                cannot be greater than Field of View {FOVvolume:.2f}"
            )

        step = 100
        isInterval = False
        interval = maxValue - minValue
        prev_VolumeDiff = float(math.inf)
        minVolumeDiff = FOVvolume

        while interval > 0.5:
            volume_cm3 = self.segmentByThreshold(
                "SegmentationSensitivity",
                segmentName,
                minValue,
                imageNode.data
            )
            VolumeDiff = volume_cm3 - tankVolume / self.units["cm^3"]

            if abs(VolumeDiff) < minVolumeDiff:
                minVolumeDiff = abs(VolumeDiff)
                thrldValue = minValue

            if VolumeDiff * prev_VolumeDiff > 0 and not isInterval:
                min_left = minValue
                minValue += step
                min_right = minValue
                prev_VolumeDiff = VolumeDiff
                isInterval = False
            else:
                isInterval = True

            if isInterval:
                if VolumeDiff * prev_VolumeDiff < 0:
                    min_right = minValue
                else:
                    prev_VolumeDiff = VolumeDiff
                    min_left = minValue

                minValue = min_left + ((min_right - min_left) / 2)

            interval = min_right - min_left

        if thrldValue != minValue:
            volume_cm3 = self.segmentByThreshold(
                "SegmentationSensitivity",
                segmentName,
                thrldValue,
                imageNode.data
            )

    def createSegmentSen_Lassmann(self, imageNode, segmentName, diameter):
        # --------------------------------------------------------------------------------------
        segmentationNode = slicer.mrmlScene.GetFirstNodeByName(
            "SegmentationSensitivity"
        )
        segmentationNode.SetReferenceImageGeometryParameterFromVolumeNode(
            imageNode.data
        )

        # Create segment editor to get access to effects
        segmentEditorWidget = slicer.qMRMLSegmentEditorWidget()
        segmentEditorWidget.setMRMLScene(slicer.mrmlScene)
        segmentEditorNode = slicer.vtkMRMLSegmentEditorNode()
        segmentEditorNode.SetOverwriteMode(
            slicer.vtkMRMLSegmentEditorNode.OverwriteNone
        )
        slicer.mrmlScene.AddDefaultNode(segmentEditorNode)
        slicer.mrmlScene.AddNode(segmentEditorNode)
        segmentEditorWidget.setMRMLSegmentEditorNode(segmentEditorNode)
        segmentEditorWidget.setSegmentationNode(segmentationNode)
        segmentEditorWidget.setSourceVolumeNode(imageNode.data)

        stats = self.getStatistics(segmentationNode, imageNode.data)

        volume = stats["Volume Threshold", "ScalarVolumeSegmentStatisticsPlugin.volume_cm3"]
        centroid = stats[
            "Volume Threshold", "LabelmapSegmentStatisticsPlugin.centroid_ras"
        ]

        # Compute height
        height = 4 * (volume * self.units["cm^3"]) / (math.pi * diameter**2)

        # Cylinder
        source = vtk.vtkCylinderSource()
        source.SetResolution(100)
        source.CappingOn()
        source.Update()
        polyData = source.GetOutput()
        transformer = vtk.vtkMatrix4x4()
        transformer.Identity()

        # Transform
        transform = vtk.vtkTransform()
        transform.Translate(centroid)  # setting the center
        transform.Concatenate(transformer)  # applying the new base
        transform.RotateX(90.0)  # alignment
        transform.Scale(
            1.3 * diameter / self.units["mm"],
            1.2 * height / self.units["mm"],
            1.3 * diameter / self.units["mm"],
        )  # setting of the dimensions

        # Transformation
        transformation = vtk.vtkTransformPolyDataFilter()
        transformation.SetTransform(transform)
        transformation.SetInputData(polyData)
        transformation.Update()
        polyData = transformation.GetOutput()
        segmentationNode.AddSegmentFromClosedSurfaceRepresentation(
            polyData, segmentName, [1.0, 0.0, 0.0]
        )

    def computeSegmenMethods(self, lnode, nodeSensitivity: float, tankVolume: str, diameter: str): 
        # Remove all segments if they exist
        SegmentNodes = slicer.mrmlScene.GetNodesByClass(Attributes().vtkMRMLSegmentationNode)
        for node in SegmentNodes:
            if node:
                slicer.mrmlScene.RemoveNode(node)

        # Create new segment node
        segmentationNode = slicer.mrmlScene.AddNewNodeByClass(
            Attributes().vtkMRMLSegmentationNode, "SegmentationSensitivity"
        )

        # Check the applicability of segmentation methods.
        if not tankVolume:
            del self.segmentExport["Volume Threshold"]
        else:
            tankVolume = float(tankVolume) * self.units["cm^3"]

        if not diameter:
            del self.segmentExport["Lassmann"]
        else:
            diameter = float(diameter) * self.units["cm"]
            if diameter == 0:
                del self.segmentExport["Lassmann"]
                slicer.util.errorDisplay("Invalid diameter", "SPECT Sensitivity")
                raise ValueError(f"Invalid diameter")

        # Compute segmentation methods
        FOVmin = 0
        self.segmentByThreshold(
            "SegmentationSensitivity",
            "Field of View",
            FOVmin,
            nodeSensitivity,
            rmNoise=False,
        )
        if "Volume Threshold" in self.segmentExport:
            self.createSegmentSen_VolumeThld(lnode, "Volume Threshold", tankVolume)
        if "Lassmann" in self.segmentExport:
            self.createSegmentSen_Lassmann(lnode, "Lassmann", diameter)

        # Make segmentation results visible in 3D
        segmentationNode.CreateClosedSurfaceRepresentation()
        segmentationDisplayNode = segmentationNode.GetDisplayNode()

        # Change overall segmentation display properties
        segmentationDisplayNode.SetOpacity3D(0.5)

        return segmentationNode
    
    def tableSPECTSensitivity(self, segmentationNode, nodeSensitivity, ActivityCorrected, duration):
        # Remove all tables if they exist
        TableNodes = slicer.mrmlScene.GetNodesByClass(Attributes().vtkMRMLTableNode)
        for node in TableNodes:
            slicer.mrmlScene.RemoveNode(node)
        segmentationNode = slicer.mrmlScene.GetFirstNodeByName(
            "SegmentationSensitivity"
        )

        # Prepare clean table
        name = f"T:TABL SPECT Sensitivity Helper"
        resultsTableNode = slicer.mrmlScene.AddNewNodeByClass(Attributes().vtkMRMLTableNode)
        resultsTableNode.SetName(name)
        table = resultsTableNode.GetTable()

        segmentColumnValue = vtk.vtkStringArray()
        segmentColumnValue.SetName("Segment")
        table.AddColumn(segmentColumnValue)

        segmentColumnValue = vtk.vtkDoubleArray()
        segmentColumnValue.SetName("Volume [cm3]")
        table.AddColumn(segmentColumnValue)

        segmentColumnValue = vtk.vtkLongArray()
        segmentColumnValue.SetName("Total Counts")
        table.AddColumn(segmentColumnValue)

        segmentColumnValue = vtk.vtkDoubleArray()
        segmentColumnValue.SetName("Sensitivity [counts/MBqs]")
        table.AddColumn(segmentColumnValue)

        segmIDs = segmentationNode.GetSegmentation().GetSegmentIDs()

        table.SetNumberOfRows(len(segmIDs))

        stats = self.getStatistics(segmentationNode, nodeSensitivity)

        for id, segmentId in enumerate(segmIDs):
            volume_cm3 = stats[
                segmentId, "ScalarVolumeSegmentStatisticsPlugin.volume_cm3"
            ]

            if volume_cm3 <= 0:
                slicer.util.errorDisplay("Can't extract Volumen from the image", 
                                         "SPECT Sensitivity")
                raise ValueError(f"Can't extract the {segmentId} volumen from the image")
            
            voxel_count = stats[
                segmentId, "ScalarVolumeSegmentStatisticsPlugin.voxel_count"
            ]
            mean = stats[segmentId, "ScalarVolumeSegmentStatisticsPlugin.mean"]

            vol = volume_cm3
            totalCounts = mean * voxel_count
            Sensitivity = totalCounts / (duration * ActivityCorrected)
            self.sensitivityResults[segmentId] = Sensitivity

            table.SetValue(id, 0, segmentId)
            table.SetValue(id, 1, f"{vol:.2f}")
            table.SetValue(id, 2, round(totalCounts))
            table.SetValue(id, 3, f"{Sensitivity:.2f}")

        resultsTableNode.SetAttribute(
            Attributes().studyCreation, utils.getCurrentTime()
        )

        # Show the Table
        vtkmrmlutils.showTable(resultsTableNode)

    def correctActivity(self, lnode, activity, duration, widget):
        Isotope = dbutils.setupIsotope(lnode.getIsotope())
        ln2 = np.log(2)
        T_h = Isotope["T_h"] * self.units["hour"]
        injTime = datetime.strptime(lnode.getInjectionTime(), Constants().timeFormat)
        residualAct = (
            float(
                vtkmrmlutils.getItemDataNodeAttributeValue(
                    lnode.nodeID, "Residual Activity (MBq)"
                )
            )
            * self.units["MBq"]
        )
        residualActTime = vtkmrmlutils.getItemDataNodeAttributeValue(
            lnode.nodeID, "Residual Activity Time"
        )
        restTime = (
            float(
                vtkmrmlutils.getItemDataNodeAttributeValue(
                    lnode.nodeID, "Decay Time (sec)"
                )
            )
            * self.units["sec"]
        )

        residualTime = datetime.strptime(residualActTime, Constants().timeFormat)
        preparationTime = (residualTime - injTime).total_seconds()

        Activity = (
            activity - (residualAct * np.exp(ln2 / T_h * (preparationTime)))
        ) / self.units["MBq"]

        # Correction for decay during rest Time
        decayRestCorrection = np.exp(-ln2 * float(restTime) / T_h)

        # Correction for decay during acquisition
        dac = ln2 * duration / T_h
        decayAcquisitionCorrection = (
            (1 - np.exp(-dac)) / dac if widget.ui.correctAct_AcqTime.checked else 1
        )

        ActivityCorrected = Activity * decayRestCorrection * decayAcquisitionCorrection

        return ActivityCorrected

    def runSPECTSensitivity(self, lnode):
        # --------------------------------------------------------------------------------------
        widget = slicer.modules.CalibrationWidget
        nodeSensitivity = lnode.data        
        activity = float(lnode.getInjectedActivity()) * self.units["MBq"]
        tankVolume = (
            (
                vtkmrmlutils.getItemDataNodeAttributeValue(
                    lnode.nodeID, "Tank Volume (cm3)"
                )
            )
        )
        diameter = (
            (
                vtkmrmlutils.getItemDataNodeAttributeValue(
                    lnode.nodeID, "Tank Diameter (cm)"
                )
            )
        )
        duration = (
            float(nodeSensitivity.GetAttribute(Attributes().acquisitionDuration))
            * self.units["sec"]
        )

        if activity <= 0:
            raise errors.IOError("Activity can't be zero")

        self.segmentExport = {
            "Volume Threshold": False,
            "Lassmann": False,
            "Field of View": True,
        }

        ActivityCorrected = self.correctActivity(lnode, 
                                                 activity,
                                                 duration, 
                                                 widget)

        segmentationNode = self.computeSegmenMethods(lnode, 
                                                     nodeSensitivity, 
                                                     tankVolume, 
                                                     diameter)
        
        widget.setupSensitivityExport(self.segmentExport)

        self.tableSPECTSensitivity(segmentationNode, 
                                   nodeSensitivity, 
                                   ActivityCorrected, 
                                   duration)

    def computeRecoveryCoefficient_SPECT(self, SPECTnode, volumeSize, diameter):
        color=[1,0,0]
        self.resampFac = 1
        widget = slicer.modules.CalibrationWidget
        segmentNodeName = "SegmentationRC_SPECT"
        # --------------------------------------------------------------------------------------
        existingMarkupsNodes = slicer.util.getNodesByClass(Attributes().vtkMRMLMarkupsFiducialNode)

        for pointListNode in existingMarkupsNodes:
            name = pointListNode.GetName()

            maskedVolume = self.createMaskedVolume(SPECTnode, 
                                                    pointListNode, 
                                                    diameter,
                                                    volumeSize)
            
            if widget.ui.SPECT_CenterOfMass_opt.checked:
                point_Ijk = self.extractMidPointCenterOfMass(maskedVolume)
                radius = diameter[name[-1]]/2 
                segmentationNode = self.createSphereVTK(SPECTnode, 
                                                        segmentNodeName, 
                                                        name, 
                                                        point_Ijk, 
                                                        radius,
                                                        color)
            else:
                segmentationNode = self.segmentRC_VolumeThreshold(maskedVolume, 
                                                                segmentNodeName, 
                                                                name, 
                                                                volumeSize,
                                                                color)
                
            slicer.mrmlScene.RemoveNode(maskedVolume)
        # self.setSourceVolumeToSegmentation(imageNode, segmentationNode)

        self.tableAndPlots(len(existingMarkupsNodes), 
                           segmentationNode, 
                           SPECTnode,
                           color)
        # --------------------------------------------------------------------------------------
    
    def segmentRC_VolumeThreshold(self, maskedVolume, segmentNodeName, name, volumeSize, color):
        widget = slicer.modules.CalibrationWidget
        arrayValues = slicer.util.arrayFromVolume(maskedVolume)
        maxValue = max(arrayValues.ravel())
        minValue = min(arrayValues.ravel())
        segmentationNode = slicer.mrmlScene.GetFirstNodeByName(segmentNodeName)
        if not segmentationNode:
            segmentationNode = slicer.mrmlScene.AddNewNodeByClass(
                Attributes().vtkMRMLSegmentationNode, segmentNodeName
            )
        
        teoricalVol = volumeSize[name[-1]]

        interval = maxValue - minValue
        prev_VolumeDiff = None
        minVolumeDiff = float('inf')
        progress = 0

        # Initialize min_left and min_right
        min_left = minValue
        min_right = maxValue 

        while interval > 0.1:
            volume_cm3 = self.segmentByThreshold(segmentNodeName, name, minValue, maskedVolume)
            VolumeDiff = volume_cm3 - teoricalVol

            # Update minimum volume difference and threshold value
            if abs(VolumeDiff) < minVolumeDiff:
                minVolumeDiff = abs(VolumeDiff)
                thrldValue = minValue
                # Break the loop if the volume difference is below 0.005
                if minVolumeDiff < 0.005:
                    break

            # If it's the first iteration, initialize prev_VolumeDiff
            if prev_VolumeDiff is None:
                prev_VolumeDiff = VolumeDiff

            # Check for sign change to update interval
            if VolumeDiff * prev_VolumeDiff < 0:
                min_right = minValue
            else:
                min_left = minValue
                prev_VolumeDiff = VolumeDiff

            # Update minValue using the bisection method
            minValue = (min_left + min_right) / 2

            # Calculate new interval
            interval = min_right - min_left

            # Update progress
            progress_increment = min(2, 100 - progress)
            progress += progress_increment
            widget.onLogicEventProgress(name[-1], progress)

        # After the loop, use the threshold value that gave the minimum volume difference
        if thrldValue != minValue:
            volume_cm3 = self.segmentByThreshold(
                segmentNodeName, name, thrldValue, maskedVolume, rmNoise=False
            )
            segmentationNode.GetSegmentation().GetSegment(name).SetColor(tuple(color))

        print(f"{name}; max_threshold = {maxValue:.2f}; min_threshold = {thrldValue:.2f}")

        widget.onLogicEventProgress(name[-1], 100)

        return segmentationNode

    def computeRecoveryCoefficient_CTbase(self, CTnode, SPECTnode, volumeSize, diameter):
        # --------------------------------------------------------------------------------------
        widget = slicer.modules.CalibrationWidget
        segmentNodeName = "SegmentationRC_CT"
        existingMarkupsNodes = slicer.util.getNodesByClass(Attributes().vtkMRMLMarkupsFiducialNode)
        color = [0,0,1]
        # --------------------------------------------------------------------------------------
        for pointListNode in existingMarkupsNodes:
            name = pointListNode.GetName()

            maskedVolume = self.createMaskedVolume(CTnode, 
                                                    pointListNode, 
                                                    diameter,
                                                    volumeSize)
            
            if widget.ui.CT_CenterOfMass_opt.checked:
                point_Ijk = self.extractMidPointCenterOfMass(maskedVolume)
            else:
                point_Ijk = self.extractMidPoint(maskedVolume,
                                                pointListNode, 
                                                diameter[name[-1]])
                
           
            radius = diameter[name[-1]]/2 
            segmentationNode = self.createSphereVTK(CTnode, 
                                                    segmentNodeName, 
                                                    name, 
                                                    point_Ijk, 
                                                    radius,
                                                    color)

            slicer.mrmlScene.RemoveNode(maskedVolume)
            widget.onLogicEventProgress(name[-1], 100)

        if widget.ui.CT_SPECTtoCT_opt.checked:
            ResampleAlgo = widget.ui.ResampleComboBox.currentText
            SPECTnode = self.resample(SPECTnode, CTnode, ResampleAlgo)
            self.setSourceVolumeToSegmentation(SPECTnode, segmentationNode)
        else:
            self.setSourceVolumeToSegmentation(SPECTnode, segmentationNode)

        self.tableAndPlots(len(existingMarkupsNodes), 
                            segmentationNode, 
                            SPECTnode,
                            color)
        
    def setSourceVolumeToSegmentation(self, imageNode, segmentationNode):
        segmentationNode.RemoveBinaryLabelmapRepresentation()
        segmentationNode.SetReferenceImageGeometryParameterFromVolumeNode(imageNode.data)
        segmentationNode.CreateBinaryLabelmapRepresentation()
        segmentationNode.CreateClosedSurfaceRepresentation()

    def resample(self, inputNode, referenceNode, interpolationMode):
        ProgressDialog = slicer.util.createProgressDialog(
            parent=None, value=0, maximum=2
        )

        ProgressDialog.labelText = f"Resampling SPECT to CT"
        ProgressDialog.value = 1

        # Output Rescaled CT using SPECT sampling
        minInput = float(min(inputNode.getArrayData().ravel()))
        newName = f"Resampled_{inputNode.name}"
        NMRSNode = vtkmrmlutils.cloneNode(inputNode.data, newName)
        NMRSNode.SetAttribute(Attributes().modality, "NMRS")
        itemID = vtkmrmlutils.getItemID(NMRSNode)
        NMRSNode = Node.new(itemID)

        # # Convert to concentration counts
        # arrayNMRS = NMRSNode.getArrayData()
        # voxelSize = (
        #             np.prod(np.array(inputNode.getSpacing()))
        #             * Constants().units["mm3"]
        #             / Constants().units["cm3"]
        #         )
        # new_arr = arrayNMRS/voxelSize
        # NMRSNode.setArrayData(new_arr)

        parameters = {
            "inputVolume": NMRSNode.data,
            "referenceVolume": referenceNode.data,
            "outputVolume": NMRSNode.data,
            "interpolationMode": interpolationMode,
            "defaultValue": minInput,
        }

        slicer.cli.run(
            slicer.modules.brainsresample,
            None,
            parameters,
            wait_for_completion=True,
            update_display=False,
        )

        self.resampFac = np.prod(inputNode.getDimensions())/np.prod(referenceNode.getDimensions())/2

        ProgressDialog.value = 2
        ProgressDialog.close()

        return NMRSNode

    def createSphereVTK(self, imageNode, segmentNodeName, segmentName, point_Ijk, radius, color):
        segmentationNode = slicer.mrmlScene.GetFirstNodeByName(segmentNodeName)
        if not segmentationNode:
            segmentationNode = slicer.mrmlScene.AddNewNodeByClass(
                Attributes().vtkMRMLSegmentationNode, segmentNodeName
            )

        # Remove segment if exists
        if segmentationNode.GetSegmentation().GetSegment(segmentName):
            segmentationNode.RemoveSegment(segmentName)
        
        # Get physical coordinates from voxel coordinates
        volumeIjkToRas = imageNode.getIJKtoRASMatrix()
        point_VolumeRas = [0, 0, 0, 1]
        volumeIjkToRas.MultiplyPoint(np.append(point_Ijk,1.0), point_VolumeRas)
        point_Ras = point_VolumeRas[0:3]

        sphere = vtk.vtkSphereSource()
        sphere.SetCenter(point_Ras)
        sphere.SetRadius(radius)
        sphere.SetPhiResolution(30)
        sphere.SetThetaResolution(30)
        sphere.Update()
        segmentationNode.AddSegmentFromClosedSurfaceRepresentation(
                            sphere.GetOutput(), segmentName, color)
        
        self.setSourceVolumeToSegmentation(imageNode, segmentationNode)

        return segmentationNode
    
    def createSphere(self, imageNode, segmentNodeName, segmentName, point_Ijk, radius):
        # Get the size and spacing of the image
        size = imageNode.getDimensions()
        spacing = imageNode.getSpacing()
         # --------------------------------------------------------------------------------------
        segmentationNode = slicer.mrmlScene.GetFirstNodeByName(segmentNodeName)
        segmentationNode.SetReferenceImageGeometryParameterFromVolumeNode(imageNode.data)

        # Remove segment if exists
        if segmentationNode.GetSegmentation().GetSegment(segmentName):
            segmentationNode.RemoveSegment(segmentName)

        segmentId = segmentationNode.GetSegmentation().AddEmptySegment(segmentName)

        volumeArray = slicer.util.arrayFromVolume(imageNode.data)
        segmentArray = slicer.util.arrayFromSegmentBinaryLabelmap(segmentationNode, segmentId, imageNode.data)

        # Modify the segmentation
        # Create a meshgrid for voxel indices
        z, y, x = np.meshgrid(
            np.arange(size[2]), np.arange(size[1]), np.arange(size[0]), indexing='ij')
        
        # Calculate the distance of each voxel from the center
        distance = np.sqrt(
            ((x - point_Ijk[0]) * spacing[0])**2 +
            ((y - point_Ijk[1]) * spacing[1])**2 +
            ((z - point_Ijk[2]) * spacing[2])**2
        )
        
        # Create the sphere by setting the intensity for voxels within the radius
        segmentArray[distance <= radius] = int(segmentName[-1])
        # segmentArray[ volumeArray >= minValue ] = 1  # create segment by simple thresholding of an image
        slicer.util.updateSegmentBinaryLabelmapFromArray(segmentArray, segmentationNode, segmentId, imageNode.data)

        return segmentationNode
    
    def extractMidPointCenterOfMass(self, imageNode):
        image_array = slicer.util.arrayFromVolume(imageNode)
        image_spacing = [1, 1, 1]
        center_Ijk = utils.Center_of_Mass(image_array, image_spacing)
        point_Ijk = [int(round(center_Ijk[2])),
                     int(round(center_Ijk[1])),
                     int(round(center_Ijk[0]))]
        # print(f"Centre of mass = {point_Ijk}")
        return point_Ijk
    
    def extractMidPoint(self, imageNode, pointListNode, max_diameter):
        centerRas = vtk.vtkVector3d(0, 0, 0)
        pointListNode.GetNthControlPointPosition(0, centerRas)

        # print(f'max_diameter = {max_diameter}')

        volumeRasToIjk = vtk.vtkMatrix4x4()
        imageNode.GetRASToIJKMatrix(volumeRasToIjk)
        point_Ijk = [0, 0, 0, 1]
        volumeRasToIjk.MultiplyPoint(np.append(centerRas,1.0), point_Ijk)
        point_Ijk = [int(round(c)) for c in point_Ijk[0:3]]

        # print(f'point_Ijk = {point_Ijk}')

        image_array = slicer.util.arrayFromVolume(imageNode)
        image_spacing = np.array(imageNode.GetSpacing())
        radius = [int(max_diameter/space) for space in image_spacing] 

        # Segmentation: Apply a threshold
        threshold_value = 0  # threshold value in Hounsfield units
        segmented_array = np.where(image_array > threshold_value, image_array, 0)

        # Parameters for the spheres
        x_inf, x_sup = [point_Ijk[0] - radius[0], point_Ijk[0] + radius[0]]
        y_inf, y_sup = [point_Ijk[1] - radius[1], point_Ijk[1] + radius[1]]
        z_inf, z_sup = [point_Ijk[2] - radius[2], point_Ijk[2] + radius[2]]
        #----------------------------------------------------------------------------

        mid_Xpoint = []
        mid_Ypoint = []

        max_X_diameter = 0
        max_Y_diameter = 0

        for _slice in range(z_inf,z_sup):
            mid_Xpoint_temp = []
            mid_Ypoint_temp = []
            for row in range(y_inf, y_sup):
                Hline_profile = segmented_array[_slice, row, :]
                if np.max(Hline_profile) == np.min(Hline_profile):
                    continue
                # Find the maximum indices in the same slice
                max_indices = np.argpartition(Hline_profile, -2)[-2:]
                # Find the mid-point between the two maximum values
                mid_Xpoint_temp.append(np.mean(max_indices))
                # Find the distance in pixels between the two maximum values
                diameter = np.diff(max_indices)[0]
                if diameter > max_X_diameter and diameter < max_diameter*1.2 :
                    select_Xslice = _slice
                    max_X_diameter = diameter

            if not np.isnan(np.median(mid_Xpoint_temp)):
                mid_Xpoint.append(np.median(mid_Xpoint_temp))
                # print(f'slice = {_slice}; x = {np.median(mid_Xpoint_temp)}')

            for colum in range(x_inf, x_sup):
                Vline_profile = segmented_array[_slice, :, colum]
                if np.max(Vline_profile) == np.min(Vline_profile):
                    continue
                # Find the maximum indices in the same slice
                max_indices = np.argpartition(Vline_profile, -2)[-2:]
                # Find the mid-point between the two maximum values
                mid_Ypoint_temp.append(np.mean(max_indices))
                # Find the distance in pixels between the two maximum values
                diameter = np.diff(max_indices)[0]
                if diameter > max_Y_diameter and diameter < max_diameter*1.2 :
                    select_Yslice = _slice
                    max_Y_diameter = diameter

            if not np.isnan(np.median(mid_Ypoint_temp)):
                mid_Ypoint.append(np.median(mid_Ypoint_temp))
                # print(f'slice = {_slice}; y = {np.median(mid_Ypoint_temp)}')

        # print('\n')

        select_slice = round((select_Xslice + select_Yslice)/2)
        if select_Xslice != select_Yslice:
            if np.abs(select_Xslice - select_Yslice) > 1:
                select_slice = select_Xslice
        
        x = round(np.median(mid_Xpoint))
        y = round(np.median(mid_Ypoint))
        z = select_slice

        diameter = round((max_Y_diameter + max_X_diameter)/2, 2)

        centreIJK = [x, y, z]
        # radius = diameter*image_spacing[0]/2

        # print('---------------------------------------------\n')

        return centreIJK
    
    def calculateActConcentration(self, imageNode):
        widget = slicer.modules.CalibrationWidget
        nodeRC = imageNode.data

        activity = float(imageNode.getInjectedActivity()) * self.units["MBq"]
        self.cameraFactor = float(
            vtkmrmlutils.getItemDataNodeAttributeValue(
                imageNode.nodeID, "Sensitivity SPECT (counts/MBqs)"
            )
        )
        volumeSyringe = float(
            vtkmrmlutils.getItemDataNodeAttributeValue(
                imageNode.nodeID, "Volunme in Syringe (ml)"
            )
        )
        duration = (
            float(nodeRC.GetAttribute(Attributes().acquisitionDuration))
            * self.units["sec"]
        )

        if activity <= 0:
            slicer.util.errorDisplay("Activity can't be zero", 
                                        "Recovery Coefficients")
            raise errors.IOError("Activity can't be zero")

        if volumeSyringe <= 0:
            slicer.util.errorDisplay("The volue in syringe can't be zero", 
                                        "Recovery Coefficients")
            raise errors.IOError("The volue in syringe can't be zero")
        
        ActivityCorrected = self.correctActivity(imageNode, 
                                                 activity,
                                                 duration, 
                                                 widget)

        self.actConcentration = ActivityCorrected / volumeSyringe
        self.acquisitionDuration = duration

    def clean(self):
        self.CTCalibration = {}
        self.SPECTSensitivity = {}
        self.SPECTRecovery = {}
        self.cameraFactor = 1
        self.actConcentration = 1
        self.acquisitionDuration = 1
        self.resampFac = 1
        self.sensitivityResults = {}
        self.segmentExport = {}
        nodes = slicer.util.getNodesByClass(Attributes().vtkMRMLScalarVolumeNode)
        for node in nodes:
            name = node.GetName()
            if "Resampled" in name :
                slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass(Attributes().vtkMRMLTableNode)
        for node in nodes:
            slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass(Attributes().vtkMRMLSegmentationNode)
        for node in nodes:
            slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass(Attributes().vtkMRMLPlotSeriesNode)
        for node in nodes:
            slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass(Attributes().vtkMRMLPlotChartNode)
        for node in nodes:
            slicer.mrmlScene.RemoveNode(node)

    
    def exportCalibration(self, node):
        # --------------------------------------------------------------------------------------
        nodeSensitivity = node.data
        duration = float(nodeSensitivity.GetAttribute(Attributes().acquisitionDuration))
        restTime = float(
            vtkmrmlutils.getItemDataNodeAttributeValue(node.nodeID, "Decay Time (sec)")
        )
        try:
            tankVolume = float(
                vtkmrmlutils.getItemDataNodeAttributeValue(node.nodeID, "Tank Volume (cm3)")
            )
        except:
            tankVolume = "Not entered"

        center = nodeSensitivity.GetAttribute(Attributes().institutionName)
        isotopeText = nodeSensitivity.GetAttribute(Attributes().isotope)

        for mtdName, mtdValue in self.segmentExport.items():
            if mtdValue:
                sensitivityMethod = mtdName
                break

        Sensitivity = self.sensitivityResults[sensitivityMethod]

        sensitivityJSON = config.readSPECTSensitivity()
        if not center in sensitivityJSON:
            sensitivityJSON[center] = {}

        if not isotopeText in sensitivityJSON[center]:
            sensitivityJSON[center][isotopeText] = {}

        date = nodeSensitivity.GetAttribute(Attributes().acquisition)
        sensitivityJSON[center][isotopeText][date] = {
            "Volume": tankVolume,  # Tank Volume
            "Sensitivity": f"{Sensitivity:.4f}",
            "Units": "counts/MBqs",
            "Time": duration,
            "Decay Time": restTime,
            "Method": sensitivityMethod,
        }
        config.writeSPECTSensitivity(sensitivityJSON)
        print(f"Export Sensitivity {sensitivityMethod} Successful")

    def exportRecoveryCoefficients(self, node):
        # --------------------------------------------------------------------------------------
        nodeRC = node.data
        duration = float(nodeRC.GetAttribute(Attributes().acquisitionDuration))
        restTime = float(
            vtkmrmlutils.getItemDataNodeAttributeValue(node.nodeID, "Decay Time (sec)")
        )
        sensitivity = float(
            vtkmrmlutils.getItemDataNodeAttributeValue(
                node.nodeID, "Sensitivity SPECT (counts/MBqs)"
            )
        )
        center = nodeRC.GetAttribute(Attributes().institutionName)
        isotopeText = nodeRC.GetAttribute(Attributes().isotope)

        RCfunction = vtkmrmlutils.getItemDataNodeAttributeValue(
            node.nodeID, "Fitting function"
        )

        volumes = vtkmrmlutils.getItemDataNodeAttributeValue(
            node.nodeID, "volumens (cm3)"
        )
        volumes = self.getListfromAttribute(volumes)
        volumesOnImage = vtkmrmlutils.getItemDataNodeAttributeValue(
            node.nodeID, "volume on image"
        )
        recoveryCoff = vtkmrmlutils.getItemDataNodeAttributeValue(
            node.nodeID, "Recovery Coefficients"
        )

        recoveryCoeffJSON = config.readRecoveryCoefficients()
        if not center in recoveryCoeffJSON:
            recoveryCoeffJSON[center] = {}

        if not isotopeText in recoveryCoeffJSON[center]:
            recoveryCoeffJSON[center][isotopeText] = {}

        date = nodeRC.GetAttribute(Attributes().acquisition)
        recoveryCoeffJSON[center][isotopeText][date] = {
            "Volume": str(sorted([float(vol) for vol in volumes])),
            "Sensitivity": sensitivity,
            "Units": "counts/MBqs",
            "Time": duration,
            "Decay Time": restTime,
            "Fitting function": RCfunction,
            "Volume on image": volumesOnImage,
            "Recovery Coefficients": recoveryCoff,
        }
        config.writeRecoveryCoefficients(recoveryCoeffJSON)
        print(f"Export Recovery Coefficients Successful")


#######################
#                     #
# CalibrationTest     #
#                     #
#######################


class CalibrationTest(ScriptedLoadableModuleTest):
    """
    This is the test case for your scripted module.
    Uses ScriptedLoadableModuleTest base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def setUp(self):
        """Do whatever is needed to reset the state - typically a scene clear will be enough."""
        slicer.mrmlScene.Clear(0)
        slicer.app.processEvents()

    def runTest(self):
        """Run as few or as many tests as needed here."""
        self.setUp()
        self.test_Calibration()

    def test_Calibration(self):
        """Ideally you should have several levels of tests.  At the lowest level
        tests should exercise the functionality of the logic with different inputs
        (both valid and invalid).  At higher levels your tests should emulate the
        way the user would interact with your code and confirm that it still works
        the way you intended.
        One of the most important features of the tests is that it should alert other
        developers when their changes will have an impact on the behavior of your
        module.  For example, if a developer removes a feature that you depend on,
        your test should break so they know that the feature is needed.
        """

        self.delayDisplay("Starting the test")
        slicer.app.aboutToQuit.connect(self.setUp)

        # Get/create input data

        # Test the module logic
        logic = CalibrationLogic()

        # Test algorithm

        self.delayDisplay("Test passed")
