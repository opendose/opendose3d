class Error(Exception):

    def __init__(self, message: str):
        self.message = message
        super().__init__()


class ConventionError(Error):
    pass


class DICOMError(Error):
    pass


class IOError(Error):
    pass


class OSError(Error):
    pass


class XMLError(Error):
    pass


class FileError(Error):
    pass


class NetworkError(Error):
    pass


class ConfigError(Error):
    pass

class FileNotFoundError(Error):
    pass
