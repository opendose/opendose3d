from Logic import vtkmrmlutils
from Logic.attributes import Attributes
from Logic.constants import Constants
from Logic.errors import ConventionError
from Logic.nodes import Node


import re
from datetime import datetime, timedelta


class NodeMRB(Node):

    def __init__(self, nodeID):
        super().__init__(nodeID)

    def complete(self) -> bool:
        """
        Check if all necessary attributes are set, as it should be if it is MRB
        """
        try:
            modality = self.getModality()
            if not modality:
                return False
        except:
            return False

        if modality in ["PT", "NM", "CT"]:
            attributes = []
            attributes.append(self.getInstanceUIDs())
            attributes.append(self.getStudyInstanceUID())
            attributes.append(self.getSeriesInstanceUID())
            attributes.append(self.getPatientID())
            attributes.append(self.getPatientName())
            attributes.append(self.getStudyCreation())
            attributes.append(self.getAcquisition())
            if modality != "CT":
                attributes.append(self.getTimeStamp())
                attributes.append(self.getAcquisitionDuration())

            for attribute in attributes:
                if attribute is None or attribute == "None" or not attribute:
                    return False

        return True

    def getInstanceUIDs(self) -> str:
        instanceUIDs = vtkmrmlutils.getItemDataNodeAttributeValue(
            self.nodeID, Attributes().instanceUIDs
        )
        return instanceUIDs

    def getStudyInstanceUID(self) -> str:
        studyInstanceUID = vtkmrmlutils.getItemDataNodeAttributeValue(
            self.nodeID, Attributes().studyInstanceUID
        )
        return studyInstanceUID

    def getSeriesInstanceUID(self) -> str:
        seriesInstanceUID = vtkmrmlutils.getItemDataNodeAttributeValue(
            self.nodeID, Attributes().seriesInstanceUID
        )
        return seriesInstanceUID

    def getInstitutionName(self) -> str:
        institutionName = vtkmrmlutils.getItemDataNodeAttributeValue(
            self.nodeID, Attributes().institutionName
        )
        return institutionName

    def getPatientID(self) -> str:
        patientID = vtkmrmlutils.getItemDataNodeAttributeValue(
            self.nodeID, Attributes().patientID
        )
        return patientID

    def getPatientName(self) -> str:
        patientName = vtkmrmlutils.getItemDataNodeAttributeValue(
            self.nodeID, Attributes().patientName
        )
        return patientName

    def getStudyCreation(self) -> str:
        studyCreation = vtkmrmlutils.getItemDataNodeAttributeValue(
            self.nodeID, Attributes().studyCreation
        )
        return studyCreation

    def getModality(self) -> str:
        modality = vtkmrmlutils.getItemDataNodeAttributeValue(
            self.nodeID, Attributes().modality
        )
        if not modality:
            modality = self.extractModalityFromName()
        return modality

    def getAcquisition(self) -> str:
        acquisition = vtkmrmlutils.getItemDataNodeAttributeValue(
            self.nodeID, Attributes().acquisition
        )
        return acquisition

    def getInjectionTime(self) -> str:
        injectionTime = vtkmrmlutils.getItemDataNodeAttributeValue(
            self.nodeID, Attributes().injectionTime
        )
        return injectionTime

    def getRadiopharmaceutical(self) -> str:
        Radiopharmaceutical = vtkmrmlutils.getItemDataNodeAttributeValue(
            self.nodeID, Attributes().radiopharmaceutical
        )
        if Radiopharmaceutical == "None":
            Radiopharmaceutical = ""
        return Radiopharmaceutical

    def getIsotope(self) -> str:
        Isotope = vtkmrmlutils.getItemDataNodeAttributeValue(
            self.nodeID, Attributes().isotope
        )
        if Isotope == "None":
            Isotope = ""
        return Isotope

    def setRadiopharmaceutical(self, radiopharmaceutical: str):
        vtkmrmlutils.setItemDataNodeAttribute(
            self.nodeID, Attributes().radiopharmaceutical, radiopharmaceutical
        )

    def setIsotope(self, isotope: str):
        vtkmrmlutils.setItemDataNodeAttribute(
            self.nodeID, Attributes().isotope, isotope
        )

    def getInjectedActivity(self) -> float:
        InjectedActivity = vtkmrmlutils.getItemDataNodeAttributeValue(
            self.nodeID, Attributes().injectedActivity
        )
        try:
            return float(InjectedActivity)
        except:
            return -1

    def setInjectedActivity(self, activity: float):
        vtkmrmlutils.setItemDataNodeAttribute(
            self.nodeID, Attributes().injectedActivity, str(activity)
        )

    def getAcquisitionDuration(self) -> str:
        acquisitionDuration = vtkmrmlutils.getItemDataNodeAttributeValue(
            self.nodeID, Attributes().acquisitionDuration
        )
        return acquisitionDuration

    def getCalibration(self) -> str:
        calibration = vtkmrmlutils.getItemDataNodeAttributeValue(
            self.nodeID, Attributes().calibrationCT
        )
        return calibration

    def getCalibrationDuration(self) -> str:
        calibrationDuration = vtkmrmlutils.getItemDataNodeAttributeValue(
            self.nodeID, Attributes().calibrationDuration
        )
        return calibrationDuration

    def getSensitivity(self) -> str:
        sensitivity = vtkmrmlutils.getItemDataNodeAttributeValue(
            self.nodeID, Attributes().sensitivity
        )
        return sensitivity

    def getSensitivityUnits(self) -> str:
        sensitivityUnits = vtkmrmlutils.getItemDataNodeAttributeValue(
            self.nodeID, Attributes().sensitivityUnits
        )
        return sensitivityUnits

    def getTimeStamp(self) -> str:
        timeStamp = vtkmrmlutils.getItemDataNodeAttributeValue(
            self.nodeID, Attributes().timeStamp
        )
        if not timeStamp:  # Create timeStamp from current time and injection time
            injectionTime = (
                self.getInjectionTime()
            )  # This must be present in DICOM header
            if not injectionTime:  # Try to use timeStamp from name
                try:
                    _, timeStamp = self.extractTime()
                    acquisition = datetime.strptime(
                        self.getAcquisition(), Constants().timeFormat
                    )
                    tdelta = timedelta(hours=float(timeStamp))
                    injection = acquisition - tdelta
                    injectionTime = injection.strftime(Constants().timeFormat)
                except:
                    self.logger.error(
                        f"No injection time and no timestamp in name for node {self.name}"
                    )
                    return "0"

            self.setTimeStamp(injectionTime)
            timeStamp = vtkmrmlutils.getItemDataNodeAttributeValue(
                self.nodeID, Attributes().timeStamp
            )
        return timeStamp

    def setTimeStamp(self, injectionTime: str):
        hours = 0
        acquisitionTimeStr = self.getAcquisition()  # This must be present
        if acquisitionTimeStr:
            vtkmrmlutils.setItemDataNodeAttribute(
                self.nodeID, Attributes().injectionTime, injectionTime
            )
            acquisitionTime = datetime.strptime(
                acquisitionTimeStr, Constants().timeFormat
            )
            injectionTime = datetime.strptime(injectionTime, Constants().timeFormat)
            difference = acquisitionTime - injectionTime
            hours = difference.total_seconds() / Constants().units["hour"]
            vtkmrmlutils.setItemDataNodeAttribute(
                self.nodeID, Attributes().timeStamp, hours
            )
        else:
            self.logger.warning(f"No acquisition time for node {self.name}")

    def rename(self, hours=-1):
        # intermediate calculation data, created by us, so no need to check
        if (
            "tabl" in self.name.lower()
            or "chrt" in self.name.lower()
            or "plot" in self.name.lower()
            or "ders" in self.name.lower()
            or "trnf" in self.name.lower()
            or "actm" in self.name.lower()
            or "ctrs" in self.name.lower()
            or "segm" in self.name.lower()
            or ("adrm" in self.name.lower() and "monte" not in self.name.lower())
        ):
            return

        if hours < 0:
            days, hours = self.extractTime()
        else:
            days = int(hours / 24)

        modality = self.getModality()
        if modality in ["NM"]:
            newName = f"J{days}:ACSC SPECT {hours}HR"
        elif modality in ["PT"]:
            newName = f"J{days}:ACSC PET {hours}HR"
        elif modality in ["CT"]:
            newName = f"J{days}:CTCT CT {hours}HR"
        # necessary for MonteCarlo manual input
        elif "monte" in self.name.lower():
            newName = f"J{days}:ADRM Monte Carlo {hours}HR"
        else:
            self.logger.info(
                f"Failed to rename node with name '{self.name}', since naming convention not met regarding modality. Example: 'CTCT 48h' or 'J9 ACSC'"
            )
            raise ConventionError(
                f"Failed to rename node with name '{self.name}', since naming convention not met regarding modality. Example: 'CTCT 48h' or 'J9 ACSC'"
            )

        # set attribute
        shNode = vtkmrmlutils.getSubjectHierarchyNode()
        shNode.SetItemName(self.nodeID, newName)

        # update instance attribute
        self.name = newName

    def extractTime(self):
        """
        Extracts days and hour by naming conventions of either specifying days or hours
        """
        # NOTE currently just extracts the hours, even if days would be specified as well

        # try to extract by hours convention
        days, hours = self.extractHours()

        # try to extract by days convention
        if hours is None:
            days, hours = self.extractDays()

        # if not conform to naming convention
        if days is None or hours is None:
            self.logger.info(
                f"Failed to rename node with name: {self.name}, since no convention met regarding time"
            )
            raise ConventionError(
                "Please rename files with specifying either days or hours. Example: 'CTCT 48h' or 'J9 ACSC'"
            )
        else:
            return int(days), int(hours)

    def extractHours(self):
        """
        Extracts hours from volume node name
        """
        # NOTE this only workes if this naming convention is abided, otherwise this will raise an error
        # NOTE conventions are: "(\d+)hr{0,1}+" and "hr{0,1}+(\d+)", e.g. "14hr" or "h14"
        p = re.compile(r"(\d+)hr?")
        times = p.findall(self.name.lower())
        expr = "(\d+)hr?|hr?+(\d+)"

        if len(times) == 0:
            p = re.compile(r"hr?(\d+)")
            times = p.findall(self.name.lower())

            if len(times) == 0:
                self.logger.error(
                    f"No time reference found in name '{self.name.lower()}' using the regex {expr}"
                )
                return None, None

        if len(times) == 1:
            hours = times[0]
            days = int(times[0]) / 24
            return int(days), int(hours)

        if len(times) > 1:
            self.logger.error(
                f"Multiple time reference found in name '{self.name.lower()}' using the regex {expr}"
            )

    def extractDays(self):
        """
        Extracts days from volume naming
        """
        # NOTE this only workes if this naming convention is abided, otherwise this will raise an error
        # NOTE conventions are: "j([0-9]+)", e.g. "j7"
        p = re.compile(r"j(\d+)")
        times = p.findall(self.name.lower())
        expr = "j(\d+)"

        if len(times) == 0:
            self.logger.error(
                f"No time reference found in name '{self.name.lower()}' using the regex {expr}"
            )
            return None, None

        if len(times) == 1:
            if int(times[0]) > 9:
                self.logger.error(
                    "Invalid input, name suggests that the data is over 9 days old"
                )
                return None, None

            days = int(times[0])
            hours = int(days) * 24
            return int(days), int(hours)

        if len(times) > 1:
            self.logger.error(
                f"Multiple time reference found in name '{self.name.lower()}' using the regex {expr}"
            )

    def extractModalityFromName(self):
        if "acsc" in self.name.lower():
            modality = "NM"
        elif "pet" in self.name.lower() or "tep" in self.name.lower():
            modality = "PT"
        elif "ctct" in self.name.lower() or ": ct" in self.name.lower():
            modality = "CT"
        elif "ctrs" in self.name.lower():
            modality = "CTRS"
        elif "adrm" in self.name.lower() or "dose" in self.name.lower():
            modality = "RTDOSE"
        elif "trnf" in self.name.lower():
            modality = "REG"
        elif "segm" in self.name.lower():
            modality = "RTSTRUCT"
        elif (
            "actm" in self.name.lower()
            or "ders" in self.name.lower()
        ):
            modality = "RWV"
        else:
            self.logger.warning(
                f"Could not retrieve modality for node with id '{self.nodeID}' from name '{self.name}'"
            )
            raise ConventionError(
                f"Could not retrieve modality for node with id '{self.nodeID}' from name '{self.name}'"
            )

        self.logger.debug(
            f"Extracted modality '{modality}' from node with id '{self.nodeID}' and name '{self.name}'"
        )
        return modality