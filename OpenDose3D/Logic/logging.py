import logging

import Logic.config as config
from Logic.errors import ConfigError
from Logic import sentry
from sentry_sdk.integrations.logging import ignore_logger

CRITICAL = 50
FATAL = CRITICAL
ERROR = 40
WARNING = 30
WARN = WARNING
INFO = 20
DEBUG = 10
NOTSET = 0

# getLogger wraps original logging.getLogger, so that we can create a env-aware logger (important for sentry)


def getLogger(name):
    env = config.read('default', 'env')
    if not env:
        config.init()
        env = config.read('default', 'env')

    if env == 'prod':
        logger = logging.getLogger(name)
    elif env == 'test':
        ignore_logger(name)
        logger = logging.getLogger(name)
    else:
        raise ConfigError(f'No env with value: {env}')

    return logger
