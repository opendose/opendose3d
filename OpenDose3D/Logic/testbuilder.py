import qt
import slicer
from pathlib import Path

import DICOMLib.DICOMUtils as DICOMUtils
from DICOMScalarVolumePlugin import DICOMScalarVolumePluginClass

from Logic.attributes import Attributes
from Logic.logging import getLogger
from Logic.testutils import AttributeValues, NodeValues, DicomValues, SegmentationValues
from Logic.vtkmrmlutils import *
from Logic.errors import Error, FileNotFoundError
from Logic.utils import getScriptPath
from Logic.nodes import Node

DATABASE_INITIALIZED = False


def setupNonDICOM(name='J2:CTCT 48h'):
    """
    Setup up a volume node for testing
    """
    attributes = Attributes()
    attributeValues = AttributeValues()
    nodeValues = NodeValues()

    # create hierarchy
    shNode = getSubjectHierarchyNode()
    subjectID = shNode.CreateSubjectItem(shNode.GetSceneItemID(), 'Subject')
    studyID = shNode.CreateStudyItem(subjectID, 'Study')

    # create nodes
    defaultVolumeNode = slicer.mrmlScene.AddNewNodeByClass(
        'vtkMRMLScalarVolumeNode', nodeValues.acsc_48h)
    defaultItemID = shNode.GetItemByDataNode(defaultVolumeNode)

    volumeNode = slicer.mrmlScene.AddNewNodeByClass(
        'vtkMRMLScalarVolumeNode', name)
    itemID = shNode.GetItemByDataNode(volumeNode)

    shNode.SetItemParent(defaultItemID, studyID)
    shNode.SetItemParent(itemID, studyID)

    # attributes
    setItemDataNodeAttribute(
        defaultItemID, attributes.acquisition, attributeValues.acquisition)
    setItemDataNodeAttribute(
        defaultItemID, attributes.acquisitionDuration, attributeValues.acquisitionDuration)
    setItemDataNodeAttribute(
        defaultItemID, attributes.modality, attributeValues.modality_acsc)

    setItemAttribute(
        defaultItemID, attributes.acquisition, attributeValues.acquisition)

    return defaultItemID, itemID

def getItemReferent(itemIDs, name='1: ACSC SPECT-CT 1HR'):
    for itemID in itemIDs:
        if getItemDataNodeName(itemID) == name:
            break
    return itemID

def setupDICOM():
    '''
    Setup a complex environment based on the loaded dicom files
    '''
    logger = getLogger('OpenDose3D.testbuilder')
    dicomValues = DicomValues()

    try:
        openDICOMDatabase()
    except:
        raise

    try:
        slicer.modules.dicomPlugins
    except:
        slicer.modules.dicomPlugins = {}

    if slicer.modules.dicomPlugins:
        oldPluginList = slicer.modules.dicomPlugins
        slicer.modules.dicomPlugins = {}
    else:
        oldPluginList = {}

    slicer.modules.dicomPlugins['DICOMScalarVolumePlugin'] = DICOMScalarVolumePluginClass

    try:
        _ = DICOMUtils.loadPatientByName(dicomValues.patientName)
    except:
        logger.error('Could not load dicom files from database')
        raise Error('Unable to load series from database')

    studyID = getStudyIDs()[0]
    itemIDs = getAllFolderChildren(studyID)

    slicer.modules.dicomPlugins = oldPluginList

    return itemIDs


def downloadDICOMdatabase():
    import requests
    logger = getLogger('OpenDose3D.testbuilder')

    fileNames = {'CT': ['CT_1h.dcm', 'CT_4h.dcm', 'CT_24h.dcm', 'CT_48h.dcm', 'CT_96h.dcm'],
                 'SPECT': ['SPECT_1h.dcm', 'SPECT_4h.dcm', 'SPECT_24h.dcm', 'SPECT_48h.dcm', 'SPECT_96h.dcm'],
                 'SEGM': ['segmentation-1HR.seg.nrrd']
                 }
    uris = {'CT': 'https://gitlab.com/opendose/opendose3d/-/raw/develop/OpenDose3D/Resources/DICOM/A_Cycle3-6848MBq-9h57/CT/',
            'SPECT': 'https://gitlab.com/opendose/opendose3d/-/raw/develop/OpenDose3D/Resources/DICOM/A_Cycle3-6848MBq-9h57/',
            'SEGM': 'https://gitlab.com/opendose/opendose3d/-/raw/develop/OpenDose3D/Resources/DICOM/'
            }

    resourcesDirectory = getScriptPath() / "Resources" / "DICOM"
    resourcesDirectory.mkdir(exist_ok=True)
    savePaths = {'CT': resourcesDirectory / 'A_Cycle3-6848MBq-9h57' / 'CT',
                 'SPECT': resourcesDirectory / 'A_Cycle3-6848MBq-9h57',
                 'SEGM': resourcesDirectory
                 }
    savePaths['SPECT'].mkdir(exist_ok=True)
    savePaths['CT'].mkdir(exist_ok=True)
    for nodetype in fileNames:
        for fileName in fileNames[nodetype]:
            fileToRetrieve = uris[nodetype] + fileName
            fileToSave = savePaths[nodetype] / fileName
            if not fileToSave.exists():
                logger.info(f"Downloading {fileToRetrieve}")
                r = requests.get(fileToRetrieve)
                if r.status_code == 200:
                    with fileToSave.open('wb') as outfile:
                        outfile.write(r.content)
                else:
                    logger.info("Download failed")


def initDICOMDatabase():
    '''
    Writes the data from folder <./OpenDose3D/Resources/Dicom> to a dicom database
    '''
    global DATABASE_INITIALIZED
    logger = getLogger('OpenDose3D.testbuilder')
    dicomValues = DicomValues()

    try:
        openDICOMDatabase()
    except:
        raise

    if DATABASE_INITIALIZED:
        logger.info("Database is already initialized")
        return

    # download e data files
    downloadDICOMdatabase()

    # see https://github.com/Slicer/Slicer/blob/master/Modules/Scripted/DICOMLib/DICOMUtils.py#L240
    logger.error('DICOM test data dir: ' + dicomValues.folderPath)
    success = DICOMUtils.importDicom(dicomValues.folderPath)
    if not success:
        logger.error('Could not import DICOM test data from directory: {}'.format(
            dicomValues.folderPath))

    # NOTE currently assumes that only 1 subject -> 1 study -> 1 series is loaded
    patientList = slicer.dicomDatabase.patients()
    studyList = slicer.dicomDatabase.studiesForPatient(patientList[0])
    seriesList = slicer.dicomDatabase.seriesForStudy(studyList[0])

    logger.error('Successfully loaded patient {} with StudyInstanceUID: {} and SeriesInstanceUID: {}'.format(
        patientList[0], studyList[0], seriesList[0]))

    DATABASE_INITIALIZED = True


def openDICOMDatabase():
    '''
    Opens a connection to the dicom database
    '''
    # NOTE this method uses a hardcoded dicom directory
    # see: https://github.com/Slicer/Slicer/blob/master/Modules/Scripted/DICOMLib/DICOMUtils.py#L60
    logger = getLogger('OpenDose3D.testbuilder')

    if slicer.dicomDatabase.isOpen:
        logger.info('Database is already opened')
        return

    settings = qt.QSettings()
    if not settings.contains('DatabaseDirectory'):
        DB_Directory = Path.home() / 'SlicerDICOMDatabase'
        qt.QSettings().setValue('DatabaseDirectory', str(DB_Directory))

    try:
        databaseDirectory = Path(settings.value('DatabaseDirectory'))
        databaseDirectory.mkdir(exist_ok=True)
    except FileNotFoundError as e:
        logger.error("The database directory cannot be created \n{}".format(e))

    try:
        databaseFilePath = databaseDirectory / 'ctkDICOM.sql'
        databaseFilePath.touch(exist_ok=True)
    except Exception as e:
        logger.error(
            "The database directory cannot be created \n{}: {}".format(type(e), e))

    logger.info(f"Database created in {databaseFilePath}")
    if not (databaseDirectory.is_dir() and databaseFilePath.is_file()):
        logger.error(
            f"The database file path '{databaseFilePath}' cannot be opened, please adjust the read/write priviliges")
        raise OSError('Database has invalid read/write privilidges')

    slicer.dicomDatabase.openDatabase(str(databaseFilePath))
    if not slicer.dicomDatabase.isOpen:
        logger.error('Unable to reconnect database')
        raise Error('Unable to connect to database')


def loadSegmentation():
    '''
    Loads the predefined segmentation for the tests
    '''
    # NOTE this method uses a hardcoded segmentation file defined exclusively for testing
    segmentationValues = SegmentationValues()

    seg = slicer.util.loadSegmentation(segmentationValues.filePath)
    seg.CreateDefaultDisplayNodes()
    seg.CreateClosedSurfaceRepresentation()
    segID = getItemID(seg)
    setItemName(segID, "J0:SEGM 1HR")
    return Node.new(segID)
