from Logic.utils import getScriptPath
from singleton_decorator import singleton

@singleton
class TestValues():

    def __init__(self):
        self.attribute = AttributeValues()
        self.node = NodeValues()
        self.dicom = DicomValues()
        self.dataNode = DataNodeValues()
        self.SegmentationValues = SegmentationValues()

@singleton
class AttributeValues():

    def __init__(self):
        self.acquisition = '1970-01-01 00:00:00'
        self.acquisitionDuration = '1800'
        self.modality_ct = 'CT'
        self.modality_monteCarlo = 'Monte'
        self.modality_acsc = 'NM'
        self.extras_fix = '0'
        self.extras_intercept = '42'
        self.extras_slope = '42'


@singleton
class NodeValues():
    # custom node names for new test nodes
    def __init__(self):
        self.ct_48h = 'J2:CTCT 48h'
        self.ct_96h = 'J2:CTCT 96h'
        self.acsc_1h = 'J0:ACSC 1h'
        self.acsc_48h = 'J2:ACSC 48h'
        self.acsc_96h = 'J2:ACSC 96h'
        self.monteCarlo_48h = 'J2:DOSE monte 48h'

        self.ct_48h_renamed = 'J2:CTCT CT 48HR'
        self.ct_96h_renamed = 'J2:CTCT CT 96HR'
        self.acsc_1h_renamed = 'J0:ACSC SPECT 1HR'
        self.acsc_48h_renamed = 'J2:ACSC SPECT 48HR'
        self.acsc_96h_renamed = 'J2:ACSC SPECT 96HR'
        self.monteCarlo_48h_renamed = 'J2:DOSE MonteCarlo 48HR'


@singleton
class DicomValues():
    # aligned with the data from folder <./OpenDose3D/Resources/Dicom>
    def __init__(self):
        self.folderPath = str(getScriptPath() / "Resources" /
                              "DICOM" / "A_Cycle3-6848MBq-9h57")
        self.instanceUIDs = '1.3.6.1.4.1.33868.20191002120418.406358'
        self.studyInstanceUID = '1.2.752.37.5.626934496.20171125.26473.4.1934'
        self.patientID = '121416'
        self.patientName = 'Patient1^Cycle3'
        self.acquisition = '2016-12-14 12:02:51'
        self.injectionTime = '2016-12-14 11:02:51'
        self.injectedActivity = '6848'
        self.acquisitionDuration = '1800.0'
        self.modality = 'NM'
        self.extras_fix = '1'
        self.extras_intercept = '0'
        self.extras_slope = '1'
        self.radiopharmaceutical = 'Lu-177'


@singleton
class DataNodeValues():
    # aligned with the data from folder <./OpenDose3D/Resources/Dicom>
    def __init__(self):
        self.name_acsc1h = '1: ACSC SPECT-CT 1HR'
        self.name_acsc48h = '1: ACSC SPECT-CT 48HR'
        self.name_acsc96h = '1: ACSC SPECT-CT 96HR'
        self.name_ct48h = '1: CT SPECT-CT 48HR'
        self.attributeNames = ('DICOM.instanceUIDs',)
        self.instanceUIDs_hashHex = 'f09dde25f717789ecdde80d1d6a3d1d848c18743d6b106e0c7e1f1c762f0c9da'


@singleton
class SegmentationValues():
    # aligned with the data from file <./OpenDose3D/Resources/DICOM/segmentation-1HR.seg.nrrd>
    def __init__(self):
        self.filePath = str(getScriptPath() / "Resources" /
                            "DICOM" / "segmentation-1HR.seg.nrrd")
