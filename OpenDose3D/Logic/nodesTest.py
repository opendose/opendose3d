from datetime import datetime

import slicer
from slicer.ScriptedLoadableModule import ScriptedLoadableModuleTest

from Logic.attributes import Attributes
from Logic.testbuilder import setupDICOM, setupNonDICOM, initDICOMDatabase
from Logic.testutils import TestValues, DicomValues
from Logic.nodes import Node
from Logic.vtkmrmlutils import *
from Logic.logging import getLogger, INFO
from Logic.utils import whoami
from Logic.errors import ConventionError
from Logic.NodeDICOM import NodeDICOM
from Logic.NodeMRB import NodeMRB


class nodesTest(ScriptedLoadableModuleTest):
    """
    This is the test case for the nodes related utils.
    Uses ScriptedLoadableModuleTest base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def setUp(self):
        self.tearDown()
        self.logger = getLogger('OpenDose3D.nodesTest')
        self.logger.setLevel(INFO)
        self.testValues = TestValues()

    def tearDown(self):
        slicer.mrmlScene.Clear(0)
        slicer.app.processEvents()

    def runTest(self):
        self.setUp()
        self.logger.info('\n\n***** nodes Module Testing *****\n')

        # since first test, setup database
        try:
            initDICOMDatabase()
        except:
            raise

        self.isArtefact_positive()
        self.isDICOM_positive()
        self.setAttributes_positive()
        self.setAttributes_positive()
        self.complete_positive()
        self.complete_negative()
        self.new_DICOM_positive()
        self.new_MRB_positive()
        self.loadDICOM_positive()
        self.rename_positive()
        self.rename_negative()
        self.extractHours_positive()
        self.extractDays_positive()
        self.extractTime_positive()
        self.extractTime_negative()
        self.extractModalityFromName_positive()
        self.extractModalityFromName_negative()
        self.compareAttributes_positive()
        self.compareAttributes_negative()
        self.getStudyInstanceUID_positive()
        self.getSeriesInstanceUID_positive()
        self.getPatientID_positive()
        self.getAcquisition_positive()
        self.getAcquisitionDuration_positive()
        self.getModality_positive()
        self.getInitialTime_positive()

        self.tearDown()
        self.logger.info('\n******** Nodes Tests passed ************\n')

    def isArtefact_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemID, _ = setupNonDICOM()
        setItemName(itemID, 'tabl')

        # act
        nodeArtefact = Node(itemID)
        ok = nodeArtefact.isArtefact()

        # assert
        self.assertTrue(ok)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def isDICOM_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()

        # act
        # assert
        for itemID in itemIDs:
            nodeDICOM = Node.new(itemID)
            ok = nodeDICOM.isDICOM()
            if nodeDICOM.getModality() in ['CT', 'PT', 'NM']:
                self.assertTrue(ok)
            else:
                self.assertFalse(ok)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def setAttributes_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()

        # act
        node = Node.new(itemIDs[0])
        node.setAttributes(DicomValues().radiopharmaceutical, DicomValues().injectedActivity)

        # assert
        self.assertIsNotNone(node.getStudyInstanceUID())
        self.assertIsNotNone(node.getSeriesInstanceUID())
        self.assertIsNotNone(node.getPatientID())
        self.assertIsNotNone(node.getStudyCreation())
        self.assertIsNotNone(node.getModality())
        self.assertIsNotNone(node.getAcquisition())
        self.assertIsNotNone(node.getAcquisitionDuration())

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def setAttributes_negative(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemID, _ = setupNonDICOM()

        # act & assert
        with self.assertRaises(ConventionError):
            _ = Node.new(itemID)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def complete_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()
        dicomData = DicomValues()
        ctset = False
        nmset = False
        for itemID in itemIDs:
            nodeDICOM = Node.new(itemID, dicomData.radiopharmaceutical, dicomData.injectedActivity)
            if not ctset and nodeDICOM.getModality() == "CT":
                nodeCT = nodeDICOM
                ctset = True
            if not nmset and nodeDICOM.getModality() == "NM":
                nodeSPECT = nodeDICOM
                nmset = True
            if nmset and ctset:
                break

        # act
        _, _, _, injectionTime = Node.getInitialTime()
        nodeSPECT.setTimeStamp(injectionTime)
        radiopharmaceutical = nodeSPECT.getIsotope()
        nodeMRBCT = NodeMRB(nodeCT.nodeID)
        nodeMRBSPECT = NodeMRB(nodeSPECT.nodeID)
        okCT = nodeMRBCT.complete()
        okNM = nodeMRBSPECT.complete()

        # assert
        self.assertEqual(radiopharmaceutical, dicomData.radiopharmaceutical)
        self.assertTrue(ctset)
        self.assertTrue(okCT)
        self.assertTrue(nmset)
        self.assertTrue(okNM)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def complete_negative(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()
        nodeMRB = NodeMRB(itemIDs[0])

        # act
        ok = nodeMRB.complete()

        # assert
        self.assertNotEqual(ok, True)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def new_DICOM_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()

        # act
        for itemID in itemIDs:
            nodeDICOM = Node.new(itemID)
            if nodeDICOM.getModality() in ['CT', 'PT', 'NM']:
                self.assertTrue(isinstance(nodeDICOM, NodeDICOM))
            else:
                self.assertFalse(isinstance(nodeDICOM, NodeDICOM))

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def new_MRB_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()
        nodeDICOM = Node.new(itemIDs[0])
        nodeDICOM.setAttributes(DicomValues().radiopharmaceutical, DicomValues().injectedActivity)

        # act
        nodeMRB = Node.new(nodeDICOM.nodeID)

        # assert
        self.assertTrue(isinstance(nodeMRB, NodeMRB))

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def loadDICOM_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()

        # act
        node = Node.new(itemIDs[0])
        dataset = node.loadDICOM()

        # assert
        # pick an arbitrary attribute
        self.assertIsNotNone(dataset.AcquisitionDate)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def rename_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()

        # act
        for itemID in itemIDs:
            node = Node.new(itemID)
            if node.getModality() == "NM" and '1HR' in node.name:
                node.name = self.testValues.node.acsc_1h
                node.rename()
                break

        # assert
        self.assertEqual(self.testValues.node.acsc_1h_renamed, node.name)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def rename_negative(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()
        node = Node.new(itemIDs[0])
        node.name = 'some invalid name'

        # act & assert
        with self.assertRaises(ConventionError):
            _, _ = node.rename()

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def extractHours_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()
        node = Node.new(itemIDs[0])
        node.name = '1H'

        # act
        days, hours = node.extractHours()

        # assert
        self.assertEqual(0, days)
        self.assertEqual(1, hours)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def extractDays_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()
        node = Node.new(itemIDs[0])
        node.name = 'J0'

        # act
        days, hours = node.extractDays()

        # assert
        self.assertEqual(0, days)
        self.assertEqual(0, hours)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def extractTime_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()
        node = Node.new(itemIDs[0])
        node.name = self.testValues.node.acsc_1h

        # act
        days, hours = node.extractTime()

        # assert
        self.assertEqual(0, days)
        self.assertEqual(1, hours)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def extractTime_negative(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()
        node = Node.new(itemIDs[0])
        node.name = 'some invalid name'

        # act & assert
        with self.assertRaises(ConventionError):
            _, _ = node.extractTime()

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def extractModalityFromName_positive(self):
        self.logger.info(f'Test {whoami()} started!')

        # arrange
        itemIDs = setupDICOM()
        node = Node.new(itemIDs[0])
        node.name = self.testValues.node.acsc_1h

        # act
        modality = node.extractModalityFromName()

        # assert
        self.assertEqual(modality, self.testValues.attribute.modality_acsc)

        self.logger.info(f'Test {whoami()} passed!')

    def extractModalityFromName_negative(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()
        node = Node.new(itemIDs[0])
        node.name = 'some invalid name'

        # act & assert
        with self.assertRaises(ConventionError):
            _ = node.extractModalityFromName()

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def compareAttributes_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        _ = setupDICOM()
        itemID_0 = getFirstNodeByName(self.testValues.dataNode.name_acsc48h)
        itemID_1 = getFirstNodeByName(self.testValues.dataNode.name_acsc96h)
        node = Node.new(itemID_0)
        reference = Node.new(itemID_1)

        # act
        ok = node.compareAttributes(reference)

        # assert
        self.assertTrue(ok)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def compareAttributes_negative(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        _ = setupDICOM()
        itemID_0 = getFirstNodeByName(
            self.testValues.dataNode.name_acsc48h)
        itemID_1 = getFirstNodeByName(
            self.testValues.dataNode.name_ct48h)
        node = Node.new(itemID_0)
        reference = Node.new(itemID_1)

        # Force different patientID for the test
        node.data.SetAttribute(Attributes().patientID, '12234')

        # act
        ok = node.compareAttributes(reference)

        # assert
        self.assertFalse(ok)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getStudyInstanceUID_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        _ = setupDICOM()
        itemID = getFirstNodeByName(
            self.testValues.dataNode.name_acsc1h)
        node = Node.new(itemID)
        node.setStudyInstanceUID()

        # act
        studyInstanceUID = node.getStudyInstanceUID()

        # assert
        self.assertEqual(
            self.testValues.dicom.studyInstanceUID, studyInstanceUID)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getSeriesInstanceUID_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        _ = setupDICOM()
        itemID = getFirstNodeByName(
            self.testValues.dataNode.name_acsc1h)
        node = Node.new(itemID)
        node.setSeriesInstanceUID()

        # act
        seriesInstanceUID = node.getSeriesInstanceUID()

        # assert
        self.assertIsNotNone(str(seriesInstanceUID))
        self.assertNotEqual("", str(seriesInstanceUID))

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getPatientID_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        _ = setupDICOM()
        itemID = getFirstNodeByName(
            self.testValues.dataNode.name_acsc1h)
        node = Node.new(itemID)
        node.setPatientID()

        # act
        patientID = node.getPatientID()

        # assert
        self.assertEqual(self.testValues.dicom.patientID, str(patientID))

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getModality_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        _ = setupDICOM()
        itemID = getFirstNodeByName(
            self.testValues.dataNode.name_acsc1h)
        node = Node.new(itemID)
        node.setModality()

        # act
        modality = node.getModality()

        # assert
        self.assertEqual(self.testValues.dicom.modality, modality)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getAcquisition_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        _ = setupDICOM()
        itemID = getFirstNodeByName(
            self.testValues.dataNode.name_acsc1h)
        node = Node.new(itemID)
        node.setAcquisition()

        # act
        acquisition = node.getAcquisition()

        # assert
        self.assertEqual(self.testValues.dicom.acquisition, str(acquisition))

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getAcquisitionDuration_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        _ = setupDICOM()
        itemID = getFirstNodeByName(
            self.testValues.dataNode.name_acsc1h)
        node = Node.new(itemID)
        node.setModality()
        node.setAcquisitionDuration()

        # act
        acquisitionDuration = node.getAcquisitionDuration()

        # assert
        self.assertEqual(
            str(self.testValues.dicom.acquisitionDuration), acquisitionDuration)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getInitialTime_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        _ = setupDICOM()
        reference = ('2016-12-14 12:02:51', 1, 6848)

        # act
        timeStamp, hours0, injectedActivity, _ = Node.getInitialTime(6848)
        timeDifference = timeStamp - \
            datetime.strptime(reference[0], "%Y-%m-%d %H:%M:%S")

        # assert
        self.assertEqual(timeDifference.total_seconds(), 0)
        self.assertEqual(hours0, reference[1])
        self.assertEqual(injectedActivity, reference[2])

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')
