# Special packages
import numpy as np
from numpy import trapz, log, exp

# Internal packages
from Logic.logging import getLogger
from Logic.utils import isNumber
from Logic.errors import ConventionError
from Logic.fitutils import (
    MonoExponential,
    BiExponential,
    TriExponential,
    MINIMIZER_METHODS,
    LMFIT_WORKING,
)

PARAMETERS = {
    "mono-exponential": 2,
    "bi-exponential": 3,
    "tri-exponential": 4,

}

def fit_functions(timePoints, zero=False):
    fit_function = {}

    if timePoints == 2:
        if not zero:
            fit_function = {
                "mono-exponential": MonoExponential,
            }
        return fit_function

    if zero:
        fit_function = {
            "bi-exponential": BiExponential,
            "tri-exponential": TriExponential,
        }
    else:
        fit_function = {
            "mono-exponential": MonoExponential,
            "bi-exponential": BiExponential,
            "tri-exponential": TriExponential,
        }

    fit_functions = {}
    for name, funct in fit_function.items():
        nparam = PARAMETERS[name]
        if int(nparam) <= timePoints - 1:
            fit_functions[name] = funct
    return fit_functions

class FitValues:
    def __init__(
        self,
        x: np.ndarray,
        y: np.ndarray,
        fit_type: str,
        lamda: float,
        forceZeroModel= False, 
        method="leastsq",
    ):
        self.x, self.y = np.array(
            [(lx, ly) for lx, ly in zip(x, y) if isNumber(lx) and isNumber(ly)]
        ).T
        self.fit_type = fit_type
        self.logger = getLogger("OpenDose3D.fitting")
        self.ln2 = log(2)
        self.lamda = lamda

        self.fit_functions = fit_functions(len(self.x), forceZeroModel)
        self.fitter = None
        if method not in MINIMIZER_METHODS:
            raise ConventionError("method not valid")

        self.method = method
        self.w_AICc = dict()

    def fit(self):
        "fits y values according to x with lower Bayesian Information Criterion (BIC) function"
        if "single point" in self.fit_type:
            self.fitting_summary = ["Single Time Point"]
            self.model_selection_report = ["No model selection"]
            return
        
        if "auto" in self.fit_type:
            FUNCTIONS = self.fit_functions
        else:
            FUNCTIONS = {}
            FUNCTIONS[self.fit_type] = self.fit_functions[self.fit_type]

        self.fitting_summary = []
       
        minBIC = 1000
        model_dict_list = []
        for d, f in FUNCTIONS.items():
            fitter = f(self.x, self.y, self.lamda, self.method)
            try:
                fitter.minimize()
            except Exception:
                self.fitter = "trapezoid"
                continue

            if not LMFIT_WORKING:
                newY = fitter.evaluate()
                residuals = newY - self.y
                chiSquare = (residuals**2).sum()
                numberOfDataPoints = len(residuals)
                numberOfVariables = len(fitter.getParametersTuple())
                _neg2_log_likel = numberOfDataPoints * np.log(
                    chiSquare / numberOfDataPoints
                )
                BIC = (
                    _neg2_log_likel + np.log(numberOfDataPoints) * numberOfVariables
                )
            else:
                BIC = fitter.res.bic

            if BIC < minBIC:
                minBIC = BIC
                self.fitter = fitter

            model_dict = fitter.model_dict()
            model_dict["equation"] = fitter
            model_dict_list.append(model_dict)

            self.fitting_summary += model_dict["Report"]

        if "auto" in self.fit_type and len(FUNCTIONS) >= 2:
            # perform Model selection   
            w_AICc = self.model_selection(model_dict_list)
            if w_AICc:
                self.fitter = max(w_AICc, key=w_AICc.get, default=None)
                self.w_AICc = w_AICc
            else:
                error_message = (
                    "No model passed the Goodness of Fit, selecting model with BIC."
                    "\nModel uncertainty might not be good."
                )
                self.fitting_summary.append(error_message)
        else:    
            # Manual Selection
            status = "No model selection was performed.\
                    \nThe model was either manually selected or was the only choice."
            self.fitting_summary.append(status)
            self.model_selection_report = []
            report = "Manual Selection"
            self.model_selection_report.append(report) 
            self.fit_type = d

        if self.fitter and not isinstance(self.fitter, str) :
            self.logger.debug(self.fitter.report)

    def __str__(self) -> str:
        return str(self.fitter)
    
    def model_selection(self, model_dict_list: list) -> dict:
        models = []
        self.model_selection_report = []

        # Filtering models based on the Goodness of fit
        for mdl in model_dict_list:
            try:
                CV_test = max(mdl["CV"]) < 50
                corr_list = mdl["correlation"]
                corr_test = max(abs(corr_list)) < 0.8 if len(corr_list) else False
                AICc_check = isinstance(mdl["AICc"], float)
            except:
                continue
            if CV_test and corr_test and AICc_check:
                models.append(mdl)

        # Calculate AICc and weighted AICc
        AICc = {model["equation"]: model["AICc"] for model in models}
        w_AICc = {}

        self.model_selection_report.append("Weighted AICc for selected models:")

        if AICc:
            AICc_min = min(AICc.values())
            delta = [np.exp(-(aicc - AICc_min) / 2) for aicc in AICc.values()]
            delta_sum = sum(delta)
            
            for equation, aicc in AICc.items():
                w_AICc[equation] = np.exp(-(aicc - AICc_min) / 2) / delta_sum
                
                report = f"{str(equation).split()[0]} = {w_AICc[equation]:.4f}"
                self.model_selection_report.append(report)
        else:
            self.model_selection_report.append('No Model Passed the Model Selection')

        return w_AICc 

    def integrate(self, T_h: float) -> tuple[float, float, float, str]:
        lamda = self.ln2 / T_h
        self.integration_summary = []
        try:
            if "single point" in self.fit_type:
                dataIntegral = 0
                dataIntegralError = 0
                totalIntegral = self.y[0] / lamda
                y0 = f'{self.y[0]:.2f}' if self.y[0] > 5e-1 else f'{self.y[0]:.2e}'
                fitString = f"y = {y0} * exp(-ln2*x / {T_h:.2f})"
                report = f"Single Time Point = {fitString} \
                            \n AUC = {totalIntegral:.2f}"
                self.integration_summary.append(report)
            elif "trapezoid" in self.fit_type:
                dataIntegral = trapz(y=self.y[1:], x=self.x[1:])
                incorp = trapz(y=self.y, x=self.x) - dataIntegral
                dataIntegralError = 0
                totalIntegral = (
                    incorp
                    + dataIntegral
                    + self.y[-1] / lamda * exp(-lamda * self.x[-1])
                )
                fitString = "Trapezoid integration"
                report = f"{fitString} = {totalIntegral:.2f} \
                            \n Teff = {T_h:.2f}h extracted from the last two time points"
                
                self.integration_summary.append(report)
            else:
                fitString = f"{self.fitter}"
                AUC = self.fitter.AUC()         
                AUC_SD = self.fitter.AUC_SD()  

                # incorp = self.fitter.integrate(0, self.x[0])[0]
                integral = self.fitter.integrate(self.x[0], self.x[-1])[0]
                # tail = self.fitter.integrate(self.x[-1], np.inf)[0]

                dataIntegral = integral
                totalIntegral = AUC

                if AUC_SD/AUC <= 1:
                    dataIntegralError = AUC_SD
                else:
                    dataIntegralError = "--"

                report = f"Model selected = {fitString} \
                            \n AUC = {AUC:.2f} +- {AUC_SD:.2f} (CV = {(100*AUC_SD/AUC):.2f} %)"
        
                self.integration_summary.append(report)

        except Exception as e:
            self.logger.error(e)
            self.logger.info(self.x)
            self.logger.info(self.y)
            fitString = "The integral does not converge"
            dataIntegral = 0
            dataIntegralError = 0
            totalIntegral = 0
            newY = self.y

        return dataIntegral, dataIntegralError, totalIntegral, fitString
