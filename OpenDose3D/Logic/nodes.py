from datetime import datetime, timedelta
from pathlib import Path
import numpy as np
import slicer
import vtk

from Logic.attributes import Attributes
from Logic.errors import IOError, ConventionError
from Logic.logging import getLogger
from Logic import vtkmrmlutils
from Logic.constants import Constants
from Logic.utils import extractMode


class Node:
    # Node is the base class for all kinds of nodes

    def __init__(self, nodeID: str):
        self.logger = getLogger("OpenDose3D.nodes")

        self.nodeID = nodeID
        self.name = vtkmrmlutils.getItemDataNodeName(self.nodeID)
        self.data = vtkmrmlutils.getItemDataNode(self.nodeID)

    @staticmethod
    def new(nodeID: str, default_isotope="", default_activity=-1):
        from Logic.NodeDICOM import NodeDICOM
        from Logic.NodeMRB import NodeMRB

        node = Node(nodeID)

        if node is None:
            raise ConventionError(f"Invalid data format for node with id '{nodeID}'")

        if node.isArtefact():
            node = NodeArtefact(nodeID)
        else:
            node = NodeMRB(nodeID)
            isotope = node.getIsotope()
            if not isotope or (default_isotope and default_isotope != isotope):
                node.setIsotope(default_isotope)
            activity = node.getInjectedActivity()
            if (
                not activity
                or activity < 0
                or (default_activity > 0 and activity != default_activity)
            ):
                node.setInjectedActivity(default_activity)

        if not node.complete():
            if node.isDICOM():
                node = NodeDICOM(nodeID, default_isotope, default_activity)
            else:
                raise ConventionError(
                    f"Invalid data format for node with id '{node.nodeID}'"
                )

        return node

    @staticmethod
    def getFolderChildren(folderID: str) -> dict:
        """
        Gets all children of a specific folder and creates a map of their attributes

        Form:
            { "identifier": Node }

        see nodes.py

        """
        itemIDs = vtkmrmlutils.getAllFolderChildren(folderID)
        logger = getLogger("OpenDose3D.nodes")
        children = {}
        for itemID in itemIDs:
            node = Node.new(itemID, "", -1)
            if (":" not in node.name) or (len(node.name.split(":")[1]) < 8):
                logger.error(f"Invalid name: {node.name}")
                continue
            key = node.name.split(":")[1][:4]
            if key == "ADRM" or key == "TABL":
                mode = extractMode(node.name)
                if key not in children:
                    children[key] = {}
                children[key][mode] = node
            else:
                children[key] = node
        return children

    @staticmethod
    def getInitialTime(
        defaultInjectedActivity=-1, defaultInjectionTime=0
    ) -> tuple[str, str, float, str]:
        logger = getLogger("OpenDose3D.nodes")
        volumeNodeIDs = vtkmrmlutils.getScalarVolumeNodes()
        hourunit = Constants().units["hour"]
        # search for initial acquisition time
        timestamp = datetime.now()
        hours0 = 0
        injectedActivity = float(defaultInjectedActivity)
        injectionTime = defaultInjectionTime
        for volumeNodeID in volumeNodeIDs:
            node = Node.new(volumeNodeID)
            if node.getModality() in ["NM", "PT"]:
                localInjectedActivity = float(node.getInjectedActivity())
                if not localInjectedActivity:
                    localInjectedActivity = float(defaultInjectedActivity)
                if localInjectedActivity > 0:
                    injectedActivity = localInjectedActivity

                acquisitionTimeStr = node.getAcquisition()
                if acquisitionTimeStr:
                    acquisitionTime = datetime.strptime(
                        acquisitionTimeStr, Constants().timeFormat
                    )
                    difference = acquisitionTime - timestamp
                    seconds = difference.total_seconds()
                    if seconds < 0:
                        timestamp = acquisitionTime

                        # Search Injection DateTime first
                        try:
                            injectionTimeStr = node.getInjectionTime()
                            if not injectionTimeStr:
                                raise IOError("no injection time in header")
                            else:
                                injectionTime = datetime.strptime(
                                    injectionTimeStr, Constants().timeFormat
                                )
                                hour0 = int(injectionTime.strftime("%H%M%S"))
                                if hour0 <= 0:  # Invalid dicom tag set in header
                                    raise IOError(
                                        f"wrong injection time in header {injectionTime}"
                                    )
                                difference = timestamp - injectionTime
                                hours0 = round(difference.total_seconds() / hourunit)
                                injectionTime = injectionTime.strftime(
                                    Constants().timeFormat
                                )
                        except (
                            Exception
                        ) as e2:  # No Injection Date Time, try to guess from name
                            logger.info(f"Failed to get injection Date Time\n{e2}")
                            try:
                                _, hours0 = node.extractTime()
                                injectionTime = acquisitionTime - timedelta(
                                    hours=hours0
                                )
                                injectionTime = injectionTime.strftime(
                                    Constants().timeFormat
                                )
                                vtkmrmlutils.setItemAttribute(
                                    node.nodeID,
                                    Attributes().injectionTime,
                                    injectionTime,
                                )
                            except Exception as e3:
                                logger.info(
                                    f"Failed to get injection Time from name, trying user input \n{e3}"
                                )
                                try:
                                    injectionTime = datetime.strptime(
                                        defaultInjectionTime, Constants().timeFormat
                                    )
                                    injectionTime = injectionTime.strftime(
                                        Constants().timeFormat
                                    )
                                except:
                                    raise ConventionError(
                                        f"Impossible to extract injection Time {defaultInjectionTime}-{injectionTime}. \n Please insert a valid injection Time."
                                    )
        getLogger("OpenDose3D.nodes").info(
            f"initial time is {timestamp} after {hours0} hours from injection"
        )
        return timestamp, hours0, injectedActivity, injectionTime

    def isArtefact(self) -> bool:
        artefactNames = ["tabl", "chrt", "plot", "segm"]
        artefact = False
        for name in artefactNames:
            if name in self.name.lower():
                artefact = True

        return artefact

    def isDICOM(self) -> bool:
        try:
            instanceUIDs = self.data.GetAttribute("DICOM.instanceUIDs").split()
            filename = Path(slicer.dicomDatabase.fileForInstance(instanceUIDs[0]))
        except:
            filename = Path("")

        # check if file is in database
        if filename.exists() and filename.is_file():
            return True
        else:
            return False

    def getArrayData(self) -> np.ndarray:
        try:
            return slicer.util.arrayFromVolume(self.data).astype(float)
        except:
            try:
                return slicer.util.array(self.name)
            except:
                print(f"{self.name} has no voxel data")
                raise IOError(f"{self.name} has no voxel data")

    def setArrayData(self, Data: np.ndarray):
        slicer.util.updateVolumeFromArray(self.data, Data)
        self.setVisualization()
        self.data.Modified()

    def rescale(self, intercept=0, slope=1):
        if intercept == 0 and slope == 1:
            return
        vox_arr = slicer.util.arrayFromVolume(self.data).astype(float)
        vox_arr = intercept + slope * vox_arr
        slicer.util.updateVolumeFromArray(self.data, vox_arr)

    def getTransformNode(self):
        return self.data.GetParentTransformNode()

    def setTransformNode(self, transformNode):
        self.data.SetAndObserveTransformNodeID(transformNode.GetID())

    def removeTransformNode(self):
        self.data.SetAndObserveTransformNodeID(None)

    def setVisualization(self):
        self.data.CreateDefaultDisplayNodes()
        vtkmrmlutils.setAndObserveColorNode(self.nodeID)

    def reorient(self, orientation="LPS"):
        vtkmrmlutils.reorient(self.data, orientation=orientation)

    def getOrigin(self) -> np.ndarray:
        return np.array(list(self.data.GetOrigin()))

    def setOrigin(self, neworigin):
        self.data.SetOrigin(tuple(list(neworigin)))

    def getSpacing(self) -> np.ndarray:
        return np.array(self.data.GetSpacing())

    def getDimensions(self):
        return self.data.GetImageData().GetDimensions()

    def getIJKtoRASMatrix(self):
        ijkToRas = vtk.vtkMatrix4x4()
        self.data.GetIJKToRASMatrix(ijkToRas)
        return ijkToRas

    def setIJKtoRASMatrix(self, ijkToRas):
        self.data.SetIJKToRASMatrix(ijkToRas)

    def setSpacing(self, newspacing):
        vtkmrmlutils.changeNodeSpacing(self.data, newspacing)

    def setNameParent(self, newName, parentID):
        self.name = newName
        vtkmrmlutils.setItemName(self.nodeID, newName)
        vtkmrmlutils.setNodeFolder(self.data, parentID)

    def compareAttributes(self, reference):
        """
        Compares the attributes of a reference node to attributes of a given node
        """
        if (self.getPatientID() == reference.getPatientID()) and (
            self.getStudyInstanceUID() == reference.getStudyInstanceUID()
        ):
            self.logger.info(
                f"Sanity check on node with id '{self.nodeID}' and reference node with id '{reference.nodeID}' passed"
            )
            return True
        else:
            self.logger.error(
                f"Sanity check on node with id '{self.nodeID}' and reference node with id '{reference.nodeID}' failed"
            )
            return False


# NodeArtefact is used for all nodes which do not have dicom data associated
class NodeArtefact(Node):

    def __init__(self, nodeID):
        super().__init__(nodeID)

    def complete(self):
        return True



