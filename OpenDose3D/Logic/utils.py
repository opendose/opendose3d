from datetime import datetime
from pathlib import Path
import os
import sys
import re
import numpy as np
import slicer

from Logic.errors import IOError

# attribute values for creating new test nodes


def whoami():
    return sys._getframe(1).f_code.co_name


def checkTesting():
    try:
        mainWindow = slicer.util.mainWindow()
        if mainWindow:
            layoutManager = slicer.app.layoutManager()
            if layoutManager:
                return False
        return True
    except Exception as e:
        print(e)
        return True


def getScriptPath():
    try:
        script_path = Path(__file__).resolve().parents[1]
    except:
        from inspect import getsourcefile
        script_path = Path(getsourcefile(lambda: 0)).resolve().parents[1]
    finally:
        return script_path


def getCurrentTime():
    return str(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))


def getSystemCoresInfo():
    if sys.platform.startswith('win'):
        from multiprocessing import cpu_count
        cpu_cores = cpu_count()
        cpu_processors = cpu_cores
        lsystem = 'windows'
    else:
        from cpu_cores import CPUCoresCounter
        fac = CPUCoresCounter.factory()
        cpu_cores = fac.get_physical_cores_count()
        cpu_processors = fac.get_physical_processors_count()
        lsystem = 'posix'

    return cpu_cores, cpu_processors, lsystem


def change_permissions_recursive(path, mode):
    for root, dirs, files in os.walk(path, topdown=False):
        for dir in [os.path.join(root, d) for d in dirs]:
            os.chmod(dir, mode)
        for lfile in [os.path.join(root, f) for f in files]:
            os.chmod(lfile, mode)


def extractMode(name):
    mode = ' '.join(name.split(':')[1].split()[1:-1])
    #REMOVE: Hack for backward compatibility
    if 'montecarlo' in mode.lower():
        return 'Monte Carlo'
    else:
        return mode


def get_valid_filename(s):
    """
    Return the given string converted to a string that can be used for a clean
    filename. Remove leading and trailing spaces; convert other spaces to
    underscores; and remove anything that is not an alphanumeric, dash,
    underscore, or dot.
    >>> get_valid_filename("john's portrait in 2004.jpg")
    'johns_portrait_in_2004.jpg'
    """
    s = str(s).strip().replace(' ', '_')
    return re.sub(r'(?u)[^-\w.]', '', s)


def Center_of_Mass(Matrix, spacing):
    ''' Calculates the center of mass of a 3D matrix
    '''
    # Shape
    shape = np.shape(Matrix)
    # Axis
    x = np.arange(0, shape[0])*spacing[0]
    y = np.arange(0, shape[1])*spacing[1]
    z = np.arange(0, shape[2])*spacing[2]
    # Mass along axis
    mx = np.sum(Matrix, axis=(1, 2))
    my = np.sum(Matrix, axis=(0, 2))
    mz = np.sum(Matrix, axis=(0, 1))
    # center of Masses
    comx = np.sum(x*mx)/np.sum(mx)
    comy = np.sum(y*my)/np.sum(my)
    comz = np.sum(z*mz)/np.sum(mz)
    return np.array([comx, comy, comz])


def checkRequirements():
    # Install all requirements
    # Updating pip is bad practice inside modules
    # Updating modules that are currently existing in Slicer is bad practice inside Slicer
    from slicer.util import pip_install
    import pkg_resources
    #pip_install('pip -U') 
    spectList = {'scipy', 'pydicom', 'sentry-sdk', 'pillow', 'cpu-cores',
                 #'xsdata', 'defusedxml', 'xmltodict', 'xmlschema', # 'lxml',
                 'pymedphys', 'matplotlib', 'scikit-image', 'coverage',
                 'singleton-decorator','importlib-metadata', 'lmfit'}

    installed = {pkg.key for pkg in pkg_resources.working_set}
    missing = spectList - installed

    if missing:
        ProgressDialog = slicer.util.createProgressDialog(parent=None, value=0, maximum=100)
        ProgressDialog.labelText = f"Install all requirements"
        ProgressDialog.show()
        for spec in missing:
            ProgressDialog.labelText = f"Installing {spec}"
            ProgressDialog.value += int(100/(len(missing)+1))
            pip_install(f"{spec}")
        ProgressDialog.close() 

def clearLayout(layout):
    if not layout:
        return
    while layout.count():
        child = layout.takeAt(0)
        if child.widget():
            child.widget().deleteLater()

def installModule(module="SlicerElastix"):
    em = slicer.app.extensionsManagerModel()
    if em.isExtensionInstalled(module):
        print(f"Detected {module}")
        return
    em.interactive = False  # prevent display of popups
    em.updateExtensionsMetadataFromServer(True, True)  # update extension metadata from server now
    if not em.downloadAndInstallExtensionByName(module, True, True):  # install dependencies, wait for installation to finish
        raise ValueError(f"Failed to install {module} extension")
    if not checkTesting():
        slicer.app.confirmRestart(f"Restart to complete {module} installation")

def isNumber(x):
    try:
        if float(x) == 0.0:
            return True
        lx = x / float(x)  # 0 would have failed this
        if lx == 1.0:
            return True
        else:
            return False
    except:
        return False


def reject_outliers(data, m=2):  # Remove outliers
    d = np.abs(data - np.median(data))
    mdev = np.median(d)
    s = d/mdev if mdev else 0
    return np.where(s < m, data, np.nan)


def hounsfield2density(ct_data):
    from Logic.constants import Constants
   
    #Create density as float array as CT ussually is integer
    ct_data = np.array(ct_data).astype(float)
    density = np.zeros_like(ct_data).astype(float)

    # Part 1: (underflow)
    density[(ct_data < -1000)] = 1.2256e-3/2
    
    # Part 2: (Air region)
    density[(ct_data >= -1000) & (ct_data < -741)] = np.polyval([9.99137722e-04, 1.00036332e+00], ct_data[(ct_data >= -1000) & (ct_data < -741)])
   
    # Part 3: (lungs region)
    density[(ct_data >= -741) & (ct_data < -70)] = np.polyval([0.00102579, 1.02011266], ct_data[(ct_data >= -741) & (ct_data < -70)])

    # Part 4: (soft tissue region)
    density[(ct_data >= -70) & (ct_data <= 98)] = np.polyval([9.08270969e-04, 1.01188611], ct_data[(ct_data >= -70) & (ct_data <= 98)])
    
    # Part 5: (between soft tissue and bone)
    density[(ct_data > 98) & (ct_data < 260)] = np.polyval([5.10836932e-04, 1.05083465], ct_data[(ct_data > 98) & (ct_data < 260)])
    
    # Part 6: (bone region)
    density[(ct_data >= 260) & (ct_data <= 3071)] = np.polyval([6.62537091e-04, 1.01139261], ct_data[(ct_data >= 260) & (ct_data <= 3071)])

    # Part 7: (overflow)
    density[(ct_data > 3071)] = 3.04604402e+00 + 1.2256e-3/2
 
    return density * Constants().units['g_cm3']

def writeTimeIntegFittingReport(name: str, report: dict):
    try:
        reportDirectory = getScriptPath() / "Report"
        reportDirectory.mkdir(parents=True, exist_ok=True)
        filePath = reportDirectory / f"{name}.txt"
        with filePath.open("w") as f:
            for segment, _report in report.items():
                f.write(f"{segment}:\n\n")
                if isinstance(_report, list):
                    for line in _report:
                        sublines = line.split('\n')
                        for subline in sublines:
                            f.write(f"\t{subline}\n")
                f.write("\n")  # Additional newline for separating segments  
    except Exception as e:
        print(f"An error occurred: {e}")