## Use: python3 1-DPKgenerator.py F 9 18
#  three arguments are expected:
#     1. The isotope Symbol
#     2. The isotope Z (atomic number)
#     3. The isotope A (atomic mass)
import numpy as np
import inspect
import sys
from os.path import expanduser
from shutil import rmtree
from pathlib import Path

a = np.concatenate((np.array([0]), np.logspace(start=-3, stop=3, num=500)))
lsize = len(a)-1
NPrimaries = 1e8

Isotope = {'Symbol': 'F', 'Z': 9, 'A': 18}  # Modify
directory = Path(expanduser('~')) / 'Documents' / 'DVK'  # Modify

# If provided use the radionuclide
if len(list(sys.argv)) > 1:
    arguments = list(sys.argv)
    Isotope = {
        'Symbol': arguments[1],
        'Z': int(float(arguments[2])),
        'A': int(float(arguments[3]))
    }
RadioNuclide = f"{Isotope['Symbol']}-{Isotope['A']}"

outdir = directory / f'output-{RadioNuclide}'
if outdir.exists():
    rmtree(str(outdir))
outdir.mkdir(parents=True, exist_ok=True)
gateMacro = directory / f'DPKspheres_{RadioNuclide}.mac'
with gateMacro.open('w') as gateMacroFile:
    s = inspect.cleandoc(f"""
        #=====================================================    
        # GEOMETRY    
        #=====================================================    
        /gate/geometry/setMaterialDatabase GateMaterials.db    
        /gate/world/geometry/setXLength 2.1 m    
        /gate/world/geometry/setYLength 2.1 m    
        /gate/world/geometry/setZLength 2.1 m    
        /gate/world/setMaterial Water    
        #=====================================================    
        # PHYSICS    
        #=====================================================    
        /gate/physics/addPhysicsList emlivermore    
        /gate/physics/addProcess                         Decay    
        /gate/physics/addProcess   RadioactiveDecay GenericIon    
        #=====================================================    
        /gate/physics/Gamma/SetCutInRegion      world 0.001 mm    
        /gate/physics/Electron/SetCutInRegion   world 0.001 mm    
        /gate/physics/Positron/SetCutInRegion   world 0.001 mm    
        /gate/physics/displayCuts    
        #=====================================================    
        # SPHERES    
        #=====================================================
    """)  +'\n'
    gateMacroFile.writelines(s)
    s="" 
    for i in range(lsize):
        s += inspect.cleandoc(f"""
            /gate/world/daughters/name                      sphere{i+1}   
            /gate/world/daughters/insert                    sphere
        """)+'\n'
    gateMacroFile.writelines(s)
    s = inspect.cleandoc(f"""           
        #=====================================================    
        # SHELLS    
        #=====================================================
    """)+'\n'
    gateMacroFile.writelines(s)
    s="" 
    for i in range(lsize):
        s += inspect.cleandoc(f"""
            /gate/sphere{i+1}/geometry/setRmin {round(a[i],6)} mm    
            /gate/sphere{i+1}/geometry/setRmax {round(a[i+1],6)} mm    
            /gate/sphere{i+1}/setMaterial Water  
        """)+'\n'
    gateMacroFile.writelines(s)
    s = inspect.cleandoc(f"""
        #=====================================================    
        # DETECTORS    
        #=====================================================    
        /gate/actor/addActor               SimulationStatisticActor stat    
        /gate/actor/stat/save              output-{RadioNuclide}/{RadioNuclide}-stat.txt    
        /gate/actor/stat/saveEveryNSeconds  3600 
    """)+'\n'
    gateMacroFile.writelines(s)
    s = ""
    for i in range(lsize):
        s += inspect.cleandoc(f"""
            /gate/actor/addActor           DoseActor  dose3D-{i+1}    
            /gate/actor/dose3D-{i+1}/save       output-{RadioNuclide}/Sphere-{i+1}.txt    
            /gate/actor/dose3D-{i+1}/attachTo              sphere{i+1}    
            /gate/actor/dose3D-{i+1}/stepHitType             random    
            /gate/actor/dose3D-{i+1}/setPosition           0 0 0 mm    
            /gate/actor/dose3D-{i+1}/setResolution         1 1 1    
            /gate/actor/dose3D-{i+1}/enableEdep            true    
            /gate/actor/dose3D-{i+1}/enableUncertaintyEdep true    
            /gate/actor/dose3D-{i+1}/enableDose            false    
            /gate/actor/dose3D-{i+1}/enableNumberOfHits    true    
            /gate/actor/dose3D-{i+1}/saveEveryNSeconds    3600 
        """)+'\n'
    gateMacroFile.writelines(s)
    s = inspect.cleandoc(f"""   
        #=====================================================    
        # INITIALIZATION    
        #=====================================================    
        /gate/run/initialize    
        /geometry/test/run    
        #=====================================================    
        # SOURCE    
        #=====================================================    
        /gate/source/addSource {RadioNuclide}    
        /gate/source/{RadioNuclide}/gps/centre 0 0 0 mm    
        /gate/source/{RadioNuclide}/gps/particle ion    
        /gate/source/{RadioNuclide}/gps/ion {Isotope["Z"]} {Isotope["A"]} 0 0    
        /gate/source/{RadioNuclide}/setForcedUnstableFlag true    
        /gate/source/{RadioNuclide}/useDefaultHalfLife    
        /gate/source/{RadioNuclide}/gps/angtype iso    
        /gate/source/{RadioNuclide}/gps/energytype Mono    
        /gate/source/{RadioNuclide}/gps/monoenergy 0. keV    
        /gate/source/list    
        #=====================================================    
        # START    
        #=====================================================    
        /gate/random/setEngineName MersenneTwister    
        /gate/random/setEngineSeed auto    
        /gate/application/setTotalNumberOfPrimaries {NPrimaries}    
        /gate/application/start   
    """)+'\n'
    gateMacroFile.writelines(s)