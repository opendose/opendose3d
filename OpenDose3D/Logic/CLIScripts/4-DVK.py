import multiprocessing
import math
import random
import itertools
import sys
import time
from joblib import Parallel, delayed, Memory
from pathlib import Path
from os.path import expanduser
from scipy.interpolate import interp1d, CubicSpline
from itertools import combinations_with_replacement, product
import numpy as np
import matplotlib.pyplot as plt
import json
try:
    from numba import cuda
    from numba.cuda.random import create_xoroshiro128p_states, xoroshiro128p_uniform_float32
    NUMBAOK = True
except:
    NUMBAOK = False

# Units
gram = 1e-3
cm = 1e-2
mm = 1e-3
g_cm3 = gram / cm**3
elCh = 1.602177e-19
MeV = 1e6*elCh
mGy = 1e-3
MBqs = 1e6
mGy_MBqs = mGy/MBqs


def human_time(t):
    # Function to get the human readable time instead of the number of seconds
    units = [('day', 86400), ('hour', 3600), ('minute', 60), ('second', 1)]
    parts = []
    secs = t
    for unit, mul in units:
        if secs / mul >= 1 or mul == 1:
            if mul > 1:
                n = int(math.floor(secs / mul))
                secs -= n * mul
            else:
                n = secs if secs != int(secs) else int(secs)
            parts.append('{:d} {}{}'.format(
                int(math.floor(n)), unit, '' if n == 1 else 's'))
    return ', '.join(parts)+', {} msec'.format(int(math.floor(secs*1000))-1000*int(math.floor(secs)))


# simulation data, self explained variables to be changed by user
# Copy and paste the same variables from the generator
directory = Path('./')  # Modify, use the same as the generator
vdkDirectory = directory / "DVK"
if not vdkDirectory.exists():
    vdkDirectory.mkdir(exist_ok=True)
Isotope = {'Symbol': 'F', 'Z': 9, 'A': 18}

RadioNuclide = f"{Isotope['Symbol']}-{Isotope['A']}"

# If provided use the radionuclide
if len(list(sys.argv)) > 1:
    arguments = list(sys.argv)
    RadioNuclide = arguments[1]

# voxel_dim = {'x':0.625*mm, 'y':0.625*mm, 'z':0.625*mm} #CT
# voxel_dim = {'x':4.42*mm, 'y':4.42*mm, 'z':4.42*mm} #SPECT
# voxel_dim = {'x':1.01821*mm, 'y':1.01821*mm, 'z':1.5*mm} #PET
voxel_dim = {'x': 3.0*mm, 'y': 3.0*mm, 'z': 3.0*mm}
# DPK are calculated for 1*g_cm3 density, use this to scale to any tissue
voxel_density = 1.0*g_cm3
relative_attenuation = 1  # water; 0.976 muscle; 0.973 bone; 0.979 adipose
#voxel_mass = voxel_density * voxel_dim['x'] * voxel_dim['y'] * voxel_dim['z']
N0 = 1e6
media = ''
if len(arguments) > 2:
    media = arguments[2]

if media:
    if media == 'soft':
        voxel_density = 1.04*g_cm3
        relative_attenuation = 0.976
    elif media == 'bone':
        voxel_density = 1.92*g_cm3
        relative_attenuation = 0.973
    elif media == 'adipose':
        voxel_density = 0.92*g_cm3
        relative_attenuation = 0.979
    elif media == 'lung':
        voxel_density = 0.2*g_cm3
        relative_attenuation = 0.978

if len(arguments) > 3:
    vdim = float(arguments[3])*mm
    voxel_dim = {'x': vdim, 'y': vdim, 'z': vdim}

vxvxmm = round(voxel_dim['x']/mm, 1)

vdkFile = vdkDirectory / f'DVK_{RadioNuclide}_{vxvxmm}_{media}.json'
if vdkFile.exists():
    sys.exit(f"File {vdkFile} already exists")

# Reading data
DPK = []
distances = []
dpkFile = directory / f'DPK_{RadioNuclide}_fine.txt'
with dpkFile.open() as f:
    x = [l.strip() for l in f]
    for l in x[1:-1]:
        if (l != ''):
            entries = l.split()
            distance = float(entries[0])*mm
            distance2 = distance**2
            distances.append(distance)
            DPK.append(distance2*float(entries[1]))
# check if data is conform
#print('DPK:{}, distances:{}'.format(len(DPK),len(distances)))

lv = 50
if len(arguments) > 4:
    lv = int(float(arguments[4]))+1
last_voxel = [min(lv, math.trunc(distances[-1] / voxel_dim['x'] / 2) + 1),
              min(lv, math.trunc(distances[-1] / voxel_dim['y'] / 2) + 1),
              round(min(lv*voxel_dim['x']/voxel_dim['z'], (distances[-1] / voxel_dim['z'] / 2) + 1))]
# last_voxel = [21,21,21]

# this is the target voxel list
#voxel_list = [[i,j,k] for k in range(last_voxel[2]) for j in range(last_voxel[1]) for i in range(last_voxel[0])]
voxel_list = np.array(
    list(product(range(last_voxel[0]), range(last_voxel[1]), range(last_voxel[2]))))
# use memory cache
cacheFile = directory / "cache_dir"
memory = Memory(str(cacheFile), verbose=0)


def voxel_list_cached(voxel_list):
    # Function to get the list of non repeated voxels
    # considering [a, b, c] = [b, a, c]
    new_k = []
    for voxel in voxel_list:
        lmin = min(voxel[0], voxel[1])
        lmax = max(voxel[0], voxel[1])
        nvoxel = [lmin, lmax, voxel[2]]
        if nvoxel not in new_k:
            new_k.append(nvoxel)
    return new_k


get_voxel_list_cached = memory.cache(voxel_list_cached)

start = time.time()
voxel_list = get_voxel_list_cached(voxel_list)
end = time.time()
print(f"Starting {len(voxel_list)} voxels - {human_time(end-start)}")
# pause

# this is the function of dose vs distance
f = CubicSpline(np.log(distances), np.log(DPK))

# Plot DPK if needed
#xnew=np.arange(0, distances[-1], (distances[-1]-distances[-2])/10)
# ynew=np.exp(f(np.log(distances)))
#xt=np.arange(distances[-1], distances[-1] + 0.5*(distances[-1]-distances[0]), (distances[-1]-distances[-2])/10)
# yt=np.exp(ft(xt))
#plt.loglog(distances, DPK, 'o', distances, ynew, '-', xt, yt, '-')
# plt.show()
# pause


def get_coordinate():
    # function to get a random coordinate inside a voxel
    x = random.uniform(0, voxel_dim['x'])
    y = random.uniform(0, voxel_dim['y'])
    z = random.uniform(0, voxel_dim['z'])
    return {'x': x, 'y': y, 'z': z}


def add_origin(point1, origin):
    # function to add target origin to the position
    return {'x': origin['x'] + point1['x'],
            'y': origin['y'] + point1['y'],
            'z': origin['z'] + point1['z']}


def distance(point1, point2):
    # distance between two points
    return math.sqrt((point2['x'] - point1['x'])**2 +
                     (point2['y'] - point1['y'])**2 +
                     (point2['z'] - point1['z'])**2)


if NUMBAOK:
    @cuda.jit
    def gpu_distances(rng_states, x0, y0, z0, xm, ym, zm, out):
        start = cuda.grid(1)
        stride = cuda.gridsize(1)

        for i in range(start, out.shape[0], stride):
            x1 = xoroshiro128p_uniform_float32(rng_states, start)*xm
            y1 = xoroshiro128p_uniform_float32(rng_states, start)*ym
            z1 = xoroshiro128p_uniform_float32(rng_states, start)*zm
            x2 = xoroshiro128p_uniform_float32(rng_states, start)*xm+x0
            y2 = xoroshiro128p_uniform_float32(rng_states, start)*ym+y0
            z2 = xoroshiro128p_uniform_float32(rng_states, start)*zm+z0
            d = math.sqrt((x2 - x1)**2 + (y2 - y1)**2 + (z2 - z1)**2)
            if d < 1e-15:
                d = 1e-15
            out[i] = d


def cpu_distances(x0, y0, z0, xm, ym, zm, size):
    Xm = np.array([xm, ym, zm])
    X0 = np.array([x0, y0, z0])
    [x1, y1, z1] = np.random.rand(3, size) * Xm[:, np.newaxis]
    [x2, y2, z2] = np.random.rand(
        3, size) * Xm[:, np.newaxis] + X0[:, np.newaxis]
    d = np.sqrt((x2 - x1)**2 + (y2 - y1)**2 + (z2 - z1)**2)
    d = np.where(d < 1e-15, 1e-15, d)
    return d


def get_doses(N, target_origin):
    if NUMBAOK:
        threads_per_block = 64
        blocks = N // threads_per_block
        out1 = np.zeros(threads_per_block * blocks, dtype=np.float32)
        rng_states = create_xoroshiro128p_states(
            threads_per_block * blocks, seed=1)
        gpu_distances[blocks, threads_per_block](rng_states,
                                                 target_origin['x'], target_origin['y'], target_origin['z'],
                                                 voxel_dim['x'], voxel_dim['y'], voxel_dim['z'], out1)
    else:
        out1 = cpu_distances(target_origin['x'], target_origin['y'], target_origin['z'],
                             voxel_dim['x'], voxel_dim['y'], voxel_dim['z'], N)
    distances = np.array(out1)
    logdistances = np.exp(f(np.log(relative_attenuation * distances)))
    distances2 = distances**2
    doses = logdistances/distances2 * \
        (relative_attenuation**3) * (voxel_density/g_cm3)**2
    return doses, doses**2, distances


def calculate(voxel):
    # function for monte carlo integration between the central and the target voxel
    start = time.time()
    # this is the coordinate of the target voxel origin
    target_origin = {'x': voxel[0] * voxel_dim['x'],
                     'y': voxel[1] * voxel_dim['y'],
                     'z': voxel[2] * voxel_dim['z']}
    d0 = math.sqrt(target_origin['x']**2 +
                   target_origin['y']**2+target_origin['z']**2)
    N = int(N0)
    #print('Starting {} with {} primaries'.format(voxel, N))
    doses, doses2, distances = get_doses(N, target_origin)
    N = len(doses)
    Dose = np.sum(doses)
    Dose2 = np.sum(doses2)
    m_distance = np.sum(distances)/N
    std = 0
    std = math.sqrt((1/(N-1))*(Dose2/N - (Dose/N)**2)) / N  # variance
    end = time.time()
    print('Ending {} - {}'.format(voxel, human_time(end - start)), end="\r")
    return (voxel, Dose/N/mGy_MBqs, std/mGy_MBqs, m_distance)


# Procedure to run in parallel several target voxels
start = time.time()
if NUMBAOK:
    results = [calculate(voxel) for voxel in voxel_list]
else:
    results = Parallel(n_jobs=multiprocessing.cpu_count())(
        delayed(calculate)(voxel) for voxel in voxel_list)
end = time.time()
print('Ending {} for {} {} - {}'.format(RadioNuclide,
                                        vxvxmm, 'mm', human_time(end - start)))

# Save S values
txtDirectory = directory / "TXT"
txtDirectory.mkdir(exist_ok=True)
txtFile = txtDirectory / f'results_{RadioNuclide}_{vxvxmm}_{media}.txt'
with txtFile.open('w') as f_results:
    f_results.write(
        'voxel, \t DVK(mGy_MBqs), \t STDDEV(mGy_MBqs), \t mean distance \n')
    for l in results:
        f_results.write('{}\n'.format(l))

DVK_json = {}
DVK_json[RadioNuclide] = {}
DVK_json[RadioNuclide][vxvxmm] = {}
for l in results:
    DVK_json[RadioNuclide][vxvxmm][str(l[0])] = l[1:]
with vdkFile.open('w') as f_results:
    f_results.write(json.dumps(DVK_json, indent=4, sort_keys=True))
