#!/usr/bin/env python3
# anonymise.py
__doc__ = """ 
Read a dicom file (or directory of files), partially "anonymise" it (them), 
by replacing Person names, patient id, optionally remove curves 
and private tags, and write result to a new file (directory)

Requirements:
  pydicom, joblib

  to install them use

  python3 -m pip install pydicom joblib

Copyright (c) 2018 Alex Vergara Gil
Modifications from original script:
  1. Parallel job to increase anonymization speed
  2. requires only input file/folder
  3. traverse folder and all subfolders, any number of nested steps
  4. Creates anonymised folder with same structure of original
  5. Handling options for running as scripted executable

 Based in anonymise script from
 Copyright (c) 2008-2012 Darcy Mason
 This file is part of pydicom, relased under an MIT license.
    See the file license.txt included with this distribution, also
    available at http://pydicom.googlecode.com
 Use at your own risk!!
 This is an example only; use only as a starting point
 Many more items need to be addressed for proper de-identifying DICOM data.
 In particular, note that pixel data could have confidential data "burned in"
 Annex E of PS3.15-2011 DICOM standard document details what must be done to 
 fully de-identify DICOM data
"""
__version__ = 0.1
usage = '''
Usage: ./anonymise.py [-h] [-i INPUT] [-p] [-c] [-d]

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        input dicom file or folder
  -p, --privates        if present remove private tags
  -c, --curves          if present remove curves
  -d, --documentation   show documentation message and exit

Note: Use at your own risk. Does not fully de-identify the DICOM data as per
the DICOM standard, e.g in Annex E of PS3.15-2011.
'''

import os, os.path
import shutil

anonym = 'anonymised'

def is_dir_empty(path):
    return next(os.scandir(path), None) is None

def anonymise(filename, output_filename, new_person_name='anonymous',
              new_patient_id='id', remove_curves=False, remove_private_tags=False):
    """Replace data element values to partly anonymise a DICOM file.
    Note: completely anonymizing a DICOM file is very complicated; there
    are many things this example code does not address. USE AT YOUR OWN RISK.
    """
    import pydicom as dicom

    # Define call-back functions for the dataset.walk() function
    def PN_callback(ds, data_element):
        """Called from the dataset "walk" recursive function for all data elements."""
        if data_element.VR == 'PN':
            data_element.value = new_person_name
    def curves_callback(ds, data_element):
        """Called from the dataset "walk" recursive function for all data elements."""
        if data_element.tag.group & 0xFF00 == 0x5000:
            del ds[data_element.tag]
    
    try:
        # Load the current dicom file to 'anonymise'
        dataset = dicom.read_file(filename)

        # Remove patient name and any other person names
        dataset.walk(PN_callback)

        # Change ID
        dataset.PatientID = new_patient_id

        # Remove data elements (should only do so if DICOM type 3 optional) 
        # Use general loop so easy to add more later
        # Could also have done: del ds.OtherPatientIDs, etc.
        for name in ['OtherPatientIDs', 'OtherPatientIDsSequence']:
            if name in dataset:
                delattr(dataset, name)

        # Same as above but for blanking data elements that are type 2.
        #for name in ['PatientBirthDate']:
        #    if name in dataset:
        #        dataset.data_element(name).value = ''
    
        # Remove private tags if function argument says to do so. Same for curves
        if remove_private_tags:
            dataset.remove_private_tags()
        if remove_curves:
            dataset.walk(curves_callback)

        # write the 'anonymised' DICOM out under the new filename
        dataset.save_as(output_filename) 
        print('Saved ', output_filename)
        return True

    except Exception as e:
        print(e)
        return False 

def anon_directory(in_dir, out_dir, cv, pt):
    import multiprocessing
    from joblib import Parallel, delayed

    if os.path.exists(out_dir):
        if not os.path.isdir(out_dir):
            raise IOError('Input is directory; output name exists but is not a directory')
    else: # out_dir does not exist; create it.
        os.makedirs(out_dir, exist_ok=True)
        print('Created ', out_dir)

    filenames = os.listdir(in_dir)
    fname = [(os.path.join(in_dir, filename), os.path.join(out_dir, filename)) for filename in filenames if not os.path.isdir(filename)]
    Parallel(n_jobs=multiprocessing.cpu_count())(delayed(anonymise)(inF[0], inF[1], anonym, 'id', cv, pt) for inF in fname)
    if is_dir_empty(out_dir):
        shutil.rmtree(out_dir)



def get_dicom_folders(in_dir):
    in_dir1 = ()
    for dirName, subdirList, fileList in os.walk(in_dir):
        if len(subdirList)==0:
            if len(fileList)==0:
                if is_dir_empty(in_dir):
                    shutil.rmtree(in_dir)
            else:
                in_dir1 += (in_dir,)
        else:
            for subDir in subdirList:
                in_dir1 += get_dicom_folders(os.path.join(dirName, subDir))
    return in_dir1

# Can run as a script:
if __name__ == "__main__":
    import argparse, sys

    ap = argparse.ArgumentParser()
    ap.add_argument("-i", "--input", required=False, help="input dicom file or folder")
    ap.add_argument("-p", "--privates", required=False, help="if present remove private tags", action='store_true')
    ap.add_argument("-c", "--curves", required=False, help="if present remove curves", action='store_true')
    ap.add_argument("-d", "--documentation", required=False, help="show documentation message and exit", action='store_true')
    args = vars(ap.parse_args())

    if args["documentation"]:  # Print help and stop
        print(__doc__)
        print(usage)
        sys.exit()

    if args["input"]: # Read input file or folder
        arg1 = args["input"]
    else: # If no input supplied use current directory
        arg1 = os.getcwd()

    pt = args["privates"] # remove private tags?
    cv = args["curves"]       # remove curves?

    if arg1[-1]==os.sep: # Handle the case where user put a final extra / in path, very common
        arg1=arg1[:-1]

    if os.path.isdir(arg1):  #  input is a directory
        arg2 = os.path.join(arg1, anonym)
        folders = get_dicom_folders(arg1)
        n = len(arg1)+1
        folders = tuple(dirn[n:] for dirn in folders)
        folders = tuple(el for el in folders if el != '') 

        for in_dir1 in folders:
            if in_dir1[0]!='.' and not anonym in in_dir1:
                in_dir = os.path.join(arg1, in_dir1)
                out_dir = os.path.join(arg2, in_dir1)
                print('Anonymizing ' + in_dir + '...')
            
                anon_directory(in_dir, out_dir, cv, pt)
        
    else: #  input is not a directory, assume a file given
        in_filename = arg1
        out_filename = in_filename + anonym
        print('Anonymizing ' + in_filename + '...')

        anonymise(in_filename, out_filename, anonym, 'id', cv, pt)
    