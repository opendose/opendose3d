import numpy as np
from pathlib import Path
from os.path import expanduser

# Copy and paste the same variables from the generator
directory = Path(expanduser('~')) / 'Documents' / \
    'DVK'  # Modify, use the same as the generator
# Modify, use the same as the generator
Isotope = {'Symbol': 'F', 'Z': 9, 'A': 18}

RadioNuclide = f"{Isotope['Symbol']}-{Isotope['A']}"
# If provided use the radionuclide
if len(list(sys.argv)) > 1:
    arguments = list(sys.argv)
    RadioNuclide = arguments[1]

outdir = directory / f'output-{RadioNuclide}'
a = np.concatenate((np.array([0]), np.logspace(start=-3, stop=3, num=500)))

with open(str(outdir / f'{RadioNuclide}-stat.txt'), 'r') as fr:
    line = fr.readlines()[1]
    NPrimaries = int(float(line.split('=')[1]))

# Generate a single file result
dpkFile = directory / f'DPK-{RadioNuclide}.txt'
with dpkFile.open('w') as fw:
    fw.write('Shell \t Radius(mm) \t Energy(MeV/Bq) \t Uncertainty\n')
    Shells = {}
    for filename in outdir.glob('*Edep.txt'):
        print("Processing {}".format(filename))
        sphereNo = int(str(filename).split('/')[1].split('-')[0][6:])
        with open(str(filename), 'r') as fr:
            line = fr.readlines()[6]
        Shells[sphereNo] = float(line)/NPrimaries
    Uncertainties = {}
    for filename in outdir.glob('*Edep-Uncertainty.txt'):
        print("Processing {}".format(filename))
        sphereNo = int(str(filename).split('/')[1].split('-')[0][6:])
        with open(str(filename), 'r') as fr:
            line = fr.readlines()[6]
        Uncertainties[sphereNo] = float(line)
    for i in np.arange(1, len(Shells)+1):
        fw.write('{} \t {} \t {} \t {}\n'.format(
            i, a[i], Shells[i], Uncertainties[i]))
