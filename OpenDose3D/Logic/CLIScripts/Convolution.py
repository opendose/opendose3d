import numpy as np
import pickle
import sys
from pathlib import Path


class Convolution:
    '''convolution class to be runned either sequentially or parallel
    for parallel execution a wrapper class must be implemented as:
        def multi_run_wrapper(args):
            convolution = Convolution(*args)
            return convolution.convolute()
    where args may be a tuple with
        args = (indexes, p0, DVK, distance_kernel, dens_array, act_array, num, boundary)
    or a pre-saved pickle file containing a dictionary with these fields
        {"p0", "DVK", "distance_kernel", "dens_array", "act_array", "num", "boundary"}
    '''

    def __init__(self, indexes, p0, DVK, distance_kernel, dens_array, act_array, num, boundary):
        self.indexes = indexes
        self.p0 = p0
        self.DVK = DVK
        self.distance_kernel = distance_kernel
        self.dens_array = dens_array
        self.act_array = act_array
        self.num = num
        self.__decodeBoundary(boundary)

    @classmethod
    def fromPickleFile(cls, indexes, pickleFileName):
        lindexes = indexes
        fileName = Path(pickleFileName)
        with fileName.open('rb') as f:
            pickled = pickle.load(f)
            lp0 = pickled['p0']
            lDVK = pickled['DVK']
            ldistance_kernel = pickled['distance_kernel']
            ldens_array = pickled['dens_array']
            lact_array = pickled['act_array']
            lnum = pickled['num']
            lboundary = pickled['boundary']

            return cls(lindexes, lp0, lDVK, ldistance_kernel, ldens_array, lact_array, lnum, lboundary)

    def __decodeBoundary(self, boundary):
        if boundary == "circular":
            self.get_index = self.__get_IndexCircular__
        elif boundary == "repeat":
            self.get_index = self.__get_IndexRepeatBound__
        else:
            raise Exception("Bad boundary, must be circular or repeat")

    def __get_IndexCircular__(self, Iinit, Iend, shape):
        "get the index of an array moduled by shape (circular indexes)"
        i1 = Iinit % shape
        i2 = Iend % shape
        if i1 > i2:
            return np.concatenate((np.arange(i1, shape), np.arange(i2+1)))
        else:
            return np.arange(i1, i2+1)

    def __get_IndexRepeatBound__(self, Iinit, Iend, shape):
        "get the index of an array moduled by shape (repeating boundary value)"
        if Iinit < 0:
            return np.concatenate(([0]*int(abs(Iinit)), np.arange(Iend+1))).astype(int)
        elif Iend >= shape:
            return np.concatenate((np.arange(Iinit, shape), [shape-1]*int(Iend-shape+1))).astype(int)
        else:
            return np.arange(Iinit, Iend+1).astype(int)

    def reject_outliers(self, data, m=2):  # Remove outliers
        d = np.abs(data - np.median(data))
        mdev = np.median(d)
        s = d/mdev if mdev else 0
        return np.where(s < m, data, np.nan)

    def convolute(self):
        ''' Non homogeneous convolution in a list of voxel (vectorized)
            Variable density correction by distance
        '''
        shape = np.shape(self.act_array)
        x = self.indexes / (shape[1]*shape[2])
        y = (self.indexes/shape[2]) % shape[1]
        z = self.indexes % shape[2]
        res = np.zeros_like(self.indexes, dtype=float)
        xi, yi, zi, points, ids = {}, {}, {}, {}, {}
        distances = np.unique(self.distance_kernel)
        for distance in distances:  # Precalculate for speed
            points[distance] = np.where(self.distance_kernel == distance)
            listOfCoordinates = np.argwhere(self.distance_kernel == distance)
            newps = []
            for coordinate in listOfCoordinates:
                lnew = 0
                for i in [0, 1, 2]:
                    lnew += abs(coordinate[i]-self.p0[i])
                newps.append(int(lnew))
            ids[distance] = newps
            if self.num > 1:  # if self.num is large enough then the average becames more realistic
                # TODO: add random displacements
                xi[distance] = np.linspace(
                    self.p0[0], points[distance][0], self.num)
                yi[distance] = np.linspace(
                    self.p0[1], points[distance][1], self.num)
                zi[distance] = np.linspace(
                    self.p0[2], points[distance][2], self.num)
            elif self.num == 1:
                xi[distance], yi[distance], zi[distance] = points[distance][0], points[distance][1], points[distance][2]
            else:  # self.num<=0:
                xi[distance], yi[distance], zi[distance] = self.p0[0], self.p0[1], self.p0[2]

        for ind in np.arange(len(self.indexes)):

            i = self.get_index(x[ind]-self.p0[0], x[ind]+self.p0[0], shape[0])
            j = self.get_index(y[ind]-self.p0[1], y[ind]+self.p0[1], shape[1])
            k = self.get_index(z[ind]-self.p0[2], z[ind]+self.p0[2], shape[2])

            # density_kernel = self.reject_outliers(self.dens_array[i][:, j][:, :, k], m=5) # This will remove air
            density_kernel = self.dens_array[i][:, j][:, :, k]
            t_activity = self.act_array[i][:, j][:, :, k]

            # Calculate average density kernel
            for distance in distances:
                density_i = np.nanmean(density_kernel[xi[distance].astype(
                    np.int), yi[distance].astype(np.int), zi[distance].astype(np.int)], axis=0)
                # FIXME: Separate activity using an identifier matrix
                activity_i = t_activity[points[distance]]
                dvk_avg = np.mean([self.DVK[distance][identifier][0]
                                   for identifier in ids[distance]])
                energy_i = activity_i * dvk_avg
                res[ind] += np.nansum(np.divide(energy_i, density_i,
                                                out=np.zeros_like(energy_i), where=density_i > 0))

        return res


if __name__ == "__main__":
    try:
        pickledInput = sys.stdin.buffer.read()
        input = pickle.loads(pickledInput)

        pickleFileName = Path(sys.argv[1])

        convolution = Convolution.fromPickleFile(
            input['indexes'], pickleFileName)  # Making the convolution pickable
        output = {}
        # Invoking the function is now thread safe
        output['data'] = convolution.convolute()
        output['lenght'] = len(output['data'])

        sys.stdout.buffer.write(pickle.dumps(output))
    except Exception as e:
        print(e)
