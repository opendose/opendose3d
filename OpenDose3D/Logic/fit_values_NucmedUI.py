# -*- coding: utf-8 -*-
"""
To generate the fitting values
Modified on 2023
@by: Nuclear Medicine Universitas Indonesia
"""

import re
import numpy as np
#import matplotlib.pyplot as plt
from numpy import trapz, log, exp

# Internal packages
from Logic.utils import isNumber
from Logic.fitutils_NucmedUI import (
    Equation2,
    Equation3,
    Equation3a,
    Equation4a,
    Equation4b,
    Equation4c
)

def fit_functions(timePoints, zero=False, scaling = "absolute"):
    fit_function = {}
    
    if timePoints == 2:
        if not zero:
            fit_function = {
                "equation-2": Equation2,
            }
        return fit_function

    if zero:
        fit_function = {
            "equation-3": Equation3,
            "equation-4b": Equation4b,
            "equation-4c": Equation4c
        }
    else:
        fit_function = {
            "equation-2": Equation2,
            "equation-3": Equation3,
            "equation-3a": Equation3a,
            "equation-4a": Equation4a,
            "equation-4b": Equation4b,
            "equation-4c": Equation4c
        }

    fit_functions = {}
    if scaling in ["absolute"]:
        for k, v in fit_function.items():
            nparam = re.findall(r"\d", k)[0]
            if int(nparam) <= timePoints - 1:
                fit_functions[k] = v
    elif scaling in ["relative"]:
        for k, v in fit_function.items():
            nparam = re.findall(r"\d", k)[0]
            if int(nparam) <= timePoints - 2:
                fit_functions[k] = v
    
    return fit_functions

class FitValues:
    def __init__(self, 
                 x: np.ndarray, 
                 y: np.ndarray, 
                 fit_type: str, 
                 lamda: float,
                 forceZeroModel = False,  
                 param_value: dict = {},
                 bayesian: dict = {}, 
                 fit_settings: str = "auto", 
                 var_model: str = "proportional", 
                 err_model: str = "model-based", 
                 scaling: str = "absolute"
                 ):
        self.x, self.y = np.array(
            [(lx, ly) for lx, ly in zip(x, y) if isNumber(lx) and isNumber(ly)]
        ).T
        self.lamda = lamda
        
        self.fit_functions = fit_functions(len(self.x),forceZeroModel, scaling)

        param_value["lamda"] = lamda
        self.param_value = param_value
        self.fit_settings = fit_settings
        
        self.bayesian = bayesian
        self.var_model = var_model
        self.err_model = err_model
        self.scaling = scaling
        
        self.fit_type = fit_type
        self.fitter = {}
        # self.fitkey = None
        self.w_AICc = dict()
        
    def fit(self):
        if "single point" in self.fit_type:
            self.fitting_summary = ["Single Time Point"]
            self.model_selection_report = ["No model selection"]
            return
        
        if "auto" in self.fit_type:
            FUNCTIONS = self.fit_functions
        else:
            FUNCTIONS = {}
            FUNCTIONS[self.fit_type] = self.fit_functions[self.fit_type]

        self.fitting_summary = []

        minBIC = 10000
        model_dict_list = []
        for d, f in FUNCTIONS.items():
            fitter = f(
                self.x,
                self.y,
                self.param_value,
                self.fit_settings,
                self.bayesian,
                self.var_model,
                self.err_model,
                self.scaling
            )
            try:
                fitter.minimize()
            except Exception:
                self.fitter = "trapezoid"
                continue

            BIC = fitter.BIC()
            if (BIC < minBIC):
                minBIC = BIC
                self.fitter = fitter
            
            model_dict = fitter.model_dict()
            model_dict["equation"] = fitter
            model_dict_list.append(model_dict)

            self.fitting_summary += model_dict["Report"]

        if "auto" in self.fit_type and len(FUNCTIONS) >= 2:
            # perform Model selection   
            w_AICc = self.model_selection(model_dict_list)
            if w_AICc:
                self.fitter = max(w_AICc, key=w_AICc.get, default=None)
                self.w_AICc = w_AICc
            else:
                error_message = (
                    "No model passed the Goodness of Fit, selecting model with BIC."
                    "\nModel uncertainty might not be good."
                )
                self.fitting_summary.append(error_message)
        else:
            # Manual Selection
            status = "No model selection was performed.\
                     \nThe model was either manually selected or was the only choice."
            self.fitting_summary.append(status)
            self.model_selection_report = []
            report = "Manual Selection"
            self.model_selection_report.append(report) 
            self.fit_type = d
    
    def __str__(self):
        return str(self.fitter)
    
    def model_selection(self, model_dict_list: list) -> dict:
        models = []
        self.model_selection_report = []

        # Filtering models based on the Goodness of fit
        for mdl in model_dict_list:
            CV_test = max(mdl["CV"]) < 50
            corr_list = mdl["correlation"]
            corr_test = max(abs(corr_list)) < 0.8 if len(corr_list) else False
            AICc_check = isinstance(mdl["AICc"], float)
            
            if CV_test and corr_test and AICc_check:
                models.append(mdl)

        # Calculate AICc and weighted AICc
        AICc = {model["equation"]: model["AICc"] for model in models}
        w_AICc = {}

        self.model_selection_report.append("Weighted AICc for selected models:")

        if AICc:
            AICc_min = min(AICc.values())
            delta = [np.exp(-(aicc - AICc_min) / 2) for aicc in AICc.values()]
            delta_sum = sum(delta)
            
            for equation, aicc in AICc.items():
                w_AICc[equation] = np.exp(-(aicc - AICc_min) / 2) / delta_sum
                
                report = f"{str(equation).split()[0]} = {w_AICc[equation]:.4f}"
                self.model_selection_report.append(report)
        else:
            self.model_selection_report.append('No Model Passed the Model Selection')

        return w_AICc     

    def integrate(self, T_h: float) -> tuple[float, float, float, str]:
        lamda = np.log(2) / T_h
        self.integration_summary = []
        if "single point" in self.fit_type:
            dataIntegral = 0
            dataIntegralError = 0
            totalIntegral = self.y[0] / lamda
            y0 = f'{self.y[0]:.2f}' if self.y[0] > 5e-1 else f'{self.y[0]:.2e}'
            fitString = f"y = {y0} * exp(-ln2*x / {T_h:.2f})"
            report = f"Single Time Point = {fitString} \
                        \n AUC = {totalIntegral:.2f}"
            self.integration_summary.append(report)
        elif "trapezoid" in self.fit_type:
            dataIntegral = trapz(y=self.y[1:], x=self.x[1:])
            incorp = trapz(y=self.y, x=self.x) - dataIntegral
            dataIntegralError = 0
            totalIntegral = (
                incorp
                + dataIntegral
                + self.y[-1] / lamda * exp(-lamda * self.x[-1])
            )
            fitString = "Trapezoid integration"
            report = f"{fitString} = {totalIntegral:.2f} \
                        \n Teff = {T_h:.2f}h extracted from the last two time points"
            self.integration_summary.append(report)
        else:  
            fitString = f"{self.fitter}"
            AUC = self.fitter.AUC()         
            AUC_SD = self.fitter.AUC_SD()  
            
            integral = self.fitter.integrate(self.x[0], self.x[-1])[0]
            
            dataIntegral = integral
            totalIntegral = AUC

            if AUC_SD/AUC <= 1:
                dataIntegralError = AUC_SD
            else:
                dataIntegralError = "--"

            report = f"Model selected = {fitString} \
                        \n AUC = {AUC:.2f} +- {AUC_SD:.2f} (CV = {(100*AUC_SD/AUC):.2f} %)"
    
            self.integration_summary.append(report)
        return dataIntegral, dataIntegralError, totalIntegral, fitString
