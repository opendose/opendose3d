import json
import logging

import slicer

from Logic.utils import getScriptPath
from Logic.constants import Constants
from Logic.errors import IOError
from Logic.jsonutils import json_zip, json_unzip


def setupIsotope(isotopeText='Lu-177'):
    return Constants().isotopes[isotopeText]


def getIsotopes():
    return Constants().isotopes


def downloaDVK(dvkFile):
    import requests
    # Create directories
    resourcesDirectory = getScriptPath() / "Resources"
    if not resourcesDirectory.exists():
        resourcesDirectory.mkdir(exist_ok=True)
    jsonDirectory = resourcesDirectory / "JSON"
    if not jsonDirectory.exists():
        jsonDirectory.mkdir(exist_ok=True)
    dvkDirectory = jsonDirectory / "DVK"
    if not dvkDirectory.exists():
        dvkDirectory.mkdir(exist_ok=True)

    # list of files to download
    baseURI = 'https://gitlab.com/opendose/opendosedvkdata/-/raw/master/DVK/'
    fileToRetrieve = baseURI + dvkFile
    fileToSave = dvkDirectory / dvkFile

    if not fileToSave.exists():
        logging.info(f"Downloading {fileToRetrieve}")
        r = requests.get(fileToRetrieve)
        if r.status_code == 200:  # download successful
            with fileToSave.open('wb') as outfile:
                outfile.write(r.content)
        else:
            logging.error("Download failed")


def readDVK(isotopeText='Lu-177', material='water', DVK_size=None, compressed=True):
    vsize = round(float(DVK_size[0])/Constants().units['mm'], 1)
    if isotopeText[-1]=='m':  # Example Tc-99m
        lisotope = f"m{isotopeText.strip('m')}"  # Put m to the beggining, in the database it is mTc-99
    else:
        lisotope = isotopeText
    dvkDirectory = getScriptPath() / "Resources" / "JSON" / "DVK"
    if compressed:
        filename = f"DVK_{lisotope}_{vsize}_{material}.json.zip"
    else:
        filename = f"DVK_{lisotope}_{vsize}_{material}.json"
    dvkFileName = dvkDirectory / filename
    if not dvkFileName.exists(): 
        downloaDVK(f"DVK_{lisotope}_{vsize}_{material}.json.zip")
        if not compressed:
            decompressDataBase()
        if not dvkFileName.exists():
            print(f"{dvkFileName} does not exists")
            return {}
    with dvkFileName.open('r') as f:
        jsc = json.load(f)
        if compressed:
            rows = dict(json_unzip(jsc))
            while rows!=jsc:
                jsc = rows
                rows = dict(json_unzip(jsc, insist=False))
        else:
            rows = dict(jsc)
        isot = [row for row in rows]
        assert(isot[0] == lisotope)
        stored_size = [row for row in rows[lisotope]]
        assert(abs(float(stored_size[0])-vsize) < 0.1)

        return dict(rows[lisotope][stored_size[0]])


def isCompressed():
    DBdirectory = getScriptPath() / "Resources" / "JSON" / "DVK"
    filenames = list(DBdirectory.glob('DVK_*.json.zip'))
    return True if filenames else False


def compressDataBase():
    DBdirectory = getScriptPath() / "Resources" / "JSON" / "DVK"
    filenames = list(DBdirectory.glob('DVK_*.json'))
    ProgressDialog = slicer.util.createProgressDialog(
        parent=None, value=0, maximum=len(filenames))
    for index, filename in enumerate(filenames):
        ProgressDialog.labelText = f"Processing {filename}..."
        ProgressDialog.setValue(index)
        if ProgressDialog.wasCanceled:
            break
        slicer.app.processEvents()
        with open(str(filename), 'r') as fr:
            js = json.load(fr)
            jsc = json_zip(js)
            newFileName = str(filename)+".zip"  # add .zip
            with open(newFileName, 'w') as fw:
                fw.write(json.dumps(jsc))
        filename.unlink()
    ProgressDialog.close()


def decompressDataBase():
    DBdirectory = getScriptPath() / "Resources" / "JSON" / "DVK"
    filenames = list(DBdirectory.glob('DVK_*.json.zip'))
    ProgressDialog = slicer.util.createProgressDialog(
        parent=None, value=0, maximum=len(filenames))
    for index, filename in enumerate(filenames):
        ProgressDialog.labelText = f"Processing {filename}..."
        ProgressDialog.setValue(index)
        if ProgressDialog.wasCanceled:
            break
        slicer.app.processEvents()
        with open(str(filename), 'r') as fr:
            jsc = json.load(fr)
            js = dict(json_unzip(jsc))
            while js!=jsc:
                jsc = js
                js = dict(json_unzip(jsc, insist=False))
            newFileName = '.'.join(str(filename).split('.')[
                :-1])  # Remove the .zip
            with open(newFileName, 'w') as fw:
                fw.write(json.dumps(js, indent=4, sort_keys=True))
        filename.unlink()
    ProgressDialog.close()
