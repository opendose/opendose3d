import platform
import re

from Logic.config import read
from Logic.errors import *
from Logic.utils import checkRequirements

checkRequirements()
import sentry_sdk as sentry


def init():
    # only init sentry if prod env
    if read('default', 'env') == 'prod':
        userID = read('default', 'userID')

        # enhance with attributes which should be set as a default for every log
        with sentry.configure_scope() as scope:
            scope.user = {'id': userID}
            scope.level = 'error'
            scope.set_tag('os', platform.platform())
            scope.set_tag('os.name', platform.system())
            scope.set_tag('os.version', platform.release())

        dns = read('sentry', 'dns')
        sentry.init(dns)


def extras(exception, attachment):
    # only include extras in prod env
    if read('default', 'env') != 'prod':
        return None

    else:
        extras = {}
        if isinstance(exception, IOError):
            extras['issue'] = 'io'
            if attachment:
                extras['content'] = attachment

        elif isinstance(exception, FileError):
            extras['issue'] = 'file'
            if attachment:
                extras['path'] = anonymizePath(attachment)

        elif isinstance(exception, ConventionError):
            extras['issue'] = 'convention'
            if attachment:
                extras['name'] = attachment

        elif isinstance(exception, DICOMError):
            extras['issue'] = 'dicom'
            if attachment:
                extras['dicom'] = anonymizeDICOM(attachment)

        elif isinstance(exception, NetworkError):
            extras['issue'] = 'network'
            if attachment:
                extras['url'] = attachment

        elif isinstance(exception, XMLError):
            extras['issue'] = 'xml'
            if attachment:
                extras['xml'] = attachment

        else:
            extras['issue'] = 'not-defined'
            if attachment:
                extras['extra'] = attachment

        return extras
