from singleton_decorator import singleton
from Logic.nonDicomFileSetDescriptor import *

@singleton
class XMLConstants():

    def __init__(self):
        self._timepointcategorydict = self.__setupTimePointCategoryDict()
        self._organscategorydict = self.__setupOrgansCategoryDict()
        self._isotopescategorydict = self.__setupIsotopesCategoryDict()
        self._radiopharmaceuticalcategorydict = self.__setupRadiopharmaceuticalCategoryDict()
        self._functiontypecategory = self.__setupFunctionTypeCategory()
        self._calculationalgorithmcategory = self.__setupCalculationAlgorithmCategory()
        self._nonDICOMclass = self.__setupNonDICOMClass()
        self._transformationtype = self.__setupTransformationType()
        self._clinicalcenters = self.__setupClinicalCenters()
        self._calibrationUnits = self.__setupCalibrationUnits()

    @property
    def TimePointCategoryDict(self):
        return self._timepointcategorydict

    def __setupTimePointCategoryDict(self):
        return {
            '6HR': TimePointCategory.VALUE_6H_PLUS_OR_MINUS_2H_POST_RAIT_TIMEPOINT,
            '24HR': TimePointCategory.VALUE_24H_PLUS_OR_MINUS_4H_POST_RAIT_TIMEPOINT,
            '48HR': TimePointCategory.VALUE_48H_PLUS_OR_MINUS_4H_POST_RAIT_TIMEPOINT,
            '72HR': TimePointCategory.VALUE_72H_PLUS_OR_MINUS_12H_POST_RAIT_TIMEPOINT,
            '96HR': TimePointCategory.VALUE_96H_PLUS_OR_MINUS_12H_POST_RAIT_TIMEPOINT,
            '168HR': TimePointCategory.VALUE_168H_PLUS_OR_MINUS_24H_POST_RAIT_TIMEPOINT
        }

    @property
    def OrgansCategoryDict(self):
        return self._organscategorydict

    def __setupOrgansCategoryDict(self):
        return {
            'LLung': OrganOrTissue.LEFT_LUNG,
            'RLung': OrganOrTissue.RIGHT_LUNG,
            'LKidney': OrganOrTissue.LEFT_KIDNEY,
            'RKidney': OrganOrTissue.RIGHT_KIDNEY,
            'Bones': OrganOrTissue.BONE,
            'Air': OrganOrTissue.AIR,
            'Tumour': OrganOrTissue.TUMOR,
            'Boundary': OrganOrTissue.BODY_SURFACE,
            'Whole body': OrganOrTissue.BODY_SURFACE,
            'L2-L4': OrganOrTissue.L2,
            'L2L4': OrganOrTissue.L2
        }

    @property
    def IsotopesCategoryDict(self):
        return self._isotopescategorydict

    def __setupIsotopesCategoryDict(self):
        return {
            'Ra-223': Isotope.RADIUM223,
            'I-131': Isotope.IODINE131,
            'Lu-177': Isotope.LUTETIUM177,
            'Y-90': Isotope.YTTRIUM90,
            'Re-188': Isotope.RHENIUM188,
            'Tb-161': Isotope.TERBIUM161,
            'Tc-99m': Isotope.TECHNETIUM99M
        }

    @property
    def RadiopharmaceuticalCategoryDict(self):
        return self._radiopharmaceuticalcategorydict

    def __setupRadiopharmaceuticalCategoryDict(self):
        return {
            'NaI': Isotope.IODINE131
        }

    @property
    def FunctionTypeCategory(self):
        return self._functiontypecategory

    def __setupFunctionTypeCategory(self):
        return {
            'trapezoid': IntegrationAlgorithm.TRAPEZOID,
            'mexp': IntegrationAlgorithm.MONO_EXPONENTIAL,
            'bexp': IntegrationAlgorithm.BI_EXPONENTIAL,
            'texp': IntegrationAlgorithm.TRI_EXPONENTIAL,
            'Single point': IntegrationAlgorithm.MONO_EXPONENTIAL
        }

    @property
    def CalculationAlgorithmCategory(self):
        return self._calculationalgorithmcategory

    def __setupCalculationAlgorithmCategory(self):
        return {
            'Local Energy Deposition': CalculationAlgorithmUsed.LOCAL_ENERGY_DEPOSITION,
            'FFT Convolution': CalculationAlgorithmUsed.FFT_CONVOLUTION,
            'MonteCarlo': CalculationAlgorithmUsed.MONTE_CARLO
        }

    @property
    def NonDICOMClass(self):
        return self._nonDICOMclass

    def __setupNonDICOMClass(self):
        return {
            'VOI': NonDicomdataClass.VOI,
            'VOIs': NonDicomdataClass.VOI_SUPERIMPOSED_ON_IMAGES,
            'DOSE': NonDicomdataClass.VALUE_3_D_ABSORBED_DOSE_MAP,
            'SEGM': NonDicomdataClass.SEGMENTATION,
            'ACTM': NonDicomdataClass.VOXEL_ACTIVITY_MAP,
            'ADRM': NonDicomdataClass.VALUE_3_D_ENERGY_DEPOSITION_RATE_MATRIX,
            'ACSC': NonDicomdataClass.NM_TOMO_RECONSTRUCTION,
            'CTCT': NonDicomdataClass.CT_RECONSTRUCTION,
            'CTRS': NonDicomdataClass.CT_RECONSTRUCTION,
            'TRNF': NonDicomdataClass.ADVANCED_ELASTIX_TRANSFORMATION,
            'DERS': NonDicomdataClass.DENSITY_IMAGE
        }

    @property
    def TransformationType(self):
        return self._transformationtype

    def __setupTransformationType(self):
        return {
            'Elastix': NonDicomdataClass.ADVANCED_ELASTIX_TRANSFORMATION,
            'useGeometryAlign': NonDicomdataClass.LINEAR_TRANSFORMATION_MATRIX,
            'BSpline': NonDicomdataClass.BSPLINE_TRANSFORMATION
        }

    @property
    def ClinicalCenters(self):
        return self._clinicalcenters

    def __setupClinicalCenters(self):
        return {
            'UKW': 'Wurzburg',
            'UMR': 'Marburg',
            'IUCT-O': 'Toulouse IUCT',
            'ICR': 'Toulouse IUCT',
            'RMH': 'Royal Marsden Hospital'
        }

    @property
    def CalibrationUnits(self):
        return self._calibrationUnits

    def __setupCalibrationUnits(self):
        return {
            'counts/MBqs': CalibrationCoefficientUnit.COUNTS_PER_SECOND_PER_MEGABECQUEREL
        }
