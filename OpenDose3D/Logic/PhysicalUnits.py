import math
from Logic.logging import getLogger
from singleton_decorator import singleton

#######################
#                     #
# Physical Units      #
#                     #
#######################


@singleton
class PhysicalUnits:
    """Helper class to deal with physical units
    Defined modes:
      Longitude, Mass, Current, Time, Activity, Force, Energy, Dose, Power
    Can handle subunits (derived), see self.eng
    To add new unit:
      self.addType('Activity', 'Bq', 1/self.second)
    To get the magnitude
      cm = PhysicalUnits().getUnit('Longitude', 'cm')
    Even if cm is not declared it will return 0.01
    The unit of time is the second, this is my own convention
    """

    def __init__(self):
        self.logger = getLogger("OpenDose3D.PhysicalUnits")
        self._eng = self.__setupEng()
        self.setupbase()
        # Main derived units
        self.addType("Activity", "Bq", 1 / self.second)
        self.becquerel = self.getUnit("Activity", "Bq")
        self.addType("Force", "N", self.kilogram * self.meter / self.second**2)
        self.Newton = self.getUnit("Force", "N")
        self.addType("Energy", "J", self.types["Force"]["N"] * self.meter)
        self.Joule = self.getUnit("Energy", "J")
        self.addType("Dose", "Gy", self.types["Energy"]["J"] / self.kilogram)
        self.Gray = self.getUnit("Dose", "Gy")
        self.addType("Power", "W", self.types["Energy"]["J"] / self.second)
        self.Watt = self.getUnit("Power", "W")
        self.addType("Power", "V", self.types["Power"]["W"] / self.Ampere)
        self.Volt = self.getUnit("Power", "V")
        #  Main physical units
        self.setupPhysicsFundamentals()
        self.addType("Energy", "eV", self.elementaryCharge * self.types["Power"]["V"])

    @property
    def eng(self):
        return self._eng

    def __setupEng(self) -> dict[str, float]:
        return {
            "p": 1e-12,
            "u": 1e-6,
            "m": 1e-3,
            "c": 1e-2,
            "d": 0.1,
            "k": 1e3,
            "M": 1e6,
            "G": 1e9,
            "T": 1e12,
            "P": 1e15,
        }

    def setupbase(self):
        self.meter = 1.0
        self.kilogram = 1.0
        self.Ampere = 1.0
        self.second = 1.0
        self.types = {
            "Longitude": {"m": self.meter},
            "Mass": {"kg": self.kilogram},
            "Current": {"A": self.Ampere},
            "Time": {
                "s": self.second,
                "m": 60.0 * self.second,
                "h": 3600.0 * self.second,
                "d": 24 * 3600.0 * self.second,
                "y": 365.24 * 24 * 3600.0 * self.second,
            },
            "Charge": {"C": self.Ampere * self.second},
        }

    def setupPhysicsFundamentals(self):
        self.elementaryCharge = 1.602177e-19 * self.Ampere * self.second
        self.speedOfLight = 299792458.0 * self.meter / self.second
        self.speedOfLightSquared = self.speedOfLight**2
        self.Dirac = 1.05457168e-34 * self.Joule * self.second
        self.Planck = 6.6260693e-34 * self.Joule * self.second
        self.magnetic = 4e-7 * math.pi * self.Newton / self.Ampere**2
        self.electric = 1.0 / (self.magnetic * self.speedOfLightSquared)
        self.gravitational = 6.6742e-11 * self.meter**3 / self.kilogram / self.second**2
        self.fineStructure = (
            self.magnetic
            * self.elementaryCharge**2
            * self.speedOfLight
            / self.Planck
            / 2
        )

    def getUnit(self, ltype: str, unit: str) -> float:
        if ltype in self.types:
            if unit in self.types[ltype]:  # If unit exists already just return it
                return self.types[ltype][unit]
        else:  # type is not declared, we are not wizards!
            self.logger.error(f"{ltype} does not exists, please define it!")
            return 0

        factor = 1
        if unit[0] in self.eng:
            factor = self.eng[unit[0]]
            rem = unit[1:]
        else:
            rem = unit
        if rem == "g":
            factor *= 1e-3
            rem = "kg"
        if rem in self.types[ltype]:
            return self.types[ltype][rem] * factor

        self.logger.error(f"{rem} does not match with {ltype},  please check!")
        return 0

    def addType(self, ltype: str, unit: str, value: float):
        if ltype not in self.types:
            self.types[ltype] = {unit: value}
        else:
            self.types[ltype][unit] = value
