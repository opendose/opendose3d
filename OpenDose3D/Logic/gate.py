from pathlib import Path
from shutil import rmtree, copyfile
import inspect
import numpy as np
import slicer

from Logic.vtkmrmlutils import saveNodeforGate, getFolderIDs, getItemName
from Logic.nodes import Node
from Logic.errors import IOError
from Logic.attributes import Attributes
from Logic.utils import getScriptPath, hounsfield2density
from Logic.constants import Constants


class Gate:
    """ Class to build Gate files """
    def __init__(self, directory, calibration, isotope):
        self.directory = Path(directory)
        self.calibration = calibration
        self.scriptPath = getScriptPath()
        self.isotope = isotope
        self.globalBox = 'GlobalBox'

    def generate(self, forcewater=False):
        folders = getFolderIDs()
        nodes0 = Node.getFolderChildren(folders[0])
        # Create the patient folder
        if "CTCT" in nodes0:
            patientName = nodes0["CTCT"].data.GetAttribute(
                Attributes().patientName)
        elif "ACSC" in nodes0:
            patientName = nodes0["ACSC"].data.GetAttribute(
                Attributes().patientName)
        else:
            raise IOError(
                "Reference node does not have either CT or SPECT acquisition")
        folderPatient = self.directory / patientName
        if folderPatient.exists():
            rmtree(str(folderPatient))
        folderPatient.mkdir()

        ProgressDialog = slicer.util.createProgressDialog(
            parent=None, value=0, maximum=len(folders))
        for index, folderID in enumerate(folders):
            folderName = getItemName(folderID)
            nodes = Node.getFolderChildren(folderID)
            if not 'ACTM' in nodes:
                continue
            ProgressDialog.labelText = f"Processing {folderName}..."
            ProgressDialog.value = index
            ProgressDialog.show()
            if ProgressDialog.wasCanceled:
                break
            slicer.app.processEvents()
            # Create the folders
            folderPath = folderPatient / folderName
            folderPath.mkdir(parents=True)
            folderMAC = folderPath / 'mac'
            folderMAC.mkdir()
            folderDATA = folderPath / 'data'
            folderDATA.mkdir()
            folderOUTPUT = folderPath / 'output'
            folderOUTPUT.mkdir()

            # Save the nodes
            ACTMarray = nodes['ACTM'].getArrayData()
            ACTMarray = np.clip(ACTMarray, 0, 10000)
            totalActivity = np.sum(ACTMarray) * Constants().units['MBq']
            voxelsize = nodes['ACTM'].data.GetSpacing()
            resolution = np.shape(ACTMarray)
            nodes['ACTM'].setArrayData(ACTMarray)
            saveNodeforGate(nodes['ACTM'].data, str(
                folderDATA / 'ACTM.mhd'))  # Gate must be RAI

            if forcewater:
                CTRSarray = np.zeros_like(nodes['CTRS'].getArrayData())
            else:
                CTRSarray = nodes['CTRS'].getArrayData()
                CTRSarray = np.clip(CTRSarray, -10000, 10000)
            nodes['CTRS'].setArrayData(CTRSarray)
            saveNodeforGate(nodes['CTRS'].data, str(
                folderDATA / 'CTRS.mhd'))

            size = [  # resolution is z,x,y
                voxelsize[0]*resolution[1],
                voxelsize[1]*resolution[2],
                voxelsize[2]*resolution[0]
            ]

            # Generate the files
            self.__generate_runScript(folderPatient)
            self.__generateDensitiesTable(folderDATA)
            self.__generateExecutor(folderMAC, totalActivity)
            self.__generateVerbose(folderMAC)
            self.__generateVisualizator(folderMAC)
            self.__generateGeometry(folderMAC)
            self.__generateActor(folderMAC, voxelsize, resolution, folderName)
            self.__generatePhysics(folderMAC)
            self.__generateSource(folderMAC, size)

        ProgressDialog.close()

    def __generateExecutor(self, folderMAC, primaries):
        executorFile = folderMAC / 'executor.mac'
        with executorFile.open('w') as f:
            s = inspect.cleandoc(f"""
                        ## V I S U A L I S A T I O N 
                        # /control/execute	mac/visu.mac
                        
                        ## G E O M E T R Y
                        /control/execute	mac/geometry.mac
                        
                        ## P H Y S I C S
                        /control/execute	mac/physics.mac
                        
                        ## A C T O R S
                        /control/execute	mac/actor.mac
                        
                        ## I N I T I A L I S A T I O N
                        /gate/run/initialize
                        
                        ## S O U R C E
                        /control/execute	mac/source.mac
                        
                        ## R A N D O M
                        /gate/random/setEngineName MersenneTwister
                        /gate/random/setEngineSeed auto
                        /control/execute	mac/verbose.mac
                        
                        ## A P P L I C A T I O N
                        /gate/application/noGlobalOutput
                        /gate/application/setTotalNumberOfPrimaries	{primaries:.0f}
                        /gate/application/start
                        exit
                    """)
            f.writelines(s)

    def __generateVerbose(self, folderMAC):
        verboseFile = folderMAC / 'verbose.mac'
        with verboseFile.open('w') as f:
            s = inspect.cleandoc("""
                        /gate/verbose	Physic    0
                        /gate/verbose	Cuts      0
                        /gate/verbose	SD        0
                        /gate/verbose	Actions   0
                        /gate/verbose	Actor     0
                        /gate/verbose	Step      0
                        /gate/verbose	Error     0
                        /gate/verbose	Warning   0
                        /gate/verbose	Output    0
                        /gate/verbose	Beam      0
                        /gate/verbose	Volume    0
                        /gate/verbose	Image     0
                        /gate/verbose	Geometry  0
                        /tracking/verbose 0
                    """)
            f.writelines(s)

    def __generateDensitiesTable(self, folderDATA):
        densitiesTable = folderDATA / 'DensitiesTable.txt'
        with densitiesTable.open('w') as f:
            s = inspect.cleandoc("""
                # ===================
                # HU	density g/cm3
                # ===================
            """) + "\n"
            f.writelines(s)
            x = [-10000, -1000, -741, -70, 0, 98, 260, 3071, 10000]
            y = hounsfield2density(x) / Constants().units['g_cm3']
            s=""
            for (lx, ly) in zip(x, y):
                s += inspect.cleandoc(f"{lx:.0f}    {ly:.4f}")  + "\n"
            f.writelines(s)
        # Copy the materials database and the table
        databaseFile = self.scriptPath / 'Resources' / 'Gate' / 'GateMaterials.db'
        copyfile(str(databaseFile), str(folderDATA / 'GateMaterials.db'))
        materialsFile = self.scriptPath / 'Resources' / 'Gate' / 'MaterialsTable.txt'
        copyfile(str(materialsFile), str(folderDATA / 'MaterialsTable.txt'))

    def __generateGeometry(self, folderMAC):
        geometryFile = folderMAC / 'geometry.mac'
        with geometryFile.open('w') as f:
            s = inspect.cleandoc(f"""
                # ===================
                # GEOMETRY
                # ===================

                /gate/geometry/setMaterialDatabase	data/GateMaterials.db

                # World
                /gate/world/geometry/setXLength 1000 cm
                /gate/world/geometry/setYLength 1000 cm
                /gate/world/geometry/setZLength 1000 cm

                /gate/HounsfieldMaterialGenerator/SetMaterialTable		data/MaterialsTable.txt
                /gate/HounsfieldMaterialGenerator/SetDensityTable		data/DensitiesTable.txt
                /gate/HounsfieldMaterialGenerator/SetDensityTolerance	0.005 g/cm3
                /gate/HounsfieldMaterialGenerator/SetOutputMaterialDatabaseFilename	data/HUmaterials.db
                /gate/HounsfieldMaterialGenerator/SetOutputHUMaterialFilename	data/HU2Mat.txt
                /gate/HounsfieldMaterialGenerator/Generate

                # Global Box
                /gate/world/daughters/name	{self.globalBox}
                /gate/world/daughters/insert	ImageNestedParametrisedVolume
                /gate/geometry/setMaterialDatabase	data/HUmaterials.db
                /gate/{self.globalBox}/geometry/setHUToMaterialFile	data/HU2Mat.txt
                /gate/{self.globalBox}/geometry/setImage	data/CTRS.mhd
                # Save Density Map
                #/gate/{self.globalBox}/geometry/buildAndDumpDensityImage	data/densMap.mhd

                /gate/{self.globalBox}/vis/forceWireframe
            """) + "\n"
            f.writelines(s)

    def __generateActor(self, folderMAC, voxelsize, resolution, timestamp):
        actorFile = folderMAC / 'actor.mac'
        with actorFile.open('w') as f:
            s = inspect.cleandoc(f"""
                # ===================
                # DETECTORS
                # ===================

                /gate/actor/addActor	DoseActor doseDistribution
                /gate/actor/doseDistribution/attachTo	{self.globalBox}
                /gate/actor/doseDistribution/stepHitType	random
                /gate/actor/doseDistribution/setPosition 0 0 0 mm
                # /gate/actor/doseDistribution/setSize	{voxelsize[0]*resolution[1]}	{voxelsize[1]*resolution[2]}	{voxelsize[2]*resolution[0]}	mm
                /gate/actor/doseDistribution/setResolution	{resolution[1]} {resolution[2]} {resolution[0]}
                /gate/actor/doseDistribution/setVoxelSize	{voxelsize[0]}	{voxelsize[1]}	{voxelsize[2]}	mm
                /gate/actor/doseDistribution/setDoseAlgorithm       MassWeighting

                /gate/actor/doseDistribution/enableEdep	true
                /gate/actor/doseDistribution/enableUncertaintyEdep	true
                /gate/actor/doseDistribution/enableSquaredEdep	false
                /gate/actor/doseDistribution/enableDose	true
                /gate/actor/doseDistribution/enableUncertaintyDose	true
                /gate/actor/doseDistribution/enableSquaredDose	false
                /gate/actor/doseDistribution/enableNumberOfHits	false
                /gate/actor/doseDistribution/save	output/doseMonteCarlo{timestamp}.mhd
                /gate/actor/doseDistribution/saveEveryNSeconds	900
                /gate/actor/doseDistribution/exportMassImage       output/doseMonteCarlo{timestamp}-Mass.mhd

                /gate/actor/addActor	SimulationStatisticActor stat
                /gate/actor/stat/save	output/SimulationStatus.txt
                /gate/actor/stat/saveEveryNSeconds	900
            """) + "\n"
            f.writelines(s)

    def __generatePhysics(self, folderMAC):
        physicsFile = folderMAC / 'physics.mac'
        with physicsFile.open('w') as f:
            s = inspect.cleandoc(f"""
                #=====================================================
                # Physic List Mechanism
                #=====================================================

                /gate/physics/addPhysicsList	emstandard_opt4
                /gate/physics/addProcess RadioactiveDecay GenericIon
                /gate/physics/addProcess Decay

                #=====================================================
                # customized CUTS
                #=====================================================

                /gate/physics/Gamma/SetCutInRegion      world 1.0 mm
                /gate/physics/Electron/SetCutInRegion   world 1.0 mm

                /gate/physics/Gamma/SetCutInRegion {self.globalBox} 0.1 mm
                /gate/physics/Electron/SetCutInRegion {self.globalBox} 0.1 mm

                # Step Function, decrease smothly the energy when a cut is reached
                # /gate/physics/processes/ElectronIonisation/setStepFunction e- 0.1 0.005 mm

                # /gate/physics/displayCuts

                #=====================================================
                #additional features
                #=====================================================

                /gate/physics/setEMin 0.1 keV
                /gate/physics/setEMax 10 GeV

                /gate/physics/setDEDXBinning 220
                /gate/physics/setLambdaBinning 220 $ mean free path

                /gate/physics/processes/eMultipleScattering/setGeometricalStepLimiterType e- distanceToBoundary
            """) + "\n"
            f.writelines(s)

    def __generateSource(self, folderMAC, size):
        sourceFile = folderMAC / 'source.mac'
        isotope = Constants().isotopes[self.isotope]
        with sourceFile.open('w') as f:
            s = inspect.cleandoc(f"""
                #=====================================================
                # {self.isotope} ion emission
                #=====================================================

                /gate/source/addSource {self.isotope} voxel
                /gate/source/{self.isotope}/reader/insert	image
                /gate/source/{self.isotope}/imageReader/translator/insert	linear
                /gate/source/{self.isotope}/imageReader/linearTranslator/setScale	1 MBq
                /gate/source/{self.isotope}/imageReader/readFile	data/ACTM.mhd
                #/gate/source/{self.isotope}/TranslateTheSourceAtThisIsoCenter          0 0 0 mm
                /gate/source/{self.isotope}/setPosition	{-size[0]/2}	{-size[1]/2}	{-size[2]/2} mm

                /gate/source/{self.isotope}/gps/particle ion
                /gate/source/{self.isotope}/gps/ion {isotope['Z']} {isotope['A']} 0 {isotope['excitation']/Constants().units['keV']}
                /gate/source/{self.isotope}/gps/ene/mono 0. keV
                /gate/source/{self.isotope}/setForcedUnstableFlag true
                #/gate/source/{self.isotope}/useDefaultHalfLife
                /gate/source/{self.isotope}/setForcedHalfLife {isotope['T_h']*Constants().units['hour']} s
                /gate/source/{self.isotope}/gps/ang/type iso
                /gate/source/{self.isotope}/gps/ene/type Mono
                
                #/gate/source/{self.isotope}/dump     1
                /gate/source/list
            """) + "\n"
            f.writelines(s)

    def __generateVisualizator(self, folderMAC):
        sourceFile = folderMAC / 'visu.mac'
        with sourceFile.open('w') as f:
            s = inspect.cleandoc(f"""
                /vis/open OGLIQt
                /vis/scene/create
                /vis/scene/add/volume
                /vis/scene/add/axes 0 0 0 500 mm
                /vis/sceneHandler/attach

                ## the following line
                /vis/viewer/set/auxiliaryEdge 1
                #/vis/geometry/set/forceSolid
                /vis/viewer/set/lineSegmentsPerCircle 200

                /vis/viewer/set/style surface

                /vis/viewer/flush
                /vis/viewer/set/hiddenEdge 1

                ## the following lines store ALL trajectories (do not use with too many events !)
                /vis/scene/add/trajectories
                /vis/scene/add/hits
                /tracking/storeTrajectory 1
                /vis/scene/endOfEventAction accumulate
                /vis/scene/notifyHandlers
            """) + "\n"
            f.writelines(s)

    def __generate_runScript(self, folderPatient):
        "User is expected to have docker working and install tmux for this script to work"
        import stat
        sourceFile = folderPatient / 'runAll.sh'
        with sourceFile.open('w') as f:
            s = inspect.cleandoc(f'''
                #!/bin/bash
                # To use the script install docker and tmux
                alias Gate92='docker run -ti --rm -v "$PWD":/APP opengatecollaboration/gate:9.2-docker'

                for f in *; do
                    if [ -d "$f" ]; then
                        echo "$f"
                        d=${{f/./_}}_{''.join(str(folderPatient).split('-')[-2:])}
                        echo "$d"
                        tmux new-session -d -s $d;          # start new detached tmux session
                        tmux send -t $d "docker login" ENTER;
                        tmux send -t $d "cd $f" ENTER;
                        tmux send -t $d "Gate92 mac/executor.mac" ENTER;          
                    fi
                done
            ''') + "\n"
            f.writelines(s)
            
        # Make the script executable
        sourceFile.chmod(sourceFile.stat().st_mode | stat.S_IEXEC)