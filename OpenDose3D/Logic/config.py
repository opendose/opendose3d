import configparser
from pathlib import Path
import uuid
import json

from Logic.errors import IOError
from Logic.utils import getScriptPath

configFiles = {
    "options": "options.json",
    "calibration": "calibration.json",
    "segmentation": "autoSegmentation.json",
    "sensitivity": "SPECTSensitivity.json",
    "recovery": "RecoveryCoefficients.json",
    "centers": "ClinicalCenters.json",
}


def init():
    write()
    return read("default", "env")


def write():
    # NOTE currently (over) writes the whole ini file, instead of only adding new attributes
    userID = uuid.uuid4()
    config = configparser.ConfigParser()
    config["default"] = {"userID": str(userID), "env": "test"}
    config["sentry"] = {
        "dns": "https://e2f1b6f0916c4f169572536431ac62c4@sentry.io/1779783"
    }
    path = getScriptPath() / "config.ini"

    if not path.exists():
        with path.open("w") as configFile:
            try:
                config.write(configFile)
            except IOError as err:
                raise err


def read(section: str, attribute: str):
    path = getScriptPath() / "config.ini"
    if path.exists():
        config = configparser.ConfigParser()
        config.read(path)
        try:
            value = config[section][attribute]
            return value
        except Exception as err:
            raise err
    else:
        return init()


def getConfigPath(section: str) -> Path:
    configFolder = getScriptPath() / "Resources" / "JSON"
    return configFolder / configFiles[section]


def writeJSONConfig(filePath: Path, value: dict):
    filePath.touch()  # Create if it does not exists
    with filePath.open("w") as f:
        f.write(json.dumps(value, indent=2))


def readJSONConfig(filePath: Path, default: dict) -> dict:
    if not filePath.exists():
        writeJSONConfig(filePath, default)
    with filePath.open("r") as f:
        return json.load(f)


def readSPECTSensitivity() -> dict:
    path = getConfigPath("sensitivity")
    return readJSONConfig(path, {"None": {}})


def readAutoSegmentation() -> dict:
    path = getConfigPath("segmentation")
    return readJSONConfig(
        path, {"AllOptions": []}
    )  # This option will give all segments by default


def readRecoveryCoefficients() -> dict:
    path = getConfigPath("recovery")
    return readJSONConfig(path, {})


def readClinicalCenters() -> dict:
    path = getConfigPath("centers")
    return readJSONConfig(path, {})


def writeSPECTSensitivity(sensitivity: dict):
    path = getConfigPath("sensitivity")
    writeJSONConfig(path, sensitivity)


def writeAutoSegmentation(segmentation: dict):
    path = getConfigPath("segmentation")
    writeJSONConfig(path, segmentation)


def writeRecoveryCoefficients(recovery: dict):
    path = getConfigPath("recovery")
    writeJSONConfig(path, recovery)


def writeClinicalCenters(centers: dict):
    path = getConfigPath("centers")
    writeJSONConfig(path, centers)


def writeOptions(options: dict):
    path = getConfigPath("options")
    writeJSONConfig(path, options)


def readOptions() -> dict:
    # Options JSON file presets
    options = {}
    optionsFile = getConfigPath("options")
    if optionsFile.exists():
        with optionsFile.open("r") as f:
            options = json.load(f)
    else:
        options = {
            "Parameters":{ 
                "Center": "None",
                "Isotope": 8,
                "InjectionActivity": "6848",
                "InjectionTime": "",
                #'usecolors': 1,
                "usePath": str(getScriptPath())
            },
            "Registration": {
                "Preset" : "Rigid"
            },
            "ADR Calculation": {
                "Calculation Material": "Water",
                "Density correction": 1,
                "Algorithm" : "Local Energy Deposition",
                "Activity threshold": 0,
                "Density threshold": 50,
                "Materials": {
                    "water": 1000,
                    "soft": 1040,
                    "bone": 1920,
                    "lung": 200,
                    "adipose": 920
                }
            },
            "Segmentation": {
                "bone marrow extraction": {
                    "enabled": 0,
                    "minHU" : 20,
                    "maxHU" : 300
                }
            },
            "Time Integration": {
                "use_fit_functions_from": "NUKFIT",
                "Model Fitting": "auto-fit"
            }
        }
        with optionsFile.open("w") as f:
            f.write(json.dumps(options, indent=1))
    return options


def readCalibration() -> dict:
    # Calibration JSON file presets
    calibration = {}
    calibrationFile = getConfigPath("calibration")
    if calibrationFile.exists():
        with calibrationFile.open("r") as f:
            calibration = json.load(f)
    else:
        calibration = {
            "CTCalibration": {
                "a0": 0.001,
                "b0": 1,
                "a1": 0.0005116346986394071,
                "b1": 1,
            },
            "SPECTSensitivity": {
                "Value": 4.53,
                "Units": "counts/MBqs",
                "Time": 1800,
            },
            "SPECTRecovery": {},
        }
        with calibrationFile.open("w") as f:
            f.write(json.dumps(calibration, indent=1))
    return calibration