from singleton_decorator import singleton

# Singleton that stores common read only attributes used in OpenDose3D
@singleton
class Attributes:

    ####################
    # DICOM attributes #
    ####################
    @property
    def institutionName(self) -> str:
        return "DICOM.InstitutionName"

    @property
    def instanceUIDs(self) -> str:
        return "DICOM.instanceUIDs"

    @property
    def studyInstanceUID(self) -> str:
        return "DICOM.StudyInstanceUID"

    @property
    def seriesInstanceUID(self) -> str:
        return "DICOM.SeriesInstanceUID"

    @property
    def patientID(self) -> str:
        return "DICOM.PatientID"

    @property
    def patientName(self) -> str:
        return "DICOM.PatientName"

    @property
    def studyCreation(self) -> str:
        return "DICOM.Study.Date"

    @property
    def modality(self) -> str:
        return "DICOM.Modality"

    @property
    def acquisition(self) -> str:
        return "DICOM.Acquisition.Date"

    @property
    def acquisitionDuration(self) -> str:
        return "DICOM.Acquisition.Duration"

    @property
    def acquisitionFrames(self) -> str:
        return "DICOM.Acquisition.Frames"

    @property
    def injectionTime(self) -> str:
        return "DICOM.Injection.Date"

    @property
    def injectedActivity(self) -> str:
        return "DICOM.Injection.Activity"

    @property
    def radiopharmaceutical(self) -> str:
        return "DICOM.Injection.Radiopharmaceutical"

    @property
    def isotope(self) -> str:
        return "DICOM.Isotope"

    @property
    def acquisitionFramesDuration(self) -> str:
        return "DICOM.AcquisitionFramesDuration"

    @property
    def acquisitionFramesTotal(self) -> str:
        return "DICOM.AcquisitionFramesTotal"

    @property
    def calibrationDuration(self) -> str:
        return "DICOM.Calibration.Duration"

    @property
    def calibrationCT(self) -> str:
        return "DICOM.Calibration.HUtoDENSfactor"

    @property
    def sensitivity(self) -> str:
        return "DICOM.Calibration.Sensitivity"

    @property
    def sensitivityUnits(self) -> str:
        return "DICOM.Calibration.SensitivityUnits"

    #####################
    # Slicer attributes #
    #####################
    @property
    def resamplingMode(self) -> str:
        """Resampling mode"""
        return "Slicer.Resampling.Mode"

    @property
    def resamplingVolume(self) -> str:
        """Resampling original volume"""
        return "Slicer.Resampling.OriginalVolume"

    @property
    def resamplingReference(self) -> str:
        """Resampling reference volume"""
        return "Slicer.Resampling.ReferenceVolume"

    @property
    def rescalingVolume(self) -> str:
        """Rescaling original volume"""
        return "Slicer.Rescaling.OriginalVolume"

    @property
    def registrationVolume(self) -> str:
        """Registration moving volume"""
        return "Slicer.Registration.MovingVolume"

    @property
    def registrationReference(self) -> str:
        """Registration fixed volume"""
        return "Slicer.Registration.FixedVolume"

    @property
    def registrationReferenceFolder(self) -> str:
        """Registration reference folder"""
        return "Slicer.Registration.ReferenceFolder"

    @property
    def registrationMode(self) -> str:
        """Registration initialize transform mode"""
        return "Slicer.Registration.initializeTransformMode"

    @property
    def registrationSampling(self) -> str:
        """Registration sampling percentage"""
        return "Slicer.Registration.samplingPercentage"

    @property
    def registrationRigid(self) -> str:
        """Registration rigid"""
        return "Slicer.Registration.Rigid"

    @property
    def registrationAffine(self) -> str:
        """Registration affine"""
        return "Slicer.Registration.Affine"

    @property
    def registrationAlgorithm(self) -> str:
        """Registration algorithm"""
        return "Slicer.Registration.Algorithm"

    ####################
    #  VTK attributes  #
    ####################
    @property
    def vtkMRMLTableNode(self) -> str:
        """VTK MRML table node"""
        return "vtkMRMLTableNode"

    @property
    def vtkMRMLScalarVolumeNode(self) -> str:
        """VTK MRML scalar volume node"""
        return "vtkMRMLScalarVolumeNode"

    @property
    def vtkMRMLSegmentationNode(self) -> str:
        """VTK MRML segmentation node"""
        return "vtkMRMLSegmentationNode"

    @property
    def vtkMRMLPlotChartNode(self) -> str:
        """VTK MRML plot chart node"""
        return "vtkMRMLPlotChartNode"

    @property
    def vtkMRMLPlotSeriesNode(self) -> str:
        """VTK MRML plot series node"""
        return "vtkMRMLPlotSeriesNode"

    @property
    def vtkMRMLTransformNode(self) -> str:
        """VTK MRML transform node"""
        return "vtkMRMLTransformNode"

    @property
    def vtkMRMLSliceCompositeNode(self) -> str:
        """VTK MRML slice composite node"""
        return "vtkMRMLSliceCompositeNode"

    @property
    def vtkMRMLFolderDisplayNode(self) -> str:
        """VTK MRML folder display node"""
        return "vtkMRMLFolderDisplayNode"

    @property
    def vtkMRMLMarkupsDisplayNode(self) -> str:
        """VTK MRML markups display node"""
        return "vtkMRMLMarkupsDisplayNode"

    @property
    def vtkMRMLMarkupsFiducialNode(self) -> str:
        """VTK MRML markups fiducial node"""
        return "vtkMRMLMarkupsFiducialNode"

    #########################
    # OpenDose3D attributes #
    #########################
    @property
    def doseACTMVolume(self) -> str:
        """OpenDose3D ACTM volume"""
        return "OpenDose3D.ADR.ACTMVolume"

    @property
    def doseAlgorithm(self) -> str:
        """OpenDose3D algorithm"""
        return "OpenDose3D.ADR.Algorithm"

    @property
    def doseAlgorithmDescription(self) -> str:
        """OpenDose3D algorithm description"""
        return "OpenDose3D.ADR.Description"

    @property
    def doseDensityCorrection(self) -> str:
        """OpenDose3D density correction"""
        return "OpenDose3D.ADR.DensityCorrection"
    
    @property
    def doseCalculationMaterial(self) -> str:
        """OpenDose3D calculation material"""
        return "OpenDose3D.ADR.CalculationMaterial"

    @property
    def doseMaterialDensity(self) -> str:
        """OpenDose3D material density"""
        return "OpenDose3D.ADR.MaterialDensity [kg/m3]"

    @property
    def doseKernelFilename(self) -> str:
        """OpenDose3D kernel filename"""
        return "OpenDose3D.ADR.KernelFilename"

    @property
    def tableCreationMode(self) -> str:
        """OpenDose3D creation mode"""
        return "OpenDose3D.CreationMode"

    @property
    def timeStamp(self) -> str:
        """OpenDose3D time stamp"""
        return "OpenDose3D.TimeStamp"
    
    @property
    def version(self) -> str:
        """OpenDose3D version"""
        return "OpenDose3D.version"
