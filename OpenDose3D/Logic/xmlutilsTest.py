import json
from pathlib import Path
import shutil

from slicer.ScriptedLoadableModule import ScriptedLoadableModuleTest
import lxml.etree as ElementTree

from Logic.logging import getLogger, INFO
from Logic.utils import whoami
from Logic.xmlutils import xmlutils
from Logic.XSDtoXMLGenerator import XSDtoXMLGenerator


class xmlutilsTest(ScriptedLoadableModuleTest):
    """
    This is the test case for the xml related utils.
    Uses ScriptedLoadableModuleTest base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def setUp(self):
        self.logger = getLogger('OpenDose3D.xmlutilsTest')
        self.logger.setLevel(INFO)
        self.xmlutils = xmlutils()

        self.testfolder = self.xmlutils.script_path / "Test"
        if self.testfolder.exists():
            shutil.rmtree(self.testfolder)
        self.testfolder.mkdir()
        self.xsdPath = Path(self.testfolder / 'test.xsd')
        self.xmlPath = Path(self.testfolder / 'test.xml')
        self.dictPath = Path(self.testfolder / 'test.json')
        self.values = {}
        self.getDefaultValues()

    def tearDown(self):
        self.xmlutils.xsd = None
        self.xmlutils.xml = None

        if self.xsdPath.exists():
            self.xsdPath.unlink()

        if self.xmlPath.exists():
            self.xmlPath.unlink()

        if self.dictPath.exists():
            self.dictPath.unlink()

    # Sample data is hardcoded in a json file
    def getDefaultValues(self):
        jsonVals = self.xmlutils.script_path / \
            "Resources" / "JSON" / "defaultXMLvalues.json"
        with jsonVals.open('r') as f:
            self.values = json.load(f)

    def createXSD(self):
        xsdString = '<?xml version="1.0"?>\n<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema">\n<xsd:element name="a" type="AType"/>\n<xsd:complexType name="AType">\n<xsd:sequence>\n<xsd:element name="b" type="xsd:string" />\n</xsd:sequence>\n</xsd:complexType>\n</xsd:schema>'

        with self.xsdPath.open('w') as f:
            f.write(xsdString)

    def createValidXML(self):
        xmlString = '<a xmlns:xsd="http://www.w3.org/2001/XMLSchema"><b>{}</b></a>'.format(
            self.values['string'])

        with self.xmlPath.open('w') as f:
            f.write(xmlString)

    def createInvalidXML(self):
        xmlString = '<a xmlns:xsd="http://www.w3.org/2001/XMLSchema"><c>{}</c></a>'.format(
            self.values['string'])

        with self.xmlPath.open('w') as f:
            f.write(xmlString)

    def createValidDict(self):
        dictjson = {'a': {
            '@xmlns:xsd': 'http://www.w3.org/2001/XMLSchema', 'b': self.values['string']}}

        with self.dictPath.open('w') as f:
            f.write(json.dumps(dictjson))

    def createInvalidDict(self):
        dictjson = {'a': {'c': self.values['string']}}

        with self.dictPath.open('w') as f:
            f.write(json.dumps(dictjson))

    def runTest(self):
        """
        Run as few or as many tests as needed here
        """
        self.setUp()
        self.tearDown()

        self.logger.info('\n\n***** xmlutils Module Testing *****\n')

        self.setXSD_positive()
        self.setXML_positive()
        self.validate_positive()
        self.validate_negative()
        self.getDictionary_positive()
        self.writeXML_positive()
        self.setDictToXML_positive()
        self.setDictToXML_negative()
        self.XSDtoXML_positive()

        self.logger.info('\n******** XML tests passed *************\n')

        self.tearDown()
        if self.testfolder.exists():
            shutil.rmtree(self.testfolder)

    def setXSD_positive(self):
        self.logger.info(f'Test {whoami()} started!')

        # arrange
        self.createXSD()

        # act
        self.xmlutils.setXSD(str(self.xsdPath))

        # assert
        self.assertIsNotNone(self.xmlutils.xsd)

        self.tearDown()

        self.logger.info(f'Test {whoami()} passed!')

    def setXML_positive(self):
        self.logger.info(f'Test {whoami()} started!')

        # arrange
        self.createXSD()
        self.createValidXML()
        self.xmlutils.setXSD(str(self.xsdPath))

        # act
        self.xmlutils.setXML(str(self.xmlPath))

        # assert
        self.assertTrue(self.xmlutils.validate())

        self.tearDown()

        self.logger.info(f'Test {whoami()} passed!')

    def validate_positive(self):
        self.logger.info(f'Test {whoami()} started!')

        # arrange
        self.createXSD()
        self.createValidXML()

        self.xmlutils.setXSD(str(self.xsdPath))
        self.xmlutils.setXML(str(self.xmlPath))

        # act
        valid = self.xmlutils.validate()

        # assert
        self.assertTrue(valid)

        self.tearDown()

        self.logger.info(f'Test {whoami()} passed!')

    def validate_negative(self):
        self.logger.info(f'Test {whoami()} started!')

        # arrange
        self.createXSD()
        self.createInvalidXML()

        # act
        valid = self.xmlutils.validate()

        # assert
        self.assertFalse(valid)

        self.tearDown()

        self.logger.info(f'Test {whoami()} passed!')

    def getDictionary_positive(self):
        self.logger.info(f'Test {whoami()} started!')

        # arrange
        self.createXSD()
        self.createValidXML()
        dictjson = {'a': {
            '@xmlns:xsd': 'http://www.w3.org/2001/XMLSchema', 'b': self.values['string']}}

        self.xmlutils.setXSD(str(self.xsdPath))
        self.xmlutils.setXML(str(self.xmlPath))

        # act
        newdict = self.xmlutils.getDictionary()

        # assert
        self.assertEqual(newdict, dictjson)

        self.tearDown()

        self.logger.info(f'Test {whoami()} passed!')

    def writeXML_positive(self):
        self.logger.info(f'Test {whoami()} started!')

        # arrange
        self.createXSD()
        self.createValidXML()

        self.xmlutils.setXSD(str(self.xsdPath))
        self.xmlutils.setXML(str(self.xmlPath))
        newPath = str(self.xmlPath.parent / 'test3.xml')
        self.xmlutils.writeXML(newPath)

        # act
        xml1 = ElementTree.tostring(ElementTree.parse(str(self.xmlPath)))
        xml2 = ElementTree.tostring(ElementTree.parse(newPath))

        # assert
        self.assertEqual(xml1, xml2)

        self.tearDown()

        self.logger.info(f'Test {whoami()} passed!')

    def XSDtoXML_positive(self):
        self.logger.info(f'Test {whoami()} started!')

        # arrange
        self.createXSD()
        self.createValidXML()

        self.xmlutils.setXSD(str(self.xsdPath))
        self.xmlutils.setXML(str(self.xmlPath))

        # act
        xml1 = ElementTree.tostring(
            ElementTree.parse(str(self.xmlPath)), encoding=str)
        with XSDtoXMLGenerator(self.xsdPath, 'a', False) as generator:
            xml2 = generator.run(False)

        xml2.trim()

        # assert
        self.assertEqual(xml1, str(xml2))
        self.assertTrue(xml2.validate())

        self.tearDown()

        self.logger.info(f'Test {whoami()} passed!')

    def setDictToXML_positive(self):
        self.logger.info(f'Test {whoami()} started!')

        # arrange
        self.createXSD()
        self.createValidDict()

        self.xmlutils.setXSD(str(self.xsdPath))

        # act
        dictjson = {}
        with self.dictPath.open('r') as f:
            dictjson = json.load(f)

        self.xmlutils.setDictToXML(dictjson)

        # assert
        self.assertTrue(self.xmlutils.validate())

        self.tearDown()

        self.logger.info(f'Test {whoami()} passed!')

    def setDictToXML_negative(self):
        self.logger.info(f'Test {whoami()} started!')

        # arrange
        self.createXSD()
        self.createInvalidDict()

        self.xmlutils.setXSD(str(self.xsdPath))

        # act
        dictjson = {}
        with self.dictPath.open('r') as f:
            dictjson = json.load(f)
        self.xmlutils.setDictToXML(dictjson)

        # assert
        self.assertFalse(self.xmlutils.validate())

        self.tearDown()

        self.logger.info(f'Test {whoami()} passed!')
