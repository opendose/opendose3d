from dataclasses import dataclass, field
from enum import Enum
from typing import List, Optional

__NAMESPACE__ = "https://www.irdbb-medirad.com"


class AbsorbedDoseCategory(Enum):
    MEAN_ABSORBED_DOSE_NORMALIZED_TO_CTDI_FREE_IN_AIR_NORMALIZED_TO_TUBE_LOAD = "mean absorbed dose normalized to CTDI free in air normalized to tube load"
    MEAN_ABSORBED_DOSE_NORMALIZED_TO_CTDI_VOL_NORMALIZED_TO_TUBE_LOAD = "mean absorbed dose normalized to CTDI vol normalized to tube load"
    MEAN_ABSORBED_RADIATION_DOSE = "mean absorbed radiation dose"
    MAXIMUM_ABSORBED_RADIATION_DOSE = "maximum absorbed radiation dose"
    MINIMUM_ABSORBED_RADIATION_DOSE = "minimum absorbed radiation dose"
    MEDIAN_ABSORBED_RADIATION_DOSE = "median absorbed radiation dose"
    MODE_ABSORBED_RADIATION_DOSE = "mode absorbed radiation dose"


class AbsorbedDoseRateUnit(Enum):
    GRAY_PER_SECOND = "gray per second"
    MILLIGRAY_PER_SECOND = "milligray per second"
    MILLIGRAY_PER_HOUR = "milligray per hour"


class AbsorbedDoseUnit(Enum):
    MILLIGRAY_PER_MILLIGRAY_PER_100_MILLIAMPERE_SECOND = "milligray per (milligray per (100 milliampere second))"
    MILLIGRAY_PER_100_MILLIAMPERE_SECOND = "milligray per (100 milliampere second)"
    MILLIGRAY = "milligray"
    MILLIGRAY_PER_MILLIGRAY_PER_1_MILLIAMPERE_SECOND = "milligray per (milligray per (1 milliampere second))"
    GRAY = "gray"


class ActivityUnit(Enum):
    BECQUEREL = "becquerel"
    KILOBECQUEREL = "kilobecquerel"
    MEGABECQUEREL = "megabecquerel"
    CURIE = "curie"
    MILLICURIE = "millicurie"
    MICROCURIE = "microcurie"


@dataclass
class AttenuatorType:
    attenuator_category: Optional[str] = field(
        default=None,
        metadata={
            "name": "AttenuatorCategory",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    equivalent_attenuator_description: Optional[str] = field(
        default=None,
        metadata={
            "name": "EquivalentAttenuatorDescription",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    equivalent_attenuator_material: Optional[str] = field(
        default=None,
        metadata={
            "name": "EquivalentAttenuatorMaterial",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    equivalent_attenuator_thickness_value: Optional[str] = field(
        default=None,
        metadata={
            "name": "EquivalentAttenuatorThicknessValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    equivalent_attenuator_thickness_unit: Optional[str] = field(
        default=None,
        metadata={
            "name": "EquivalentAttenuatorThicknessUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    equivalent_attenuator_model: Optional[str] = field(
        default=None,
        metadata={
            "name": "EquivalentAttenuatorModel",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class CtnumberCalibrationCurveReference:
    class Meta:
        name = "CTNumberCalibrationCurveReference"

    reference_calibration_date: Optional[str] = field(
        default=None,
        metadata={
            "name": "ReferenceCalibrationDate",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class CtrelevantCalibrationReference:
    class Meta:
        name = "CTRelevantCalibrationReference"

    reference_calibration_date: Optional[str] = field(
        default=None,
        metadata={
            "name": "ReferenceCalibrationDate",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


class CalculationAlgorithmUsed(Enum):
    LOCAL_ENERGY_DEPOSITION = "local energy deposition"
    FFT_CONVOLUTION = "FFT convolution"
    MONTE_CARLO = "Monte Carlo"


class CalibrationCoefficientUnit(Enum):
    COUNTS_PER_SECOND_PER_MEGABECQUEREL = "counts per second per megabecquerel"


class CountsInRoiunit(Enum):
    COUNTS = "counts"


class CountsInVoiunit(Enum):
    COUNTS = "counts"


class CountsUnit(Enum):
    COUNTS = "counts"


@dataclass
class DensityPhantom:
    density_phantom_name: Optional[str] = field(
        default=None,
        metadata={
            "name": "DensityPhantomName",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    density_phantom_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "DensityPhantomIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


class EnergyDepositionRateUnit(Enum):
    JOULE_PER_SECOND = "joule per second"
    MEGAELECTRONVOLT_PER_SECOND = "megaelectronvolt per second"


class HotInsertVolumeUnit(Enum):
    MILLILITER = "milliliter"
    CUBIC_CENTIMETER = "cubic centimeter"


class IncorporationFunction(Enum):
    CONSTANT = "constant"
    LINEAR = "linear"
    EXPONENTIAL = "exponential"


class IntegrationAlgorithm(Enum):
    TRAPEZOID = "trapezoid"
    MONO_EXPONENTIAL = "mono_exponential"
    BI_EXPONENTIAL = "bi_exponential"
    TRI_EXPONENTIAL = "tri_exponential"
    OTHER = "other"


class Isotope(Enum):
    RADIUM223 = "radium223"
    IODINE131 = "iodine131"
    LUTETIUM177 = "lutetium177"
    YTTRIUM90 = "yttrium90"
    RHENIUM188 = "rhenium188"
    TECHNETIUM99M = "technetium99m"
    TERBIUM161 = "terbium161"


class KernelLimitForConvolutionsUnit(Enum):
    MILLIMETER = "millimeter"
    CENTIMETER = "centimeter"


class MassUnit(Enum):
    GRAM = "gram"
    KILOGRAM = "kilogram"


@dataclass
class MethodSettingType:
    method_setting: Optional[str] = field(
        default=None,
        metadata={
            "name": "MethodSetting",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    method_setting_value: Optional[str] = field(
        default=None,
        metadata={
            "name": "MethodSettingValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    method_setting_unit: Optional[str] = field(
        default=None,
        metadata={
            "name": "MethodSettingUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


class NonDicomdataClass(Enum):
    VOI = "VOI"
    VOI_SUPERIMPOSED_ON_IMAGES = "VOI superimposed on images"
    VALUE_3_D_ABSORBED_DOSE_MAP = "3D absorbed dose map"
    SEGMENTATION = "segmentation"
    VOXEL_ACTIVITY_MAP = "voxel activity map"
    VALUE_3_D_ENERGY_DEPOSITION_RATE_MATRIX = "3D Energy Deposition Rate Matrix"
    NM_TOMO_RECONSTRUCTION = "NM Tomo Reconstruction"
    CT_RECONSTRUCTION = "CT Reconstruction"
    LINEAR_TRANSFORMATION_MATRIX = "Linear transformation matrix"
    BSPLINE_TRANSFORMATION = "BSpline transformation"
    ADVANCED_ELASTIX_TRANSFORMATION = "Advanced elastix transformation"
    DENSITY_IMAGE = "Density Image"


class NonDicomdataFormat(Enum):
    ZIPPED_IMAGE_J_CONTOURS_FORMAT = "zipped imageJ contours format"
    TIFF_FORMAT_EMBEDDING_IMAGE_J_CONTOURS = "TIFF format embedding imageJ contours"
    ZIPPED_PSEUDO_DICOM_IMPACT_MC = "zipped pseudo DICOM ImpactMC"
    GIF_FORMAT_EMBEDDING_IMAGE_J_CONTOURS = "GIF format embedding imageJ contours"
    NRRD_FORMAT = "NRRD format"
    STL_FORMAT = "STL format"
    HDF4_FORMAT = "HDF4 format"
    HDF5_FORMAT = "HDF5 format"


class OrganMassUnit(Enum):
    GRAM = "gram"
    KILOGRAM = "kilogram"


class OrganOrTissue(Enum):
    BONE = "bone"
    BONE_SURFACES = "bone surfaces"
    RED_BONE_MARROW = "red bone marrow"
    BLADDER = "bladder"
    BREASTS = "breasts"
    LEFT_MALE_BREAST = "left male breast"
    LEFT_FEMALE_BREAST = "left female breast"
    RIGHT_MALE_BREAST = "right male breast"
    RIGHT_FEMALE_BREAST = "right female breast"
    COLON = "colon"
    TESTES = "testes"
    LEFT_TESTIS = "left testis"
    RIGHT_TESTIS = "right testis"
    OVARIES = "ovaries"
    LEFT_OVARY = "left ovary"
    RIGHT_OVARY = "right ovary"
    LIVER = "liver"
    LUNGS = "lungs"
    LEFT_LUNG = "left lung"
    RIGHT_LUNG = "right lung"
    ESOPHAGUS = "esophagus"
    SKIN = "skin"
    STOMACH = "stomach"
    THYROID = "thyroid"
    BRAIN = "brain"
    SALIVARY_GLANDS = "salivary glands"
    PAROTID_GLANDS = "parotid glands"
    LEFT_PAROTID_GLAND = "left parotid gland"
    RIGHT_PAROTID_GLAND = "right parotid gland"
    SUBMANDIBULAR_GLANDS = "submandibular glands"
    LEFT_SUBMANDIBULAR_GLAND = "left submandibular gland"
    RIGHT_SUBMANDIBULAR_GLAND = "right submandibular gland"
    ADRENALS = "adrenals"
    EXTRATHORACIC_REGION = "extrathoracic region"
    GALLBLADDER = "gallbladder"
    HEART = "heart"
    KIDNEYS = "kidneys"
    LEFT_KIDNEY = "left kidney"
    RIGHT_KIDNEY = "right kidney"
    LYMPHATIC_NODES = "lymphatic nodes"
    MUSCLE = "muscle"
    ORAL_MUCOSA = "oral mucosa"
    PANCREAS = "pancreas"
    PROSTATE = "prostate"
    SMALL_INTESTINE = "small intestine"
    SPLEEN = "spleen"
    THYMUS = "thymus"
    UTERUS = "uterus"
    AIR = "air"
    BODY_SURFACE = "body surface"
    TUMOR = "tumor"
    SOFT_TISSUE = "soft tissue"
    L2 = "L2"


@dataclass
class PlanarCalibrationFactorReference:
    planar_calibration_factor_date: Optional[str] = field(
        default=None,
        metadata={
            "name": "PlanarCalibrationFactorDate",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


class PlanarCalibrationFactorUnit(Enum):
    COUNTS_PER_SECOND_PER_MEGABECQUEREL = "counts per second per megabecquerel"


class PostAdminActivity(Enum):
    BECQUEREL = "becquerel"
    KILOBECQUEREL = "kilobecquerel"
    MEGABECQUEREL = "megabecquerel"
    CURIE = "curie"
    MILLICURIE = "millicurie"
    MICROCURIE = "microcurie"


class PostAdminBackgroundActivity(Enum):
    BECQUEREL = "becquerel"
    KILOBECQUEREL = "kilobecquerel"
    MEGABECQUEREL = "megabecquerel"
    CURIE = "curie"
    MILLICURIE = "millicurie"
    MICROCURIE = "microcurie"


class PreAdminActivity(Enum):
    BECQUEREL = "becquerel"
    KILOBECQUEREL = "kilobecquerel"
    MEGABECQUEREL = "megabecquerel"
    CURIE = "curie"
    MILLICURIE = "millicurie"
    MICROCURIE = "microcurie"


class PreAdminBackgroundActivity(Enum):
    BECQUEREL = "becquerel"
    KILOBECQUEREL = "kilobecquerel"
    MEGABECQUEREL = "megabecquerel"
    CURIE = "curie"
    MILLICURIE = "millicurie"
    MICROCURIE = "microcurie"


@dataclass
class ProcessExecutionContext:
    date_time_process_started: Optional[str] = field(
        default=None,
        metadata={
            "name": "DateTimeProcessStarted",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    performing_institution: Optional[str] = field(
        default=None,
        metadata={
            "name": "PerformingInstitution",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class RoiidentifierContainer:
    class Meta:
        name = "ROIIdentifierContainer"

    roiidentifier: List[str] = field(
        default_factory=list,
        metadata={
            "name": "ROIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class RoiidentifierUsedContainer:
    class Meta:
        name = "ROIIdentifierUsedContainer"

    roiidentifier_used: List[str] = field(
        default_factory=list,
        metadata={
            "name": "ROIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


class Radiopharmaceutical(Enum):
    SODIUM_IODIDE_I131 = "sodiumIodideI131"


class RealDensityOfMaterialUnit(Enum):
    HOUNSFIELD = "hounsfield"


@dataclass
class ReferencedClinicalResearchStudy:
    clinical_research_study_id: Optional[str] = field(
        default=None,
        metadata={
            "name": "ClinicalResearchStudyID",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    clinical_research_study_title: Optional[str] = field(
        default=None,
        metadata={
            "name": "ClinicalResearchStudyTitle",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class SpectrecoveryCoefficientCurve:
    class Meta:
        name = "SPECTRecoveryCoefficientCurve"

    spectrecovery_coefficient_curve: Optional[str] = field(
        default=None,
        metadata={
            "name": "SPECTRecoveryCoefficientCurve",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class SimulatedImagingDevicesContainer:
    simulated_imaging_device: List[str] = field(
        default_factory=list,
        metadata={
            "name": "SimulatedImagingDevice",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class SoftwareNameContainer:
    software_name: List[str] = field(
        default_factory=list,
        metadata={
            "name": "SoftwareName",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


class StatisticalSubCategory(Enum):
    MEAN = "mean"
    MAXIMUM = "maximum"
    MINIMUM = "minimum"
    MEDIAN = "median"
    MODE = "mode"


class TimeIntegratedActivityPerVoiunit(Enum):
    MEGABECQUEREL_XSECOND = "MegabecquerelXSecond"
    MEGABECQUEREL_XHOUR = "MegabecquerelXHour"


class TimePointCategory(Enum):
    VALUE_168H_PLUS_OR_MINUS_24H_POST_RAIT_TIMEPOINT = "168h plus or minus 24h post RAIT timepoint"
    VALUE_96H_PLUS_OR_MINUS_12H_POST_RAIT_TIMEPOINT = "96h plus or minus 12h post RAIT timepoint"
    VALUE_72H_PLUS_OR_MINUS_12H_POST_RAIT_TIMEPOINT = "72h plus or minus 12h post RAIT timepoint"
    VALUE_48H_PLUS_OR_MINUS_4H_POST_RAIT_TIMEPOINT = "48h plus or minus 4h post RAIT timepoint"
    VALUE_24H_PLUS_OR_MINUS_4H_POST_RAIT_TIMEPOINT = "24h plus or minus 4h post RAIT timepoint"
    VALUE_6H_PLUS_OR_MINUS_2H_POST_RAIT_TIMEPOINT = "6h plus or minus 2h post RAIT timepoint"
    ADDITIONAL_TIMEPOINT = "additional timepoint"


@dataclass
class TimePointIdentifierContainer:
    time_point_identifier: List[str] = field(
        default_factory=list,
        metadata={
            "name": "TimePointIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class TimePointIdentifierUsedContainer:
    time_point_identifier_used: List[str] = field(
        default_factory=list,
        metadata={
            "name": "TimePointIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


class TimeUnit(Enum):
    SECONDS = "seconds"
    MINUTES = "minutes"
    HOURS = "hours"
    DAYS = "days"
    MONTHS = "months"
    YEARS = "years"


@dataclass
class TransformationIdentifierContainer:
    transformation_identifier: List[str] = field(
        default_factory=list,
        metadata={
            "name": "TransformationIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


class TransformationType(Enum):
    LINEAR_TRANSFORMATION_MATRIX = "Linear transformation matrix"
    BSPLINE_TRANSFORMATION = "BSpline transformation"
    ADVANCED_ELASTIX_TRANSFORMATION = "Advanced elastix transformation"


@dataclass
class VoiidentifierContainer:
    class Meta:
        name = "VOIIdentifierContainer"

    voiidentifier_used: List[int] = field(
        default_factory=list,
        metadata={
            "name": "VOIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class VoiidentifierUsedContainer:
    class Meta:
        name = "VOIIdentifierUsedContainer"


class VoivolumeUnit(Enum):
    MILLILITER = "milliliter"
    CUBIC_CENTIMETER = "cubic centimeter"


class VolumeUnit(Enum):
    MILLILITER = "milliliter"
    CUBIC_CENTIMETER = "cubic centimeter"


@dataclass
class VoxelAbsorbedDoseMapIdentifierUsed:
    voxel_absorbed_dose_map_identifier_used: List[str] = field(
        default_factory=list,
        metadata={
            "name": "VoxelAbsorbedDoseMapIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class VoxelAbsorbedDoseMapIdentifierUsedContainer:
    voxel_absorbed_dose_map_identifier_used: List[str] = field(
        default_factory=list,
        metadata={
            "name": "VoxelAbsorbedDoseMapIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


class VoxelBasedDistributionOfAbsorbedDoseCategory(Enum):
    ABSORBED_DOSE_NORMALIZED_TO_CTDI_FREE_IN_AIR_NORMALIZED_TO_TUBE_LOAD = "absorbed dose normalized to CTDI free in air normalized to tube load"
    ABSORBED_DOSE_NORMALIZED_TO_CTDI_VOL_NORMALIZED_TO_TUBE_LOAD = "absorbed dose normalized to CTDI vol normalized to tube load"
    ABSORBED_DOSE = "absorbed dose"


@dataclass
class AbsorbedDoseInVoi:
    class Meta:
        name = "AbsorbedDoseInVOI"

    absorbed_dose_in_voivalue: Optional[float] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseInVOIValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    absorbed_dose_unit: Optional[AbsorbedDoseUnit] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    absorbed_dose_in_voiuncertainty: Optional[float] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseInVOIUncertainty",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    voiidentifier_list: Optional[VoiidentifierContainer] = field(
        default=None,
        metadata={
            "name": "VOIIdentifierList",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class AbsorbedDosePerVoitype:
    class Meta:
        name = "AbsorbedDosePerVOIType"

    absorbed_dose_category: Optional[AbsorbedDoseCategory] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseCategory",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    statistical_sub_category: Optional[StatisticalSubCategory] = field(
        default=None,
        metadata={
            "name": "StatisticalSubCategory",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    absorbed_dose_value: Optional[str] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    absorbed_dose_unit: Optional[AbsorbedDoseUnit] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier: Optional[int] = field(
        default=None,
        metadata={
            "name": "VOIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class AbsorbedDoseRatePerVoiatTimePoint:
    class Meta:
        name = "AbsorbedDoseRatePerVOIAtTimePoint"

    absorbed_dose_rate_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseRateValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    absorbed_dose_rate_unit: Optional[AbsorbedDoseRateUnit] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseRateUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier: Optional[int] = field(
        default=None,
        metadata={
            "name": "VOIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class ActualPlanarCalibrationFactor:
    planar_calibration_factor_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "PlanarCalibrationFactorValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    planar_calibration_factor_unit: Optional[PlanarCalibrationFactorUnit] = field(
        default=None,
        metadata={
            "name": "PlanarCalibrationFactorUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class AdministeredActivity:
    administered_activity_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "AdministeredActivityValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    activity_unit: Optional[ActivityUnit] = field(
        default=None,
        metadata={
            "name": "ActivityUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class AttenuatorTypeContainer:
    attenuator_type: List[AttenuatorType] = field(
        default_factory=list,
        metadata={
            "name": "AttenuatorType",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class CalibrationCoefficient:
    calibration_coefficient_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "CalibrationCoefficientValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    calibration_coefficient_unit: Optional[CalibrationCoefficientUnit] = field(
        default=None,
        metadata={
            "name": "CalibrationCoefficientUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    reference_calibration_date: Optional[str] = field(
        default=None,
        metadata={
            "name": "ReferenceCalibrationDate",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    isotope: Optional[Isotope] = field(
        default=None,
        metadata={
            "name": "Isotope",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class ColdInsert:
    volume_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "VolumeValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    volume_unit: Optional[VolumeUnit] = field(
        default=None,
        metadata={
            "name": "VolumeUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    cold_insert_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "ColdInsertIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class CountsPerRoiatTimePoint:
    class Meta:
        name = "CountsPerROIAtTimePoint"

    counts_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "CountsValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    counts_unit: Optional[CountsUnit] = field(
        default=None,
        metadata={
            "name": "CountsUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    roiidentifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "ROIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class CountsPerVoiatTimePoint:
    class Meta:
        name = "CountsPerVOIAtTimePoint"

    counts_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "CountsValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    counts_unit: Optional[CountsUnit] = field(
        default=None,
        metadata={
            "name": "CountsUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier: Optional[int] = field(
        default=None,
        metadata={
            "name": "VOIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class CurveFittingMethod:
    incorporation_function: Optional[IncorporationFunction] = field(
        default=None,
        metadata={
            "name": "IncorporationFunction",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    integration_algorithm: Optional[IntegrationAlgorithm] = field(
        default=None,
        metadata={
            "name": "IntegrationAlgorithm",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    fitting_function: Optional[str] = field(
        default=None,
        metadata={
            "name": "FittingFunction",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class Dicomdata:
    class Meta:
        name = "DICOMData"

    dicomstudy_uid: Optional[str] = field(
        default=None,
        metadata={
            "name": "DICOMStudyUID",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    dicomseries_uid: Optional[str] = field(
        default=None,
        metadata={
            "name": "DICOMSeriesUID",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    transformations_used: Optional[TransformationIdentifierContainer] = field(
        default=None,
        metadata={
            "name": "TransformationsUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class DataActivityPerRoiatTimePoint:
    class Meta:
        name = "DataActivityPerROIAtTimePoint"

    data_activity_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "DataActivityValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    activity_unit: Optional[ActivityUnit] = field(
        default=None,
        metadata={
            "name": "ActivityUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    roiidentifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "ROIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class DataActivityPerVoiatTimePoint:
    class Meta:
        name = "DataActivityPerVOIAtTimePoint"

    data_activity_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "DataActivityValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    activity_unit: Optional[ActivityUnit] = field(
        default=None,
        metadata={
            "name": "ActivityUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier: Optional[int] = field(
        default=None,
        metadata={
            "name": "VOIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class ElementOfCtnumberCalibrationCurve:
    class Meta:
        name = "ElementOfCTNumberCalibrationCurve"

    hounsfield_measured_value: Optional[int] = field(
        default=None,
        metadata={
            "name": "HounsfieldMeasuredValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    real_density_of_material_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "RealDensityOfMaterialValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    real_density_of_material_unit: Optional[RealDensityOfMaterialUnit] = field(
        default=None,
        metadata={
            "name": "RealDensityOfMaterialUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    material_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "MaterialIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class ElementOfSpectrecoveryCoefficientCurve:
    class Meta:
        name = "ElementOfSPECTRecoveryCoefficientCurve"

    ratio_measured_activity_to_true_activity: Optional[float] = field(
        default=None,
        metadata={
            "name": "RatioMeasuredActivityToTrueActivity",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    hot_insert_volume_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "HotInsertVolumeValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    hot_insert_volume_unit: Optional[HotInsertVolumeUnit] = field(
        default=None,
        metadata={
            "name": "HotInsertVolumeUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    hot_insert_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "HotInsertIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class EnergyDepositionRatePerVoiatTimePoint:
    class Meta:
        name = "EnergyDepositionRatePerVOIAtTimePoint"

    energy_deposition_rate_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "EnergyDepositionRateValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    energy_deposition_rate_unit: Optional[EnergyDepositionRateUnit] = field(
        default=None,
        metadata={
            "name": "EnergyDepositionRateUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier: Optional[int] = field(
        default=None,
        metadata={
            "name": "VOIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class HotInsert:
    volume_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "VolumeValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    volume_unit: Optional[VolumeUnit] = field(
        default=None,
        metadata={
            "name": "VolumeUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    pre_admin_activity_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "PreAdminActivityValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    pre_admin_activity: Optional[PreAdminActivity] = field(
        default=None,
        metadata={
            "name": "PreAdminActivity",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    pre_admin_activity_timestamp: Optional[str] = field(
        default=None,
        metadata={
            "name": "PreAdminActivityTimestamp",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    post_admin_activity_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "PostAdminActivityValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    post_admin_activity: Optional[PostAdminActivity] = field(
        default=None,
        metadata={
            "name": "PostAdminActivity",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    post_admin_activity_timestamp: Optional[str] = field(
        default=None,
        metadata={
            "name": "PostAdminActivityTimestamp",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    hot_insert_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "HotInsertIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    isotope: Optional[Isotope] = field(
        default=None,
        metadata={
            "name": "Isotope",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class KernelLimitForConvolutions:
    kernel_limit_for_convolutions_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "KernelLimitForConvolutionsValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    kernel_limit_for_convolutions_unit: Optional[KernelLimitForConvolutionsUnit] = field(
        default=None,
        metadata={
            "name": "KernelLimitForConvolutionsUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class MassPerVoiatTimePoint:
    class Meta:
        name = "MassPerVOIAtTimePoint"

    mass_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "MassValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    mass_unit: Optional[MassUnit] = field(
        default=None,
        metadata={
            "name": "MassUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier: Optional[int] = field(
        default=None,
        metadata={
            "name": "VOIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class MeanAbsorbedDoseInRoi:
    class Meta:
        name = "MeanAbsorbedDoseInROI"

    mean_absorbed_dose_in_roivalue: Optional[float] = field(
        default=None,
        metadata={
            "name": "MeanAbsorbedDoseInROIValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    absorbed_dose_unit: Optional[AbsorbedDoseUnit] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    roiidentifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "ROIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class MeanAbsorbedDoseInVoi:
    class Meta:
        name = "MeanAbsorbedDoseInVOI"

    mean_absorbed_dose_in_voivalue: Optional[float] = field(
        default=None,
        metadata={
            "name": "MeanAbsorbedDoseInVOIValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    absorbed_dose_unit: Optional[AbsorbedDoseUnit] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier: Optional[int] = field(
        default=None,
        metadata={
            "name": "VOIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class MeanAbsorbedDoseRateInRoi:
    class Meta:
        name = "MeanAbsorbedDoseRateInROI"

    mean_absorbed_dose_rate_in_roivalue: Optional[float] = field(
        default=None,
        metadata={
            "name": "MeanAbsorbedDoseRateInROIValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    absorbed_dose_rate_unit: Optional[AbsorbedDoseRateUnit] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseRateUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    roiidentifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "ROIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class MeanAbsorbedDoseRateInVoi:
    class Meta:
        name = "MeanAbsorbedDoseRateInVOI"

    mean_absorbed_dose_rate_in_voivalue: Optional[float] = field(
        default=None,
        metadata={
            "name": "MeanAbsorbedDoseRateInVOIValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    absorbed_dose_rate_unit: Optional[AbsorbedDoseRateUnit] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseRateUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier: Optional[int] = field(
        default=None,
        metadata={
            "name": "VOIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class MethodSettingTypeContainer:
    method_setting_type: List[MethodSettingType] = field(
        default_factory=list,
        metadata={
            "name": "MethodSettingType",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class NmrelevantCalibrationReference:
    class Meta:
        name = "NMRelevantCalibrationReference"

    reference_calibration_date: Optional[str] = field(
        default=None,
        metadata={
            "name": "ReferenceCalibrationDate",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    isotope: Optional[Isotope] = field(
        default=None,
        metadata={
            "name": "Isotope",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class NonDicomdata:
    class Meta:
        name = "NonDICOMData"

    non_dicomdata_class: Optional[NonDicomdataClass] = field(
        default=None,
        metadata={
            "name": "NonDICOMDataClass",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    non_dicomdata_format: Optional[NonDicomdataFormat] = field(
        default=None,
        metadata={
            "name": "NonDICOMDataFormat",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    non_dicomdata_file_name: Optional[str] = field(
        default=None,
        metadata={
            "name": "NonDICOMDataFileName",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    fhiridentifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "FHIRIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    transformations_used: Optional[TransformationIdentifierContainer] = field(
        default=None,
        metadata={
            "name": "TransformationsUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class OrganMass:
    organ_mass_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "OrganMassValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    organ_mass_unit: Optional[OrganMassUnit] = field(
        default=None,
        metadata={
            "name": "OrganMassUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class PatientOrganMassIn2Ddosimetry:
    class Meta:
        name = "PatientOrganMassIn2DDosimetry"

    patient_organ_mass_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "PatientOrganMassValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    mass_unit: Optional[MassUnit] = field(
        default=None,
        metadata={
            "name": "MassUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    roiidentifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "ROIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class PatientOrganMassInHybridDosimetry:
    patient_organ_mass_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "PatientOrganMassValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    mass_unit: Optional[MassUnit] = field(
        default=None,
        metadata={
            "name": "MassUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier: Optional[int] = field(
        default=None,
        metadata={
            "name": "VOIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class RadioBiologicalCalculation:
    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier_used: Optional[object] = field(
        default=None,
        metadata={
            "name": "VOIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voxel_absorbed_dose_map_identifier_used_container: Optional[VoxelAbsorbedDoseMapIdentifierUsedContainer] = field(
        default=None,
        metadata={
            "name": "VoxelAbsorbedDoseMapIdentifierUsedContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    radio_biological_calculation_method: Optional[str] = field(
        default=None,
        metadata={
            "name": "RadioBiologicalCalculationMethod",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    radio_biological_calculation_parameters: Optional[str] = field(
        default=None,
        metadata={
            "name": "RadioBiologicalCalculationParameters",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    biological_effective_dose: Optional[str] = field(
        default=None,
        metadata={
            "name": "BiologicalEffectiveDose",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class RadioBiologicalCalculationIn3Dslide1Dosimetry:
    class Meta:
        name = "RadioBiologicalCalculationIn3DSlide1Dosimetry"

    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier_used: Optional[int] = field(
        default=None,
        metadata={
            "name": "VOIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voxel_absorbed_dose_map_identifier_used: Optional[VoxelAbsorbedDoseMapIdentifierUsed] = field(
        default=None,
        metadata={
            "name": "VoxelAbsorbedDoseMapIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    radio_biological_calculation_method: Optional[str] = field(
        default=None,
        metadata={
            "name": "RadioBiologicalCalculationMethod",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    radio_biological_calculation_parameters: Optional[str] = field(
        default=None,
        metadata={
            "name": "RadioBiologicalCalculationParameters",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    biological_effective_dose: Optional[str] = field(
        default=None,
        metadata={
            "name": "BiologicalEffectiveDose",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class RadioBiologicalCalculationInHybridOr3Dslide2Dosimetry:
    class Meta:
        name = "RadioBiologicalCalculationInHybridOr3DSlide2Dosimetry"

    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier_used: Optional[VoiidentifierContainer] = field(
        default=None,
        metadata={
            "name": "VOIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    radio_biological_calculation_method: Optional[str] = field(
        default=None,
        metadata={
            "name": "RadioBiologicalCalculationMethod",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    radio_biological_calculation_parameters: Optional[str] = field(
        default=None,
        metadata={
            "name": "RadioBiologicalCalculationParameters",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    biological_effective_dose: Optional[str] = field(
        default=None,
        metadata={
            "name": "BiologicalEffectiveDose",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class Tank:
    volume_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "VolumeValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    volume_unit: Optional[VolumeUnit] = field(
        default=None,
        metadata={
            "name": "VolumeUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    pre_admin_background_activity_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "PreAdminBackgroundActivityValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    pre_admin_background_activity: Optional[PreAdminBackgroundActivity] = field(
        default=None,
        metadata={
            "name": "PreAdminBackgroundActivity",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    pre_admin_background_activity_timestamp: Optional[str] = field(
        default=None,
        metadata={
            "name": "PreAdminBackgroundActivityTimestamp",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    post_admin_background_activity_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "PostAdminBackgroundActivityValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    post_admin_background_activity: Optional[PostAdminBackgroundActivity] = field(
        default=None,
        metadata={
            "name": "PostAdminBackgroundActivity",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    post_admin_background_activity_timestamp: Optional[str] = field(
        default=None,
        metadata={
            "name": "PostAdminBackgroundActivityTimestamp",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    tank_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "TankIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    isotope: Optional[Isotope] = field(
        default=None,
        metadata={
            "name": "Isotope",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class TimeIntegratedActivityCoefficientPerRoi:
    class Meta:
        name = "TimeIntegratedActivityCoefficientPerROI"

    time_integrated_activity_coefficient_per_roivalue: Optional[float] = field(
        default=None,
        metadata={
            "name": "TimeIntegratedActivityCoefficientPerROIValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_unit: Optional[TimeUnit] = field(
        default=None,
        metadata={
            "name": "TimeUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    roiidentifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "ROIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class TimeIntegratedActivityCoefficientPerVoi:
    class Meta:
        name = "TimeIntegratedActivityCoefficientPerVOI"

    time_integrated_activity_coefficient_per_voivalue: Optional[float] = field(
        default=None,
        metadata={
            "name": "TimeIntegratedActivityCoefficientPerVOIValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_unit: Optional[TimeUnit] = field(
        default=None,
        metadata={
            "name": "TimeUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier_list: Optional[VoiidentifierContainer] = field(
        default=None,
        metadata={
            "name": "VOIIdentifierList",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class TimeIntegratedActivityPerRoi:
    class Meta:
        name = "TimeIntegratedActivityPerROI"

    time_integrated_activity_per_roivalue: Optional[float] = field(
        default=None,
        metadata={
            "name": "TimeIntegratedActivityPerROIValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_unit: Optional[TimeUnit] = field(
        default=None,
        metadata={
            "name": "TimeUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    roiidentifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "ROIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class TimePointDescriptionElement:
    time_point_category: Optional[TimePointCategory] = field(
        default=None,
        metadata={
            "name": "TimePointCategory",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_distance_from_reference_event_value: Optional[int] = field(
        default=None,
        metadata={
            "name": "TimePointDistanceFromReferenceEventValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_unit: Optional[TimeUnit] = field(
        default=None,
        metadata={
            "name": "TimeUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class AbsorbedDoseInVoicontainer:
    class Meta:
        name = "AbsorbedDoseInVOIContainer"

    absorbed_dose_in_voiproduced: List[AbsorbedDoseInVoi] = field(
        default_factory=list,
        metadata={
            "name": "AbsorbedDoseInVOIProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class AbsorbedDosePerVoitypeContainer:
    class Meta:
        name = "AbsorbedDosePerVOITypeContainer"

    absorbed_dose_per_voitype: List[AbsorbedDosePerVoitype] = field(
        default_factory=list,
        metadata={
            "name": "AbsorbedDosePerVOIType",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class AbsorbedDoseRatePerVoiatTimePointContainer:
    class Meta:
        name = "AbsorbedDoseRatePerVOIAtTimePointContainer"

    absorbed_dose_rate_per_voiat_time_point_produced: List[AbsorbedDoseRatePerVoiatTimePoint] = field(
        default_factory=list,
        metadata={
            "name": "AbsorbedDoseRatePerVOIAtTimePointProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class AcquisitionSettings:
    siteadministeringthetreatment: Optional[str] = field(
        default=None,
        metadata={
            "name": "Siteadministeringthetreatment",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    dateandtimeofinjection: Optional[str] = field(
        default=None,
        metadata={
            "name": "Dateandtimeofinjection",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    pre_administered_activity: Optional[AdministeredActivity] = field(
        default=None,
        metadata={
            "name": "PreAdministeredActivity",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    post_administered_activity: Optional[AdministeredActivity] = field(
        default=None,
        metadata={
            "name": "PostAdministeredActivity",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    radiopharmaceutical: Optional[Radiopharmaceutical] = field(
        default=None,
        metadata={
            "name": "Radiopharmaceutical",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    isotope: Optional[Isotope] = field(
        default=None,
        metadata={
            "name": "Isotope",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class CtacqForCtnumberCalibrationCurve:
    class Meta:
        name = "CTAcqForCTNumberCalibrationCurve"

    density_phantom_used: Optional[DensityPhantom] = field(
        default=None,
        metadata={
            "name": "DensityPhantomUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    ctrecon_produced: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "CTReconProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class ColdInsertContainer:
    cold_insert: List[ColdInsert] = field(
        default_factory=list,
        metadata={
            "name": "ColdInsert",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class CountsPerRoiatTimePointContainer:
    class Meta:
        name = "CountsPerROIAtTimePointContainer"

    counts_per_roiat_time_point_produced: List[CountsPerRoiatTimePoint] = field(
        default_factory=list,
        metadata={
            "name": "CountsPerROIAtTimePointProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class CountsPerVoiatTimePointContainer:
    class Meta:
        name = "CountsPerVOIAtTimePointContainer"

    counts_per_voiat_time_point_produced: List[CountsPerVoiatTimePoint] = field(
        default_factory=list,
        metadata={
            "name": "CountsPerVOIAtTimePointProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class CountsPerVoiatTimePointProducedContainer:
    class Meta:
        name = "CountsPerVOIAtTimePointProducedContainer"

    counts_per_voiat_time_point_produced: List[CountsPerVoiatTimePoint] = field(
        default_factory=list,
        metadata={
            "name": "CountsPerVOIAtTimePointProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class DicomdataContainer:
    class Meta:
        name = "DICOMDataContainer"

    dicomdata: List[Dicomdata] = field(
        default_factory=list,
        metadata={
            "name": "DICOMData",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class DataActivityPerRoiatTimePointContainer:
    class Meta:
        name = "DataActivityPerROIAtTimePointContainer"

    data_activity_per_roiat_time_point_produced: List[DataActivityPerRoiatTimePoint] = field(
        default_factory=list,
        metadata={
            "name": "DataActivityPerROIAtTimePointProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class DataActivityPerVoiatTimePointContainer:
    class Meta:
        name = "DataActivityPerVOIAtTimePointContainer"

    data_activity_per_voiat_time_point_produced: List[DataActivityPerVoiatTimePoint] = field(
        default_factory=list,
        metadata={
            "name": "DataActivityPerVOIAtTimePointProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class ElementOfSpectrecoveryCoefficientCurveContainer:
    class Meta:
        name = "ElementOfSPECTRecoveryCoefficientCurveContainer"

    element_of_spectrecovery_coefficient_curve: List[ElementOfSpectrecoveryCoefficientCurve] = field(
        default_factory=list,
        metadata={
            "name": "ElementOfSPECTRecoveryCoefficientCurve",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class ElementsOfCtnumberCalibrationCurve:
    class Meta:
        name = "ElementsOfCTNumberCalibrationCurve"

    element_of_ctnumber_calibration_curve: List[ElementOfCtnumberCalibrationCurve] = field(
        default_factory=list,
        metadata={
            "name": "ElementOfCTNumberCalibrationCurve",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class EnergyDepositionRateCalculationIn3Ddosimetry:
    class Meta:
        name = "EnergyDepositionRateCalculationIn3DDosimetry"

    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier_used: Optional[VoiidentifierContainer] = field(
        default=None,
        metadata={
            "name": "VOIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    time_point_identifier_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    ctrecon_resampled_on_nmreference_used: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "CTReconResampledOnNMReferenceUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    nmtomo_recon_used: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "NMTomoReconUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    calculation_algorithm_used: Optional[CalculationAlgorithmUsed] = field(
        default=None,
        metadata={
            "name": "CalculationAlgorithmUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    kernel_limit_for_convolutions_used: Optional[KernelLimitForConvolutions] = field(
        default=None,
        metadata={
            "name": "KernelLimitForConvolutionsUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    three_dim_energy_deposition_rate_matrix_at_time_point_produced: Optional[NonDicomdata] = field(
        default=None,
        metadata={
            "name": "ThreeDimEnergyDepositionRateMatrixAtTimePointProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class EnergyDepositionRateCalculationInHybridDosimetry:
    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voxel_data_activity_at_time_point_identifier: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "VoxelDataActivityAtTimePointIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    non_dicom_voxel_data_activity_at_time_point_identifier: Optional[NonDicomdata] = field(
        default=None,
        metadata={
            "name": "NonDicomVoxelDataActivityAtTimePointIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    ctrecon_resampled_on_spectused: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "CTReconResampledOnSPECTUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    three_dim_energy_deposition_rate_matrix_at_time_point_produced: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "ThreeDimEnergyDepositionRateMatrixAtTimePointProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    non_dicomthree_dim_energy_deposition_rate_matrix_at_time_point_produced: Optional[NonDicomdata] = field(
        default=None,
        metadata={
            "name": "NonDICOMThreeDimEnergyDepositionRateMatrixAtTimePointProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class EnergyDepositionRatePerVoiatTimePointContainer:
    class Meta:
        name = "EnergyDepositionRatePerVOIAtTimePointContainer"

    energy_deposition_rate_per_voiat_time_point: List[EnergyDepositionRatePerVoiatTimePoint] = field(
        default_factory=list,
        metadata={
            "name": "EnergyDepositionRatePerVOIAtTimePoint",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class HotInsertContainer:
    hot_insert: List[HotInsert] = field(
        default_factory=list,
        metadata={
            "name": "HotInsert",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class MassPerVoiatTimePointContainer:
    class Meta:
        name = "MassPerVOIAtTimePointContainer"

    mass_per_voiat_time_point_produced: List[MassPerVoiatTimePoint] = field(
        default_factory=list,
        metadata={
            "name": "MassPerVOIAtTimePointProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class MeanAbsorbedDoseInRoicontainer:
    class Meta:
        name = "MeanAbsorbedDoseInROIcontainer"

    mean_absorbed_dose_in_roiproduced: List[MeanAbsorbedDoseInRoi] = field(
        default_factory=list,
        metadata={
            "name": "MeanAbsorbedDoseInROIProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class MeanAbsorbedDoseInVoicontainer:
    class Meta:
        name = "MeanAbsorbedDoseInVOIContainer"

    mean_absorbed_dose_in_voi: List[MeanAbsorbedDoseInVoi] = field(
        default_factory=list,
        metadata={
            "name": "MeanAbsorbedDoseInVOI",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class MeanAbsorbedDoseRateInRoicontainer:
    class Meta:
        name = "MeanAbsorbedDoseRateInROIContainer"

    mean_absorbed_dose_rate_in_roiproduced: List[MeanAbsorbedDoseRateInRoi] = field(
        default_factory=list,
        metadata={
            "name": "MeanAbsorbedDoseRateInROIProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class MeanAbsorbedDoseRateInVoicontainer:
    class Meta:
        name = "MeanAbsorbedDoseRateInVOIContainer"

    mean_absorbed_dose_rate_in_voi: List[MeanAbsorbedDoseRateInVoi] = field(
        default_factory=list,
        metadata={
            "name": "MeanAbsorbedDoseRateInVOI",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class MonteCarloMethodType:
    monte_carlo_method: Optional[str] = field(
        default=None,
        metadata={
            "name": "MonteCarloMethod",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    software_names: Optional[SoftwareNameContainer] = field(
        default=None,
        metadata={
            "name": "SoftwareNames",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    monte_carlo_method_setting: Optional[MethodSettingTypeContainer] = field(
        default=None,
        metadata={
            "name": "MonteCarloMethodSetting",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    simulated_imaging_devices: Optional[SimulatedImagingDevicesContainer] = field(
        default=None,
        metadata={
            "name": "SimulatedImagingDevices",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class NonDicomdataContainer:
    class Meta:
        name = "NonDICOMDataContainer"

    non_dicomdata: List[NonDicomdata] = field(
        default_factory=list,
        metadata={
            "name": "NonDICOMData",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class PatientOrganMassInHybridDosimetryContainer:
    patient_organ_mass_in_hybrid_dosimetry: List[PatientOrganMassInHybridDosimetry] = field(
        default_factory=list,
        metadata={
            "name": "PatientOrganMassInHybridDosimetry",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class PatientOrganMassUsedContainer:
    patient_organ_mass_used: List[PatientOrganMassIn2Ddosimetry] = field(
        default_factory=list,
        metadata={
            "name": "PatientOrganMassUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class PlanarDataCorrections:
    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    nmstatic_used: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "NMStaticUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    corrections_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "CorrectionsUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    nmstatic_corrected_produced: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "NMStaticCorrectedProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class PlanarImageCorrections:
    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    nmstatic_used: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "NMStaticUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    corrections_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "CorrectionsUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    nmstatic_corrected_produced: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "NMStaticCorrectedProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class PlanarSensitivityCalculation:
    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    roiidentifier_used: Optional[RoiidentifierContainer] = field(
        default=None,
        metadata={
            "name": "ROIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    nmstatic_corrected_used: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "NMStaticCorrectedUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    planar_calibration_coefficient_produced: Optional[CalibrationCoefficient] = field(
        default=None,
        metadata={
            "name": "PlanarCalibrationCoefficientProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class Roi:
    class Meta:
        name = "ROI"

    roiidentifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "ROIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    organ_or_tissue: Optional[OrganOrTissue] = field(
        default=None,
        metadata={
            "name": "OrganOrTissue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    dicomroicontainer: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "DICOMROIContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    non_dicomroicontainer: Optional[NonDicomdata] = field(
        default=None,
        metadata={
            "name": "NonDICOMROIContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class RoiinSpect:
    class Meta:
        name = "ROIInSPECT"

    roiidentifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "ROIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    counts_in_roivalue: Optional[float] = field(
        default=None,
        metadata={
            "name": "CountsInROIValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    counts_in_roiunit: Optional[CountsInRoiunit] = field(
        default=None,
        metadata={
            "name": "CountsInROIUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    phantom_part_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "PhantomPartIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    dicomroicontainer: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "DICOMROIContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    non_dicomroicontainer: Optional[NonDicomdata] = field(
        default=None,
        metadata={
            "name": "NonDICOMROIContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class SpectsensitivityCalculation:
    class Meta:
        name = "SPECTSensitivityCalculation"

    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiin_ctidentifier_used: Optional[VoiidentifierContainer] = field(
        default=None,
        metadata={
            "name": "VOIInCTIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiin_spectidentifier_used: Optional[VoiidentifierContainer] = field(
        default=None,
        metadata={
            "name": "VOIInSPECTIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    spectcalibration_coefficient_produced: Optional[CalibrationCoefficient] = field(
        default=None,
        metadata={
            "name": "SPECTCalibrationCoefficientProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class SegmentationMethodType:
    segmentation_method: Optional[str] = field(
        default=None,
        metadata={
            "name": "SegmentationMethod",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    software_names_used: Optional[SoftwareNameContainer] = field(
        default=None,
        metadata={
            "name": "SoftwareNamesUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    segmentation_method_setting: Optional[MethodSettingTypeContainer] = field(
        default=None,
        metadata={
            "name": "SegmentationMethodSetting",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class TimeIntegratedActivityCoefficientPerRoicontainer:
    class Meta:
        name = "TimeIntegratedActivityCoefficientPerROIcontainer"

    time_integrated_activity_coefficient_per_roiproduced: List[TimeIntegratedActivityCoefficientPerRoi] = field(
        default_factory=list,
        metadata={
            "name": "TimeIntegratedActivityCoefficientPerROIProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class TimeIntegratedActivityPerRoicontainer:
    class Meta:
        name = "TimeIntegratedActivityPerROIcontainer"

    time_integrated_activity_per_roiproduced: List[TimeIntegratedActivityPerRoi] = field(
        default_factory=list,
        metadata={
            "name": "TimeIntegratedActivityPerROIProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class TimeIntegratedActivityPerVoi:
    class Meta:
        name = "TimeIntegratedActivityPerVOI"

    residence_time_per_voivalue: Optional[float] = field(
        default=None,
        metadata={
            "name": "ResidenceTimePerVOIValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_unit: Optional[TimeUnit] = field(
        default=None,
        metadata={
            "name": "TimeUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_integrated_activity_per_voivalue: Optional[float] = field(
        default=None,
        metadata={
            "name": "TimeIntegratedActivityPerVOIValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_integrated_activity_per_voiunit: Optional[TimeIntegratedActivityPerVoiunit] = field(
        default=None,
        metadata={
            "name": "TimeIntegratedActivityPerVOIUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier_list: Optional[VoiidentifierContainer] = field(
        default=None,
        metadata={
            "name": "VOIIdentifierList",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    pkassessment_method_used: Optional[CurveFittingMethod] = field(
        default=None,
        metadata={
            "name": "PKAssessmentMethodUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class TimePointDescription:
    time_point_description_element: List[TimePointDescriptionElement] = field(
        default_factory=list,
        metadata={
            "name": "TimePointDescriptionElement",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class VoiinCt:
    class Meta:
        name = "VOIInCT"

    voiidentifier: Optional[int] = field(
        default=None,
        metadata={
            "name": "VOIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voivolume_value: Optional[float] = field(
        default=None,
        metadata={
            "name": "VOIVolumeValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voivolume_unit: Optional[VoivolumeUnit] = field(
        default=None,
        metadata={
            "name": "VOIVolumeUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    phantom_part_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "PhantomPartIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    dicomvoicontainer: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "DICOMVOIContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    non_dicomvoicontainer: Optional[NonDicomdata] = field(
        default=None,
        metadata={
            "name": "NonDICOMVOIContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class VoiinSpect:
    class Meta:
        name = "VOIInSPECT"

    voiidentifier: Optional[int] = field(
        default=None,
        metadata={
            "name": "VOIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    counts_in_voivalue: Optional[float] = field(
        default=None,
        metadata={
            "name": "CountsInVOIValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    counts_in_voiunit: Optional[CountsInVoiunit] = field(
        default=None,
        metadata={
            "name": "CountsInVOIUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    phantom_part_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "PhantomPartIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    dicomvoicontainer: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "DICOMVOIContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    non_dicomvoicontainer: Optional[NonDicomdata] = field(
        default=None,
        metadata={
            "name": "NonDICOMVOIContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class VoxelBasedDistributionOfAbsorbedDoseType:
    voxel_based_distribution_of_absorbed_dose_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "VoxelBasedDistributionOfAbsorbedDoseIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voxel_based_distribution_of_absorbed_dose_category: Optional[VoxelBasedDistributionOfAbsorbedDoseCategory] = field(
        default=None,
        metadata={
            "name": "VoxelBasedDistributionOfAbsorbedDoseCategory",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    absorbed_dose_unit: Optional[AbsorbedDoseUnit] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseUnit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    non_dicomvoxel_based_absorbed_dose_distribution: Optional[NonDicomdata] = field(
        default=None,
        metadata={
            "name": "NonDICOMVoxelBasedAbsorbedDoseDistribution",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    dicomvoxel_based_absorbed_dose_distribution: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "DICOMVoxelBasedAbsorbedDoseDistribution",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class AbsorbedDoseCalculationInRoiin2Ddosimetry:
    class Meta:
        name = "AbsorbedDoseCalculationInROIIn2DDosimetry"

    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    roiidentifier_used: Optional[RoiidentifierUsedContainer] = field(
        default=None,
        metadata={
            "name": "ROIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    organ_svalue_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "OrganSValueUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    patient_organ_mass_used: Optional[PatientOrganMassInHybridDosimetry] = field(
        default=None,
        metadata={
            "name": "PatientOrganMassUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    organ_mass_reference: Optional[OrganMass] = field(
        default=None,
        metadata={
            "name": "OrganMassReference",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    mean_absorbed_dose_in_roiproduced: Optional[MeanAbsorbedDoseInRoicontainer] = field(
        default=None,
        metadata={
            "name": "MeanAbsorbedDoseInROIProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class AbsorbedDoseCalculationInRoiinHybridDosimetry:
    class Meta:
        name = "AbsorbedDoseCalculationInROIInHybridDosimetry"

    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    roiidentifier_used: Optional[RoiidentifierContainer] = field(
        default=None,
        metadata={
            "name": "ROIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    organ_svalue_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "OrganSValueUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    patient_organ_mass_used: Optional[PatientOrganMassInHybridDosimetry] = field(
        default=None,
        metadata={
            "name": "PatientOrganMassUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    organ_mass_reference: Optional[OrganMass] = field(
        default=None,
        metadata={
            "name": "OrganMassReference",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    mean_absorbed_dose_in_voiproduced: Optional[MeanAbsorbedDoseInVoicontainer] = field(
        default=None,
        metadata={
            "name": "MeanAbsorbedDoseInVOIProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class AbsorbedDoseCalculationInVoi:
    class Meta:
        name = "AbsorbedDoseCalculationInVOI"

    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    segmentation_identifier_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "SegmentationIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    absorbed_dose_calculation_method_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseCalculationMethodUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voxel_absorbed_dose_map_produced: Optional[NonDicomdataContainer] = field(
        default=None,
        metadata={
            "name": "VoxelAbsorbedDoseMapProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    absorbed_dose_in_voicontainer: Optional[AbsorbedDoseInVoicontainer] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseInVOIContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class AbsorbedDoseRateCalculationIn2Ddosimetry:
    class Meta:
        name = "AbsorbedDoseRateCalculationIn2DDosimetry"

    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    roiidentifier_used: List[str] = field(
        default_factory=list,
        metadata={
            "name": "ROIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )
    pre_administered_activity_used: Optional[AdministeredActivity] = field(
        default=None,
        metadata={
            "name": "PreAdministeredActivityUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    post_administered_activity_used: Optional[AdministeredActivity] = field(
        default=None,
        metadata={
            "name": "PostAdministeredActivityUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    organ_svalue_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "OrganSValueUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    reference_organ_mass_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "ReferenceOrganMassUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    patient_organ_mass_used: Optional[PatientOrganMassUsedContainer] = field(
        default=None,
        metadata={
            "name": "PatientOrganMassUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    mean_absorbed_dose_rate_in_roiproduced: Optional[MeanAbsorbedDoseRateInRoicontainer] = field(
        default=None,
        metadata={
            "name": "MeanAbsorbedDoseRateInROIProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class AbsorbedDoseRateCalculationInHybridDosimetry:
    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier_used: Optional[VoiidentifierContainer] = field(
        default=None,
        metadata={
            "name": "VOIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    pre_administered_activity_used: Optional[AdministeredActivity] = field(
        default=None,
        metadata={
            "name": "PreAdministeredActivityUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    post_administered_activity_used: Optional[AdministeredActivity] = field(
        default=None,
        metadata={
            "name": "PostAdministeredActivityUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    patient_organ_mass_used: Optional[PatientOrganMassInHybridDosimetryContainer] = field(
        default=None,
        metadata={
            "name": "PatientOrganMassUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    mean_absorbed_dose_rate_in_voiproduced: Optional[MeanAbsorbedDoseRateInVoicontainer] = field(
        default=None,
        metadata={
            "name": "MeanAbsorbedDoseRateInVOIProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class ActivityScaling:
    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier_used: Optional[TimePointIdentifierContainer] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier_used: Optional[VoiidentifierContainer] = field(
        default=None,
        metadata={
            "name": "VOIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    roiidentifier_used: Optional[RoiidentifierContainer] = field(
        default=None,
        metadata={
            "name": "ROIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    data_activity_per_roiat_time_point: Optional[DataActivityPerRoiatTimePointContainer] = field(
        default=None,
        metadata={
            "name": "DataActivityPerROIAtTimePoint",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class CtnumberCalibrationCurve:
    class Meta:
        name = "CTNumberCalibrationCurve"

    elements_of_ctnumber_calibration_curve: Optional[ElementsOfCtnumberCalibrationCurve] = field(
        default=None,
        metadata={
            "name": "ElementsOfCTNumberCalibrationCurve",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    reference_calibration_date: Optional[str] = field(
        default=None,
        metadata={
            "name": "ReferenceCalibrationDate",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class CalculationOfAbsorbedDosesInVois:
    class Meta:
        name = "CalculationOfAbsorbedDosesInVOIs"

    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voxel_based_distribution_of_absorbed_dose_identifier_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "VoxelBasedDistributionOfAbsorbedDoseIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier_used: Optional[VoiidentifierContainer] = field(
        default=None,
        metadata={
            "name": "VOIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    absorbed_dose_per_voiproduced: Optional[AbsorbedDosePerVoitypeContainer] = field(
        default=None,
        metadata={
            "name": "AbsorbedDosePerVOIProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class CalculationOfVoxelMap:
    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    attenuator_used: Optional[AttenuatorTypeContainer] = field(
        default=None,
        metadata={
            "name": "AttenuatorUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    monte_carlo_method_used: Optional[MonteCarloMethodType] = field(
        default=None,
        metadata={
            "name": "MonteCarloMethodUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    dicomctimage_data_used: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "DICOMCTImageDataUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voxel_based_distribution_of_absorbed_dose_produced: Optional[VoxelBasedDistributionOfAbsorbedDoseType] = field(
        default=None,
        metadata={
            "name": "VoxelBasedDistributionOfAbsorbedDoseProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class DoseRateCurveFitVoitimeIntegration:
    class Meta:
        name = "DoseRateCurveFitVOITimeIntegration"

    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier_used: Optional[VoiidentifierContainer] = field(
        default=None,
        metadata={
            "name": "VOIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier_used: Optional[TimePointIdentifierUsedContainer] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    pkassessment_method_used: Optional[CurveFittingMethod] = field(
        default=None,
        metadata={
            "name": "PKAssessmentMethodUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    pre_administered_activity_used: Optional[AdministeredActivity] = field(
        default=None,
        metadata={
            "name": "PreAdministeredActivityUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    post_administered_activity_used: Optional[AdministeredActivity] = field(
        default=None,
        metadata={
            "name": "PostAdministeredActivityUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    absorbed_dose_in_voiproduced: Optional[AbsorbedDoseInVoicontainer] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseInVOIProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class GeometricalTransformation:
    geometrical_transformation_value: Optional[NonDicomdataContainer] = field(
        default=None,
        metadata={
            "name": "GeometricalTransformationValue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    transformation_type: Optional[TransformationType] = field(
        default=None,
        metadata={
            "name": "TransformationType",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    transformation_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "TransformationIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    dicomctsource_coordinate_space_used: Optional[DicomdataContainer] = field(
        default=None,
        metadata={
            "name": "DICOMCTSourceCoordinateSpaceUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    dicomctdestination_coordinate_space_used: Optional[DicomdataContainer] = field(
        default=None,
        metadata={
            "name": "DICOMCTDestinationCoordinateSpaceUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    dicomspectsource_coordinate_space_used: Optional[DicomdataContainer] = field(
        default=None,
        metadata={
            "name": "DICOMSPECTSourceCoordinateSpaceUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    dicomspectdestination_coordinate_space_used: Optional[DicomdataContainer] = field(
        default=None,
        metadata={
            "name": "DICOMSPECTDestinationCoordinateSpaceUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    non_dicomsource_coordinate_space_used: Optional[NonDicomdataContainer] = field(
        default=None,
        metadata={
            "name": "NonDICOMSourceCoordinateSpaceUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    non_dicomdestination_coordinate_space_used: Optional[NonDicomdataContainer] = field(
        default=None,
        metadata={
            "name": "NonDICOMDestinationCoordinateSpaceUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class Nmphantom:
    class Meta:
        name = "NMPhantom"

    nmphantom_name: Optional[str] = field(
        default=None,
        metadata={
            "name": "NMPhantomName",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    nmphantom_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "NMPhantomIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    hot_insert_container: Optional[HotInsertContainer] = field(
        default=None,
        metadata={
            "name": "HotInsertContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    cold_insert_container: Optional[ColdInsertContainer] = field(
        default=None,
        metadata={
            "name": "ColdInsertContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    tank: Optional[Tank] = field(
        default=None,
        metadata={
            "name": "Tank",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class PlanarDataAcquisition:
    time_point_description: Optional[TimePointDescription] = field(
        default=None,
        metadata={
            "name": "TimePointDescription",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    nmrelevant_calibration_reference: Optional[NmrelevantCalibrationReference] = field(
        default=None,
        metadata={
            "name": "NMRelevantCalibrationReference",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    nmstatic_produced: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "NMStaticProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    non_dicom_nmstatic_produced: Optional[NonDicomdata] = field(
        default=None,
        metadata={
            "name": "NonDicomNMStaticProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class PlanarImageSegmentation:
    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    nmstatic_corrected_used: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "NMStaticCorrectedUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    segmentation_method_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "SegmentationMethodUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    roiin_spectproduced: List[RoiinSpect] = field(
        default_factory=list,
        metadata={
            "name": "ROIInSPECTProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class RoiplanarActivityDetermination:
    class Meta:
        name = "ROIPlanarActivityDetermination"

    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    roiidentifier_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "ROIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    planar_calibration_factor_reference: Optional[NmrelevantCalibrationReference] = field(
        default=None,
        metadata={
            "name": "PlanarCalibrationFactorReference",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    data_activity_per_roiat_time_point_container: Optional[DataActivityPerRoiatTimePointContainer] = field(
        default=None,
        metadata={
            "name": "DataActivityPerROIAtTimePointContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class Roicontainer:
    class Meta:
        name = "ROIcontainer"

    roi: List[Roi] = field(
        default_factory=list,
        metadata={
            "name": "ROI",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class SpectacqCtacqAndReconstruction:
    class Meta:
        name = "SPECTAcqCTAcqAndReconstruction"

    time_point_description: Optional[TimePointDescription] = field(
        default=None,
        metadata={
            "name": "TimePointDescription",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    nmrelevant_calibration_reference: Optional[NmrelevantCalibrationReference] = field(
        default=None,
        metadata={
            "name": "NMRelevantCalibrationReference",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    ctrelevant_calibration_reference: Optional[CtrelevantCalibrationReference] = field(
        default=None,
        metadata={
            "name": "CTRelevantCalibrationReference",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    nmtomo_produced: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "NMTomoProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    ctrecon_produced: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "CTReconProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class SpectrecoveryCoefficientCurveInCalibration:
    class Meta:
        name = "SPECTRecoveryCoefficientCurveInCalibration"

    elements_of_spectrecovery_coefficient_curve: Optional[ElementOfSpectrecoveryCoefficientCurveContainer] = field(
        default=None,
        metadata={
            "name": "ElementsOfSPECTRecoveryCoefficientCurve",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    reference_calibration_date: Optional[str] = field(
        default=None,
        metadata={
            "name": "ReferenceCalibrationDate",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    isotope: Optional[Isotope] = field(
        default=None,
        metadata={
            "name": "Isotope",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class SumAndScalingAbsorbedDoseRateCalculation:
    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier_used: Optional[VoiidentifierContainer] = field(
        default=None,
        metadata={
            "name": "VOIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    time_point_identifier_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    segmentation_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "SegmentationUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    three_dim_energy_deposition_rate_matrix_at_time_point_used: Optional[NonDicomdata] = field(
        default=None,
        metadata={
            "name": "ThreeDimEnergyDepositionRateMatrixAtTimePointUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    absorbed_dose_rate_per_voiat_time_point_produced: Optional[AbsorbedDoseRatePerVoiatTimePointContainer] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseRatePerVOIAtTimePointProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class SumAndScalingEnergyDepositionRate:
    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier_used: Optional[TimePointIdentifierContainer] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    roiidentifier_used: Optional[RoiidentifierContainer] = field(
        default=None,
        metadata={
            "name": "ROIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiin_spectidentifier_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "VOIInSPECTIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    energy_deposition_rate_per_voiat_time_point_produced: Optional[EnergyDepositionRatePerVoiatTimePointContainer] = field(
        default=None,
        metadata={
            "name": "EnergyDepositionRatePerVOIAtTimePointProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class TimeAbsorbedDoseCurveFit:
    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    roiidentifier_used: Optional[RoiidentifierUsedContainer] = field(
        default=None,
        metadata={
            "name": "ROIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier_used_container: Optional[TimePointIdentifierUsedContainer] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifierUsedContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    pkassessment_method_used: Optional[CurveFittingMethod] = field(
        default=None,
        metadata={
            "name": "PKAssessmentMethodUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    mean_absorbed_dose_in_roicontainer: Optional[MeanAbsorbedDoseInRoicontainer] = field(
        default=None,
        metadata={
            "name": "MeanAbsorbedDoseInROIcontainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class TimeAbsorbedDoseRateCurveFit:
    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier_used: Optional[VoiidentifierContainer] = field(
        default=None,
        metadata={
            "name": "VOIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier_used: Optional[TimePointIdentifierContainer] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    pkassessment_method_used: Optional[CurveFittingMethod] = field(
        default=None,
        metadata={
            "name": "PKAssessmentMethodUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    mean_absorbed_dose_in_voiproduced: Optional[MeanAbsorbedDoseInVoicontainer] = field(
        default=None,
        metadata={
            "name": "MeanAbsorbedDoseInVOIProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class TimeActivityCurveFit:
    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    roiidentifier_used: Optional[RoiidentifierUsedContainer] = field(
        default=None,
        metadata={
            "name": "ROIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier_used_container: Optional[TimePointIdentifierUsedContainer] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifierUsedContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    pre_administered_activity_used: Optional[AdministeredActivity] = field(
        default=None,
        metadata={
            "name": "PreAdministeredActivityUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    post_administered_activity_used: Optional[AdministeredActivity] = field(
        default=None,
        metadata={
            "name": "PostAdministeredActivityUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    pkassessment_method_used: Optional[CurveFittingMethod] = field(
        default=None,
        metadata={
            "name": "PKAssessmentMethodUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_integrated_activity_coefficient_per_roicontainer: Optional[TimeIntegratedActivityCoefficientPerRoicontainer] = field(
        default=None,
        metadata={
            "name": "TimeIntegratedActivityCoefficientPerROIcontainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_integrated_activity_per_roicontainer: Optional[TimeIntegratedActivityPerRoicontainer] = field(
        default=None,
        metadata={
            "name": "TimeIntegratedActivityPerROIcontainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class TimeActivityCurveFitIn3Ddosimetry:
    class Meta:
        name = "TimeActivityCurveFitIn3DDosimetry"

    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiidentifier_used: Optional[int] = field(
        default=None,
        metadata={
            "name": "VOIIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    segmentation_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "SegmentationIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    time_point_identifier_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    pre_administered_activity_used: Optional[AdministeredActivity] = field(
        default=None,
        metadata={
            "name": "PreAdministeredActivityUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    post_administered_activity_used: Optional[AdministeredActivity] = field(
        default=None,
        metadata={
            "name": "PostAdministeredActivityUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    pkassessment_method_used: Optional[CurveFittingMethod] = field(
        default=None,
        metadata={
            "name": "PKAssessmentMethodUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    time_integrated_activity_coefficient_per_voiproduced: Optional[TimeIntegratedActivityCoefficientPerVoi] = field(
        default=None,
        metadata={
            "name": "TimeIntegratedActivityCoefficientPerVOIProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    time_integrated_activity_per_voiproduced: Optional[TimeIntegratedActivityPerVoi] = field(
        default=None,
        metadata={
            "name": "TimeIntegratedActivityPerVOIProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class TimeIntegratedActivityPerVoicontainer:
    class Meta:
        name = "TimeIntegratedActivityPerVOIContainer"

    time_integrated_activity_per_voi: List[TimeIntegratedActivityPerVoi] = field(
        default_factory=list,
        metadata={
            "name": "TimeIntegratedActivityPerVOI",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class Voi:
    class Meta:
        name = "VOI"

    voiidentifier: Optional[int] = field(
        default=None,
        metadata={
            "name": "VOIIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    organ_or_tissue: Optional[OrganOrTissue] = field(
        default=None,
        metadata={
            "name": "OrganOrTissue",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    dicomdata_container: Optional[DicomdataContainer] = field(
        default=None,
        metadata={
            "name": "DICOMDataContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    non_dicomdata_container: Optional[NonDicomdataContainer] = field(
        default=None,
        metadata={
            "name": "NonDICOMDataContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    time_point_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    organ_mass: Optional[OrganMass] = field(
        default=None,
        metadata={
            "name": "OrganMass",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class VoiactivityDetermination:
    class Meta:
        name = "VOIActivityDetermination"

    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    segmentation_identifier_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "SegmentationIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    spectcalibration_factor_reference_used: Optional[NmrelevantCalibrationReference] = field(
        default=None,
        metadata={
            "name": "SPECTCalibrationFactorReferenceUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    spectrecovery_coefficient_curve_reference_used: Optional[NmrelevantCalibrationReference] = field(
        default=None,
        metadata={
            "name": "SPECTRecoveryCoefficientCurveReferenceUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    data_activity_per_voiat_time_point_container: Optional[DataActivityPerVoiatTimePointContainer] = field(
        default=None,
        metadata={
            "name": "DataActivityPerVOIAtTimePointContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    voxel_activity_map_produced: Optional[NonDicomdataContainer] = field(
        default=None,
        metadata={
            "name": "VoxelActivityMapProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class VoiinCtcontainer:
    class Meta:
        name = "VOIInCTcontainer"

    voiin_ct: List[VoiinCt] = field(
        default_factory=list,
        metadata={
            "name": "VOIInCT",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class VoiinSpectcontainer:
    class Meta:
        name = "VOIInSPECTcontainer"

    voiin_spect: List[VoiinSpect] = field(
        default_factory=list,
        metadata={
            "name": "VOIInSPECT",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class AbsorbedDoseRateCalculationIn2DdosimetryContainer:
    class Meta:
        name = "AbsorbedDoseRateCalculationIn2DDosimetryContainer"

    absorbed_dose_rate_calculation_in2_ddosimetry: List[AbsorbedDoseRateCalculationIn2Ddosimetry] = field(
        default_factory=list,
        metadata={
            "name": "AbsorbedDoseRateCalculationIn2DDosimetry",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class CtsegmentationInCalibration:
    class Meta:
        name = "CTSegmentationInCalibration"

    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    nmtomo_recon_used: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "NMTomoReconUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    ctrecon_used: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "CTReconUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    segmentation_method_method_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "SegmentationMethodMethodUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiin_ctproduced: Optional[VoiinCtcontainer] = field(
        default=None,
        metadata={
            "name": "VOIInCTProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiin_spectproduced: Optional[VoiinSpectcontainer] = field(
        default=None,
        metadata={
            "name": "VOIInSPECTProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class CalculationOfCtnumberCalibrationCurve:
    class Meta:
        name = "CalculationOfCTNumberCalibrationCurve"

    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    ctrecon_used: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "CTReconUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    ctnumber_calibration_curve_produced: Optional[CtnumberCalibrationCurve] = field(
        default=None,
        metadata={
            "name": "CTNumberCalibrationCurveProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class DoseRateCurveFitVoitimeIntegrationContainer:
    class Meta:
        name = "DoseRateCurveFitVOITimeIntegrationContainer"

    dose_rate_curve_fit_voitime_integration: List[DoseRateCurveFitVoitimeIntegration] = field(
        default_factory=list,
        metadata={
            "name": "DoseRateCurveFitVOITimeIntegration",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class GeometricalTransformationContainer:
    geometrical_transformation: List[GeometricalTransformation] = field(
        default_factory=list,
        metadata={
            "name": "GeometricalTransformation",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class HybridDosimetryPerTimePoint:
    energy_deposition_rate_calculation_in_hybrid_dosimetry: Optional[EnergyDepositionRateCalculationInHybridDosimetry] = field(
        default=None,
        metadata={
            "name": "EnergyDepositionRateCalculationInHybridDosimetry",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    sum_and_scaling_energy_deposition_rate: Optional[SumAndScalingEnergyDepositionRate] = field(
        default=None,
        metadata={
            "name": "SumAndScalingEnergyDepositionRate",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    absorbed_dose_rate_calculation_in_hybrid_dosimetry: Optional[AbsorbedDoseRateCalculationInHybridDosimetry] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseRateCalculationInHybridDosimetry",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class HybridDosimetryViaTimeActivityCurveFit:
    activity_scaling: Optional[ActivityScaling] = field(
        default=None,
        metadata={
            "name": "ActivityScaling",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_activity_curve_fit: Optional[TimeActivityCurveFit] = field(
        default=None,
        metadata={
            "name": "TimeActivityCurveFit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    absorbed_dose_calculation_in_roiin_hybrid_dosimetry: Optional[AbsorbedDoseCalculationInRoiinHybridDosimetry] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseCalculationInROIInHybridDosimetry",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class PlanarAcquisition:
    phantom_used: Optional[Nmphantom] = field(
        default=None,
        metadata={
            "name": "PhantomUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    nmstatic_produced: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "NMStaticProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class SpectacqCtacqAndReconstructionContainer:
    class Meta:
        name = "SPECTAcqCTAcqAndReconstructionContainer"

    spectacq_ctacq_and_reconstruction: List[SpectacqCtacqAndReconstruction] = field(
        default_factory=list,
        metadata={
            "name": "SPECTAcqCTAcqAndReconstruction",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class SpectacqCtacqAndReconstructionInCalibration:
    class Meta:
        name = "SPECTAcqCTAcqAndReconstructionInCalibration"

    phantom_used: Optional[Nmphantom] = field(
        default=None,
        metadata={
            "name": "PhantomUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    nmtomo_produced: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "NMTomoProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    ctrecon_produced: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "CTReconProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class Spectreconstruction:
    class Meta:
        name = "SPECTReconstruction"

    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    nmtomo_used: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "NMTomoUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    ctrecon_used: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "CTReconUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    reconstruction_method_and_corrections_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "ReconstructionMethodAndCorrectionsUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    ctnumber_calibration_curve_used: Optional[CtnumberCalibrationCurve] = field(
        default=None,
        metadata={
            "name": "CTNumberCalibrationCurveUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    ctnumber_calibration_curve_reference: Optional[CtnumberCalibrationCurveReference] = field(
        default=None,
        metadata={
            "name": "CTNumberCalibrationCurveReference",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    nmtomo_recon_produced: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "NMTomoReconProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class SpectreconstructionInCalibration:
    class Meta:
        name = "SPECTReconstructionInCalibration"

    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    nmtomo_used: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "NMTomoUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    ctrecon_used: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "CTReconUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    reconstruction_method_and_corrections_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "ReconstructionMethodAndCorrectionsUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    ctnumber_calibration_curve_used: Optional[CtnumberCalibrationCurve] = field(
        default=None,
        metadata={
            "name": "CTNumberCalibrationCurveUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    ctnumber_calibration_curve_reference: Optional[CtnumberCalibrationCurveReference] = field(
        default=None,
        metadata={
            "name": "CTNumberCalibrationCurveReference",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    nmtomo_recon_produced: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "NMTomoReconProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class SpectrecoveryCoefficientCurveCalculation:
    class Meta:
        name = "SPECTRecoveryCoefficientCurveCalculation"

    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiin_ctidentifier_used: Optional[VoiidentifierContainer] = field(
        default=None,
        metadata={
            "name": "VOIInCTIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiin_spectidentifier_used: Optional[VoiidentifierContainer] = field(
        default=None,
        metadata={
            "name": "VOIInSPECTIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    phantom_identifier_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "PhantomIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    spectcalibration_coefficient_reference_used: Optional[NmrelevantCalibrationReference] = field(
        default=None,
        metadata={
            "name": "SPECTCalibrationCoefficientReferenceUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    spectrecovery_coefficient_curve_in_calibration_produced: Optional[SpectrecoveryCoefficientCurveInCalibration] = field(
        default=None,
        metadata={
            "name": "SPECTRecoveryCoefficientCurveInCalibrationProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class SimpleCtmonteCarloDosimetry:
    class Meta:
        name = "SimpleCTMonteCarloDosimetry"

    calculation_of_voxel_map: Optional[CalculationOfVoxelMap] = field(
        default=None,
        metadata={
            "name": "CalculationOfVoxelMap",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    calculation_of_absorbed_doses_in_vois: Optional[CalculationOfAbsorbedDosesInVois] = field(
        default=None,
        metadata={
            "name": "CalculationOfAbsorbedDosesInVOIs",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class SumAndScalingAbsorbedDoseRateCalculationContainer:
    sum_and_scaling_absorbed_dose_rate_calculation: List[SumAndScalingAbsorbedDoseRateCalculation] = field(
        default_factory=list,
        metadata={
            "name": "SumAndScalingAbsorbedDoseRateCalculation",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class TimeActivityCurveFitIn3DdosimetryContainer:
    class Meta:
        name = "TimeActivityCurveFitIn3DDosimetryContainer"

    time_activity_curve_fit_in3_ddosimetry: List[TimeActivityCurveFitIn3Ddosimetry] = field(
        default_factory=list,
        metadata={
            "name": "TimeActivityCurveFitIn3DDosimetry",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class TwoDimDosimetryViaTimeActivityCurveFit:
    time_activity_curve_fit: Optional[TimeActivityCurveFit] = field(
        default=None,
        metadata={
            "name": "TimeActivityCurveFit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    absorbed_dose_calculation_in_roiin2_ddosimetry: Optional[AbsorbedDoseCalculationInRoiin2Ddosimetry] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseCalculationInROIIn2DDosimetry",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class VoiactivityDeterminationContainer:
    class Meta:
        name = "VOIActivityDeterminationContainer"

    voiactivity_determination: List[VoiactivityDetermination] = field(
        default_factory=list,
        metadata={
            "name": "VOIActivityDetermination",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class Voilist:
    class Meta:
        name = "VOIList"

    voi: List[Voi] = field(
        default_factory=list,
        metadata={
            "name": "VOI",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class VoiProducedContainer:
    voiproduced: List[Voi] = field(
        default_factory=list,
        metadata={
            "name": "VOIProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class CtnumberCalibrationWorkfow:
    class Meta:
        name = "CTNumberCalibrationWorkfow"

    ctacq_for_ctnumber_calibration_curve: Optional[CtacqForCtnumberCalibrationCurve] = field(
        default=None,
        metadata={
            "name": "CTAcqForCTNumberCalibrationCurve",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    calculation_of_ctnumber_calibration_curve: Optional[CalculationOfCtnumberCalibrationCurve] = field(
        default=None,
        metadata={
            "name": "CalculationOfCTNumberCalibrationCurve",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class Ctsegmentation:
    class Meta:
        name = "CTSegmentation"

    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    dicomimage_used: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "DICOMImageUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    segmentation_method_used: Optional[SegmentationMethodType] = field(
        default=None,
        metadata={
            "name": "SegmentationMethodUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    voidescriptor_produced: Optional[Voilist] = field(
        default=None,
        metadata={
            "name": "VOIDescriptorProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class HybridDosimetryPerTimePointContainer:
    hybrid_dosimetry_per_time_point: List[HybridDosimetryPerTimePoint] = field(
        default_factory=list,
        metadata={
            "name": "HybridDosimetryPerTimePoint",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class PlanarCalibrationWorkflow:
    planar_acquisition: Optional[PlanarAcquisition] = field(
        default=None,
        metadata={
            "name": "PlanarAcquisition",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    planar_image_corrections: Optional[PlanarImageCorrections] = field(
        default=None,
        metadata={
            "name": "PlanarImageCorrections",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    planar_image_segmentation: Optional[PlanarImageSegmentation] = field(
        default=None,
        metadata={
            "name": "PlanarImageSegmentation",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    planar_sensitivity_calculation: Optional[PlanarSensitivityCalculation] = field(
        default=None,
        metadata={
            "name": "PlanarSensitivityCalculation",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class SpectctcalibrationWorkflow:
    class Meta:
        name = "SPECTCTCalibrationWorkflow"

    spectacq_ctacq_and_reconstruction_in_calibration: Optional[SpectacqCtacqAndReconstructionInCalibration] = field(
        default=None,
        metadata={
            "name": "SPECTAcqCTAcqAndReconstructionInCalibration",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    spectreconstruction_in_calibration: Optional[SpectreconstructionInCalibration] = field(
        default=None,
        metadata={
            "name": "SPECTReconstructionInCalibration",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    ctsegmentation_in_calibration: Optional[CtsegmentationInCalibration] = field(
        default=None,
        metadata={
            "name": "CTSegmentationInCalibration",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    spectsensitivity_calculation: Optional[SpectsensitivityCalculation] = field(
        default=None,
        metadata={
            "name": "SPECTSensitivityCalculation",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    spectrecovery_coefficient_curve_calculation: Optional[SpectrecoveryCoefficientCurveCalculation] = field(
        default=None,
        metadata={
            "name": "SPECTRecoveryCoefficientCurveCalculation",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class SpectreconstructionContainer:
    class Meta:
        name = "SPECTReconstructionContainer"

    spectreconstruction: List[Spectreconstruction] = field(
        default_factory=list,
        metadata={
            "name": "SPECTReconstruction",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class Segmentation:
    segmentation_identifier: Optional[str] = field(
        default=None,
        metadata={
            "name": "SegmentationIdentifier",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voicontainer: Optional[VoiProducedContainer] = field(
        default=None,
        metadata={
            "name": "VOIContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    dicomdata_container: Optional[DicomdataContainer] = field(
        default=None,
        metadata={
            "name": "DICOMDataContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    non_dicomdata_container: Optional[NonDicomdataContainer] = field(
        default=None,
        metadata={
            "name": "NonDICOMDataContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class TwoDimDosimetryViaAbsorbedDoseRateCalculation:
    absorbed_dose_rate_calculation_in2_ddosimetry_container: Optional[AbsorbedDoseRateCalculationIn2DdosimetryContainer] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseRateCalculationIn2DDosimetryContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_absorbed_dose_curve_fit: Optional[TimeAbsorbedDoseCurveFit] = field(
        default=None,
        metadata={
            "name": "TimeAbsorbedDoseCurveFit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class CtcalibrationWorkflow:
    class Meta:
        name = "CTCalibrationWorkflow"

    ctnumber_calibration_workfow: Optional[CtnumberCalibrationWorkfow] = field(
        default=None,
        metadata={
            "name": "CTNumberCalibrationWorkfow",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    spectreconstruction_in_calibration: Optional[SpectreconstructionInCalibration] = field(
        default=None,
        metadata={
            "name": "SPECTReconstructionInCalibration",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    ctsegmentation_in_calibration: Optional[CtsegmentationInCalibration] = field(
        default=None,
        metadata={
            "name": "CTSegmentationInCalibration",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    spectsensitivity_calculation: Optional[SpectsensitivityCalculation] = field(
        default=None,
        metadata={
            "name": "SPECTSensitivityCalculation",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    spectrecovery_coefficient_curve_calculation: Optional[SpectrecoveryCoefficientCurveCalculation] = field(
        default=None,
        metadata={
            "name": "SPECTRecoveryCoefficientCurveCalculation",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class HybridDosimetryViaAbsorbedDoseRateCalculation:
    hybrid_dosimetry_per_time_point_container: Optional[HybridDosimetryPerTimePointContainer] = field(
        default=None,
        metadata={
            "name": "HybridDosimetryPerTimePointContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_absorbed_dose_rate_curve_fit: Optional[TimeAbsorbedDoseRateCurveFit] = field(
        default=None,
        metadata={
            "name": "TimeAbsorbedDoseRateCurveFit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class PlanarDataSegmentationWithRegistrationAndPropagation:
    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    nmstatic_corrected_used: Optional[DicomdataContainer] = field(
        default=None,
        metadata={
            "name": "NMStaticCorrectedUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    non_dicomnmstatic_corrected_used: Optional[NonDicomdataContainer] = field(
        default=None,
        metadata={
            "name": "NonDICOMNMStaticCorrectedUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    segmentation_method_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "SegmentationMethodUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    counts_per_roiat_time_point_container: Optional[CountsPerRoiatTimePointContainer] = field(
        default=None,
        metadata={
            "name": "CountsPerROIAtTimePointContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    roiproduced: Optional[Roicontainer] = field(
        default=None,
        metadata={
            "name": "ROIProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    segmentation_produced: Optional[Segmentation] = field(
        default=None,
        metadata={
            "name": "SegmentationProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    nmstatic_corrected_resampled_on_common_reference_produced: Optional[DicomdataContainer] = field(
        default=None,
        metadata={
            "name": "NMStaticCorrectedResampledOnCommonReferenceProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    non_dicomnmstatic_corrected_resampled_on_common_reference_produced: Optional[NonDicomdataContainer] = field(
        default=None,
        metadata={
            "name": "NonDICOMNMStaticCorrectedResampledOnCommonReferenceProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class PlanarDataSegmentationWithoutRegistration:
    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    nmstatic_corrected_used: Optional[DicomdataContainer] = field(
        default=None,
        metadata={
            "name": "NMStaticCorrectedUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    non_dicomnmstatic_corrected_used: Optional[NonDicomdataContainer] = field(
        default=None,
        metadata={
            "name": "NonDICOMNMStaticCorrectedUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    segmentation_method_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "SegmentationMethodUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    counts_per_roiat_time_point_container: Optional[CountsPerRoiatTimePointContainer] = field(
        default=None,
        metadata={
            "name": "CountsPerROIAtTimePointContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    roiproduced: Optional[Roicontainer] = field(
        default=None,
        metadata={
            "name": "ROIProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    segmentation_produced: Optional[Segmentation] = field(
        default=None,
        metadata={
            "name": "SegmentationProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class RegistrationVoisegmentationAndPropagation:
    class Meta:
        name = "RegistrationVOISegmentationAndPropagation"

    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    counts_per_voiat_time_point_produced_container: Optional[CountsPerVoiatTimePointProducedContainer] = field(
        default=None,
        metadata={
            "name": "CountsPerVOIAtTimePointProducedContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    nmtomo_recon_used: Optional[DicomdataContainer] = field(
        default=None,
        metadata={
            "name": "NMTomoReconUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    ctrecon_used: Optional[DicomdataContainer] = field(
        default=None,
        metadata={
            "name": "CTReconUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    image_processing_method_method_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "ImageProcessingMethodMethodUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    segmentation: Optional[Segmentation] = field(
        default=None,
        metadata={
            "name": "Segmentation",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    ctrecon_resampled_on_common_reference_produced: Optional[DicomdataContainer] = field(
        default=None,
        metadata={
            "name": "CTReconResampledOnCommonReferenceProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    nmtomo_recon_resampled_on_common_reference_produced: Optional[DicomdataContainer] = field(
        default=None,
        metadata={
            "name": "NMTomoReconResampledOnCommonReferenceProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    non_dicomctrecon_resampled_on_common_reference_produced: Optional[NonDicomdataContainer] = field(
        default=None,
        metadata={
            "name": "NonDICOMCTReconResampledOnCommonReferenceProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    non_dicomnmtomo_recon_resampled_on_common_reference_produced: Optional[NonDicomdataContainer] = field(
        default=None,
        metadata={
            "name": "NonDICOMNMTomoReconResampledOnCommonReferenceProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    density_image_produced: Optional[NonDicomdataContainer] = field(
        default=None,
        metadata={
            "name": "DensityImageProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    geometrical_transformation_container: Optional[GeometricalTransformationContainer] = field(
        default=None,
        metadata={
            "name": "GeometricalTransformationContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class SpectdataAcquisitionAndReconstruction:
    class Meta:
        name = "SPECTDataAcquisitionAndReconstruction"

    spectacq_ctacq_and_reconstruction_container: Optional[SpectacqCtacqAndReconstructionContainer] = field(
        default=None,
        metadata={
            "name": "SPECTAcqCTAcqAndReconstructionContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    spectreconstruction_container: Optional[SpectreconstructionContainer] = field(
        default=None,
        metadata={
            "name": "SPECTReconstructionContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class VoisegmentationVoimassDetermination:
    class Meta:
        name = "VOISegmentationVOIMassDetermination"

    process_execution_context: Optional[ProcessExecutionContext] = field(
        default=None,
        metadata={
            "name": "ProcessExecutionContext",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    nmtomo_recon_used: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "NMTomoReconUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    ctrecon_used: Optional[Dicomdata] = field(
        default=None,
        metadata={
            "name": "CTReconUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voisegmentation_method_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "VOISegmentationMethodUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    counts_per_voiat_time_point_container: Optional[CountsPerVoiatTimePointContainer] = field(
        default=None,
        metadata={
            "name": "CountsPerVOIAtTimePointContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    voiproduced: Optional[VoiProducedContainer] = field(
        default=None,
        metadata={
            "name": "VOIProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    segmentation_produced: Optional[Segmentation] = field(
        default=None,
        metadata={
            "name": "SegmentationProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    ctrecon_resampled_on_nmreference_produced: Optional[DicomdataContainer] = field(
        default=None,
        metadata={
            "name": "CTReconResampledOnNMReferenceProduced",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    mass_per_voiat_time_point_container: Optional[MassPerVoiatTimePointContainer] = field(
        default=None,
        metadata={
            "name": "MassPerVOIAtTimePointContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class Wp2Subtask212WorkflowData:
    class Meta:
        name = "WP2subtask212WorkflowData"

    ctsegmentation: Optional[Ctsegmentation] = field(
        default=None,
        metadata={
            "name": "CTSegmentation",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    simple_ctmonte_carlo_dosimetry: List[SimpleCtmonteCarloDosimetry] = field(
        default_factory=list,
        metadata={
            "name": "SimpleCTMonteCarloDosimetry",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class CalibrationWorkflow:
    spectctcalibration_workflow: Optional[SpectctcalibrationWorkflow] = field(
        default=None,
        metadata={
            "name": "SPECTCTCalibrationWorkflow",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    planar_calibration_workflow: Optional[PlanarCalibrationWorkflow] = field(
        default=None,
        metadata={
            "name": "PlanarCalibrationWorkflow",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    ctcalibration_workflow: Optional[CtcalibrationWorkflow] = field(
        default=None,
        metadata={
            "name": "CTCalibrationWorkflow",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class PlanarDataAcquisitionAndProcessing:
    planar_data_acquisition: Optional[PlanarDataAcquisition] = field(
        default=None,
        metadata={
            "name": "PlanarDataAcquisition",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    planar_data_corrections: Optional[PlanarDataCorrections] = field(
        default=None,
        metadata={
            "name": "PlanarDataCorrections",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    planar_data_segmentation_without_registration: Optional[PlanarDataSegmentationWithoutRegistration] = field(
        default=None,
        metadata={
            "name": "PlanarDataSegmentationWithoutRegistration",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    planar_data_segmentation_with_registration_and_propagation: Optional[PlanarDataSegmentationWithRegistrationAndPropagation] = field(
        default=None,
        metadata={
            "name": "PlanarDataSegmentationWithRegistrationAndPropagation",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    roiplanar_activity_determination: Optional[RoiplanarActivityDetermination] = field(
        default=None,
        metadata={
            "name": "ROIPlanarActivityDetermination",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class RegistrationVoisegmentationAndPropagationContainer:
    class Meta:
        name = "RegistrationVOISegmentationAndPropagationContainer"

    registration_voisegmentation_and_propagation: List[RegistrationVoisegmentationAndPropagation] = field(
        default_factory=list,
        metadata={
            "name": "RegistrationVOISegmentationAndPropagation",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class SpectdataAcquisitionAndProcessing:
    class Meta:
        name = "SPECTDataAcquisitionAndProcessing"

    spectacq_ctacq_and_reconstruction: Optional[SpectacqCtacqAndReconstruction] = field(
        default=None,
        metadata={
            "name": "SPECTAcqCTAcqAndReconstruction",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    spectreconstruction: Optional[Spectreconstruction] = field(
        default=None,
        metadata={
            "name": "SPECTReconstruction",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    registration_voisegmentation: Optional[RegistrationVoisegmentationAndPropagation] = field(
        default=None,
        metadata={
            "name": "RegistrationVOISegmentation",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiactivity_determination: Optional[VoiactivityDetermination] = field(
        default=None,
        metadata={
            "name": "VOIActivityDetermination",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_point_identifier_used: Optional[str] = field(
        default=None,
        metadata={
            "name": "TimePointIdentifierUsed",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class VoisegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation:
    class Meta:
        name = "VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation"

    voisegmentation_voimass_determination: Optional[VoisegmentationVoimassDetermination] = field(
        default=None,
        metadata={
            "name": "VOISegmentationVOIMassDetermination",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiactivity_determination: Optional[VoiactivityDetermination] = field(
        default=None,
        metadata={
            "name": "VOIActivityDetermination",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    energy_deposition_rate_calculation_in3_ddosimetry: Optional[EnergyDepositionRateCalculationIn3Ddosimetry] = field(
        default=None,
        metadata={
            "name": "EnergyDepositionRateCalculationIn3DDosimetry",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    sum_and_scaling_absorbed_dose_rate_calculation_container: Optional[SumAndScalingAbsorbedDoseRateCalculationContainer] = field(
        default=None,
        metadata={
            "name": "SumAndScalingAbsorbedDoseRateCalculationContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )


@dataclass
class PlanarDataAcquisitionAndProcessingContainer:
    planar_data_acquisition_and_processing: List[PlanarDataAcquisitionAndProcessing] = field(
        default_factory=list,
        metadata={
            "name": "PlanarDataAcquisitionAndProcessing",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class ThreeDimDosimetrySlide1Workflow:
    class Meta:
        name = "ThreeDimDosimetrySlide1workflow"

    spectdata_acquisition_and_reconstruction: Optional[SpectdataAcquisitionAndReconstruction] = field(
        default=None,
        metadata={
            "name": "SPECTDataAcquisitionAndReconstruction",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    registration_voisegmentation_and_propagation_container: Optional[RegistrationVoisegmentationAndPropagationContainer] = field(
        default=None,
        metadata={
            "name": "RegistrationVOISegmentationAndPropagationContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voiactivity_determination_container: Optional[VoiactivityDeterminationContainer] = field(
        default=None,
        metadata={
            "name": "VOIActivityDeterminationContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    time_activity_curve_fit_in3_ddosimetry_container: Optional[TimeActivityCurveFitIn3DdosimetryContainer] = field(
        default=None,
        metadata={
            "name": "TimeActivityCurveFitIn3DDosimetryContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    absorbed_dose_calculation_in_voi: Optional[AbsorbedDoseCalculationInVoi] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseCalculationInVOI",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    radio_biological_calculation_in3_dslide1_dosimetry: Optional[RadioBiologicalCalculationIn3Dslide1Dosimetry] = field(
        default=None,
        metadata={
            "name": "RadioBiologicalCalculationIn3DSlide1Dosimetry",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class VoisegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculationContainer:
    class Meta:
        name = "VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculationContainer"

    voisegmentation_energy_deposition_calculation_absorbed_dose_rate_calculation: List[VoisegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation] = field(
        default_factory=list,
        metadata={
            "name": "VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "min_occurs": 1,
        }
    )


@dataclass
class HybridDosimetryworkflow:
    planar_data_acquisition_and_processing_container: Optional[PlanarDataAcquisitionAndProcessingContainer] = field(
        default=None,
        metadata={
            "name": "PlanarDataAcquisitionAndProcessingContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    spectdata_acquisition_and_processing: Optional[SpectdataAcquisitionAndProcessing] = field(
        default=None,
        metadata={
            "name": "SPECTDataAcquisitionAndProcessing",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    hybrid_dosimetry_via_absorbed_dose_rate_calculation: Optional[HybridDosimetryViaAbsorbedDoseRateCalculation] = field(
        default=None,
        metadata={
            "name": "HybridDosimetryViaAbsorbedDoseRateCalculation",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    hybrid_dosimetry_via_time_activity_curve_fit: Optional[HybridDosimetryViaTimeActivityCurveFit] = field(
        default=None,
        metadata={
            "name": "HybridDosimetryViaTimeActivityCurveFit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    radio_biological_calculation_in_hybrid_or3_dslide2_dosimetry: Optional[RadioBiologicalCalculationInHybridOr3Dslide2Dosimetry] = field(
        default=None,
        metadata={
            "name": "RadioBiologicalCalculationInHybridOr3DSlide2Dosimetry",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class ThreeDimDosimetrySlide2Workflow:
    class Meta:
        name = "ThreeDimDosimetrySlide2workflow"

    spectdata_acquisition_and_reconstruction: Optional[SpectdataAcquisitionAndReconstruction] = field(
        default=None,
        metadata={
            "name": "SPECTDataAcquisitionAndReconstruction",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    voisegmentation_energy_deposition_calculation_absorbed_dose_rate_calculation_container: Optional[VoisegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculationContainer] = field(
        default=None,
        metadata={
            "name": "VOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculationContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    dose_rate_curve_fit_voitime_integration_container: Optional[DoseRateCurveFitVoitimeIntegrationContainer] = field(
        default=None,
        metadata={
            "name": "DoseRateCurveFitVOITimeIntegrationContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    radio_biological_calculation_in_hybrid_or3_dslide2_dosimetry: Optional[RadioBiologicalCalculationInHybridOr3Dslide2Dosimetry] = field(
        default=None,
        metadata={
            "name": "RadioBiologicalCalculationInHybridOr3DSlide2Dosimetry",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    registration_voisegmentation_and_propagation_container: Optional[RegistrationVoisegmentationAndPropagationContainer] = field(
        default=None,
        metadata={
            "name": "RegistrationVOISegmentationAndPropagationContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    absorbed_dose_calculation_in_voi: Optional[AbsorbedDoseCalculationInVoi] = field(
        default=None,
        metadata={
            "name": "AbsorbedDoseCalculationInVOI",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class TwoDimDosimetryworkflow:
    planar_data_acquisition_and_processing_container: Optional[PlanarDataAcquisitionAndProcessingContainer] = field(
        default=None,
        metadata={
            "name": "PlanarDataAcquisitionAndProcessingContainer",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
            "required": True,
        }
    )
    two_dim_dosimetry_via_absorbed_dose_rate_calculation: Optional[TwoDimDosimetryViaAbsorbedDoseRateCalculation] = field(
        default=None,
        metadata={
            "name": "TwoDimDosimetryViaAbsorbedDoseRateCalculation",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    two_dim_dosimetry_via_time_activity_curve_fit: Optional[TwoDimDosimetryViaTimeActivityCurveFit] = field(
        default=None,
        metadata={
            "name": "TwoDimDosimetryViaTimeActivityCurveFit",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )
    radio_biological_calculation: Optional[RadioBiologicalCalculation] = field(
        default=None,
        metadata={
            "name": "RadioBiologicalCalculation",
            "type": "Element",
            "namespace": "https://www.irdbb-medirad.com",
        }
    )


@dataclass
class NonDicomFileSetDescriptor:
    class Meta:
        namespace = "https://www.irdbb-medirad.com"

    referenced_clinical_research_study: Optional[ReferencedClinicalResearchStudy] = field(
        default=None,
        metadata={
            "name": "ReferencedClinicalResearchStudy",
            "type": "Element",
            "required": True,
        }
    )
    patient_id: Optional[str] = field(
        default=None,
        metadata={
            "name": "PatientId",
            "type": "Element",
            "required": True,
        }
    )
    acquisition_settings: Optional[AcquisitionSettings] = field(
        default=None,
        metadata={
            "name": "AcquisitionSettings",
            "type": "Element",
        }
    )
    calibration_workflow: Optional[CalibrationWorkflow] = field(
        default=None,
        metadata={
            "name": "CalibrationWorkflow",
            "type": "Element",
        }
    )
    wp2subtask212_workflow_data: Optional[Wp2Subtask212WorkflowData] = field(
        default=None,
        metadata={
            "name": "WP2subtask212WorkflowData",
            "type": "Element",
        }
    )
    three_dim_dosimetry_slide1workflow: Optional[ThreeDimDosimetrySlide1Workflow] = field(
        default=None,
        metadata={
            "name": "ThreeDimDosimetrySlide1workflow",
            "type": "Element",
        }
    )
    two_dim_dosimetryworkflow: Optional[TwoDimDosimetryworkflow] = field(
        default=None,
        metadata={
            "name": "TwoDimDosimetryworkflow",
            "type": "Element",
        }
    )
    hybrid_dosimetryworkflow: Optional[HybridDosimetryworkflow] = field(
        default=None,
        metadata={
            "name": "HybridDosimetryworkflow",
            "type": "Element",
        }
    )
    three_dim_dosimetry_slide2workflow: Optional[ThreeDimDosimetrySlide2Workflow] = field(
        default=None,
        metadata={
            "name": "ThreeDimDosimetrySlide2workflow",
            "type": "Element",
        }
    )
