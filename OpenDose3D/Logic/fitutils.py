from __future__ import annotations
from abc import abstractmethod
import string
import numpy as np
from numpy import log
from scipy.integrate import quad

try:
    import lmfit

    LMFIT_WORKING = True
except:
    from scipy.optimize.minpack import curve_fit

    LMFIT_WORKING = False
    print("lmfit package not properly installed, using scipy curve_fit...")

from Logic.errors import ConventionError

if LMFIT_WORKING:
    # Methods not listed here are not working in our user-case, for a complete list see lmfit.minimizer.minimize function
    MINIMIZER_METHODS = {
        "leastsq": "Levenberg-Marquardt (default)",
        "least_squares": "Least-Squares minimization trust-region",
        "differential_evolution": "differential evolution",
        "brute": "brute force",
        "basinhopping": "basinhopping",
        **lmfit.minimizer.SCALAR_METHODS,
    }
else:
    MINIMIZER_METHODS = {"leastsq": "Levenberg-Marquardt (default)"}

MINIMIZER_METHODS_INV = dict([(v, k) for k, v in MINIMIZER_METHODS.items()])


class FitFunction:

    def __init__(self, x: np.ndarray, y: np.ndarray, lamda: float, method="leastsq"):
        self.x = x
        self.y = y
        self.height = y.max()
        self.width = x.max()

        if method not in MINIMIZER_METHODS:
            raise ImportError("method not valid")

        self.method = method
        self.ln2 = log(2)
        self.T_phys = log(2) / lamda

        if LMFIT_WORKING:
            self.parameters = self.initParameters()
            self.fitter = lmfit.Minimizer(
                self.residuals,
                self.parameters,
                fcn_args=(self.x,),
                fcn_kws={"data": self.y},
            )
        else:
            self.parameters = {}
            self.initParametersNoLMFit()

        self.res: np.ndarray = None
        self.report = ""

    def residuals(
        self,
        parameters: lmfit.Parameters | dict,
        x: np.ndarray,
        data: np.ndarray = None,
    ) -> np.ndarray:
        if LMFIT_WORKING:
            model = self.model(parameters, x, data)
        else:
            p = tuple(parameters.values())
            model = self.function(x, *p)

        if data is None:
            return model
        return model - data

    @abstractmethod
    def initParameters(self) -> lmfit.Parameters:
        pass

    @abstractmethod
    def initParametersNoLMFit(self) -> dict:
        pass

    @abstractmethod
    def model(
        self,
        parameters: lmfit.Parameters | dict,
        x: np.ndarray,
        data: np.ndarray = None,
    ):
        # model compatible with leastsq
        pass

    @abstractmethod
    def dmodel(
        self,
        parameters: lmfit.Parameters | dict,
        x: np.ndarray,
        data: np.ndarray = None,
    ):
        # jacobian compatible with leastsq
        pass

    @abstractmethod
    def function(self, x: np.ndarray, *args):
        # function definition y = f(x)
        pass

    @abstractmethod
    def jacobian(self, x: np.ndarray, *args):
        # jacobian definition
        pass

    def getParametersDict(self) -> dict:
        if LMFIT_WORKING:
            return self.parameters.valuesdict()
        else:
            return self.parameters

    def getParametersTuple(self, parameters: lmfit.Parameters | dict | None) -> tuple:
        if parameters is None:
            return tuple(self.getParametersDict().values())            
        elif LMFIT_WORKING:
            return tuple(parameters.valuesdict().values())
        else:
            return tuple(parameters.values())
        
    def getErrorTuple(self) -> tuple:
        return tuple(param.stderr for param in self.parameters.values() if param.stderr is not None)
        
    def getParameters(self) -> tuple:
        return self.getParametersTuple(None)

    def minimize(self, useJacobian=False):
        if LMFIT_WORKING:
            if useJacobian:
                if self.method in ["leastsq"]:
                    kws = {"Dfun": self.dmodel, "col_deriv": 1}
                elif self.method in [*tuple(lmfit.minimizer.SCALAR_METHODS.keys())]:
                    kws = {"jac": self.jacobian}
                else:
                    kws = {}
            else:
                kws = {}
            self.res = self.fitter.minimize(method=self.method, **kws)
            self.parameters = self.res.params
            self.report = lmfit.fit_report(self.res)
        else:
            fit, _ = curve_fit(self.function, self.x, self.y, self.parameters)
            self.parameters = dict(zip(string.ascii_lowercase, fit))
            self.report = ""

    def evaluate(self, newx: np.ndarray = None) -> np.ndarray:
        if newx is None:
            lnewx = np.array(self.x)
        else:
            lnewx = np.array(newx)

        return np.array(self.residuals(self.parameters, lnewx))

    def plot(self, x: np.ndarray) -> np.ndarray:
        p = self.getParameters()
        plt = self.function(x, *p)
        return plt

    def integrate(self, loBound: float, hiBound: float) -> float:
        return quad(lambda x: self.residuals(self.parameters, x), loBound, hiBound)
    
    def model_dict(self) -> dict:
        # Extract the covariance matrix and calculate the correlation matrix
        covar_matrix = self.res.covar
        if covar_matrix is not None:
            std_devs = np.sqrt(np.diag(covar_matrix))  # Standard deviations of parameters
            correlation = covar_matrix / np.outer(std_devs, std_devs)  # Normalize covariance matrix to get correlation matrix
        else:
            correlation = "Covariance matrix is not available. Ensure the fit converged properly."

        # Extract off-diagonal elements of the correlation matrix
        if isinstance(correlation, np.ndarray):
            filters = correlation != np.diag(correlation)
            corr_list = correlation[filters]
        else:
            corr_list = []

        # Extract parameters, AUC, and their standard deviations
        params = self.getParameters()
        AUC = self.AUC()
        try:
            params_sd = self.getErrorTuple()
            AUC_SD = self.AUC_SD()
        except (np.linalg.LinAlgError, ValueError, RuntimeWarning):
            params_sd = np.full(len(params), np.inf)
            AUC_SD = np.inf
            error_msg = (
                "Inverse Hessian is singular; covariance cannot be calculated."
                "\nConsider using different initial values."
            )
            fitting_summary = [error_msg]
        else:
            fitting_summary = []

        # Calculate Coefficient of Variation (CV)
        CV = [100 * param_sd / param for param, param_sd in zip(params, params_sd)]

        # Compile a report summary for the fitting process
        fitting_summary.append(self.__repr__())
        for i, (param, param_sd, cv) in enumerate(zip(params, params_sd, CV)):
            report_params = f"p{i + 1} = {param:.4f} +- {param_sd:.4f} (CV: {cv:.2f}%)"
            fitting_summary.append(report_params)

        AIC = self.res.aic  # AIC
        n = len(self.y)  # Number of data points
        k = len(params)  # Number of fitted parameters

        # Compute AICc
        try:
            AICc = AIC + (2 * k * (k + 1)) / (n - k - 1)
        except ZeroDivisionError:
            AICc = "Time Point doesn't match for AICc calculation"

        # Add overall report details
        report = (
            f"AICc = {AICc}\n"
            f"BIC = {self.res.bic}\n"
            f"Correlation Matrix = \n{correlation if isinstance(correlation, np.ndarray) else correlation}\n"
        )
        fitting_summary.append(report)

        # Create the model dictionary
        mdict = {
            "function": self.__repr__(),
            "params": params,
            "params_sd": params_sd,
            "CV": CV,
            "correlation": corr_list,
            "AICc": AICc,
            "BIC": self.res.bic,
            "AUC": AUC,
            "AUC_SD": AUC_SD,
            "Report": fitting_summary,
        }

        return mdict

class MonoExponential(FitFunction):
    def function(
        self, x: np.ndarray, a: float, b: float
    ) -> np.ndarray:  # "mono exponential function"
        return a * np.exp(-self.ln2 * x / b)

    def jacobian(
        self, x: np.ndarray, a: float, b: float
    ) -> np.ndarray:  # "mono exponential function jacobian"
        v = np.exp(-self.ln2 * x / b)
        return np.array([v, a * v * (self.ln2 * x / b**2)])

    def model(self, parameters, x, data=None):
        a, b = self.getParametersTuple(parameters)
        return self.function(x, a, b)

    def dmodel(self, parameters, x, data=None):
        a, b = self.getParametersTuple(parameters)
        return self.jacobian(x, a, b)

    def initParameters(self):
        parameters = lmfit.Parameters()
        parameters.add("a", value=self.height, min=0)
        parameters.add("b", value=self.width / 5, min=0, max=self.T_phys)

        return parameters

    def minimize(self):
        super().minimize(True)

    def initParametersNoLMFit(self) -> dict:
        self.parameters = {"a": self.height, "b": self.width / 5}

    def __repr__(self):
        p = self.getParameters()
        return f"mexp: y = {p[0]:.2f} * exp(-ln2*x / {p[1]:.2f})"
    
    def AUC(self) -> float:
        a, teff = self.getParameters()
        AUC = a*teff/self.ln2
        return AUC

    def AUC_SD(self) -> float:
        params = self.parameters
        params_sd = self.getErrorTuple()
        if not params_sd or len(params_sd) != len(params):
            return np.inf
        a, teff = self.getParameters()
        a_sd, teff_sd = self.getErrorTuple()
        AUC = a*teff/self.ln2
        AUC_SD = AUC * np.sqrt((a_sd / a) ** 2 + (teff_sd / teff) ** 2)
        return AUC_SD

class BiExponential(FitFunction):
    def function(
        self, x: np.ndarray, a: float, b: float, d: float
    ) -> np.ndarray:  # "bi exponential function"
        return a * (np.exp(-self.ln2 * x / b) - np.exp(-self.ln2 * x / d))

    def jacobian(
        self, x: np.ndarray, a: float, b: float, d: float
    ) -> np.ndarray:  # "bi exponential function jacobian"
        v1, v2 = np.exp(-self.ln2 * x / b), np.exp(-self.ln2 * x / d)
        return np.array(
            [v1 - v2, a * v1 * (self.ln2 * x / b**2), -a * v2 * (self.ln2 * x / d**2)]
        )

    def model(self, parameters, x, data=None):
        a, b, d = self.getParametersTuple(parameters)
        return self.function(x, a, b, d)

    def dmodel(self, parameters, x, data=None):
        a, b, d = self.getParametersTuple(parameters)
        return self.jacobian(x, a, b, d)

    def initParameters(self):
        parameters = lmfit.Parameters()
        parameters.add("a", value=self.height, min=0)
        parameters.add("b", value=self.width / 5, min=0, max=self.T_phys)
        parameters.add("d", value=self.width / 10, min=0, max=self.T_phys)
        return parameters

    def minimize(self):
        super().minimize(True)

    def initParametersNoLMFit(self):
        self.parameters = {"a": self.height, "b": self.width, "d": self.width / 10}

    def __repr__(self):
        p = self.getParameters()
        return f"bexp: y = {p[0]:.2f} * [ exp(-ln2*x / {p[1]:.2f}) - exp(-ln2*x / {p[2]:.2f}) ]"
    
    def AUC(self) -> float:
        a, b, d = self.getParameters()
        AUC = a*(b - d)/self.ln2
        return AUC

    def AUC_SD(self) -> float:
        params = self.parameters
        params_sd = self.getErrorTuple()
        if not params_sd or len(params_sd) != len(params):
            return np.inf
        a, b, d = self.getParameters()
        a_sd, b_sd, d_sd = self.getErrorTuple()
        AUC1 = a*b / self.ln2
        AUC2 = a*d / self.ln2
        AUC_SD1 = AUC1 * np.sqrt((a_sd / a) ** 2 + (b_sd / b) ** 2)
        AUC_SD2 = AUC2 * np.sqrt((a_sd / a) ** 2 + (d_sd / d) ** 2)
        AUC_SD = np.sqrt((AUC_SD1) ** 2 + (AUC_SD2) ** 2)
        return AUC_SD


class TriExponential(FitFunction):
    def function(
        self, x: np.ndarray, a: float, b: float, c: float, d: float
    ) -> np.ndarray:  # "tri exponential function"
        return (
            a * np.exp(-self.ln2 * x / b)
            - c * np.exp(-self.ln2 * x / d)
            - (a - c) * np.exp(-self.ln2 * x / (self.T_phys))
        )

    def jacobian(
        self, x: np.ndarray, a: float, b: float, c: float, d: float, f: float
    ) -> np.ndarray:  # "tri exponential function jacobian"
        v1, v2, v3 = (
            np.exp(-self.ln2 * x / b),
            np.exp(-self.ln2 * x / d),
            np.exp(-self.ln2 * x / self.T_phys)
        )
        return np.array(
            [
                v1 - v3,
                a * v1 * (self.ln2 * x / b**2),
                -v2 + v3,
                -c * v2 * (self.ln2 * x / d**2),
            ]
        )

    def model(self, parameters, x, data=None):
        a, b, c, d = self.getParametersTuple(parameters)
        return self.function(x, a, b, c, d)

    def dmodel(self, parameters, x, data=None):
        a, b, c, d = self.getParametersTuple(parameters)
        return self.jacobian(x, a, b, c, d)

    def initParameters(self):
        parameters = lmfit.Parameters()
        parameters.add("a", value=self.height, min=0)
        parameters.add("b", value=self.width / 10, min=0, max=self.T_phys)
        parameters.add("c", value=self.height, min=0)
        parameters.add("d", value=self.width / 10, min=0, max=self.T_phys)
        return parameters

    def minimize(self):
        super().minimize(True)

    def initParametersNoLMFit(self) -> dict:
        self.parameters = {
            "a": self.height,
            "b": self.width / 5,
            "c": self.height,
            "d": self.width / 5,
        }

    def __repr__(self):
        p = self.getParameters()
        return f"texp: y = {p[0]:.2f} * exp(-ln2*x / {p[1]:.2f}) - {p[2]:.2f} * exp(-ln2*x / {p[3]:.2f}) - ({p[0]:.2f}-{p[2]:.2f}) * exp(-ln2*x/ {self.T_phys:.2f})"
    
    def AUC(self) -> float:
        a, b, c, d = self.getParameters()
        AUC = a*b/ self.ln2 - c*d/ self.ln2 - (a - c)* self.T_phys/self.ln2
        return AUC

    def AUC_SD(self) -> float:
        params = self.parameters
        params_sd = self.getErrorTuple()
        if not params_sd or len(params_sd) != len(params):
            return np.inf
        a, b, c, d = self.getParameters()
        a_sd, b_sd, c_sd, d_sd = self.getErrorTuple()
        AUC1 = a*b / self.ln2
        AUC2 = c*d / self.ln2
        AUC_SD1 = AUC1 * np.sqrt((a_sd / a) ** 2 + (b_sd / b) ** 2)
        AUC_SD2 = AUC2 * np.sqrt((c_sd / c) ** 2 + (d_sd / d) ** 2)
        AUC_SD = np.sqrt((AUC_SD1) ** 2 + (AUC_SD2) ** 2)
        return AUC_SD


class RecoveryFunction(FitFunction):
    def function(self, x, b, c):  # "y = 1 - 1 / (1 + (x / b) ^ c)"
        return 1 - 1 / (1 + np.power(x / b, c))

    def jacobian(self, x, b, c):  # "recovery function jacobian"
        v1 = np.power(x / b, c)
        v2 = (1 + v1) ** 2
        return np.array([-(c / b) * v1 / v2, v1 * np.log(x / b) / v2])

    def model(self, parameters, x, data=None):
        b, c = self.getParametersTuple(parameters)
        return self.function(x, b, c)

    def dmodel(self, parameters, x, data=None):
        b, c = self.getParametersTuple(parameters)
        return self.jacobian(x, b, c)

    def initParameters(self):
        parameters = lmfit.Parameters()
        parameters.add("b", value=0.5, min=0)
        parameters.add("c", value=1, min=0)
        return parameters

    def minimize(self):
        return super().minimize(True)

    def initParametersNoLMFit(self) -> dict:
        self.parameters = {"b": self.width / 5, "c": 1.0}

    def __repr__(self):
        p = self.getParameters()
        return f"R(v) = 1 - 1 / (1 + (v / {p[0]:.2f}) ^ {p[1]:.2f})"