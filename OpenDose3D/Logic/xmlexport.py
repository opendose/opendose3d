import importlib
import shutil
from pathlib import Path

import vtk
import slicer

from lxml import etree
from xsdata.formats.dataclass.serializers import XmlSerializer
from xsdata.formats.dataclass.serializers.config import SerializerConfig

from Logic.nonDicomFileSetDescriptor import *
from Logic.utils import getScriptPath, get_valid_filename
from Logic.config import readSPECTSensitivity, writeSPECTSensitivity
from Logic.vtkmrmlutils import *
from Logic.nodes import Node
from Logic.attributes import Attributes
from Logic.xmlconstants import XMLConstants
from Logic.errors import IOError

# pyxb.utils.domutils.BindingDOMSupport.Reset()
# namespace = pyxb.namespace.NamespaceForURI(
#     'https://www.irdbb-medirad.com', create_if_missing=True)
# namespace.configureCategories(['typeBinding', 'elementBinding'])
# pyxb.utils.domutils.BindingDOMSupport.DeclareNamespace(namespace, 'irdbb')

mod = importlib.import_module('Logic', 'nonDicomFileSetDescriptor')
importlib.reload(mod)


# class DescriptorWrapper(NonDicomFileSetDescriptor_):
#     pass


# NonDicomFileSetDescriptor_._SetSupersedingClass(
#     DescriptorWrapper)


class XML_MEDIRAD():

    def __init__(self, referenceFolderID, outputFolder, ClinicalStudyTitle, ClinicalStudyID, Calibration=False):
        self.referenceFolderID = referenceFolderID
        self.outputFolder = outputFolder
        self.ClinicalStudyTitle = ClinicalStudyTitle
        self.ClinicalStudyID = ClinicalStudyID
        self.patientFolder = Path('~')
        self.studyFolder = Path('~')
        self.dataFolder = Path('~')
        self.calibration =Calibration
        self.center=""

        self.organs = [str(organ.value).lower() for organ in OrganOrTissue]

        if not Calibration:
            self.folders = getFolderIDs()
            self.nodes = Node.getFolderChildren(referenceFolderID)
            self.folderNames = getFolderNames()
        else:
            self.nodes = {}

    def __getSchemasVersion(self):
        xsdPath = getScriptPath() / "Resources" / "XSD" / \
            "NonDicomFileSetDescriptor.xsd"
        with xsdPath.open("r") as f:
            lines = f.read().splitlines()
            for line in lines:
                if "VersionSchemas" in line:
                    return str(line[4:-3])
        return "VersionSchemas : unknown"

    def __getAdministeredActivity(self, value):
        lAdministeredActivity = AdministeredActivity()
        lAdministeredActivity.administered_activity_value = value
        lAdministeredActivity.activity_unit = ActivityUnit.MEGABECQUEREL
        return lAdministeredActivity

    def __getAcquisitionSettings(self):
        lAcquisitionSettings = AcquisitionSettings()
        lAcquisitionSettings.siteadministeringthetreatment = XMLConstants().ClinicalCenters[self.center]
        lAcquisitionSettings.dateandtimeofinjection = self.nodes["ACSC"].data.GetAttribute(
            Attributes().injectionTime)
        lAcquisitionSettings.pre_administered_activity = self.__getAdministeredActivity(
            self.nodes["ACTM"].data.GetAttribute(Attributes().injectedActivity))
        lAcquisitionSettings.post_administered_activity = self.__getAdministeredActivity(
            0)
        # TODO add more definitions in XSD
        lAcquisitionSettings.radiopharmaceutical = Radiopharmaceutical.SODIUM_IODIDE_I131
        lAcquisitionSettings.isotope = Isotope(XMLConstants(
        ).IsotopesCategoryDict[self.nodes["ACTM"].data.GetAttribute(Attributes().isotope)])

        return lAcquisitionSettings

    def __getAcquisitionSettingsCalibration(self, tankNode):
        lAcquisitionSettings = AcquisitionSettings()
        lAcquisitionSettings.siteadministeringthetreatment = XMLConstants().ClinicalCenters[self.center]
        lAcquisitionSettings.dateandtimeofinjection = tankNode.getInjectionTime()
        lAcquisitionSettings.pre_administered_activity = self.__getAdministeredActivity(tankNode.getInjectedActivity())
        lAcquisitionSettings.post_administered_activity = self.__getAdministeredActivity(0)
        # TODO add more definitions in XSD
        lAcquisitionSettings.radiopharmaceutical = Radiopharmaceutical.SODIUM_IODIDE_I131
        lAcquisitionSettings.isotope = Isotope(XMLConstants().IsotopesCategoryDict[tankNode.getIsotope()])

        return lAcquisitionSettings

    def __prepareDirectory(self, study):
        if "CTCT" in self.nodes:
            patientName = self.nodes["CTCT"].getPatientName()
        elif "ACSC" in self.nodes:
            patientName = self.nodes["ACSC"].getPatientName()
        else:
            raise IOError(
                "Reference node does not have either CT or SPECT acquisition")

        patientName = get_valid_filename(patientName)
        try:
            self.center = patientName.split('-')[2]
        except:
            self.center = 'CRCT'

        self.patientFolder = Path(self.outputFolder) / patientName
        if not self.patientFolder.exists():
            self.patientFolder.mkdir()

        self.studyFolder = self.patientFolder / study
        if self.studyFolder.exists():
            shutil.rmtree(self.studyFolder)
        self.studyFolder.mkdir()

        self.dataFolder = self.studyFolder / "Data"
        if self.dataFolder.exists():
            shutil.rmtree(self.dataFolder)
        self.dataFolder.mkdir()

    def __printOutput(self, xmlSchema, elementName):
        root = etree.fromstring(xmlSchema.toxml(element_name=elementName))
        comment = etree.Comment(self.__getSchemasVersion())
        root.insert(0, comment)
        return etree.tostring(root, encoding='UTF-8', xml_declaration=True, pretty_print=True).decode()

    def __writeOutput(self, xmlSchema, fileName):
        config = SerializerConfig(pretty_print=True, xml_declaration=True)
        serializer = XmlSerializer(config=config)
        XMLString = serializer.render(
            xmlSchema, ns_map={"irdbb": "https://www.irdbb-medirad.com"})
        # root = etree.fromstring(str(XMLString))
        # comment = etree.Comment(self.__getSchemasVersion())
        # root.insert(0, comment)
        # XMLString = etree.tostring(
        #     root, encoding='UTF-8', xml_declaration=True, pretty_print=True).decode()
        with fileName.open('w') as f:
            f.write(XMLString)

    def __getGeometricalTransformation(self, node, nodeName):
        lGeometricalTransformation = GeometricalTransformation()
        lGeometricalTransformation.geometrical_transformation_value = NonDicomdataContainer()
        lnonDICOMData = self.__getNonDICOMData(node, nodeName, 'TRNF')
        lGeometricalTransformation.geometrical_transformation_value.non_dicomdata.append(
            lnonDICOMData)
        try:
            lGeometricalTransformation.transformation_type = XMLConstants(
            ).TransformationType[getDataNodeAttributeValue(node, Attributes().registrationMode)]
        except:
            lGeometricalTransformation.transformation_type = XMLConstants(
            ).TransformationType['useGeometryAlign']
        lGeometricalTransformation.transformation_identifier = str.replace(
            nodeName, ":", "")

        fixedID = int(getDataNodeAttributeValue(
            node, Attributes().registrationReference))
        fixedName = getItemName(fixedID)
        fixedNode = getItemDataNode(fixedID)
        if 'CTCT' in fixedName or 'CTRS' in fixedName:
            lGeometricalTransformation.dicomctdestination_coordinate_space_used = DicomdataContainer()
            lGeometricalTransformation.dicomctdestination_coordinate_space_used.dicomdata.append(
                self.__getDICOMData(fixedNode))
        elif 'ACSC' in fixedName:
            lGeometricalTransformation.dicomspectdestination_coordinate_space_used = DicomdataContainer()
            lGeometricalTransformation.dicomspectdestination_coordinate_space_used.dicomdata.append(
                self.__getDICOMData(fixedNode))
        else:
            lGeometricalTransformation.non_dicomdestination_coordinate_space_used = NonDicomdataContainer()
            lGeometricalTransformation.non_dicomdestination_coordinate_space_used.non_dicomdata.append(
                self.__getNonDICOMData(fixedNode, fixedName, 'DERS'))

        movingID = int(getDataNodeAttributeValue(
            node, Attributes().registrationVolume))
        movingName = getItemName(movingID)
        movingNode = getItemDataNode(movingID)
        if 'CTCT' in movingName or 'CTRS' in movingName:
            lGeometricalTransformation.dicomctdestination_coordinate_space_used = DicomdataContainer()
            lGeometricalTransformation.dicomctdestination_coordinate_space_used.dicomdata.append(
                self.__getDICOMData(movingNode))
        elif 'ACSC' in movingName:
            lGeometricalTransformation.dicomspectdestination_coordinate_space_used = DicomdataContainer()
            lGeometricalTransformation.dicomspectdestination_coordinate_space_used.dicomdata.append(
                self.__getDICOMData(movingNode))
        else:
            lGeometricalTransformation.non_dicomdestination_coordinate_space_used = NonDicomdataContainer()
            lGeometricalTransformation.non_dicomdestination_coordinate_space_used.non_dicomdata.append(
                self.__getNonDICOMData(movingNode, movingName, 'ACTM'))

        return lGeometricalTransformation

    def __getNonDICOMData(self, node, nodeName, nodeClass):
        if node is None:
            raise IOError(f'{nodeName} is not valid node')

        _nonDICOMData = NonDicomdata()
        fileHeaderName = str.replace(nodeName, ":", "")
        if nodeClass == 'TRNF':
            _nonDICOMData.non_dicomdata_format = NonDicomdataFormat.HDF5_FORMAT
            _nonDICOMData.non_dicomdata_file_name = f'Data/{fileHeaderName}.h5'
            try:
                _nonDICOMData.non_dicomdata_class = NonDicomdataClass(XMLConstants(
                ).TransformationType[getDataNodeAttributeValue(node, Attributes().registrationMode)])
            except:
                _nonDICOMData.non_dicomdata_class = NonDicomdataClass(XMLConstants(
                ).TransformationType['useGeometryAlign'])
        else:
            _nonDICOMData.non_dicomdata_format = NonDicomdataFormat.NRRD_FORMAT
            _nonDICOMData.non_dicomdata_class = NonDicomdataClass(XMLConstants(
            ).NonDICOMClass[nodeClass])
            if nodeClass == 'SEGM':
                _nonDICOMData.non_dicomdata_file_name = f'Data/{fileHeaderName}.seg.nrrd'
            else:
                _nonDICOMData.non_dicomdata_file_name = f'Data/{fileHeaderName}.nrrd'

        transfornationNode = node.GetParentTransformNode()
        transformationID = getItemID(transfornationNode)
        if transformationID > 0:
            _nonDICOMData.transformations_used = TransformationIdentifierContainer()
            _nonDICOMData.transformations_used.transformation_identifier.append(str.replace(
                getItemName(transformationID), ":", ""))

        fileName = self.studyFolder / _nonDICOMData.non_dicomdata_file_name
        print(fileName)
        if not fileName.exists():
            saveNode(node, fileName)

        return _nonDICOMData

    def __getDICOMData(self, node):
        lDICOMData = Dicomdata()
        lDICOMData.dicomstudy_uid = getDataNodeAttributeValue(node,
                                                              Attributes().studyInstanceUID)
        lDICOMData.dicomseries_uid = getDataNodeAttributeValue(node,
                                                               Attributes().seriesInstanceUID)
        return lDICOMData

    def __getProcessExecutionContext(self, studyCreation):
        lProcessExecutionContext = ProcessExecutionContext()
        lProcessExecutionContext.date_time_process_started = studyCreation
        lProcessExecutionContext.performing_institution = "CRCT"
        return lProcessExecutionContext

    def __getNMRelevantCalibrationReference(self, node):
        lNMRelevantCalibrationReference = NmrelevantCalibrationReference()
        lNMRelevantCalibrationReference.reference_calibration_date = getDataNodeAttributeValue(node, Attributes().acquisition)
        lNMRelevantCalibrationReference.isotope = Isotope(XMLConstants(
        ).IsotopesCategoryDict[getDataNodeAttributeValue(node, Attributes().isotope)])
        return lNMRelevantCalibrationReference

    def __getSPECTAcqCTAcqAndReconstruction(self, folderName):

        lSPECTAcqCTAcqAndReconstruction = SpectacqCtacqAndReconstruction()
        if "CTCT" in self.nodes:
            lSPECTAcqCTAcqAndReconstruction.ctrecon_produced = self.__getDICOMData(
                self.nodes["CTCT"].data)
            lSPECTAcqCTAcqAndReconstruction.ctrelevant_calibration_reference = CtrelevantCalibrationReference()
            lSPECTAcqCTAcqAndReconstruction.ctrelevant_calibration_reference.reference_calibration_date = self.nodes["CTCT"].data.GetAttribute(
                Attributes().acquisition)
        elif "CTRS" in self.nodes:
            lSPECTAcqCTAcqAndReconstruction.ctrecon_produced = self.__getDICOMData(
                self.nodes["CTRS"].data)
            lSPECTAcqCTAcqAndReconstruction.ctrelevant_calibration_reference = CtrelevantCalibrationReference()
            lSPECTAcqCTAcqAndReconstruction.ctrelevant_calibration_reference.reference_calibration_date = self.nodes["CTRS"].data.GetAttribute(
                Attributes().acquisition)
        else:
            lSPECTAcqCTAcqAndReconstruction.ctrecon_produced = Dicomdata("", "")

        if "ACSC" in self.nodes:
            lSPECTAcqCTAcqAndReconstruction.nmtomo_produced = self.__getDICOMData(
                self.nodes["ACSC"].data)
            lSPECTAcqCTAcqAndReconstruction.nmrelevant_calibration_reference = self.__getNMRelevantCalibrationReference(
                self.nodes["ACTM"].data)
        else:
            lSPECTAcqCTAcqAndReconstruction.nmtomo_produced = Dicomdata("", "")

        TPDE = TimePointDescriptionElement()
        if folderName in str(XMLConstants().TimePointCategoryDict.keys()):
            TPDE.time_point_category = TimePointCategory(XMLConstants(
            ).TimePointCategoryDict[folderName])
        elif abs(6-int(folderName[:-2])) <= 2:
            TPDE.time_point_category = TimePointCategory(XMLConstants(
            ).TimePointCategoryDict['6HR'])
        else:
            TPDE.time_point_category = TimePointCategory.ADDITIONAL_TIMEPOINT
        TPDE.time_unit = TimeUnit.HOURS
        TPDE.time_point_distance_from_reference_event_value = int(
            float(self.nodes["ACSC"].data.GetAttribute(Attributes().timeStamp)))
        TPDE.time_point_identifier = folderName
        lSPECTAcqCTAcqAndReconstruction.time_point_description = TimePointDescription()
        lSPECTAcqCTAcqAndReconstruction.time_point_description.time_point_description_element.append(
            TPDE)

        return lSPECTAcqCTAcqAndReconstruction

    def fillACTM_XMLfile(self):

        ProgressDialog = slicer.util.createProgressDialog(
            parent=None, value=0, maximum=len(self.folders))

        self.__prepareDirectory('ActivityIntegration')
        tableNodes = slicer.mrmlScene.GetNodesByClass('vtkMRMLTableNode')
        nameActivity = 'T:TABL Activity Activity summary'
        nameDose = 'T:TABL Activity Activity integrated'
        activityTableB = False
        doseTableB = False
        for node in tableNodes:
            if nameActivity == node.GetName():
                activityTable = node.GetTable()
                activityTableNode = node
                activityTableB = True
            elif nameDose == node.GetName():
                doseTable = node.GetTable()
                doseTableNode = node
                doseTableB = True

        if not(activityTableB and doseTableB):
            raise IOError('Absorbed Dose table not yet created')

        activityTableArray = GetTableAsArray(activityTableNode)
        doseTableArray = GetTableAsArray(doseTableNode)

        u3DSlide1 = NonDicomFileSetDescriptor()

        u3DSlide1.referenced_clinical_research_study = ReferencedClinicalResearchStudy()
        u3DSlide1.referenced_clinical_research_study.clinical_research_study_id = self.ClinicalStudyID
        u3DSlide1.referenced_clinical_research_study.clinical_research_study_title = self.ClinicalStudyTitle

        if "CTCT" in self.nodes:
            u3DSlide1.patient_id = self.nodes["CTCT"].data.GetAttribute(
                Attributes().patientID)
        elif "ACSC" in self.nodes:
            u3DSlide1.patient_id = self.nodes["ACSC"].data.GetAttribute(
                Attributes().patientID)
        else:
            raise IOError(
                "Reference node does not have either CT or SPECT acquisition")

        u3DSlide1.acquisition_settings = self.__getAcquisitionSettings()

        if "SEGM" not in self.nodes:
            SegmentationNodes = slicer.mrmlScene.GetNodesByClass(
                'vtkMRMLSegmentationNode')
            for segmentation in SegmentationNodes:
                referenceSegmentationNode = segmentation
                break

            if not 'referenceSegmentationNode' in locals():
                raise IOError("No segmentations found")

        u3DSlide1.three_dim_dosimetry_slide1workflow = ThreeDimDosimetrySlide1Workflow()
        u3DSlide1.three_dim_dosimetry_slide1workflow.spectdata_acquisition_and_reconstruction = SpectdataAcquisitionAndReconstruction()
        u3DSlide1.three_dim_dosimetry_slide1workflow.spectdata_acquisition_and_reconstruction.spectacq_ctacq_and_reconstruction_container = SpectacqCtacqAndReconstructionContainer()
        u3DSlide1.three_dim_dosimetry_slide1workflow.registration_voisegmentation_and_propagation_container = RegistrationVoisegmentationAndPropagationContainer()
        u3DSlide1.three_dim_dosimetry_slide1workflow.voiactivity_determination_container = VoiactivityDeterminationContainer()
        u3DSlide1.three_dim_dosimetry_slide1workflow.time_activity_curve_fit_in3_ddosimetry_container = TimeActivityCurveFitIn3DdosimetryContainer()
        u3DSlide1.three_dim_dosimetry_slide1workflow.absorbed_dose_calculation_in_voi = AbsorbedDoseCalculationInVoi()

        u3DSlide1.three_dim_dosimetry_slide1workflow.absorbed_dose_calculation_in_voi.process_execution_context = self.__getProcessExecutionContext(
            getDataNodeAttributeValue(activityTableNode, Attributes().studyCreation))
        u3DSlide1.three_dim_dosimetry_slide1workflow.absorbed_dose_calculation_in_voi.absorbed_dose_calculation_method_used = "Local Energy Deposition"
        u3DSlide1.three_dim_dosimetry_slide1workflow.absorbed_dose_calculation_in_voi.absorbed_dose_in_voicontainer = AbsorbedDoseInVoicontainer()

        for folderIndex, folderID in enumerate(self.folders):
            folderName = getItemName(folderID)

            ProgressDialog.labelText = f"Processing {folderName}..."
            ProgressDialog.value = folderIndex
            if ProgressDialog.wasCanceled:
                break
            slicer.app.processEvents()

            self.nodes = Node.getFolderChildren(folderID)
            SPECTAcqCTAcqAndReconstruction = self.__getSPECTAcqCTAcqAndReconstruction(
                folderName)
            u3DSlide1.three_dim_dosimetry_slide1workflow.spectdata_acquisition_and_reconstruction.spectacq_ctacq_and_reconstruction_container.spectacq_ctacq_and_reconstruction.append(
                SPECTAcqCTAcqAndReconstruction)

            lRegistrationVOISegmentationAndPropagation = RegistrationVoisegmentationAndPropagation()
            lRegistrationVOISegmentationAndPropagation.time_point_identifier_used = folderName
            lRegistrationVOISegmentationAndPropagation.image_processing_method_method_used = "Semi-automatic segmentation"
            lRegistrationVOISegmentationAndPropagation.segmentation = Segmentation()
            lRegistrationVOISegmentationAndPropagation.ctrecon_used = DicomdataContainer(
                [SPECTAcqCTAcqAndReconstruction.ctrecon_produced])
            lRegistrationVOISegmentationAndPropagation.nmtomo_recon_used = DicomdataContainer(
                [SPECTAcqCTAcqAndReconstruction.nmtomo_produced])
            lRegistrationVOISegmentationAndPropagation.density_image_produced = NonDicomdataContainer()
            if "DERS" in self.nodes:
                lnonDICOMData = self.__getNonDICOMData(
                    self.nodes["DERS"].data, self.nodes["DERS"].name, 'DERS')
            else:
                lnonDICOMData = NonDicomdata()
            lRegistrationVOISegmentationAndPropagation.density_image_produced.non_dicomdata.append(
                lnonDICOMData)

            if "TRNF" in self.nodes:
                lRegistrationVOISegmentationAndPropagation.geometrical_transformation_container = GeometricalTransformationContainer()
                GeometricalTransformation = self.__getGeometricalTransformation(
                    self.nodes["TRNF"].data, self.nodes["TRNF"].name)
                lRegistrationVOISegmentationAndPropagation.geometrical_transformation_container.geometrical_transformation.append(
                    GeometricalTransformation)

            # search for the closest value to timestamp actually present in the table
            timestamp = float(
                self.nodes["ACTM"].data.GetAttribute(Attributes().timeStamp))
            timestamplist = list(activityTableArray['time (h)'])
            (activityRow, timestamp) = min(
                enumerate(timestamplist), key=lambda x: abs(x[1]-timestamp))

            if "SEGM" in self.nodes:
                segmentationNode = self.nodes["SEGM"].data
                segmentationName = self.nodes["SEGM"].name
            else:
                segmentationNode = referenceSegmentationNode
                segmentationName = segmentationNode.GetName()

            fileHeaderName = str.replace(segmentationName, ":", "")
            visibleSegmentIDs = vtk.vtkStringArray()
            segmentationNode.GetDisplayNode().SetAllSegmentsVisibility(True)
            segmentationNode.GetDisplayNode().GetVisibleSegmentIDs(visibleSegmentIDs)

            segmentationCreation = getDataNodeAttributeValue(
                segmentationNode,  Attributes().studyCreation)
            if not segmentationCreation:
                segmentationCreation = getCurrentTime()
            lRegistrationVOISegmentationAndPropagation.process_execution_context = self.__getProcessExecutionContext(
                segmentationCreation)
            lRegistrationVOISegmentationAndPropagation.segmentation.segmentation_identifier = fileHeaderName
            lRegistrationVOISegmentationAndPropagation.segmentation.voicontainer = VoiProducedContainer()

            lVOIActivityDetermination = VoiactivityDetermination()
            lVOIActivityDetermination.time_point_identifier_used = folderName
            lVOIActivityDetermination.segmentation_identifier_used = fileHeaderName
            lVOIActivityDetermination.process_execution_context = self.__getProcessExecutionContext(
                self.nodes["ACTM"].data.GetAttribute(Attributes().studyCreation))
            lVOIActivityDetermination.spectcalibration_factor_reference_used = self.__getNMRelevantCalibrationReference(
                self.nodes["ACTM"].data)
            lVOIActivityDetermination.spectrecovery_coefficient_curve_reference_used = self.__getNMRelevantCalibrationReference(
                self.nodes["ACTM"].data)
            lVOIActivityDetermination.voxel_activity_map_produced = NonDicomdataContainer()
            nonDICOMData = self.__getNonDICOMData(
                self.nodes["ACTM"].data, self.nodes["ACTM"].name, 'ACTM')
            lVOIActivityDetermination.voxel_activity_map_produced.non_dicomdata.append(
                nonDICOMData)

            lVOIActivityDetermination.data_activity_per_voiat_time_point_container = DataActivityPerVoiatTimePointContainer()

            for segmentIndex in range(visibleSegmentIDs.GetNumberOfValues()):
                segmentID = visibleSegmentIDs.GetValue(segmentIndex)
                segmentName = segmentationNode.GetSegmentation().GetSegment(segmentID).GetName()
                segmentIdentifier = segmentIndex*1000 + folderIndex + 1000
                for name in activityTableArray:
                    if segmentName in name:
                        if 'M_' in name:
                            marray = activityTableArray[name]
                        elif 'CA_' in name:
                            carray = activityTableArray[name]

                lVOI = Voi()
                lVOI.voiidentifier = segmentIdentifier
                lVOI.time_point_identifier = folderName
                if segmentName.lower() in self.organs:
                    lVOI.organ_or_tissue = OrganOrTissue(segmentName.lower())
                elif segmentName in XMLConstants().OrgansCategoryDict:
                    lVOI.organ_or_tissue = OrganOrTissue(XMLConstants(
                    ).OrgansCategoryDict[segmentName])
                else:
                    lVOI.organ_or_tissue = OrganOrTissue.SOFT_TISSUE
                lVOI.organ_mass = OrganMass()
                lVOI.organ_mass.organ_mass_value = float(marray[activityRow])
                lVOI.organ_mass.organ_mass_unit = OrganMassUnit.KILOGRAM
                lRegistrationVOISegmentationAndPropagation.segmentation.voicontainer.voiproduced.append(
                    lVOI)

                lDataActivityPerVOIAtTimePoint = DataActivityPerVoiatTimePoint()
                lDataActivityPerVOIAtTimePoint.voiidentifier = segmentIdentifier
                lDataActivityPerVOIAtTimePoint.time_point_identifier = folderName
                lDataActivityPerVOIAtTimePoint.data_activity_value = float(
                    carray[activityRow])
                lDataActivityPerVOIAtTimePoint.activity_unit = ActivityUnit.MEGABECQUEREL
                lVOIActivityDetermination.data_activity_per_voiat_time_point_container.data_activity_per_voiat_time_point_produced.append(
                    lDataActivityPerVOIAtTimePoint)

                if folderIndex == 0:
                    lTimeActivityCurveFitIn3DDosimetry = TimeActivityCurveFitIn3Ddosimetry()
                    lTimeActivityCurveFitIn3DDosimetry.process_execution_context = self.__getProcessExecutionContext(
                        getDataNodeAttributeValue(activityTableNode, Attributes().studyCreation))
                    lTimeActivityCurveFitIn3DDosimetry.pre_administered_activity_used = u3DSlide1.acquisition_settings.pre_administered_activity
                    lTimeActivityCurveFitIn3DDosimetry.post_administered_activity_used = u3DSlide1.acquisition_settings.post_administered_activity
                    lTimeActivityCurveFitIn3DDosimetry.time_integrated_activity_per_voiproduced = TimeIntegratedActivityPerVoi()
                    lTimeActivityCurveFitIn3DDosimetry.time_integrated_activity_per_voiproduced.voiidentifier_list = VoiidentifierContainer([
                                                                                                                                            segmentIdentifier])

                    rowDoseArray = doseTableArray['Segment'].index(
                        f"CA_{segmentName}")
                    lTimeActivityCurveFitIn3DDosimetry.time_integrated_activity_per_voiproduced.time_integrated_activity_per_voivalue = float(
                        str(doseTableArray['Cummulated Activity (MBqh)'][rowDoseArray]))
                    lTimeActivityCurveFitIn3DDosimetry.time_integrated_activity_per_voiproduced.time_integrated_activity_per_voiunit = TimeIntegratedActivityPerVoiunit.MEGABECQUEREL_XHOUR
                    lTimeActivityCurveFitIn3DDosimetry.time_integrated_activity_per_voiproduced.residence_time_per_voivalue = float(str(
                        lTimeActivityCurveFitIn3DDosimetry.time_integrated_activity_per_voiproduced.time_integrated_activity_per_voivalue /
                        float(lTimeActivityCurveFitIn3DDosimetry.pre_administered_activity_used.administered_activity_value)))
                    lTimeActivityCurveFitIn3DDosimetry.time_integrated_activity_per_voiproduced.time_unit = TimeUnit.HOURS
                    lTimeActivityCurveFitIn3DDosimetry.time_integrated_activity_per_voiproduced.pkassessment_method_used = CurveFittingMethod()
                    algorithm = str.split(
                        doseTableArray['Fit function'][rowDoseArray], ':')[0]
                    algorithm = XMLConstants().FunctionTypeCategory[algorithm]
                    lTimeActivityCurveFitIn3DDosimetry.time_integrated_activity_per_voiproduced.pkassessment_method_used.incorporation_function = getDataNodeAttributeValue(
                        doseTableNode, "PKincorporation")
                    lTimeActivityCurveFitIn3DDosimetry.time_integrated_activity_per_voiproduced.pkassessment_method_used.integration_algorithm = IntegrationAlgorithm(
                        algorithm)
                    lTimeActivityCurveFitIn3DDosimetry.time_integrated_activity_per_voiproduced.pkassessment_method_used.fitting_function = doseTableArray[
                        'Fit function'][rowDoseArray]

                    lTimeActivityCurveFitIn3DDosimetry.time_integrated_activity_coefficient_per_voiproduced = TimeIntegratedActivityCoefficientPerVoi()
                    lTimeActivityCurveFitIn3DDosimetry.time_integrated_activity_coefficient_per_voiproduced.time_integrated_activity_coefficient_per_voivalue = lTimeActivityCurveFitIn3DDosimetry.time_integrated_activity_per_voiproduced.residence_time_per_voivalue
                    lTimeActivityCurveFitIn3DDosimetry.time_integrated_activity_coefficient_per_voiproduced.time_unit = TimeUnit.HOURS
                    lTimeActivityCurveFitIn3DDosimetry.time_integrated_activity_coefficient_per_voiproduced.voiidentifier_list = VoiidentifierContainer([
                                                                                                                                                        segmentIdentifier])

                    u3DSlide1.three_dim_dosimetry_slide1workflow.time_activity_curve_fit_in3_ddosimetry_container.time_activity_curve_fit_in3_ddosimetry.append(
                        lTimeActivityCurveFitIn3DDosimetry)

                    lAbsorbedDoseInVOIProduced = AbsorbedDoseInVoi()
                    lAbsorbedDoseInVOIProduced.voiidentifier_list = VoiidentifierContainer([
                                                                                           segmentIdentifier])
                    lAbsorbedDoseInVOIProduced.absorbed_dose_in_voivalue = float(
                        str(doseTableArray['Absorbed Dose by LED (Gy)'][rowDoseArray]))
                    lAbsorbedDoseInVOIProduced.absorbed_dose_unit = AbsorbedDoseUnit.GRAY
                    u3DSlide1.three_dim_dosimetry_slide1workflow.absorbed_dose_calculation_in_voi.absorbed_dose_in_voicontainer.absorbed_dose_in_voiproduced.append(
                        lAbsorbedDoseInVOIProduced)
                else:
                    u3DSlide1.three_dim_dosimetry_slide1workflow.time_activity_curve_fit_in3_ddosimetry_container.time_activity_curve_fit_in3_ddosimetry[
                        segmentIndex].time_integrated_activity_per_voiproduced.voiidentifier_list.voiidentifier_used.append(segmentIdentifier)
                    u3DSlide1.three_dim_dosimetry_slide1workflow.absorbed_dose_calculation_in_voi.absorbed_dose_in_voicontainer.absorbed_dose_in_voiproduced[segmentIndex].voiidentifier_list.voiidentifier_used.append(
                        segmentIdentifier)
                    u3DSlide1.three_dim_dosimetry_slide1workflow.time_activity_curve_fit_in3_ddosimetry_container.time_activity_curve_fit_in3_ddosimetry[
                        segmentIndex].time_integrated_activity_coefficient_per_voiproduced.voiidentifier_list.voiidentifier_used.append(segmentIdentifier)

            lRegistrationVOISegmentationAndPropagation.segmentation.non_dicomdata_container = NonDicomdataContainer()
            nonDICOMData = self.__getNonDICOMData(
                segmentationNode, segmentationName, 'SEGM')
            lRegistrationVOISegmentationAndPropagation.segmentation.non_dicomdata_container.non_dicomdata.append(
                nonDICOMData)

            lRegistrationVOISegmentationAndPropagation.non_dicomctrecon_resampled_on_common_reference_produced = NonDicomdataContainer()
            nonDICOMData = self.__getNonDICOMData(
                self.nodes["CTRS"].data, self.nodes["CTRS"].name, 'CTRS')
            lRegistrationVOISegmentationAndPropagation.non_dicomctrecon_resampled_on_common_reference_produced.non_dicomdata.append(
                nonDICOMData)

            lRegistrationVOISegmentationAndPropagation.non_dicomnmtomo_recon_resampled_on_common_reference_produced = NonDicomdataContainer()
            nonDICOMData = self.__getNonDICOMData(
                self.nodes["ACSC"].data, self.nodes["ACSC"].name, 'ACSC')
            lRegistrationVOISegmentationAndPropagation.non_dicomnmtomo_recon_resampled_on_common_reference_produced.non_dicomdata.append(
                nonDICOMData)

            u3DSlide1.three_dim_dosimetry_slide1workflow.registration_voisegmentation_and_propagation_container.registration_voisegmentation_and_propagation.append(
                lRegistrationVOISegmentationAndPropagation)
            u3DSlide1.three_dim_dosimetry_slide1workflow.voiactivity_determination_container.voiactivity_determination.append(
                lVOIActivityDetermination)

        ProgressDialog.close()
        self.__writeOutput(u3DSlide1, self.studyFolder / '3D-DosimetrySlide1Worflow.xml')

    def __setupModeAlgo(self, mode, algorithm):
        if 'dose' in mode.lower():
            if 'FFT' in algorithm:
                localAlgorithm = 'FFT Convolution'
            elif 'monte' in algorithm.lower():
                localAlgorithm = 'MonteCarlo'
            else:
                localAlgorithm = 'Local Energy Deposition'
        else:
            localAlgorithm = 'Activity'
        return localAlgorithm

    def fillADRM_XMLfile(self, mode, algorithm=''):
        ProgressDialog = slicer.util.createProgressDialog(
            parent=None, value=0, maximum=len(self.folders))
        self.__prepareDirectory('ADRIntegration')
        tableNodes = slicer.mrmlScene.GetNodesByClass('vtkMRMLTableNode')
        localAlgorithm = self.__setupModeAlgo(mode, algorithm)
        nameDoseRate = f'T:TABL {mode} {localAlgorithm} summary'
        nameDose = f'T:TABL {mode} {localAlgorithm} integrated'
        doseRateTableB = False
        doseTableB = False
        for node in tableNodes:
            if nameDoseRate == node.GetName():
                doseRateTable = node.GetTable()
                doseRateTableNode = node
                doseRateTableB = True
            elif nameDose == node.GetName():
                doseTable = node.GetTable()
                doseTableNode = node
                doseTableB = True

        if not(doseRateTableB and doseTableB):
            raise IOError('Absorbed Dose table not yet created')

        doseRateTableArray = GetTableAsArray(doseRateTableNode)
        doseTableArray = GetTableAsArray(doseTableNode)

        u3DSlide2 = NonDicomFileSetDescriptor()

        u3DSlide2.referenced_clinical_research_study = ReferencedClinicalResearchStudy()
        u3DSlide2.referenced_clinical_research_study.clinical_research_study_id = self.ClinicalStudyID
        u3DSlide2.referenced_clinical_research_study.clinical_research_study_title = self.ClinicalStudyTitle

        if "CTCT" in self.nodes:
            u3DSlide2.patient_id = self.nodes["CTCT"].data.GetAttribute(
                Attributes().patientID)
        elif "ACSC" in self.nodes:
            u3DSlide2.patient_id = self.nodes["ACSC"].data.GetAttribute(
                Attributes().patientID)
        else:
            raise IOError(
                "Reference node does not have either CT or SPECT acquisition")

        u3DSlide2.acquisition_settings = self.__getAcquisitionSettings()

        if "SEGM" not in self.nodes:
            SegmentationNodes = slicer.mrmlScene.GetNodesByClass(
                'vtkMRMLSegmentationNode')
            for segmentation in SegmentationNodes:
                referenceSegmentationNode = segmentation
                break

            if 'referenceSegmentationNode' not in locals():
                raise IOError("No segmentations found")

        u3DSlide2.three_dim_dosimetry_slide2workflow = ThreeDimDosimetrySlide2Workflow()
        u3DSlide2.three_dim_dosimetry_slide2workflow.spectdata_acquisition_and_reconstruction = SpectdataAcquisitionAndReconstruction()
        u3DSlide2.three_dim_dosimetry_slide2workflow.spectdata_acquisition_and_reconstruction.spectacq_ctacq_and_reconstruction_container = SpectacqCtacqAndReconstructionContainer()
        u3DSlide2.three_dim_dosimetry_slide2workflow.registration_voisegmentation_and_propagation_container = RegistrationVoisegmentationAndPropagationContainer()
        u3DSlide2.three_dim_dosimetry_slide2workflow.voisegmentation_energy_deposition_calculation_absorbed_dose_rate_calculation_container = VoisegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculationContainer()
        u3DSlide2.three_dim_dosimetry_slide2workflow.dose_rate_curve_fit_voitime_integration_container = DoseRateCurveFitVoitimeIntegrationContainer()

        u3DSlide2.three_dim_dosimetry_slide2workflow.absorbed_dose_calculation_in_voi = AbsorbedDoseCalculationInVoi()
        u3DSlide2.three_dim_dosimetry_slide2workflow.absorbed_dose_calculation_in_voi.process_execution_context = self.__getProcessExecutionContext(
            getDataNodeAttributeValue(doseRateTableNode, Attributes().studyCreation))
        u3DSlide2.three_dim_dosimetry_slide2workflow.absorbed_dose_calculation_in_voi.absorbed_dose_calculation_method_used = getDataNodeAttributeValue(
            doseTableNode, "ADalgorithm")
        u3DSlide2.three_dim_dosimetry_slide2workflow.absorbed_dose_calculation_in_voi.absorbed_dose_in_voicontainer = AbsorbedDoseInVoicontainer()

        for folderIndex, folderID in enumerate(self.folders):
            folderName = getItemName(folderID)

            ProgressDialog.labelText = f"Processing {folderName}..."
            ProgressDialog.value = folderIndex
            if ProgressDialog.wasCanceled:
                break
            slicer.app.processEvents()

            self.nodes = Node.getFolderChildren(folderID)
            SPECTAcqCTAcqAndReconstruction = self.__getSPECTAcqCTAcqAndReconstruction(
                folderName)
            u3DSlide2.three_dim_dosimetry_slide2workflow.spectdata_acquisition_and_reconstruction.spectacq_ctacq_and_reconstruction_container.spectacq_ctacq_and_reconstruction.append(
                SPECTAcqCTAcqAndReconstruction)

            lRegistrationVOISegmentationAndPropagation = RegistrationVoisegmentationAndPropagation()
            lRegistrationVOISegmentationAndPropagation.time_point_identifier_used = folderName
            lRegistrationVOISegmentationAndPropagation.image_processing_method_method_used = "Semi-automatic segmentation"
            lRegistrationVOISegmentationAndPropagation.segmentation = Segmentation()
            lRegistrationVOISegmentationAndPropagation.ctrecon_used = DicomdataContainer()
            lRegistrationVOISegmentationAndPropagation.ctrecon_used.dicomdata.append(
                SPECTAcqCTAcqAndReconstruction.ctrecon_produced)
            lRegistrationVOISegmentationAndPropagation.nmtomo_recon_used = DicomdataContainer()
            lRegistrationVOISegmentationAndPropagation.nmtomo_recon_used.dicomdata.append(
                SPECTAcqCTAcqAndReconstruction.nmtomo_produced)
            lRegistrationVOISegmentationAndPropagation.density_image_produced = NonDicomdataContainer()
            if "DERS" in self.nodes:
                lnonDICOMData = self.__getNonDICOMData(
                    self.nodes["DERS"].data, self.nodes["DERS"].name, 'DERS')
            else:
                lnonDICOMData = NonDicomdata()
            lRegistrationVOISegmentationAndPropagation.density_image_produced.non_dicomdata.append(
                lnonDICOMData)

            if "TRNF" in self.nodes:
                lRegistrationVOISegmentationAndPropagation.geometrical_transformation_container = GeometricalTransformationContainer()
                GeometricalTransformation = self.__getGeometricalTransformation(
                    self.nodes["TRNF"].data, self.nodes["TRNF"].name)
                lRegistrationVOISegmentationAndPropagation.geometrical_transformation_container.geometrical_transformation.append(
                    GeometricalTransformation)

            # search for the closest value to timestamp actually present in the table
            timestamp = float(
                self.nodes["ADRM"][localAlgorithm].data.GetAttribute(Attributes().timeStamp))
            timestamplist = list(doseRateTableArray['time (h)'])
            (doseRateRow, timestamp) = min(
                enumerate(timestamplist), key=lambda x: abs(x[1]-timestamp))
            print(doseRateRow, timestamp)

            if "SEGM" in self.nodes:
                segmentationNode = self.nodes["SEGM"].data
                segmentationName = self.nodes["SEGM"].name
            else:
                segmentationNode = referenceSegmentationNode
                segmentationName = segmentationNode.GetName()

            fileHeaderNameSegmentation = str.replace(segmentationName, ":", "")
            visibleSegmentIDs = vtk.vtkStringArray()
            segmentationNode.GetDisplayNode().SetAllSegmentsVisibility(True)
            segmentationNode.GetDisplayNode().GetVisibleSegmentIDs(visibleSegmentIDs)

            segmentationCreation = getDataNodeAttributeValue(segmentationNode,
                                                             Attributes().studyCreation)
            if not segmentationCreation:
                segmentationCreation = getCurrentTime()
            lRegistrationVOISegmentationAndPropagation.process_execution_context = self.__getProcessExecutionContext(
                segmentationCreation)
            lRegistrationVOISegmentationAndPropagation.segmentation.segmentation_identifier = fileHeaderNameSegmentation
            lRegistrationVOISegmentationAndPropagation.segmentation.voicontainer = VoiProducedContainer()

            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation = VoisegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation()

            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.voisegmentation_voimass_determination = VoisegmentationVoimassDetermination()
            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.voisegmentation_voimass_determination.process_execution_context = self.__getProcessExecutionContext(
                segmentationCreation)
            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.voisegmentation_voimass_determination.time_point_identifier_used = folderName
            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.voisegmentation_voimass_determination.ctrecon_used = SPECTAcqCTAcqAndReconstruction.ctrecon_produced
            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.voisegmentation_voimass_determination.nmtomo_recon_used = SPECTAcqCTAcqAndReconstruction.nmtomo_produced
            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.voisegmentation_voimass_determination.voisegmentation_method_used = lRegistrationVOISegmentationAndPropagation.image_processing_method_method_used
            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.voisegmentation_voimass_determination.ctrecon_resampled_on_nmreference_produced = RegistrationVoisegmentationAndPropagation.ctrecon_used
            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.voisegmentation_voimass_determination.mass_per_voiat_time_point_container = MassPerVoiatTimePointContainer()

            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.voiactivity_determination = VoiactivityDetermination()
            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.voiactivity_determination.time_point_identifier_used = folderName
            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.voiactivity_determination.segmentation_identifier_used = fileHeaderNameSegmentation
            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.voiactivity_determination.process_execution_context = self.__getProcessExecutionContext(
                self.nodes["ACTM"].data.GetAttribute(Attributes().studyCreation))
            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.voiactivity_determination.spectcalibration_factor_reference_used = self.__getNMRelevantCalibrationReference(
                self.nodes["ACTM"].data)
            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.voiactivity_determination.spectrecovery_coefficient_curve_reference_used = self.__getNMRelevantCalibrationReference(
                self.nodes["ACTM"].data)
            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.voiactivity_determination.voxel_activity_map_produced = NonDicomdataContainer()
            lnonDICOMData = self.__getNonDICOMData(
                self.nodes["ACTM"].data, self.nodes["ACTM"].name, 'ACTM')
            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.voiactivity_determination.voxel_activity_map_produced.non_dicomdata.append(
                lnonDICOMData)

            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.energy_deposition_rate_calculation_in3_ddosimetry = EnergyDepositionRateCalculationIn3Ddosimetry()
            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.energy_deposition_rate_calculation_in3_ddosimetry.process_execution_context = self.__getProcessExecutionContext(
                self.nodes["ADRM"][localAlgorithm].data.GetAttribute(Attributes().studyCreation))
            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.energy_deposition_rate_calculation_in3_ddosimetry.time_point_identifier_used = folderName
            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.energy_deposition_rate_calculation_in3_ddosimetry.calculation_algorithm_used = XMLConstants(
            ).CalculationAlgorithmCategory[localAlgorithm]
            if 'convolution' in str(XMLConstants().CalculationAlgorithmCategory[localAlgorithm]).lower():
                lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.energy_deposition_rate_calculation_in3_ddosimetry.kernel_limit_for_convolutions_used = KernelLimitForConvolutions()
                lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.energy_deposition_rate_calculation_in3_ddosimetry.kernel_limit_for_convolutions_used.kernel_limit_for_convolutions_value = self.nodes["ADRM"][localAlgorithm].data.GetAttribute(
                    Attributes().doseKernelLimit)
                lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.energy_deposition_rate_calculation_in3_ddosimetry.kernel_limit_for_convolutions_used.kernel_limit_for_convolutions_unit = KernelLimitForConvolutionsUnit.MILLIMETER

            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.energy_deposition_rate_calculation_in3_ddosimetry.ctrecon_resampled_on_nmreference_used = SPECTAcqCTAcqAndReconstruction.ctrecon_produced
            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.energy_deposition_rate_calculation_in3_ddosimetry.nmtomo_recon_used = SPECTAcqCTAcqAndReconstruction.nmtomo_produced
            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.energy_deposition_rate_calculation_in3_ddosimetry.three_dim_energy_deposition_rate_matrix_at_time_point_produced = self.__getNonDICOMData(
                self.nodes["ADRM"][localAlgorithm].data, self.nodes["ADRM"][localAlgorithm].name, 'ADRM')

            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.sum_and_scaling_absorbed_dose_rate_calculation_container = SumAndScalingAbsorbedDoseRateCalculationContainer()

            lSumAndScalingAbsorbedDoseRateCalculation = SumAndScalingAbsorbedDoseRateCalculation()
            lSumAndScalingAbsorbedDoseRateCalculation.process_execution_context = self.__getProcessExecutionContext(
                getDataNodeAttributeValue(doseRateTableNode, Attributes().studyCreation))
            # SumAndScalingAbsorbedDoseRateCalculation.VOIIdentifierUsed = VOIIdentifierContainer()
            lSumAndScalingAbsorbedDoseRateCalculation.time_point_identifier_used = folderName
            lSumAndScalingAbsorbedDoseRateCalculation.segmentation_used = fileHeaderNameSegmentation
            lSumAndScalingAbsorbedDoseRateCalculation.three_dim_energy_deposition_rate_matrix_at_time_point_used = self.__getNonDICOMData(
                self.nodes["ADRM"][localAlgorithm].data, self.nodes["ADRM"][localAlgorithm].name, 'ADRM')
            lSumAndScalingAbsorbedDoseRateCalculation.absorbed_dose_rate_per_voiat_time_point_produced = AbsorbedDoseRatePerVoiatTimePointContainer()

            for segmentIndex in range(visibleSegmentIDs.GetNumberOfValues()):
                segmentID = visibleSegmentIDs.GetValue(segmentIndex)
                segmentName = segmentationNode.GetSegmentation().GetSegment(segmentID).GetName()
                segmentIdentifier = segmentIndex*1000 + folderIndex + 1000
                for name in doseRateTableArray:
                    if segmentName in name:
                        if 'M_' in name:
                            marray = doseRateTableArray[name]
                        elif 'DR_' in name:
                            carray = doseRateTableArray[name]

                lVOI = Voi()
                lVOI.voiidentifier = segmentIdentifier
                lVOI.time_point_identifier = folderName
                if segmentName.lower() in self.organs:
                    lVOI.organ_or_tissue = OrganOrTissue(segmentName.lower())
                elif segmentName in str(XMLConstants().OrgansCategoryDict):
                    lVOI.organ_or_tissue = OrganOrTissue(XMLConstants(
                    ).OrgansCategoryDict[segmentName])
                else:
                    lVOI.organ_or_tissue = OrganOrTissue.SOFT_TISSUE
                lVOI.organ_mass = OrganMass()
                lVOI.organ_mass.organ_mass_value = float(marray[doseRateRow])
                lVOI.organ_mass.organ_mass_unit = OrganMassUnit.KILOGRAM
                lRegistrationVOISegmentationAndPropagation.segmentation.voicontainer.voiproduced.append(
                    lVOI)

                lMassPerVOIAtTimePoint = MassPerVoiatTimePoint()
                lMassPerVOIAtTimePoint.voiidentifier = segmentIdentifier
                lMassPerVOIAtTimePoint.time_point_identifier = folderName
                lMassPerVOIAtTimePoint.mass_value = float(marray[doseRateRow])
                lMassPerVOIAtTimePoint.mass_unit = MassUnit.KILOGRAM
                lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.voisegmentation_voimass_determination.mass_per_voiat_time_point_container.mass_per_voiat_time_point_produced.append(
                    lMassPerVOIAtTimePoint)

                lAbsorbedDoseRatePerVOIAtTimePoint = AbsorbedDoseRatePerVoiatTimePoint()
                lAbsorbedDoseRatePerVOIAtTimePoint.time_point_identifier = folderName
                lAbsorbedDoseRatePerVOIAtTimePoint.voiidentifier = segmentIdentifier
                lAbsorbedDoseRatePerVOIAtTimePoint.absorbed_dose_rate_value = float(
                    carray[doseRateRow])
                lAbsorbedDoseRatePerVOIAtTimePoint.absorbed_dose_rate_unit = AbsorbedDoseRateUnit.MILLIGRAY_PER_HOUR
                lSumAndScalingAbsorbedDoseRateCalculation.absorbed_dose_rate_per_voiat_time_point_produced.absorbed_dose_rate_per_voiat_time_point_produced.append(
                    lAbsorbedDoseRatePerVOIAtTimePoint)

                if folderIndex == 0:
                    lDoseRateCurveFitVOITimeIntegration = DoseRateCurveFitVoitimeIntegration()
                    lDoseRateCurveFitVOITimeIntegration.process_execution_context = self.__getProcessExecutionContext(
                        getDataNodeAttributeValue(doseRateTableNode, Attributes().studyCreation))
                    lDoseRateCurveFitVOITimeIntegration.time_point_identifier_used = TimePointIdentifierUsedContainer([
                                                                                                                      folderName])
                    lDoseRateCurveFitVOITimeIntegration.voiidentifier_used = VoiidentifierContainer()
                    lDoseRateCurveFitVOITimeIntegration.voiidentifier_used.voiidentifier_used.append(
                        segmentIdentifier)
                    lDoseRateCurveFitVOITimeIntegration.pre_administered_activity_used = u3DSlide2.acquisition_settings.pre_administered_activity
                    lDoseRateCurveFitVOITimeIntegration.post_administered_activity_used = u3DSlide2.acquisition_settings.post_administered_activity

                    rowDoseArray = doseTableArray['Segment'].index(
                        f"DR_{segmentName}")
                    lDoseRateCurveFitVOITimeIntegration.pkassessment_method_used = CurveFittingMethod()
                    algorithm = str.split(
                        doseTableArray['Fit function'][rowDoseArray], ':')[0]
                    algorithm = XMLConstants().FunctionTypeCategory[algorithm]
                    lDoseRateCurveFitVOITimeIntegration.pkassessment_method_used.incorporation_function = getDataNodeAttributeValue(
                        doseTableNode, "PKincorporation")
                    lDoseRateCurveFitVOITimeIntegration.pkassessment_method_used.integration_algorithm = algorithm
                    lDoseRateCurveFitVOITimeIntegration.pkassessment_method_used.fitting_function = doseTableArray[
                        'Fit function'][rowDoseArray]

                    lDoseRateCurveFitVOITimeIntegration.absorbed_dose_in_voiproduced = AbsorbedDoseInVoicontainer()
                    lAbsorbedDoseinVOI = AbsorbedDoseInVoi()
                    lAbsorbedDoseinVOI.absorbed_dose_in_voivalue = float(
                        str(doseTableArray['Absorbed Dose (Gy)'][rowDoseArray]))
                    lAbsorbedDoseinVOI.absorbed_dose_unit = AbsorbedDoseUnit.GRAY
                    lAbsorbedDoseinVOI.absorbed_dose_in_voiuncertainty = float(
                        str(doseTableArray['STDDEV (Gy)'][rowDoseArray]))
                    lAbsorbedDoseinVOI.voiidentifier_list = VoiidentifierContainer()
                    lAbsorbedDoseinVOI.voiidentifier_list.voiidentifier_used.append(
                        segmentIdentifier)
                    lDoseRateCurveFitVOITimeIntegration.absorbed_dose_in_voiproduced.absorbed_dose_in_voiproduced.append(
                        lAbsorbedDoseinVOI)
                    u3DSlide2.three_dim_dosimetry_slide2workflow.dose_rate_curve_fit_voitime_integration_container.dose_rate_curve_fit_voitime_integration.append(
                        lDoseRateCurveFitVOITimeIntegration)

                    lAbsorbedDoseInVOIProduced = AbsorbedDoseInVoi()
                    lAbsorbedDoseInVOIProduced.voiidentifier_list = VoiidentifierContainer()
                    lAbsorbedDoseInVOIProduced.voiidentifier_list.voiidentifier_used.append(
                        segmentIdentifier)
                    lAbsorbedDoseInVOIProduced.absorbed_dose_in_voivalue = float(
                        str(doseTableArray['Absorbed Dose (Gy)'][rowDoseArray]))
                    lAbsorbedDoseInVOIProduced.absorbed_dose_unit = AbsorbedDoseUnit.GRAY
                    u3DSlide2.three_dim_dosimetry_slide2workflow.absorbed_dose_calculation_in_voi.absorbed_dose_in_voicontainer.absorbed_dose_in_voiproduced.append(
                        lAbsorbedDoseInVOIProduced)
                else:
                    u3DSlide2.three_dim_dosimetry_slide2workflow.dose_rate_curve_fit_voitime_integration_container.dose_rate_curve_fit_voitime_integration[
                        segmentIndex].voiidentifier_used.voiidentifier_used.append(segmentIdentifier)
                    u3DSlide2.three_dim_dosimetry_slide2workflow.dose_rate_curve_fit_voitime_integration_container.dose_rate_curve_fit_voitime_integration[
                        segmentIndex].absorbed_dose_in_voiproduced.absorbed_dose_in_voiproduced[0].voiidentifier_list.voiidentifier_used.append(segmentIdentifier)
                    u3DSlide2.three_dim_dosimetry_slide2workflow.absorbed_dose_calculation_in_voi.absorbed_dose_in_voicontainer.absorbed_dose_in_voiproduced[
                        segmentIndex].voiidentifier_list.voiidentifier_used.append(segmentIdentifier)

            lRegistrationVOISegmentationAndPropagation.segmentation.non_dicomdata_container = NonDicomdataContainer()
            lnonDICOMData = self.__getNonDICOMData(
                segmentationNode, segmentationName, 'SEGM')
            lRegistrationVOISegmentationAndPropagation.segmentation.non_dicomdata_container.non_dicomdata.append(
                lnonDICOMData)
            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.voisegmentation_voimass_determination.segmentation_produced = RegistrationVoisegmentationAndPropagation.segmentation
            lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation.sum_and_scaling_absorbed_dose_rate_calculation_container.sum_and_scaling_absorbed_dose_rate_calculation.append(
                lSumAndScalingAbsorbedDoseRateCalculation)

            lRegistrationVOISegmentationAndPropagation.non_dicomctrecon_resampled_on_common_reference_produced = NonDicomdataContainer()
            lnonDICOMData = self.__getNonDICOMData(
                self.nodes["CTRS"].data, self.nodes["CTRS"].name, 'CTRS')
            lRegistrationVOISegmentationAndPropagation.non_dicomctrecon_resampled_on_common_reference_produced.non_dicomdata.append(
                lnonDICOMData)

            lRegistrationVOISegmentationAndPropagation.non_dicomnmtomo_recon_resampled_on_common_reference_produced = NonDicomdataContainer()
            lnonDICOMData = self.__getNonDICOMData(
                self.nodes["ACSC"].data, self.nodes["ACSC"].name, 'ACSC')
            lRegistrationVOISegmentationAndPropagation.non_dicomnmtomo_recon_resampled_on_common_reference_produced.non_dicomdata.append(
                lnonDICOMData)

            u3DSlide2.three_dim_dosimetry_slide2workflow.registration_voisegmentation_and_propagation_container.registration_voisegmentation_and_propagation.append(
                lRegistrationVOISegmentationAndPropagation)
            u3DSlide2.three_dim_dosimetry_slide2workflow.voisegmentation_energy_deposition_calculation_absorbed_dose_rate_calculation_container.voisegmentation_energy_deposition_calculation_absorbed_dose_rate_calculation.append(
                lVOISegmentationEnergyDepositionCalculationAbsorbedDoseRateCalculation)

        ProgressDialog.close()
        self.__writeOutput(u3DSlide2, self.studyFolder / '3D-DosimetrySlide2Worflow.xml')

    def fillSensitivity_XMLfile(self, tankNode):
        self.nodes["ACSC"] = tankNode
        self.__prepareDirectory('Calibration')
        sensitivity = readSPECTSensitivity()
        if not self.center in sensitivity:
            raise IOError(f"{self.center} is not yet added to database")
        isotope = tankNode.getIsotope()
        if not isotope in sensitivity[self.center]:
            raise IOError(f"Sensitivity is not yet calculated, {isotope} is not added")
        date = tankNode.getAcquisition()
        if not date in sensitivity[self.center][isotope]:
            raise IOError(f"Sensitivity is not yet calculated, {date} is not added")
        sensitivity_value = sensitivity[self.center][isotope][date]['Sensitivity']
        sensitivity_unit  = XMLConstants().CalibrationUnits[sensitivity[self.center][isotope][date]['Units']]
        tank_volume = getItemDataNodeAttributeValue(tankNode.nodeID, "Tank Volume (cm^3)")
        uCalibration = NonDicomFileSetDescriptor()

        uCalibration.referenced_clinical_research_study = ReferencedClinicalResearchStudy()
        uCalibration.referenced_clinical_research_study.clinical_research_study_id = self.ClinicalStudyID
        uCalibration.referenced_clinical_research_study.clinical_research_study_title = self.ClinicalStudyTitle

        uCalibration.patient_id = tankNode.getPatientID()

        uCalibration.acquisition_settings = self.__getAcquisitionSettingsCalibration(tankNode)

        uCalibration.calibration_workflow = CalibrationWorkflow()
        uCalibration.calibration_workflow.spectctcalibration_workflow = SpectctcalibrationWorkflow()
        uCalibration.calibration_workflow.spectctcalibration_workflow.spectsensitivity_calculation = SpectsensitivityCalculation()

        uCalibration.calibration_workflow.spectctcalibration_workflow.spectsensitivity_calculation.process_execution_context = self.__getProcessExecutionContext(
                        tankNode.getStudyCreation())
        uCalibration.calibration_workflow.spectctcalibration_workflow.spectsensitivity_calculation.spectcalibration_coefficient_produced = CalibrationCoefficient()
        uCalibration.calibration_workflow.spectctcalibration_workflow.spectsensitivity_calculation.spectcalibration_coefficient_produced.isotope = Isotope(XMLConstants(
                        ).IsotopesCategoryDict[isotope])
        uCalibration.calibration_workflow.spectctcalibration_workflow.spectsensitivity_calculation.spectcalibration_coefficient_produced.calibration_coefficient_value = sensitivity_value
        uCalibration.calibration_workflow.spectctcalibration_workflow.spectsensitivity_calculation.spectcalibration_coefficient_produced.calibration_coefficient_unit = sensitivity_unit
        uCalibration.calibration_workflow.spectctcalibration_workflow.spectsensitivity_calculation.spectcalibration_coefficient_produced.reference_calibration_date = date

        uCalibration.calibration_workflow.spectctcalibration_workflow.spectacq_ctacq_and_reconstruction_in_calibration = SpectacqCtacqAndReconstructionInCalibration()
        uCalibration.calibration_workflow.spectctcalibration_workflow.spectacq_ctacq_and_reconstruction_in_calibration.phantom_used = Nmphantom()
        uCalibration.calibration_workflow.spectctcalibration_workflow.spectacq_ctacq_and_reconstruction_in_calibration.phantom_used.nmphantom_identifier = f"{self.center} calibration tank"
        uCalibration.calibration_workflow.spectctcalibration_workflow.spectacq_ctacq_and_reconstruction_in_calibration.phantom_used.nmphantom_name = f"{self.center} calibration tank"
        uCalibration.calibration_workflow.spectctcalibration_workflow.spectacq_ctacq_and_reconstruction_in_calibration.phantom_used.tank = Tank()
        uCalibration.calibration_workflow.spectctcalibration_workflow.spectacq_ctacq_and_reconstruction_in_calibration.phantom_used.tank.isotope = Isotope(XMLConstants(
                        ).IsotopesCategoryDict[isotope])
        uCalibration.calibration_workflow.spectctcalibration_workflow.spectacq_ctacq_and_reconstruction_in_calibration.phantom_used.tank.tank_identifier = f"{self.center} calibration tank"     
        uCalibration.calibration_workflow.spectctcalibration_workflow.spectacq_ctacq_and_reconstruction_in_calibration.phantom_used.tank.volume_value = tank_volume
        uCalibration.calibration_workflow.spectctcalibration_workflow.spectacq_ctacq_and_reconstruction_in_calibration.phantom_used.tank.volume_unit = VolumeUnit.CUBIC_CENTIMETER
        uCalibration.calibration_workflow.spectctcalibration_workflow.spectacq_ctacq_and_reconstruction_in_calibration.phantom_used.tank.pre_admin_background_activity_value = tankNode.getInjectedActivity()
        uCalibration.calibration_workflow.spectctcalibration_workflow.spectacq_ctacq_and_reconstruction_in_calibration.phantom_used.tank.pre_admin_background_activity = ActivityUnit.MEGABECQUEREL
        uCalibration.calibration_workflow.spectctcalibration_workflow.spectacq_ctacq_and_reconstruction_in_calibration.phantom_used.tank.pre_admin_background_activity_timestamp = tankNode.getInjectionTime()
        uCalibration.calibration_workflow.spectctcalibration_workflow.spectacq_ctacq_and_reconstruction_in_calibration.phantom_used.tank.post_admin_background_activity_value = 0
        uCalibration.calibration_workflow.spectctcalibration_workflow.spectacq_ctacq_and_reconstruction_in_calibration.phantom_used.tank.post_admin_background_activity = ActivityUnit.MEGABECQUEREL
        uCalibration.calibration_workflow.spectctcalibration_workflow.spectacq_ctacq_and_reconstruction_in_calibration.phantom_used.tank.post_admin_background_activity_timestamp = tankNode.getInjectionTime()

        self.__writeOutput(uCalibration, self.studyFolder / 'CalibrationWorflow.xml')