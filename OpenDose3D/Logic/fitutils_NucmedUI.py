# -*- coding: utf-8 -*-
"""
All the functions for Fitting
Modified on 2023
@by: Nuclear Medicine Universitas Indonesia
"""

import numpy as np
from abc import abstractmethod
from inspect import signature
from itertools import product
from scipy.integrate import quad
from scipy.optimize import minimize

class FitFunction():
    def __init__(
            self, 
            time: np.ndarray, 
            data: np.ndarray, 
            param_value: dict, 
            fit_settings: dict, 
            bayesian: dict,
            var_model: str, 
            err_model: str, 
            scaling: str
        ):
        self.time = time
        self.data = data
        self.lamda = param_value["lamda"]
        
        if fit_settings in ["auto"]:
            self.varconst = [1, 0, 0]
            self.ftol = 1e-04
        elif var_model in ["proportional"]:
            _, _, _, SD, self.ftol = fit_settings.values()
            self.varconst = [0, SD*SD, 2]
        elif var_model in ["constant"]:
            _, _, _, SD, self.ftol = fit_settings.values()
            self.varconst = [SD*SD, 0, 0]
        elif var_model in ["poisson"]:
            _, _, _, SD, self.ftol = fit_settings.values()
            self.varconst = [0, SD*SD, 1]
        else:
            A, B, C, _, self.ftol = fit_settings.values()
            self.varconst = [A, B, C]
        
        self.bayesian = bayesian
        self.err_model = err_model
        self.scaling = scaling
        
        self.parameters = []
        self.bounds = []
        self.initParameters(param_value)

        self.res = None
        self.pcov = []
        self.report = ""
    
    def residuals(
            self, 
            parameters: np.ndarray, 
            time: np.ndarray, 
            data: np.ndarray
        ) -> np.ndarray:
        param = tuple(parameters)
        model = self.function(time, *param)
        residual = model - data
        return residual

    def variance(
            self, 
            parameters: np.ndarray, 
            time: np.ndarray, 
            data: np.ndarray
        ) -> np.ndarray:
        A, B, C = self.varconst
        if self.err_model in ["data-based"]:
            theta2 = A + B * (np.power(data, C)) #(data**C)
        elif self.err_model in ["model-based"]:
            param = tuple(parameters)
            model = self.function(time, *param)
            theta2 = A + B*(np.power(model, C)) #(model**C)
        return theta2
    
    def ScalingFactor(self, parameters: np.ndarray) -> float:
        theta2 = self.variance(parameters, self.time, self.data)
        residual = self.residuals(parameters, self.time, self.data)
        if self.scaling in ["absolute"]:
            nu = 1
        elif self.scaling in ["relative"]:
            subject_num = len(self.data)
            nu = (1/subject_num)*sum(residual*residual/theta2)
        return nu
    
    def ScaledVariance(
            self,
            parameters: np.ndarray,
            time: np.ndarray,
            data: np.ndarray
        ) -> np.ndarray:
        theta2 = self.variance(parameters, time, data)
        nu = self.ScalingFactor(parameters)
        sigma2 = nu*theta2
        return sigma2
    
    def OF_noBayesian(
            self,
            parameters: np.ndarray,
            time: np.ndarray,
            data: np.ndarray
        ) -> np.ndarray:
        residual = self.residuals(parameters, time, data)
        sigma2 = self.ScaledVariance(parameters, time, data)
        OF = np.log(2 * np.pi * sigma2) + (residual * residual / sigma2)
        return OF
        
    def minimize(self) -> None:
        if len(self.parameters) == 0:
            sig = signature(self.function)
            param_name = list(sig.parameters)[1:]
            param_matrix = []
            for name in param_name:
                grad = np.log(max(self.data))/self.time[0]
                if name in ['a', 'b', 'c']:
                    max_value = 1
                    test_line = np.linspace(0, max_value, 11)[1:-1]
                if 'A' in name:
                    max_value = 5*max(self.data)
                    test_line = np.linspace(0, max_value, 16)[1:-1]
                if 'L' in name:
                    max_value = 5*grad
                    test_line = np.geomspace(max_value, 1e-9*grad, 16)[1:-1]
                if 'l' in name:
                    max_value = 100*grad
                    test_line = np.geomspace(max_value, 1e-9*grad, 16)[1:-1]
                self.bounds.append((0, max_value))
                param_matrix.append(test_line)
            
            param_of = []
            for param in product(*param_matrix):
                res = self.residuals(param, self.time, self.data)
                of = sum(res*res)
                param_of.append((param, of))
            param_of.sort(key = lambda x: x[1] if not np.isnan(x[1]) else np.inf)
            
            param_list = [param_of[i][0] for i in range(0,3)] 
            OF = 999999999
            for parameter in param_list:
                res_temp = minimize(
                    self.objective,
                    parameter,
                    args = (self.time, self.data),
                    bounds = self.bounds, 
                    method = "Nelder-Mead", 
                    options = {"fatol": self.ftol}
                    )
                OF_temp = res_temp.fun
                if OF_temp < OF:
                    self.res = res_temp
        else:
            self.res = minimize(
                self.objective,
                self.parameters,
                args=(self.time, self.data),
                bounds=self.bounds, 
                method="Nelder-Mead", 
                options={"fatol":self.ftol}
                )
        self.parameters = self.res.x
        self.report = ""
    
    @abstractmethod
    def function(self, x: np.ndarray, *args) -> np.ndarray:
        pass
    
    @abstractmethod
    def initParameters(self, SV):
        pass
    
    @abstractmethod
    def objective(
            self, 
            parameters: np.ndarray,
            time: np.ndarray,
            data: np.ndarray
        ) -> np.ndarray:
        pass

    @abstractmethod
    def AUC(self) -> float:
        pass

    @abstractmethod
    def AUC_SD(self) -> float:
        pass
    
    def getParametersDict(self):
        return self.parameters.valuesdict()

    def getParametersTuple(self) -> tuple:
        return tuple(self.parameters)
    
    def getParameters(self) -> tuple:
        return tuple(self.parameters)
    
    def inverse_hessian(self) -> np.ndarray:
        param = list(self.parameters)
        bounds = self.bounds
        function = self.function(self.time, *param)
        sigma2 = self.ScaledVariance(self.parameters, self.time, self.data)
        jacob1 = []
        jacob2 = []
        for index, par in enumerate(param):
            factor = self.ftol * (bounds[index][1] - bounds[index][0]) / 10
            param_perturbation = param.copy()
            param_perturbation[index] = param[index] + factor
            diff_model = (
                self.function(self.time, *param_perturbation) - function
            ) / factor
            jacob1.append(diff_model)
            jacob2.append(diff_model/sigma2)
        jacob1 = np.array(jacob1).T
        jacob2 = np.array(jacob2).T
        hess = np.dot(jacob1.T, jacob2)
        inv_hess = np.linalg.inv(hess)
        return inv_hess
        
    def reduce_chi2(self) -> float:
        residual = self.residuals(self.parameters, self.time, data=self.data)
        sigma2 = self.ScaledVariance(self.parameters, self.time, self.data)
        subject_num = len(self.data)
        parameter_num = len(self.getParametersTuple())
        if self.scaling in ["absolute"]:
            reduce_chi2 = 1
        elif self.scaling in ["relative"]:
            dof = (subject_num - parameter_num)
            reduce_chi2 = sum(residual * residual / sigma2) / dof
        return reduce_chi2

    def getErrorTuple(self) -> tuple:
        self.pcov = self.inverse_hessian()
        reduce_chi2 = self.reduce_chi2()
        reduce_cov = reduce_chi2 * self.pcov
        error_param = np.sqrt(np.diag(reduce_cov))
        return tuple(error_param)

    def corrcov(self) -> np.ndarray:
        reduce_chi2 = self.reduce_chi2()
        reduce_cov = reduce_chi2 * self.pcov
        nu = np.sqrt(np.diag(reduce_cov))
        outer_nu = np.outer(nu, nu)
        correlation = reduce_cov / outer_nu
        correlation[reduce_cov == 0] = 0
        return correlation

    def evaluate(self, newx=None) -> np.ndarray:
        if newx is None:
            lnewx = np.array(self.x)
        else:
            lnewx = np.array(newx)
        p = tuple(self.parameters)
        model = self.function(lnewx, *p)
        return np.array(model)

    def plot(self, time: np.ndarray) -> np.ndarray:
        p = self.getParametersTuple()
        plt = self.function(time, *p)
        return plt    

    def AICc(self) -> float:
        OF = self.objective(self.parameters, self.time, data=self.data)
        n = len(self.data)
        k = len(self.getParametersTuple())
        if self.scaling in ["absolute"]:
            AICc = OF + 2 * k + 2 * (k + 1) * k / (n - k - 1)
        elif self.scaling in ["relative"]:
            AICc = OF + 2 * (k + 1) + 2 * (k + 1) * (k + 2) / (n - k - 2)
        return AICc

    def BIC(self) -> float:
        OF = self.objective(self.parameters, self.time, data=self.data)
        n = len(self.data)
        k = len(self.getParametersTuple())
        BIC = OF + k * np.log(n)
        return BIC
    
    def model_dict(self) -> dict:
        # Calculate objective function value
        OF = self.objective(self.parameters, self.time, data=self.data)
        fitting_summary = [self.__repr__()]

        # Calculate BIC and AICc
        BIC = self.BIC()
        try:
            AICc = self.AICc()
        except ZeroDivisionError:
            AICc = "Time Point doesn't match for AICc calculation"

        # Get model parameters and AUC
        params = self.getParametersTuple()
        AUC = self.AUC()

        # Calculate standard deviations of parameters and AUC
        try:
            params_sd = self.getErrorTuple()
            AUC_SD = self.AUC_SD()
        except (np.linalg.LinAlgError, ValueError, RuntimeWarning):
            params_sd = np.full(len(params), np.inf)
            AUC_SD = np.inf
            error_msg = (
                "Inverse Hessian is singular; covariance cannot be calculated."
                "\nConsider using different initial values."
            )
            fitting_summary.append(error_msg)

        # Calculate parameter correlation and coefficient of variation (CV)
        correlation = self.corrcov()
        nu = self.ScalingFactor(self.parameters)
        corr_list = correlation[correlation != np.diag(correlation)]

        CV = []
        for i, (param, param_sd) in enumerate(zip(params, params_sd)):
            cv = 100 * param_sd / param
            CV.append(cv)
            report_params = f"p{i + 1} = {param:.4f} +- {param_sd:.4f} (CV: {cv:.2f}%)"
            fitting_summary.append(report_params)

        # Compile report
        report = (
            f"OF = {OF:.4f}\n"
            f"Scaled data variance = {nu}\n"
            f"AICc = {AICc}\n"
            f"BIC = {BIC:.4f}\n"
            f"Correlation Matrix =\n{correlation}\n"
        )
        fitting_summary.append(report)

        # Construct the model dictionary
        mdict = {
            "function": self.__repr__(),
            "params": params,
            "params_sd": params_sd,
            "CV": CV,
            "correlation": corr_list,
            "AICc": AICc,
            "BIC": BIC,
            "AUC": AUC,
            "AUC_SD": AUC_SD,
            "Report": fitting_summary,
        }
        return mdict

    def integrate(self, loBound, hiBound):
        return quad(lambda x: self.evaluate(x), loBound, hiBound)

##############################################################################
"""
The following Equations is the Collection of Sum Of Exponential (SOE) Functions
from Hardiansyah, et al. (2024). 'Single-Time-Point Renal Dosimetry Using
Nonlinear Mixed-Effects Modeling and Population-Based Model Selection in 
[177Lu]Lu-PSMA-617 Therapy'. Journal of Nuclear Medicine. 
DOI: 10.2967/jnumed.123.266268
"""

class Equation2(FitFunction):
    def function(
            self,
            x: np.ndarray,
            A1: float,
            L1: float
        ) -> np.ndarray:
        fun = A1 * np.exp(-(L1 + self.lamda) * x)
        if A1 > 0 and L1 > 0:
            return fun
        else:
            return np.inf
    
    def minimize(self): 
        return super().minimize()
    
    def initParameters(self, SV: dict):
        if len(SV) != 1:
            A1value = SV["A1value"]
            L1value = SV["L1value"]
            self.parameters = [A1value, L1value]
            
            A1min, A1max = SV["A1min"], SV["A1max"]
            L1min, L1max = SV["L1min"], SV["L1max"]
            A1_bounds = (A1min, A1max)
            L1_bounds = (L1min, L1max)
            self.bounds = [A1_bounds, L1_bounds]
        
    def objective(
            self, parameters: np.ndarray,
            time: np.ndarray,
            data: np.ndarray
        ) -> float:
        OF = self.OF_noBayesian(parameters, time, data)
        if len(self.bayesian) != 0:
            A1mean = self.bayesian["A1mean"]
            A1sd = self.bayesian["A1sd"]
            L1mean = self.bayesian["L1mean"]
            L1sd = self.bayesian["L1sd"]
            A1_OF = ((parameters[0] - A1mean) / A1sd) ** 2 - \
                np.log(2 * np.pi * (A1sd ** 2))
            L1_OF = ((parameters[1] - L1mean) / L1sd) ** 2 - \
                np.log(2 * np.pi * (L1sd ** 2))
            return sum(OF) + A1_OF + L1_OF
        else:
            return sum(OF)

    def AUC(self) -> float:
        A1_fit, L1_fit = self.parameters
        AUC = A1_fit / (L1_fit + self.lamda)
        return AUC

    def AUC_SD(self) -> float:
        A1_fit, L1_fit = self.parameters
        A1_sd, L1_sd = self.getErrorTuple()
        AUC = self.AUC()
        AUC_SD = AUC * np.sqrt((A1_sd / A1_fit) ** 2 + (L1_sd / L1_fit) ** 2)
        return AUC_SD

    def __repr__(self) -> str:
        p = self.getParametersTuple()
        equation = f"f2: y = {p[0]:.3e} * exp(-({p[1]:.3e} + {self.lamda:.3e}) * x)"
        return equation

class Equation3(FitFunction):
    def function(
            self,
            x: np.ndarray,
            A1: float,
            L1: float,
            l2: float
        ) -> np.ndarray:
        fun = A1 * np.exp(-(L1 + self.lamda) * x) - \
            A1 * np.exp(-(l2 + self.lamda) * x)
        if A1 > 0 and l2 > L1 >0:
            return fun
        else:
            return np.inf

    def minimize(self):
        return super().minimize()

    def initParameters(self, SV: dict):
        if len(SV) != 1:
            A1value = SV["A1value"]
            L2value = SV["L2value"]
            L1value = SV["L1value"]
            self.parameters = [A1value, L1value, L2value]
            
            A1min, A1max = SV["A1min"], SV["A1max"]
            L1min, L1max = SV["L1min"], SV["L1max"]
            L2min, L2max = SV["L2min"], SV["L2max"]
            A1_bounds = (A1min, A1max)
            L1_bounds = (L1min, L1max)
            L2_bounds = (L2min, L2max)
            self.bounds = [A1_bounds, L1_bounds, L2_bounds]
    
    def objective(
            self,
            parameters: np.ndarray,
            time: np.ndarray,
            data: np.ndarray
        ) -> float:
        OF = self.OF_noBayesian(parameters, time, data)
        if len(self.bayesian) != 0:
            A1mean = self.bayesian["A1mean"]
            A1sd = self.bayesian["A1sd"]
            L1mean = self.bayesian["L1mean"]
            L1sd = self.bayesian["L1sd"]
            L2mean = self.bayesian["L2mean"]
            L2sd = self.bayesian["L2sd"]
            A1_OF = ((parameters[0] - A1mean) / A1sd) ** 2 - \
                np.log(2 * np.pi * (A1sd ** 2))
            L1_OF = ((parameters[2] - L1mean) / L1sd) ** 2 - \
                np.log(2 * np.pi * (L1sd ** 2))
            L2_OF = ((parameters[3] - L2mean) / L2sd) ** 2 - \
                np.log(2 * np.pi * (L2sd ** 2))
            return sum(OF) + A1_OF + L1_OF +L2_OF
        else:
            return sum(OF)

    def AUC(self) -> float:
        A1_fit, L1_fit, L2_fit = self.parameters
        AUC = A1_fit / (L1_fit + self.lamda) - A1_fit / (L2_fit + self.lamda)
        return AUC

    def AUC_SD(self) -> float:
        A1_fit, L1_fit, L2_fit = self.parameters
        A1_sd, L1_sd, L2_sd = self.getErrorTuple()
        AUC1 = A1_fit / (L1_fit + self.lamda)
        AUC2 = A1_fit / (L2_fit + self.lamda)
        AUC_SD1 = AUC1 * np.sqrt((A1_sd / A1_fit) ** 2 + (L1_sd / L1_fit) ** 2)
        AUC_SD2 = AUC2 * np.sqrt((A1_sd / A1_fit) ** 2 + (L2_sd / L2_fit) ** 2)
        AUC_SD = np.sqrt((AUC_SD1) ** 2 + (AUC_SD2) ** 2)
        return AUC_SD

    def __repr__(self) -> str:
        p = self.getParametersTuple()
        equation_string = \
            f"f3: y = {p[0]:.3e} * exp(-({p[1]:.3e} + {self.lamda:.3e}) * x)" + \
            f" - {p[0]:.3e} * exp(-({p[2]:.3e} + {self.lamda:.3e}) * x)"
        return equation_string

class Equation3a(FitFunction):
    def function(
            self,
            x: np.ndarray,
            A1: float,
            a: float, 
            L1: float
        ) -> np.ndarray:
        fun = A1 * a * np.exp(-(L1 + self.lamda) * x) + \
            A1 * (1 - a) * np.exp(-(self.lamda) * x)
        if A1 > 0 and L1 > 0 and 1 > a > 0:
            return fun
        else:
            return np.inf

    def initParameters(self, SV: dict):
        if len(SV) != 1:
            A1value = SV["A1value"]
            a_value = SV["a_value"]
            L1value = SV["L1value"]
            self.parameters = [A1value, a_value, L1value]
            
            A1min, A1max = SV["A1min"], SV["A1max"]
            L1min, L1max = SV["L1min"], SV["L1max"]
            A1_bounds = (A1min, A1max)
            L1_bounds = (L1min, L1max)
            a_bounds = (0, 1)
            self.bounds = [A1_bounds, a_bounds, L1_bounds]
    
    def objective(
            self,
            parameters: np.ndarray, 
            time: np.ndarray, 
            data: np.ndarray
        ) -> float:
        OF = self.OF_noBayesian(parameters, time, data)
        if len(self.bayesian) != 0:
            A1mean = self.bayesian["A1mean"]
            A1sd = self.bayesian["A1sd"]
            a_mean = self.bayesian["a_mean"]
            a_sd = self.bayesian["a_sd"]
            L1mean = self.bayesian["L1mean"]
            L1sd = self.bayesian["L1sd"]
            A1_OF = ((parameters[0] - A1mean) / A1sd) ** 2 - \
                np.log(2 * np.pi * (A1sd ** 2))
            a_OF = ((parameters[1] - a_mean) / a_sd) ** 2 - \
                np.log(2 * np.pi * (a_sd ** 2))
            L1_OF = ((parameters[2] - L1mean) / L1sd) ** 2 - \
                np.log(2 * np.pi * (L1sd ** 2))
            return sum(OF) + A1_OF + a_OF + L1_OF
        else:
            return sum(OF)
    
    def AUC(self) -> float:
        A1_fit, a_fit, L1_fit = self.parameters
        AUC = (A1_fit * a_fit) / (L1_fit + self.lamda) + \
            A1_fit * (1 - a_fit) / self.lamda
        return AUC
    
    def AUC_SD(self) -> float:
        A1_fit, a_fit, L1_fit = self.parameters
        A1_sd, a_sd, L1_sd = self.getErrorTuple()
        AUC1 = (A1_fit * a_fit) / (L1_fit + self.lamda)
        AUC2 = A1_fit * (1 - a_fit) / self.lamda
        AUC_SD1 = AUC1 * np.sqrt(
            (A1_sd / A1_fit) ** 2 +
            (L1_sd / L1_fit) ** 2 +
            (a_sd / a_fit) ** 2
        )
        AUC_SD2 = AUC2 * np.sqrt(
            (A1_sd / A1_fit) ** 2 + 
            (a_sd / a_fit) ** 2
        )
        AUC_SD = np.sqrt(AUC_SD1 ** 2 + AUC_SD2 ** 2)
        return AUC_SD
    
    def __repr__(self) -> str:
        p = self.getParametersTuple()
        equation_string = \
            f"f3a: y = {p[0]:.3e}*{p[1]:.3e}*exp(-({p[2]:.3e}+{self.lamda:.3e})*x)" + \
            f" + {p[0]:.3e}*(1-{p[1]:.3e})*exp(-{self.lamda:.3e}*x)"
        return equation_string

class Equation4a(FitFunction):
    def function(
            self,
            x: np.ndarray, 
            A1: float, 
            A2: float, 
            L1: float, 
            L2: float
        ) -> np.ndarray:
        fun = A1 * np.exp(-(L1 + self.lamda) * x) + \
            A2 * np.exp(-(L2 + self.lamda) * x)
        if A1 > 0 and A2 > 0 and L1 > 0 and L2 > 0:
            return fun
        else:
            return np.inf

    def initParameters(self, SV: dict):
        if len(SV) != 1:
            A1value = SV["A1value"]
            A2value = SV["A2value"]
            L1value = SV["L1value"]
            L2value = SV["L2value"]
            self.parameters = [A1value, A2value, L1value, L2value]
            
            A1min, A1max = SV["A1min"], SV["A1max"]
            A2min, A2max = SV["A2min"], SV["A2max"]
            L1min, L1max = SV["L1min"], SV["L1max"]
            L2min, L2max = SV["L2min"], SV["L2max"]
            A1_bounds = (A1min, A1max)
            A2_bounds = (A2min, A2max)
            L1_bounds = (L1min, L1max)
            L2_bounds = (L2min, L2max)
            self.bounds = [A1_bounds, A2_bounds, L1_bounds, L2_bounds]
    
    def objective(
            self, parameters: np.ndarray, time: np.ndarray, data: np.ndarray
        ) -> float:
        OF = self.OF_noBayesian(parameters, time, data)
        if len(self.bayesian) != 0:
            A1mean = self.bayesian["A1mean"]
            A1sd = self.bayesian["A1sd"]
            A2mean = self.bayesian["A2mean"]
            A2sd = self.bayesian["A2sd"]
            L1mean = self.bayesian["L1mean"]
            L1sd = self.bayesian["L1sd"]
            L2mean = self.bayesian["L2mean"]
            L2sd = self.bayesian["L2sd"]
            A1_OF = ((parameters[0] - A1mean) / A1sd) ** 2 - \
                np.log(2 * np.pi * (A1sd ** 2))
            A2_OF = ((parameters[1] - A2mean) / A2sd) ** 2 - \
                np.log(2 * np.pi * (A2sd ** 2))
            L1_OF = ((parameters[2] - L1mean) / L1sd) ** 2 - \
                np.log(2 * np.pi * (L1sd ** 2))
            L2_OF = ((parameters[3] - L2mean) / L2sd) ** 2 - \
                np.log(2 * np.pi * (L2sd ** 2))
            return sum(OF) + A1_OF + A2_OF + L1_OF +L2_OF
        else:
            return sum(OF)
    
    def AUC(self) -> float:
        A1_fit, A2_fit, L1_fit, L2_fit = self.parameters
        AUC = A1_fit / (L1_fit + self.lamda) + A2_fit / (L2_fit + self.lamda)
        return AUC
    
    def AUC_SD(self) -> float:
        A1_fit, A2_fit, L1_fit, L2_fit = self.parameters
        A1_sd, A2_sd, L1_sd, L2_sd = self.getErrorTuple()
        AUC1 = A1_fit / (L1_fit + self.lamda)
        AUC2 = A2_fit / (L2_fit + self.lamda)
        AUC_SD1 = AUC1 * np.sqrt((A1_sd / A1_fit) ** 2 + (L1_sd / L1_fit) ** 2)
        AUC_SD2 = AUC2 * np.sqrt((A2_sd / A2_fit) ** 2 + (L2_sd / L2_fit) ** 2)
        AUC_SD = np.sqrt((AUC_SD1) ** 2 + (AUC_SD2) ** 2)
        return AUC_SD
    
    def __repr__(self) -> str:
        p = self.getParametersTuple()
        equation_string = \
            f"f4a: y = {p[0]:.3e} * exp(-({p[2]:.3e} + {self.lamda:.3e}) * x)" + \
            f" + {p[1]:.3e} * exp(-({p[3]:.3e} + {self.lamda:.3e}) * x)"
        return equation_string

class Equation4b(FitFunction):
    def function(
            self, 
            x: np.ndarray, 
            A1: float, 
            A2: float, 
            L1: float, 
            l2: float
        ) -> np.ndarray:
        Lbc = 60 * np.log(2)
        fun = A1 * np.exp(-(L1 + self.lamda) * x) - \
            A2 * np.exp(-(l2 + self.lamda) * x) - \
            (A1 - A2) * np.exp(-(Lbc + self.lamda) * x)
        if A1 > 0 and A2 > 0 and L1 > 0 and l2 > 0:
            return fun
        else:
            return np.inf

    def minimize(self):
        return super().minimize()

    def initParameters(self, SV: dict):
        if len(SV) != 1:
            A1value = SV["A1value"]
            A2value = SV["A2value"]
            L1value = SV["L1value"]
            L2value = SV["L2value"]
            self.parameters = [A1value, A2value, L1value, L2value]
            
            A1min, A1max = SV["A1min"], SV["A1max"]
            A2min, A2max = SV["A2min"], SV["A2max"]
            L1min, L1max = SV["L1min"], SV["L1max"]
            L2min, L2max = SV["L2min"], SV["L2max"]
            A1_bounds = (A1min, A1max)
            A2_bounds = (A2min, A2max)
            L1_bounds = (L1min, L1max)
            L2_bounds = (L2min, L2max)
            self.bounds = [A1_bounds, A2_bounds, L1_bounds, L2_bounds]
    
    def objective(
            self, parameters: np.ndarray, time: np.ndarray, data: np.ndarray
        ) -> float:
        OF = self.OF_noBayesian(parameters, time, data)
        if len(self.bayesian) != 0:
            A1mean = self.bayesian["A1mean"]
            A1sd = self.bayesian["A1sd"]
            A2mean = self.bayesian["A2mean"]
            A2sd = self.bayesian["A2sd"]
            L1mean = self.bayesian["L1mean"]
            L1sd = self.bayesian["L1sd"]
            L2mean = self.bayesian["L2mean"]
            L2sd = self.bayesian["L2sd"]
            A1_OF = ((parameters[0] - A1mean) / A1sd) ** 2 - \
                np.log(2 * np.pi * (A1sd ** 2))
            A2_OF = ((parameters[1] - A2mean) / A2sd) ** 2 - \
                np.log(2 * np.pi * (A2sd ** 2))
            L1_OF = ((parameters[2] - L1mean) / L1sd) ** 2 - \
                np.log(2 * np.pi * (L1sd ** 2))
            L2_OF = ((parameters[3] - L2mean) / L2sd) ** 2 - \
                np.log(2 * np.pi * (L2sd ** 2))
            return sum(OF) + A1_OF + A2_OF + L1_OF +L2_OF
        else:
            return sum(OF)

    def AUC(self) -> float:
        A1_fit, A2_fit, L1_fit, L2_fit = self.parameters
        AUC = A1_fit / (L1_fit + self.lamda) - \
            A2_fit / (L2_fit + self.lamda) - \
            (A1_fit - A2_fit) / (60 * np.log(2) + self.lamda)
        return AUC

    def AUC_SD(self) -> float:
        A1_fit, A2_fit, L1_fit, L2_fit = self.parameters
        A1_sd, A2_sd, L1_sd, L2_sd = self.getErrorTuple()
        AUC1 = A1_fit / (L1_fit + self.lamda)
        AUC2 = A2_fit / (L2_fit + self.lamda)
        AUC_SD1 = AUC1 * np.sqrt((A1_sd / A1_fit) ** 2 + (L1_sd / L1_fit) ** 2)
        AUC_SD2 = AUC2 * np.sqrt((A2_sd / A2_fit) ** 2 + (L2_sd / L2_fit) ** 2)
        AUC_SD = np.sqrt(
            (AUC_SD1) ** 2 + (AUC_SD2) ** 2 + (A1_sd) ** 2 + (A2_sd) ** 2
        )
        return AUC_SD

    def __repr__(self) -> str:
        Lbc = 60*np.log(2)
        p = self.getParametersTuple()
        equation_string = \
            f"f4b: y = {p[0]:.3e}*exp(-({p[2]:.3e}+{self.lamda:.3e})*x)" + \
            f" - {p[1]:.3e}*exp(-({p[3]:.3e}+{self.lamda:.3e})*x)" + \
            f" - ({p[0]:.3e}-{p[1]:.3e})*exp(-({Lbc:.3e})+{self.lamda:.3e})*x"
        return equation_string

class Equation4c(FitFunction):
    def function(
            self, 
            x: np.ndarray, 
            A1: float, 
            A2: float, 
            L1: float, 
            L2: float
        ) -> np.ndarray:
        Lbc = 60 * np.log(2)
        fun = A1 * np.exp(-(L1 + self.lamda) * x) + \
            A2 * np.exp(-(L2 + self.lamda) * x) - \
            (A1 + A2) * np.exp(-(Lbc + self.lamda) * x)
        if A1 > 0 and A2 > 0 and L1 > 0 and L2 > 0:
            return fun
        else:
            return np.inf

    def minimize(self):
        return super().minimize()

    def initParameters(self, SV: dict):
        if len(SV) != 1:
            A1value = SV["A1value"]
            A2value = SV["A2value"]
            L1value = SV["L1value"]
            L2value = SV["L2value"]
            self.parameters = [A1value, A2value, L1value, L2value]
            
            A1min, A1max = SV["A1min"], SV["A1max"]
            A2min, A2max = SV["A2min"], SV["A2max"]
            L1min, L1max = SV["L1min"], SV["L1max"]
            L2min, L2max = SV["L2min"], SV["L2max"]
            A1_bounds = (A1min, A1max)
            A2_bounds = (A2min, A2max)
            L1_bounds = (L1min, L1max)
            L2_bounds = (L2min, L2max)
            self.bounds = [A1_bounds, A2_bounds, L1_bounds, L2_bounds]
    
    def objective(
            self, parameters: np.ndarray, time: np.ndarray, data: np.ndarray
        ) -> float:
        OF = self.OF_noBayesian(parameters, time, data)
        if len(self.bayesian) != 0:
            A1mean = self.bayesian["A1mean"]
            A1sd = self.bayesian["A1sd"]
            A2mean = self.bayesian["A2mean"]
            A2sd = self.bayesian["A2sd"]
            L1mean = self.bayesian["L1mean"]
            L1sd = self.bayesian["L1sd"]
            L2mean = self.bayesian["L2mean"]
            L2sd = self.bayesian["L2sd"]
            A1_OF = ((parameters[0] - A1mean) / A1sd) ** 2 - \
                np.log(2 * np.pi * (A1sd ** 2))
            A2_OF = ((parameters[1] - A2mean) / A2sd) ** 2 - \
                np.log(2 * np.pi * (A2sd ** 2))
            L1_OF = ((parameters[2] - L1mean) / L1sd) ** 2 - \
                np.log(2 * np.pi * (L1sd ** 2))
            L2_OF = ((parameters[3] - L2mean) / L2sd) ** 2 - \
                np.log(2 * np.pi * (L2sd ** 2))
            return sum(OF) + A1_OF + A2_OF + L1_OF +L2_OF
        else:
            return sum(OF)

    def AUC(self) -> float:
        A1_fit, A2_fit, L1_fit, L2_fit = self.parameters
        AUC = A1_fit / (L1_fit + self.lamda) + \
            A2_fit / (L2_fit + self.lamda) - \
            (A1_fit + A2_fit) / (60 * np.log(2) + self.lamda)
        return AUC

    def AUC_SD(self) -> float:
        A1_fit, A2_fit, L1_fit, L2_fit = self.parameters
        A1_sd, A2_sd, L1_sd, L2_sd = self.getErrorTuple()
        AUC1 = A1_fit / (L1_fit + self.lamda)
        AUC2 = A2_fit / (L2_fit + self.lamda)
        AUC_SD1 = AUC1 * np.sqrt((A1_sd / A1_fit) ** 2 + (L1_sd / L1_fit) ** 2)
        AUC_SD2 = AUC2 * np.sqrt((A2_sd / A2_fit) ** 2 + (L2_sd / L2_fit) ** 2)
        AUC_SD = np.sqrt(
            (AUC_SD1) ** 2 + (AUC_SD2) ** 2 + (A1_sd) ** 2 + (A2_sd) ** 2
        )
        return AUC_SD

    def __repr__(self) -> str:
        Lbc = 60*np.log(2)
        p = self.getParametersTuple()
        equation_string = \
            f"f4c: y = {p[0]:.3e}*exp(-({p[2]:.3e}+{self.lamda:.3e})*x)" + \
            f" + {p[1]:.3e}*exp(-({p[3]:.3e}+{self.lamda:.3e})*x)" + \
            f" - ({p[0]:.3e}+{p[1]:.3e})*exp(-({Lbc:.3e})+{self.lamda:.3e})*x"
        return equation_string
    
class RecoveryFunction(FitFunction):
    def function(self, x, b, c):  # "y = 1 - 1 / (1 + (x / b) ^ c)"
        return 1 - 1 / (1 + np.power(x / b, c))

    def minimize(self):
        return super().minimize(True)

    def __repr__(self):
        p = self.getParameters()
        return f"R(v) = 1 - 1 / (1 + (v / {p[0]:.2f}) ^ {p[1]:.2f})"