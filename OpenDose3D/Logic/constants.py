from __future__ import annotations
from singleton_decorator import singleton
from Logic.PhysicalUnits import PhysicalUnits
from Logic.config import readOptions


@singleton
class Constants:

    def __init__(self):
        self.timeFormat = "%Y-%m-%d %H:%M:%S"
        self.enableColors = True  # If False no colors are displayed
        self._units = self.__setupUnits()
        self._elements = self.__setupElements()
        self._materials = self.__setupMaterials()
        self._isotopes = self.__setupIsotopes()
        self._colors = self.__setupColors()
        self._strings = self.__setupStrings()

    @property
    def units(self) -> dict:
        return self._units

    def __setupUnits(self) -> dict[str:float]:
        result = {}
        physicUnits = PhysicalUnits()
        result["MBq"] = physicUnits.getUnit("Activity", "MBq")
        result["Gy"] = physicUnits.getUnit("Dose", "Gy")
        result["mGy"] = physicUnits.getUnit("Dose", "mGy")
        result["hour"] = physicUnits.getUnit("Time", "h")
        result["gram"] = physicUnits.getUnit("Mass", "g")
        result["kg"] = physicUnits.getUnit("Mass", "kg")
        result["cm3"] = physicUnits.getUnit("Longitude", "cm") ** 3
        result["mm"] = physicUnits.getUnit("Longitude", "mm")
        result["mm3"] = result["mm"] ** 3
        result["g_cm3"] = result["gram"] / result["cm3"]
        result["kg_m3"] = result["kg"] / physicUnits.getUnit("Longitude", "m") ** 3
        result["Joule"] = physicUnits.getUnit("Energy", "J")
        result["keV"] = physicUnits.getUnit("Energy", "keV")
        result["MeV"] = physicUnits.getUnit("Energy", "MeV")
        result["mGy_MBqs"] = (
            result["mGy"] / result["MBq"] / physicUnits.getUnit("Time", "s")
        )
        result["mGy_h"] = result["mGy"] / result["hour"]
        result["MBqh"] = result["MBq"] * result["hour"]
        return result

    @property
    def elements(self) -> dict:
        return self._elements

    def __setupElements(self) -> dict[str : list[str]]:
        # The following is for guessing the isotope from the Dicom header which can be anything
        return {  # one*, two, three and all characters identifying the element
            "I": ["i", "io", "iod", "iodine"],
            "Lu": ["lu", "lut", "luthetium"],
            "Y": ["y", "yt", "ytt", "yttrium"],
            "Tb": ["tb", "ter", "terbium"],
            "Ho": ["ho", "hol", "holmium"],
            "F": ["f", "fl", "fluor", "fluorine"],
            "Cu": ["cu", "cop", "copper"],
            "Tc": ["tc", "tec", "technetium"],
            "Ga": ["ga", "gal", "gallium"],
            "In": ["in", "ind", "indium"],
            "Sm": ["sm", "sam", "samarium"],
        }

    @property
    def materials(self) -> dict:
        return self._materials

    def __setupMaterials(self) -> dict[str : dict[str : float | str]]:
        import json
        # Define the names for each material
        material_names = {
            "water": "Water",
            "soft": "Soft Tissue",
            "bone": "Cortical Bone",
            "lung": "Lungs",
            "adipose": "Adipose"
        }
        options = readOptions()
        materials = {}
        for key, density in options["ADR Calculation"]["Materials"].items():
            density = density * self.units["kg_m3"]
            name = material_names[key]
            materials[key] = {"density": density, "name": name}
                
        return materials

    @property
    def isotopes(self) -> dict[str : dict[str : float | str]]:
        return self._isotopes

    def __setupIsotopes(self) -> dict[str : dict[str : float | str]]:
        # TODO: The following can be an external json
        return {
            "I-124": {
                "Z": 53,
                "A": 124,
                "name": "I-124",
                "energy": 0.1942902001527291 * self.units["MeV"],
                "excitation": 0 * self.units["keV"],
                "T_h": 4.176 * 24,
                "T_eff": 68,
            },
            "I-131": {
                "Z": 53,
                "A": 131,
                "name": "I-131",
                "energy": 0.19197150954058237 * self.units["MeV"],
                "excitation": 0 * self.units["keV"],
                "T_h": 192.4968,
                "T_eff": 68,
            },
            "Lu-177": {
                "Z": 71,
                "A": 177,
                "name": "Lu-177",
                "energy": 0.14822654849246394 * self.units["MeV"],
                "excitation": 0 * self.units["keV"],
                "T_h": 159.528,
                "T_eff": 159.528,
            },
            "Y-90": {
                "Z": 39,
                "A": 90,
                "name": "Y-90",
                "energy": 0.9318135072491792 * self.units["MeV"],
                "excitation": 0 * self.units["keV"],
                "T_h": 64.1,
                "T_eff": 64.1,
            },
            "Tb-161": {
                "Z": 65,
                "A": 161,
                "name": "Tb-161",
                "energy": 0.2024886595428373 * self.units["MeV"],
                "excitation": 0 * self.units["keV"],
                "T_h": 6.906 * 24,
                "T_eff": 6.906 * 24,
            },
            "Ho-166": {
                "Z": 67,
                "A": 166,
                "name": "Ho-166",
                "energy": 0.6963382921351617 * self.units["MeV"],
                "excitation": 0 * self.units["keV"],
                "T_h": 26.8,
                "T_eff": 26.8,
            },
            "Cu-64": {
                "Z": 29,
                "A": 64,
                "name": "Cu-64",
                "energy": 0.12475065996150267 * self.units["MeV"],
                "excitation": 0 * self.units["keV"],
                "T_h": 12.7,
                "T_eff": 12.7,
            },
            "Cu-67": {
                "Z": 29,
                "A": 67,
                "name": "Cu-67",
                "energy": 0.15038785752871697 * self.units["MeV"],
                "excitation": 0 * self.units["keV"],
                "T_h": 61.83,
                "T_eff": 61.83,
            },
            "Tc-99m": {
                "Z": 43,
                "A": 99,
                "name": "Tc-99m",
                "energy": 0.01618872154622644 * self.units["MeV"],
                "excitation": 143 * self.units["keV"],
                "T_h": 6.015,
                "T_eff": 6.015,
            },
            "F-18": {
                "Z": 9,
                "A": 18,
                "name": "F-18",
                "energy": 1.2301889248 * self.units["MeV"],
                "excitation": 0 * self.units["keV"],
                "T_h": 1.8295,
                "T_eff": 1.8295,
            },
            "Ga-68": {
                "Z": 31,
                "A": 68,
                "name": "Ga-68",
                "energy": 1.647596 * self.units["MeV"],
                "excitation": 0 * self.units["keV"],
                "T_h": 67.71 / 60,
                "T_eff": 67.71 / 60,
            },
            "In-111": {
                "Z": 49,
                "A": 111,
                "name": "In-111",
                "energy": 0.034820 * self.units["MeV"],
                "excitation": 0 * self.units["keV"],
                "T_h": 2.8047 * 24,
                "T_eff": 2.8047 * 24,
            },
            "Sm-153": {
                "Z": 62,
                "A": 153,
                "name": "Sm-153",
                "energy": 0.269890 * self.units["MeV"],
                "excitation": 0 * self.units["keV"],
                "T_h": 46.5,
                "T_eff": 46.5,
            },
        }

    @property
    def colors(self) -> dict:
        return self._colors

    def __setupColors(self) -> dict[str : str | list[str]]:
        return {
            "Grays": ["#EDE6DE", "#DDD6CD", "#CDC6BC", "#BDB6AB"],
            "Violets": ["#D1D0DE", "#B5B5CA", "#A999B7", "#9C7EA2"],
            "Parameters": "#EDE6DE",  # light gray
            "Processing": "#D1D0DE",  # teal
            "Calculation": "#BDB6AB",  # dark gray
            "Slicer": "#9C9EB5",  # violet
        }

    @property
    def strings(self) -> dict:
        return self._strings

    def __setupStrings(self) -> dict[str:str]:
        # TODO: Add all strings here. Add translation support.
        return {
            "Scaling not Done": "Study scaling has not been performed, please do so before calling this method",
            "Choose Directory": "Choose Directory",
        }

    def createCSS(self, color: str) -> str:
        return f":enabled {{ color: black; background-color: {color} }}"
