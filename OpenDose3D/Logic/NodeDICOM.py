import numpy as np
import pydicom
from Logic import vtkmrmlutils
from Logic.NodeMRB import NodeMRB
from Logic.attributes import Attributes
from Logic.constants import Constants
from Logic.dbutils import getIsotopes
from Logic.errors import DICOMError, IOError


import slicer


import re
from datetime import datetime

# NodeMRB is used for previously exported nodes which are expected to have
# dicom attributes stored in their itemDataNodes
# NodeDICOM is used for all nodes which do have dicom data associated
# Look up dicom attributes on http://dicomlookup.com or
# https://northstar-www.dartmouth.edu/doc/nodeIDl/html_6.2/DICOM_Attributes.html
class NodeDICOM(NodeMRB):

    def __init__(self, nodeID: str, default_isotope: str, default_activity: str):
        super().__init__(nodeID)
        self.dataset = self.loadDICOM()
        self.setAttributes(default_isotope, default_activity)
        self.fixNonStandardCorrection()

    def loadDICOM(self):
        """
        Loads DICOM dataset
        """
        instanceUIDs = self.data.GetAttribute("DICOM.instanceUIDs").split()
        # get first dicom header as a reference for all other slices
        filename = slicer.dicomDatabase.fileForInstance(instanceUIDs[0])
        dataset = pydicom.read_file(filename)
        return dataset

    def setAttributes(self, default_isotope: str, default_activity: str):
        """
        Sets all itemDataNodeAttributes which are readable from DICOM data
        """
        self.setModality()
        modality = self.getModality()

        # this is always set
        studyInstanceUID = self.getStudyInstanceUID()
        if not studyInstanceUID:
            self.setStudyInstanceUID()
            studyInstanceUID = self.getStudyInstanceUID()

        seriesInstanceUID = self.getSeriesInstanceUID()
        if not seriesInstanceUID:
            self.setSeriesInstanceUID()
            seriesInstanceUID = self.getSeriesInstanceUID()

        institutionName = self.getInstitutionName()
        if not institutionName:
            self.setInstitutionName()
            institutionName = self.getInstitutionName()

        patientID = self.getPatientID()
        if not patientID:
            self.setPatientID()
            patientID = self.getPatientID()

        patientName = self.getPatientName()
        if not patientName:
            self.setPatientName()
            patientName = self.getPatientName()

        studyCreation = self.getStudyCreation()
        if not studyCreation:
            self.setStudyCreation()
            studyCreation = self.getStudyCreation()

        acquisition = self.getAcquisition()
        if not acquisition or acquisition is None:
            self.setAcquisition()
            acquisition = self.getAcquisition()

        acquisitionDuration = self.getAcquisitionDuration()
        if not acquisitionDuration:
            self.setAcquisitionDuration()
            acquisitionDuration = self.getAcquisitionDuration()

        if modality in ["PT", "NM"]:
            radiopharmaceutical = self.getIsotope()
            if (
                not radiopharmaceutical
                or radiopharmaceutical == "None"
                or radiopharmaceutical is None
            ):
                try:
                    radiopharmaceutical = self.readRadiopharmaceutical()
                except Exception as e:
                    self.logger.warning(f"{e}")
                    radiopharmaceutical = default_isotope
            self.setIsotope(radiopharmaceutical)

            injectedActivity = self.getInjectedActivity()
            if not injectedActivity:
                try:
                    injectedActivity = self.readInjectionActivity()
                except Exception as e:
                    self.logger.warning(f"{e}")
                    injectedActivity = default_activity
            vtkmrmlutils.setItemDataNodeAttribute(
                self.nodeID, Attributes().injectedActivity, injectedActivity
            )

            try:
                dicomInjectionDateTime = datetime.strptime(
                    self.readInjectionDateTime(), "%Y%m%d%H%M%S"
                )
                dicomInjectionDateTimeStr = dicomInjectionDateTime.strftime("%H%M%S")
                if int(dicomInjectionDateTimeStr) <= 0:
                    raise
                else:
                    injectionTime = dicomInjectionDateTime.strftime(
                        Constants().timeFormat
                    )
            except:
                try:
                    acqDate = datetime.strptime(
                        acquisition, Constants().timeFormat
                    ).strftime("%Y%m%d")
                    dicomInjectionTimeStr = self.readInjectionTime()
                    if int(dicomInjectionTimeStr) <= 0:
                        raise
                    else:
                        injectionTime = datetime.strptime(
                            f"{acqDate}{dicomInjectionTimeStr}", "%Y%m%d%H%M%S"
                        ).strftime(Constants().timeFormat)
                except:
                    injectionTime = ""

            if injectionTime:
                vtkmrmlutils.setItemDataNodeAttribute(
                    self.nodeID, Attributes().injectionTime, injectionTime
                )

                try:
                    timeStamp = self.getTimeStamp()
                except:
                    timeStamp = ""
                if not timeStamp:
                    timeStamp = (
                        datetime.strptime(acquisition, Constants().timeFormat)
                        - datetime.strptime(injectionTime, Constants().timeFormat)
                    ).total_seconds() / Constants().units["hour"]
                    vtkmrmlutils.setItemDataNodeAttribute(
                        self.nodeID, Attributes().timeStamp, timeStamp
                    )

    def setStudyInstanceUID(self):
        try:
            studyInstanceUID = self.readStudyInstanceUID()
        except:
            studyInstanceUID = ""

        if not studyInstanceUID:
            self.logger.warning(f"Attribute '{Attributes().studyInstanceUID}' is empty")
            raise DICOMError(f"Attributes '{Attributes().studyInstanceUID}' is empty")

        vtkmrmlutils.setItemDataNodeAttribute(
            self.nodeID, Attributes().studyInstanceUID, studyInstanceUID
        )

    def setSeriesInstanceUID(self):
        try:
            seriesInstanceUID = self.readSeriesInstanceUID()
        except:
            seriesInstanceUID = ""

        if not seriesInstanceUID:
            self.logger.warning(
                f"Attribute '{Attributes().seriesInstanceUID}' is empty"
            )
            raise DICOMError(f"Attribute '{Attributes().seriesInstanceUID}' is empty")

        vtkmrmlutils.setItemDataNodeAttribute(
            self.nodeID, Attributes().seriesInstanceUID, seriesInstanceUID
        )

    def setInstitutionName(self):
        try:
            institutionName = self.readInstitutionName()
        except:
            institutionName = "None"

        if institutionName == "None":
            self.logger.warning(f"Attribute '{Attributes().institutionName}' is empty")

        vtkmrmlutils.setItemDataNodeAttribute(
            self.nodeID, Attributes().institutionName, institutionName
        )

    def setPatientID(self):
        try:
            patientID = self.readPatientID()
        except:
            patientID = ""

        if not patientID:
            self.logger.warning(f"Attribute '{Attributes().patientID}' is empty")
            raise DICOMError(f"Attribute '{Attributes().patientID}' is empty")

        vtkmrmlutils.setItemDataNodeAttribute(
            self.nodeID, Attributes().patientID, patientID
        )

    def setPatientName(self):
        try:
            patientName = self.readPatientName()
        except:
            patientName = self.getPatientID()

        if not patientName:
            self.logger.warning(f"Attribute '{Attributes().patientName}' is empty")
            raise DICOMError(f"Attribute '{Attributes().patientName}' is empty")

        vtkmrmlutils.setItemDataNodeAttribute(
            self.nodeID, Attributes().patientName, patientName
        )

    def setStudyCreation(self):
        try:
            studyCreation = self.readStudyCreation()
        except:
            studyCreation = ""

        if not studyCreation:
            self.logger.warning(f"Attribute '{Attributes().studyCreation}' is empty")
            raise DICOMError(f"Attribute '{Attributes().studyCreation}' is empty")

        vtkmrmlutils.setItemDataNodeAttribute(
            self.nodeID, Attributes().studyCreation, studyCreation
        )

    def setModality(self):
        try:
            modality = self.readModality()
        except:
            modality = ""

        if not modality or modality not in ["CT", "PT", "NM"]:
            self.logger.debug(f"Attribute '{Attributes().modality}' is empty")
            try:
                modality = self.extractModalityFromName()
            except:
                raise IOError(
                    f"The name {self.name} does not contain modality information"
                )
        vtkmrmlutils.setItemDataNodeAttribute(
            self.nodeID, Attributes().modality, modality
        )

    def setAcquisition(self):
        try:
            acquisition = self.readAcquisition()
        except:
            acquisition = ""

        if not acquisition:
            self.logger.warning(f"Attribute '{Attributes().acquisition}' is empty")
            raise DICOMError(f"Attribute '{Attributes().acquisition}' is empty")

        vtkmrmlutils.setItemDataNodeAttribute(
            self.nodeID, Attributes().acquisition, acquisition
        )

    def setAcquisitionDuration(self, duration=""):
        try:
            acquisitionDuration = self.readAcquisitionDuration()
        except:
            acquisitionDuration = duration

        if not acquisitionDuration:
            self.logger.warning(
                f"Attribute '{Attributes().acquisitionDuration}' is empty"
            )

        vtkmrmlutils.setItemDataNodeAttribute(
            self.nodeID, Attributes().acquisitionDuration, acquisitionDuration
        )

    def readStudyInstanceUID(self) -> str:
        # NOTE Attribute: (0x0020, 0x000d) - StudyInstanceUID
        try:
            if [0x0020, 0x000D] in self.dataset:
                studyInstanceUID = self.dataset[0x0020, 0x000D].value
                return studyInstanceUID

        except:
            self.logger.warning(
                f"Failed to read DICOM attribute '{Attributes().studyInstanceUID}'"
            )
            raise DICOMError(
                f"Failed to read DICOM attribute '{Attributes().studyInstanceUID}'"
            )

    def readSeriesInstanceUID(self) -> str:
        # NOTE Attribute: (0x0020, 0x000e) - SeriesInstanceUID
        try:
            if [0x0020, 0x000E] in self.dataset:
                seriesInstanceUID = self.dataset[0x0020, 0x000E].value
                return seriesInstanceUID

        except:
            self.logger.warning(
                f"Failed to read DICOM attribute '{Attributes().seriesInstanceUID}'"
            )
            raise DICOMError(
                f"Failed to read DICOM attribute '{Attributes().seriesInstanceUID}'"
            )

    def readPatientName(self) -> str:
        # NOTE Attribute: (0x0010, 0x0010) - PatientName
        try:
            if [0x0010, 0x0010] in self.dataset:
                patientID = self.dataset[0x0010, 0x0010].value
                return patientID

        except:
            self.logger.warning(
                f"Failed to read DICOM attribute '{Attributes().patientName}'"
            )
            raise DICOMError(
                f"Failed to read DICOM attribute '{Attributes().patientName}'"
            )

    def readPatientID(self) -> str:
        # NOTE Attribute: (0x0010, 0x0020) - PatientID
        try:
            if [0x0010, 0x0020] in self.dataset:
                patientID = self.dataset[0x0010, 0x0020].value
                return patientID

        except:
            self.logger.warning(
                f"Failed to read DICOM attribute '{Attributes().patientID}'"
            )
            raise DICOMError(
                f"Failed to read DICOM attribute '{Attributes().patientID}'"
            )

    def readStudyCreation(self) -> str:
        # NOTE Attribute: (0x0008, 0x002) - StudyDate
        # TODO this is currently only the date, we would need to also have the time (0008, 0030)
        try:
            if [0x0008, 0x0020] in self.dataset:
                studyCreation = self.dataset[0x0008, 0x0020].value
                return studyCreation

        except:
            self.logger.warning(
                f"Failed to read DICOM attribute '{Attributes().studyCreation}'"
            )
            raise DICOMError(
                f"Failed to read DICOM attribute '{Attributes().studyCreation}'"
            )

    def readInstitutionName(self) -> str:
        # NOTE Attribute: (0x0008, 0x0080) - InstitutionName
        if [0x0008, 0x0080] in self.dataset and self.dataset[0x0008, 0x0080].value:
            institutionName = self.dataset[0x0008, 0x0080].value
            return institutionName
        else:
            self.logger.info(
                f"Failed to read DICOM attribute '{Attributes().institutionName}'"
            )
            # raise DICOMError(
            #     f"Failed to read DICOM attribute '{Attributes().institutionName}'"
            # )

    def readModality(self) -> str:
        # NOTE Attribute: (0x0008, 0x0060) - Modality
        if [0x0008, 0x0060] in self.dataset and self.dataset[0x0008, 0x0060].value:
            modality = self.dataset[0x0008, 0x0060].value
            if [0x0054, 0x1001] in self.dataset and self.dataset[
                0x0054, 0x1001
            ].value == "BQML":
                return "PT"
            return modality
        else:
            self.logger.warning(
                f"Failed to read DICOM attribute '{Attributes().modality}'"
            )
            raise DICOMError(
                f"Failed to read DICOM attribute '{Attributes().modality}'"
            )

    def readAcquisition(self) -> str:
        # NOTE Attribute: (0x0008, 0x002A) - AcquisitionDateTime
        # NOTE Attribute: (0x0008, 0x0022) - AcquisitionTime
        # NOTE Attribute: (0x0008, 0x0032) - AcquisitionDate
        try:
            if [0x0008, 0x002A] in self.dataset and self.dataset[0x0008, 0x002A].value:
                dateTime = self.dataset[0x0008, 0x002A].value
                acquisition = datetime.strptime(dateTime[:14], "%Y%m%d%H%M%S")
            else:
                date = self.dataset[0x0008, 0x0022].value
                time = self.dataset[0x0008, 0x0032].value
                acquisition = datetime.strptime(date + time[:6], "%Y%m%d%H%M%S")
            return str(acquisition)

        except:
            self.logger.warning(
                f"Failed to read DICOM attribute '{Attributes().acquisition}'"
            )
            raise DICOMError(
                f"Failed to read DICOM attribute '{Attributes().acquisition}'"
            )

    def readAcquisitionDuration(self) -> str:
        # NOTE this functions has to be called once the modality already has been retrieved
        # NOTE Attribute: (no such attribute) - AquisitionDuration
        # NOTE Attribute: (0x0018, 0x1242) - ActualFrameDuration
        # NOTE Attribute: (0x0054, 0x0052) - RotationInformationSequence
        # NOTE Attribute: (0x0054, 0x0053) - NumberOfFramesInRotation
        # TODO Add the Siemens case

        modality = self.getModality()
        if not modality:
            self.logger.warning(
                f"Failed to extract '{Attributes().acquisitionDuration}', missing modality"
            )
            raise DICOMError(
                f"Failed to extract '{Attributes().acquisitionDuration}', missing modality"
            )

        if modality in ["NM", "PT"]:
            if [0x0018, 0x1242] in self.dataset and self.dataset[0x0018, 0x1242].value:
                acquisitionFramesDuration = int(self.dataset[0x0018, 0x1242].value)
            elif (
                [0x0054, 0x0052] in self.dataset
                and [0x0018, 0x1242] in self.dataset[0x0054, 0x0052][0]
                and self.dataset[0x0054, 0x0052][0][0x0018, 0x1242].value
            ):
                acquisitionFramesDuration = int(
                    self.dataset[0x0054, 0x0052][0][0x0018, 0x1242].value
                )
            else:
                self.logger.warning(
                    f"Failed to read DICOM attribute '{Attributes().acquisitionFramesDuration}'"
                )
                raise DICOMError(
                    f"Failed to read DICOM attribute '{Attributes().acquisitionFramesDuration}'"
                )

            if modality == "PT":
                acquisitionFramesTotal = 1
            elif [0x0054, 0x0053] in self.dataset and self.dataset[
                0x0054, 0x0053
            ].value:
                acquisitionFramesTotal = int(self.dataset[0x0054, 0x0053].value)
            elif (
                [0x0054, 0x0052] in self.dataset
                and [0x0054, 0x0053] in self.dataset[0x0054, 0x0052][0]
                and self.dataset[0x0054, 0x0052][0][0x0054, 0x0053].value
            ):
                acquisitionFramesTotal = int(
                    self.dataset[0x0054, 0x0052][0][0x0054, 0x0053].value
                )
            else:
                self.logger.warning(
                    f"Failed to read DICOM attribute '{Attributes().acquisitionFramesDuration}'"
                )
                raise DICOMError(
                    f"Failed to read DICOM attribute '{Attributes().acquisitionFramesDuration}'"
                )

            acquisitionDuration = float(
                acquisitionFramesTotal * acquisitionFramesDuration / 1000
            )

        elif modality == "CT":

            if [0x0018, 0x1150] in self.dataset:
                acquisitionDuration = self.dataset[0x0018, 0x1150].value
            elif [0x0043, 0x104E] in self.dataset:
                acquisitionDuration = self.dataset[0x0043, 0x104E].value
            else:
                acquisitionDuration = 0
                self.logger.warning(
                    f"Failed to read DICOM attribute '{Attributes().acquisitionFramesDuration}'"
                )
                raise DICOMError(
                    f"Failed to read DICOM attribute '{Attributes().acquisitionFramesDuration}'"
                )

            acquisitionDuration = int(acquisitionDuration)

        else:
            self.logger.warning(
                f"Failed to extract '{Attributes().acquisitionDuration}', invalid modality '{modality}'"
            )
            raise DICOMError(
                f"Failed to extract '{Attributes().acquisitionDuration}', invalid modality '{modality}'"
            )

        return str(acquisitionDuration)

    def readInjectionTime(self) -> str:
        # NOTE Attribute: Injection Time could be located in [0x0018, 0x1072] or [0x0054, 0x0016][0][0x0018, 0x1072]
        if [0x0018, 0x1072] in self.dataset and self.dataset[0x0018, 0x1072].value:
            return self.dataset[0x0018, 0x1072].value[:6]
        elif (
            ([0x0054, 0x0016] in self.dataset)
            and ([0x0018, 0x1072] in self.dataset[0x0054, 0x0016][0])
            and self.dataset[0x0054, 0x0016][0][0x0018, 0x1072].value
        ):
            return self.dataset[0x0054, 0x0016][0][0x0018, 0x1072].value[:6]
        else:
            self.logger.warning(
                f"Failed to retrieve DICOM attribute: {Attributes().injectionTime}"
            )
            raise DICOMError(
                f"Not DICOM attribute set for {Attributes().injectionTime}"
            )

    def readInjectionDateTime(self) -> str:
        # NOTE Attribute: Injection DateTime could be located in [0x0018, 0x1078] or [0x0054, 0x0016][0][0x0018, 0x1078]
        if [0x0018, 0x1078] in self.dataset and self.dataset[0x0018, 0x1078].value:
            return self.dataset[0x0018, 0x1078].value[:14]
        elif (
            ([0x0054, 0x0016] in self.dataset)
            and ([0x0018, 0x1078] in self.dataset[0x0054, 0x0016][0])
            and self.dataset[0x0054, 0x0016][0][0x0018, 0x1078].value
        ):
            return self.dataset[0x0054, 0x0016][0][0x0018, 0x1078].value[:14]
        else:
            self.logger.warning(
                f"Failed to retrieve DICOM attribute: {Attributes().injectionTime}"
            )
            raise DICOMError(
                f"Not DICOM attribute set for {Attributes().injectionTime}"
            )

    def readInjectionActivity(self) -> str:
        # NOTE Attribute: Injected Activity could be located in [0x0018, 0x1074] or [0x0054, 0x0016][0][0x0018, 0x1074]
        if [0x0018, 0x1074] in self.dataset and self.dataset[0x0018, 0x1074].value:
            return self.dataset[0x0018, 0x1074].value
        elif (
            ([0x0054, 0x0016] in self.dataset)
            and ([0x0018, 0x1074] in self.dataset[0x0054, 0x0016][0])
            and self.dataset[0x0054, 0x0016][0][0x0018, 0x1074].value
        ):
            return self.dataset[0x0054, 0x0016][0][0x0018, 0x1074].value
        else:
            self.logger.warning(
                f"Failed to retrieve DICOM attribute: {Attributes().injectedActivity}"
            )
            raise DICOMError(
                f"Not DICOM attribute set for {Attributes().injectedActivity}"
            )

    def decodeElement(self, elementText: str) -> str:
        for element, list in Constants().elements.items():
            if elementText.lower() in list:
                return element

        return ""

    def decodeIsotope(self, isotopeText: str) -> str:
        "Converts whatever string referring to isotope to standard nn-AAA isotope name"
        Isotopes = getIsotopes()
        for Isotope in Isotopes:  # Search directly first
            if Isotope.lower() in isotopeText.lower():
                return Isotope
        #  We found nothing, lets decode then
        # isotope name and then AAA
        pattern = re.compile(r"([a-zA-Z]+)([\-_\s\^]{1})?(\d{1,3})")
        search = pattern.search(isotopeText)
        if search is not None:
            decrypted = search.groups()
            decrypted = [d for d in decrypted if d and d.strip()]
            correctedText = "-".join([self.decodeElement(decrypted[0]), decrypted[1]])
            for Isotope in Isotopes:
                if correctedText.lower() in Isotope.lower():
                    return Isotope

        # AAA and then isotope name
        pattern = re.compile(r"(\d{1,3})([\-_\s\^]{1})?([a-zA-Z]+)")
        search = pattern.search(isotopeText)
        if search is not None:
            decrypted = search.groups()
            decrypted = [d for d in decrypted if d and d.strip()]
            correctedText = "-".join([self.decodeElement(decrypted[1]), decrypted[0]])
            print(decrypted)
            for Isotope in Isotopes:
                if correctedText.lower() in Isotope.lower():
                    return Isotope

        return ""  # nothing was found, return empty string

    def readRadiopharmaceutical(self) -> str:
        # NOTE Attribute: Radiopharmaceutical could be located in several places
        Radiopharmaceuticals = []  # create a list of all the common places
        if 0x00180031 in self.dataset:
            Radiopharmaceutical = self.dataset[0x00180031].value
            Radiopharmaceuticals.append(Radiopharmaceutical)
        if 0x00181030 in self.dataset:
            Radiopharmaceutical = self.dataset[0x00181030].value
            Radiopharmaceuticals.append(Radiopharmaceutical)
        if 0x00181031 in self.dataset:
            Radiopharmaceutical = self.dataset[0x00181031].value
            Radiopharmaceuticals.append(Radiopharmaceutical)
        if 0x00540016 in self.dataset and 0x00180031 in self.dataset[0x00540016][0]:
            Radiopharmaceutical = self.dataset[0x00540016][0][0x00180031].value
            Radiopharmaceuticals.append(Radiopharmaceutical)
        if 0x00540016 in self.dataset and 0x00181031 in self.dataset[0x00540016][0]:
            Radiopharmaceutical = self.dataset[0x00540016][0][0x00181031].value
            Radiopharmaceuticals.append(Radiopharmaceutical)
        if (
            0x00540016 in self.dataset
            and 0x00540300 in self.dataset[0x00540016][0]
            and 0x00080104 in self.dataset[0x00540016][0][0x00540300][0]
        ):
            Radiopharmaceutical = self.dataset[0x00540016][0][0x00540300][0][
                0x00080104
            ].value
            Radiopharmaceuticals.append(Radiopharmaceutical)
        if (
            0x00540016 in self.dataset
            and 0x00540300 in self.dataset[0x00540016][0]
            and 0x00180031 in self.dataset[0x00540016][0][0x00540300][0]
        ):
            Radiopharmaceutical = self.dataset[0x00540016][0][0x00540300][0][
                0x00180031
            ].value
            Radiopharmaceuticals.append(Radiopharmaceutical)
        # Now try to decypher at least one of the found strings
        for Radiopharmaceutical in Radiopharmaceuticals:
            isotope = self.decodeIsotope(Radiopharmaceutical)
            if isotope:
                return isotope

        self.logger.warning(
            f"Failed to retrieve DICOM attribute: {Attributes().isotope}"
        )
        raise DICOMError(f"Not DICOM attribute set for {Attributes().isotope}")

    def readNonStandardStationCorrection(self) -> tuple[float, float]:
        # NOTE Attribute: (0x33, 0x1038) - Siemens factor
        # NOTE Attribute: [0x40, 0x9096][0][0x40, 0x9224] - Hermes intercept
        # NOTE Attribute: [0x40, 0x9096][0][0x40, 0x9225] - Hermes slope

        # Search Siemens data
        if [0x33, 0x1038] in self.dataset:
            factor = float(self.dataset[0x33, 0x1038].value)
        else:
            factor = 1.0

        # Check if this is a Hermes node
        if (
            0x00409096 in self.dataset
            and 0x00409224 in self.dataset[0x00409096][0]
            and 0x00409225 in self.dataset[0x00409096][0]
        ):
            intercept = float(self.dataset[0x00409096][0][0x00409224].value)
            slope = float(self.dataset[0x00409096][0][0x00409225].value)
            return intercept, slope / factor
        else:  # Not Hermes node
            modality = self.getModality()
            if modality == "CT":  # avoid oversampling outside FOV
                vox_arr = slicer.util.arrayFromVolume(self.data).astype(
                    float
                )  # the value is not in DICOM so we must calculate it
                smallestValue = np.amin(vox_arr.ravel())

                if smallestValue >= 0:
                    intercept = -1000 - smallestValue
                    slope = 1
                    return intercept, slope / factor
                else:
                    return 0, 1 / factor

            else:  # Everything else
                return 0, 1 / factor

    def fixNonStandardCorrection(self):
        if not vtkmrmlutils.hasItemAttribute(self.nodeID, "fixed"):
            intercept, slope = self.readNonStandardStationCorrection()
            if intercept != 0 or slope != 1:
                vox_arr = slicer.util.arrayFromVolume(self.data).astype(float)
                vox_arr = intercept + slope * vox_arr
                slicer.util.updateVolumeFromArray(self.data, vox_arr)
                vtkmrmlutils.setItemAttribute(self.nodeID, "fixed", "1")