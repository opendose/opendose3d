import slicer
from slicer.ScriptedLoadableModule import ScriptedLoadableModuleTest

from Logic.attributes import Attributes
from Logic.logging import getLogger, INFO
from Logic.utils import whoami
from Logic.nodes import Node
from Logic.testbuilder import setupDICOM, setupNonDICOM, initDICOMDatabase
from Logic.testutils import TestValues
from Logic.vtkmrmlutils import *


class vtkmrmlutilsTest(ScriptedLoadableModuleTest):
    """
    This is the test case for the vtkmrml related utils.
    Uses ScriptedLoadableModuleTest base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def setUp(self):
        self.tearDown()

        # logger
        self.logger = getLogger('OpenDose3D.vtkmrmrlutilsTest')
        self.logger.setLevel(INFO)

        # modules
        self.attributes = Attributes()
        self.testValues = TestValues()

    def tearDown(self):
        slicer.mrmlScene.Clear(0)
        slicer.app.processEvents()

    def runTest(self):
        """
        Run as few or as many tests as needed here
        """
        self.setUp()
        self.logger.info('\n\n***** vtkmrmlutils Module Testing *****\n')

        # since first test, setup database
        try:
            initDICOMDatabase()
        except:
            raise

        self.isDICOM_false_positive()
        self.isDICOM_true_positive()
        self.getSubjectIDs_positive()
        self.getStudyIDs_positive()
        self.getResultIDs_positive()
        self.getFolderIDs_positive()
        self.getScalarVolumeNodes_positive()
        self.getFirstNodeByName_positive()
        self.getItemDataNode_positive()
        self.getItemDataNodeName_positive()
        self.getItemDataNodeAttributeNames_positive()
        self.getItemDataNodeAttributeValue_positive()
        self.getItemDataNodeAttributeValue_negative()
        self.hasItemDataNodeAttribute_positive()
        self.hasItemDataNodeAttribute_negative()
        self.setItemDataNodeAttribute_positive()
        self.getItemName_positive()
        self.setItemName_positive()
        self.getItemAttributeNames_positive()
        self.getItemAttributeNames_negative()
        self.hasItemAttribute_positive()
        self.hasItemAttribute_negative()
        self.getItemAttributeValue_positive()
        self.getItemAttributeValue_negative()
        self.setItemFolder_positive()
        self.createFolder_positive()
        self.createResultsFolder_positive()
        self.getFolderID_positive()
        self.getParentFolder_positive()
        self.getAllFolderChildren_positive()
        self.getFolderChildrenByPattern_positive()
        self.setAndObserveColorNode_positive()
        self.getCommonNameSubString_positive()

        self.tearDown()
        self.logger.info('\n******* vtkmrmlutils tests passed **********\n')

    def isDICOM_false_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemID, _ = setupNonDICOM()

        # act
        lisDICOM = isDICOM(itemID)

        # assert
        self.assertFalse(lisDICOM)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def isDICOM_true_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()

        # act
        lisDICOM = isDICOM(itemIDs[0])

        # assert
        self.assertTrue(lisDICOM)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getSubjectIDs_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        shNode = getSubjectHierarchyNode()
        subjectID_0 = shNode.CreateSubjectItem(
            shNode.GetSceneItemID(), 'Subject 0')
        subjectID_1 = shNode.CreateSubjectItem(
            shNode.GetSceneItemID(), 'Subject 1')

        # act
        subjectIDs = getSubjectIDs()

        # assert
        self.assertEqual(2, len(subjectIDs))
        self.assertEqual(subjectID_0, subjectIDs[0])
        self.assertEqual(subjectID_1, subjectIDs[1])

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getStudyIDs_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        shNode = getSubjectHierarchyNode()
        subjectID = shNode.CreateSubjectItem(
            shNode.GetSceneItemID(), 'Subject')
        studyID_0 = shNode.CreateStudyItem(subjectID, 'Study 0')
        studyID_1 = shNode.CreateStudyItem(subjectID, 'Study 1')

        # act
        studyIDs = getStudyIDs()

        # assert
        self.assertEqual(2, len(studyIDs))
        self.assertEqual(studyID_0, studyIDs[0])
        self.assertEqual(studyID_1, studyIDs[1])

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getResultIDs_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        shNode = getSubjectHierarchyNode()
        subjectID = shNode.CreateSubjectItem(
            shNode.GetSceneItemID(), 'Subject')
        studyID = shNode.CreateStudyItem(subjectID, 'Study')
        resultID_0 = createFolder(studyID, 'Results0')
        resultID_1 = createFolder(studyID, 'Results1')
        setItemAttribute(resultID_0, 'Results', 1)
        setItemAttribute(resultID_1, 'Results', 1)

        # act
        resultIDs = getResultIDs()

        # assert
        self.assertEqual(len(resultIDs), 2)
        self.assertEqual(resultID_0, resultIDs[0])
        self.assertEqual(resultID_1, resultIDs[1])

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getFolderIDs_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        shNode = getSubjectHierarchyNode()
        subjectID = shNode.CreateSubjectItem(
            shNode.GetSceneItemID(), 'Subject')
        studyID = shNode.CreateStudyItem(subjectID, 'Study')
        folderID_0 = createFolder(studyID, 'Folder 0')
        folderID_1 = createFolder(studyID, 'Folder 1')

        # act
        folderIDs = getFolderIDs()

        # assert
        self.assertEqual(len(folderIDs), 2)
        self.assertEqual(folderID_0, folderIDs[0])
        self.assertEqual(folderID_1, folderIDs[1])

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getScalarVolumeNodes_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        _, _ = setupNonDICOM()

        # act
        nodeIDs = getScalarVolumeNodes()

        # assert
        self.assertIsNotNone(nodeIDs)
        self.assertEqual(2, len(nodeIDs))
        self.assertNotEqual(0, nodeIDs[0])

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getFirstNodeByName_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemID, _ = setupNonDICOM()

        # act
        nodeID = getFirstNodeByName(self.testValues.node.acsc_48h)

        # assert
        self.assertEqual(itemID, nodeID)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getItemDataNode_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()
        node = Node.new(itemIDs[0])

        # act
        dataNode = getItemDataNode(itemIDs[0])

        # assert
        self.assertEqual(node.data, dataNode)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getItemDataNodeName_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemID, _ = setupNonDICOM()

        # act
        name = getItemDataNodeName(itemID)

        # assert
        self.assertEqual(self.testValues.node.acsc_48h, name)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getItemDataNodeAttributeNames_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemID, _ = setupNonDICOM()

        # act
        attributeNames = getItemDataNodeAttributeNames(itemID)

        # assert
        self.assertEqual(3, len(attributeNames))

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def hasItemDataNodeAttribute_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemID, _ = setupNonDICOM()

        # act
        hasAttribute = hasItemDataNodeAttribute(
            itemID, self.attributes.acquisition)

        # assert
        self.assertTrue(hasAttribute)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def hasItemDataNodeAttribute_negative(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemID, _ = setupNonDICOM()

        # act
        hasAttribute = hasItemDataNodeAttribute(
            itemID, 'an invalid attribute')

        # assert
        self.assertFalse(hasAttribute)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getItemDataNodeAttributeValue_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        _ = setupDICOM()
        itemID = getFirstNodeByName(
            self.testValues.dataNode.name_acsc1h)
        self.logger.info(itemID)

        # act
        value = getItemDataNodeAttributeValue(
            itemID, self.attributes.instanceUIDs)

        # assert
        self.assertEqual(self.testValues.dicom.instanceUIDs, value)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getItemDataNodeAttributeValue_negative(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()

        # act
        value = getItemDataNodeAttributeValue(
            itemIDs[0], 'a non-existing attribute')

        # assert
        self.assertEqual(None, value)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def setItemDataNodeAttribute_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemID, _ = setupNonDICOM()
        attributeName = 'name'
        attributeValue = 'value'

        # act
        setItemDataNodeAttribute(
            itemID, attributeName, attributeValue)

        # assert
        value = getItemDataNodeAttributeValue(
            itemID, attributeName)
        self.assertEqual(attributeValue, value)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getItemName_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemID, _ = setupNonDICOM()

        # act
        name = getItemName(itemID)

        # assert
        self.assertEqual(name, self.testValues.node.acsc_48h)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def setItemName_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemID, _ = setupNonDICOM()

        # act
        newName = 'some new name'
        setItemName(itemID, newName)

        # assert
        name = getItemName(itemID)
        self.assertEqual(newName, name)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getItemAttributeNames_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemID, _ = setupNonDICOM()

        # act
        attributeNames = getItemAttributeNames(itemID)

        # assert
        self.assertEqual(len(attributeNames), 1)
        self.assertEqual(attributeNames[0], self.attributes.acquisition)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getItemAttributeNames_negative(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        _, itemID = setupNonDICOM()

        # act
        attributeNames = getItemAttributeNames(itemID)

        # assert
        self.assertEqual(len(attributeNames), 0)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def hasItemAttribute_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemID, _ = setupNonDICOM()

        # act
        hasAttribute = hasItemAttribute(
            itemID, self.attributes.acquisition)

        # assert
        self.assertTrue(hasAttribute)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def hasItemAttribute_negative(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        _, itemID = setupNonDICOM()

        # act
        hasAttribute = hasItemAttribute(
            itemID, self.attributes.acquisition)

        # assert
        self.assertFalse(hasAttribute)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getItemAttributeValue_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemID, _ = setupNonDICOM()

        # act
        attributeValue = getItemAttributeValue(
            itemID, self.attributes.acquisition)

        # assert
        self.assertEqual(attributeValue, self.testValues.attribute.acquisition)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getItemAttributeValue_negative(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        _, itemID = setupNonDICOM()

        # act
        attributeValue = getItemAttributeValue(
            itemID, self.attributes.acquisition)

        # assert
        self.assertEqual(attributeValue, '')

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def setItemAttribute_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemID, _ = setupNonDICOM()
        attributeName = 'AttributeName'
        attributeValue = 'AttributeValue'

        # act
        setItemAttribute(itemID, attributeName, attributeValue)

        # assert
        value = getItemAttributeValue(itemID, attributeName)
        self.assertEqual(attributeValue, value)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def setItemFolder_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        shNode = getSubjectHierarchyNode()
        subjectID = shNode.CreateSubjectItem(
            shNode.GetSceneItemID(), 'Subject')
        folderID = shNode.CreateStudyItem(subjectID, 'Study')

        volumeNode = slicer.mrmlScene.AddNewNodeByClass(
            'vtkMRMLScalarVolumeNode', 'Node')
        itemID = shNode.GetItemByDataNode(volumeNode)

        # act
        setItemFolder(itemID, folderID)

        # assert
        childID = getAllFolderChildren(folderID)[0]
        self.assertEqual(itemID, childID)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def createFolder_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        shNode = getSubjectHierarchyNode()
        parentID = shNode.GetSceneItemID()
        name = 'Folder'

        # act
        folderID = createFolder(parentID, name)

        # assert
        parentID_actual = getParentFolder(folderID)
        name_actual = shNode.GetItemName(folderID)

        self.assertEqual(parentID, parentID_actual)
        self.assertEqual(name, name_actual)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def createResultsFolder_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        shNode = getSubjectHierarchyNode()
        parentID = shNode.GetSceneItemID()
        name = 'Results'

        # act
        resultID = createResultsFolder(parentID)

        # assert
        parentID_actual = getParentFolder(resultID)
        name_actual = shNode.GetItemName(resultID)
        level = getItemAttributeValue(resultID, 'Level')

        self.assertEqual(parentID, parentID_actual)
        self.assertEqual(name, name_actual)
        self.assertEqual('Folder', level)
        self.assertTrue(shNode.HasItemAttribute(resultID, 'Results'))

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getFolderID_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        _, _ = setupNonDICOM()

        # act
        folderID = getFolderID(self.testValues.node.ct_48h)

        # assert
        shNode = getSubjectHierarchyNode()
        volumeNode = slicer.mrmlScene.GetFirstNodeByClass(
            'vtkMRMLScalarVolumeNode')
        self.assertEqual(shNode.GetItemParent(
            shNode.GetItemByDataNode(volumeNode)), folderID)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getParentFolder_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        shNode = getSubjectHierarchyNode()
        subjectID = shNode.CreateSubjectItem(
            shNode.GetSceneItemID(), 'Subject')
        studyID = shNode.CreateStudyItem(subjectID, 'Study')
        volumeNode = slicer.mrmlScene.AddNewNodeByClass(
            'vtkMRMLScalarVolumeNode', 'Node')
        itemID = shNode.GetItemByDataNode(volumeNode)
        shNode.SetItemParent(itemID, studyID)

        # act
        folderID = getParentFolder(itemID)

        # assert
        self.assertEqual(studyID, folderID)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getAllFolderChildren_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        shNode = getSubjectHierarchyNode()
        subjectID = shNode.CreateSubjectItem(
            shNode.GetSceneItemID(), 'Subject')
        studyID_0 = shNode.CreateStudyItem(subjectID, 'Study 0')
        studyID_1 = shNode.CreateStudyItem(subjectID, 'Study 1')

        # act
        children = getAllFolderChildren(subjectID)

        # assert
        self.assertEqual([studyID_0, studyID_1], children)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getFolderChildren_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        _, _ = setupNonDICOM()

        # act
        folderID = getFolderID(self.testValues.node.ct_48h)
        children = Node.getFolderChildren(folderID)

        # assert
        shNode = getSubjectHierarchyNode()
        volumeID = shNode.GetItemByDataNode(children['CTCT'].data)

        self.assertIsNotNone(children)
        self.assertEqual(children['CTCT'].nodeID, volumeID)
        self.assertEqual(children['CTCT'].data, self.testValues.node.ct_48h)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getFolderChildrenByPattern_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemID, _ = setupNonDICOM()
        pattern = 'ACSC'

        # act
        folderID = getFolderID(self.testValues.node.acsc_48h)
        childID = getFolderChildrenByPattern(folderID, pattern)[0]
        childName = getItemName(childID)

        # assert
        self.assertEqual(itemID, childID)
        self.assertEqual(self.testValues.node.acsc_48h, childName)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def setAndObserveColorNode_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemID, _ = setupNonDICOM()
        node = getItemDataNode(itemID)
        node.CreateDefaultDisplayNodes()
        displayNode = node.GetScalarVolumeDisplayNode()

        # act
        setAndObserveColorNode(itemID)

        # assert
        colorNodeID = displayNode.GetColorNodeID()
        petNodeID = getColorNodePETID()

        self.assertEqual(petNodeID, colorNodeID)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def getCommonNameSubString_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemName_0 = "teststring"
        itemName_1 = "test "

        # act
        match = getCommonNameSubString(itemName_0, itemName_1)

        # assert
        self.assertEqual("test", match)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')
