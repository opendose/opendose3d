import os
import importlib
import slicer
from slicer.ScriptedLoadableModule import ScriptedLoadableModuleTest

from Logic.logging import getLogger, INFO
from Logic.utils import whoami, installModule, checkTesting
from Logic.attributes import Attributes
from Logic.errors import IOError, ConventionError
from Logic.OpenDose3DLogic import OpenDose3DLogic
from Logic.testutils import TestValues
from Logic.testbuilder import *
from Logic.vtkmrmlutils import *
from Logic.constants import Constants
from Logic.PhysicalUnits import PhysicalUnits
from Logic.nodes import Node

moduleLoader = importlib.util.find_spec("Elastix")
if not moduleLoader:
    try:
        installModule("SlicerElastix")
        moduleLoader = importlib.util.find_spec("Elastix")
    except IOError as e:
        print(e.message)


class OpenDose3DTest(ScriptedLoadableModuleTest):
    """
    This is the test case for your scripted module.
    Uses ScriptedLoadableModuleTest base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def setUp(self):
        self.tearDown()
        self.MeV = PhysicalUnits().getUnit('Energy', 'MeV')

        # logger
        self.logger = getLogger('OpenDose3D.Tests')
        self.logger.setLevel(INFO)

        # modules
        self.logic = OpenDose3DLogic()
        self.attributes = Attributes()
        self.testValues = TestValues()

        # since first test, setup database
        try:
            initDICOMDatabase()
        except:
            raise

    def tearDown(self):
        slicer.mrmlScene.Clear(0)
        slicer.app.processEvents()

    def runTest(self):
        """
        Run as few or as many tests as needed here.
        """
        self.setUp()
        self.logger.info('\n\n***** OpenDose3D Module Testing *****\n')

        self.standardiseHierarchy_set_positive()
        self.standardiseHierarchy_default_positive()
        self.standardiseHierarchy_subjects_negative()
        self.standardiseHierarchy_results_negative()
        self.standardiseNodes_DICOM_positive()
        self.standardiseNodes_MRB_positive()
        #self.standardiseNodes_negative()
        self.standardise_positive()
        self.setupIsotope_default_positive()
        self.setupIsotope_custom_positive()
        # self.checkAttributes_positive()
        # self.checkAttributes_negative()

        # self.FullTest_positive()

        self.tearDown()
        self.logger.info(
            '\n******** OpenDose3D Tests passed ***************\n')

    def standardiseHierarchy_set_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        subjectName = 'Predefined Subject'
        studyName = 'Predefined Study'

        shNode = getSubjectHierarchyNode()
        subjectID = shNode.CreateSubjectItem(
            shNode.GetSceneItemID(), subjectName)
        studyID = shNode.CreateStudyItem(subjectID, studyName)

        # act
        subjectID_actual, studyID_actual = self.logic.standardiseHierarchy()

        # assert
        self.assertEqual(subjectID, subjectID_actual)
        self.assertEqual(
            subjectName, getItemName(subjectID_actual))
        self.assertEqual(studyID, studyID_actual)
        self.assertEqual(studyName, getItemName(studyID_actual))

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def standardiseHierarchy_default_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        subjectName = 'Subject'
        studyName = 'Study'

        # act
        subjectID, studyID = self.logic.standardiseHierarchy()

        # assert
        self.assertEqual(subjectName, getItemName(subjectID))
        self.assertEqual(studyName, getItemName(studyID))

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def standardiseHierarchy_subjects_negative(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        subjectName_0 = 'Subject 0'
        subjectName_1 = 'Subject 1'

        shNode = getSubjectHierarchyNode()
        _ = shNode.CreateSubjectItem(shNode.GetSceneItemID(), subjectName_0)
        _ = shNode.CreateSubjectItem(shNode.GetSceneItemID(), subjectName_1)

        # act & assert
        with self.assertRaises(ConventionError) as context:
            _, _ = self.logic.standardiseHierarchy()
            exception = context.exception.message
            self.assertEqual(
                'Scene contains 2 subjects. Please remove all but one subject.', exception)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def standardiseHierarchy_studies_negative(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        subjectName = 'Subject'
        studyName_0 = 'Study 0'
        studyName_1 = 'Study 1'

        shNode = getSubjectHierarchyNode()
        subjectID = shNode.CreateSubjectItem(
            shNode.GetSceneItemID(), subjectName)
        _ = shNode.CreateStudyItem(subjectID, studyName_0)
        _ = shNode.CreateStudyItem(subjectID, studyName_1)

        # act & assert
        with self.assertRaises(ConventionError) as context:
            _, _ = self.logic.standardiseHierarchy()
            exception = context.exception.message
            self.assertEqual(
                'Subject contains 2 studies. Please remove all but one study.', exception)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def standardiseHierarchy_results_negative(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        subjectName = 'Subject'
        studyName = 'Study'
        resultName_0 = 'Results 0'
        resultName_1 = 'Results 1'

        shNode = getSubjectHierarchyNode()
        subjectID = shNode.CreateSubjectItem(
            shNode.GetSceneItemID(), subjectName)
        studyID = shNode.CreateStudyItem(subjectID, studyName)
        resultID_0 = createFolder(studyID, resultName_0)
        resultID_1 = createFolder(studyID, resultName_1)

        setItemAttribute(resultID_0, 'Results', 1)
        setItemAttribute(resultID_1, 'Results', 1)

        # act & assert
        with self.assertRaises(ConventionError) as context:
            _, _ = self.logic.standardiseHierarchy()

        exception = context.exception.message
        self.assertEqual(
            'Study contains 2 results folder. Please remove all but one results folder.', exception)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def standardiseNodes_DICOM_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()
        studyID = getStudyIDs()[0]
        dicomData = DicomValues()

        # act
        self.logic.standardiseNodes(studyID, float(dicomData.injectedActivity), dicomData.injectionTime)

        # assert
        folderIDs = getFolderIDs()
        self.logger.info(f"folderIDS: {folderIDs}")
        self.assertEqual(5, len(folderIDs))

        for folderID in folderIDs:
            nodes = Node.getFolderChildren(folderID)
            self.logger.info(f"nodes: {nodes}")
            self.assertEqual(2, len(nodes.keys()))
            self.assertIn('ACSC', nodes)
            self.assertIn('CTCT', nodes)
            
            for node in nodes:    
                self.logger.info(f"node: {nodes[node].name}")

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def standardiseNodes_MRB_positive(self):
        #TODO: This test is failing
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()
        studyID = getStudyIDs()[0]
        dicomData = DicomValues()

        # dicom to mrb
        self.logic.standardiseNodes(studyID, float(dicomData.injectedActivity), dicomData.injectionTime)

        # act
        self.logic.standardiseNodes(studyID, float(dicomData.injectedActivity), '2016-12-14 09:59:00')

        # assert
        folderIDs = getFolderIDs()
        self.logger.info(f"folderIDS: {folderIDs}")
        #self.assertEqual(5, len(folderIDs)) # Fails here

        for folderID in folderIDs:
            nodes = Node.getFolderChildren(folderID)
            self.logger.info(f"nodes: {nodes}")
            #self.assertEqual(2, len(nodes.keys()))
            #self.assertTrue('ACSC' in nodes)
            #self.assertTrue('CTCT' in nodes)
            
            for node in nodes:    
                self.logger.info(f"node: {nodes[node].name}")

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def standardiseNodes_negative(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        shNode = getSubjectHierarchyNode()
        subjectID = shNode.CreateSubjectItem(
            shNode.GetSceneItemID(), 'Subject')
        studyID = shNode.CreateStudyItem(subjectID, 'Study')
        dicomData = DicomValues()

        # willingly just place node somewhere
        badNode = slicer.mrmlScene.AddNewNodeByClass(
            'vtkMRMLScalarVolumeNode', 'some invalid name')
        badNodeID = getItemID(badNode)
        setItemAttribute(badNodeID, self.attributes.modality, 'NM')

        # act & assert
        with self.assertRaises(ConventionError):
            self.logic.standardiseNodes(studyID, float(dicomData.injectedActivity), dicomData.injectionTime)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def standardise_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()
        itemIDref = getItemReferent(itemIDs)

        # act
        self.logic.standardise()

        # assert
        resultsIDs = getResultIDs()
        folderIDs = getFolderIDs()
        folderID = getAllFolderChildren(folderIDs[0])

        self.assertEqual(1, len(resultsIDs))
        self.assertTrue(6, len(folderIDs))
        self.assertTrue(2, len(folderID))
        self.assertIn(itemIDref, folderID)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def setObserveColorNode_CT_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        _ = setupNonDICOM()
        volumeNode = slicer.mrmlScene.GetFirstNodeByClass(
            'vtkMRMLScalarVolumeNode')

        # act
        self.logic.setObserveColorNode(volumeNode, self.testValues.node.ct_48h)

        # assert
        displayNode = volumeNode.GetScalarVolumeDisplayNode()
        self.assertEqual(displayNode.GetColorNodeID(),
                         self.logic.GreynodeColor.GetID())

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def importMonteCarlo_hierarchy_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        _ = setupNonDICOM(self.testValues.node.monteCarlo_48h)

        # act
        self.logic.importMonteCarlo()

        # assert
        shNode = getSubjectHierarchyNode()
        sceneItemID = shNode.GetSceneItemID()

        subjectID = shNode.GetItemChildWithName(sceneItemID, 'Subject')
        studyID = shNode.GetItemChildWithName(subjectID, '48HR')
        volumeNode = shNode.GetItemChildWithName(
            studyID, 'J2:DOSE MonteCarlo 48HR')

        self.assertIsNotNone(subjectID)
        self.assertIsNotNone(studyID)
        self.assertIsNotNone(volumeNode)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def setupIsotope_default_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        name = 'Lu-177'
        energy = 0.14822654849246394 * self.MeV
        T_h = round(6.647 * 24, 3)
        Z = 71
        A = 177

        # act
        self.logic.Isotope = Constants().isotopes[name]
        self.logic.Isotope['name'] = name

        # assert
        self.assertEqual(name, self.logic.Isotope['name'])
        self.assertEqual(Z, self.logic.Isotope['Z'])
        self.assertEqual(A, self.logic.Isotope['A'])
        self.assertEqual(energy, self.logic.Isotope['energy'])
        self.assertEqual(T_h, self.logic.Isotope['T_h'])

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def setupIsotope_custom_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        name = 'I-131'
        energy = 0.19197150954058237 * self.MeV
        T_h = 8.0207 * 24
        Z = 53
        A = 131

        # act
        self.logic.Isotope = Constants().isotopes[name]
        self.logic.Isotope['name'] = name

        # assert
        self.assertEqual(name, self.logic.Isotope['name'])
        self.assertEqual(Z, self.logic.Isotope['Z'])
        self.assertEqual(A, self.logic.Isotope['A'])
        self.assertEqual(energy, self.logic.Isotope['energy'])
        self.assertEqual(T_h, self.logic.Isotope['T_h'])

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def checkAttributes_positive(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        _ = setupDICOM()
        self.logic.standardise()

        # act
        ok = self.logic.checkAttributes()

        # assert
        self.assertTrue(ok)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def checkAttributes_negative(self):
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()
        dicomData = DicomValues()
        setItemAttribute(itemIDs[0], self.attributes.patientID, '0')

        self.logic.standardise(float(dicomData.injectedActivity), dicomData.injectionTime)

        # act
        ok = self.logic.checkAttributes()

        # assert
        self.assertFalse(ok)

        self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')

    def FullTest_positive(self):
        self.setUp()
        self.logger.info(f'Test {whoami()} started!')
        self.tearDown()

        # arrange
        itemIDs = setupDICOM()
        itemIDref = getItemReferent(itemIDs)
        dicomData = DicomValues()
        name = 'Lu-177'
        Z = 71
        A = 177
        energy = 0.14822654849246394 * self.MeV
        T_h = round(6.647 * 24, 3)

        # act Standardise
        self.logger.info('Standardise started!')
        self.logic.standardise(float(dicomData.injectedActivity), dicomData.injectionTime)

        # assert
        resultsIDs = getResultIDs()
        folderIDs = getFolderIDs()
        folderID = getAllFolderChildren(folderIDs[0])

        self.assertEqual(1, len(resultsIDs))
        self.assertTrue(6, len(folderIDs))
        self.assertTrue(2, len(folderID))
        self.assertIn(itemIDref, folderID)
        self.logger.info('Standardise passed!')
        slicer.app.processEvents()

        # act setupIsotope
        self.logger.info('Setup isotope started!')
        self.logic.Isotope = Constants().isotopes['Lu-177']

        # assert
        self.assertEqual(name, self.logic.Isotope['name'])
        self.assertEqual(Z, self.logic.Isotope['Z'])
        self.assertEqual(A, self.logic.Isotope['A'])
        self.assertEqual(energy, self.logic.Isotope['energy'])
        self.assertEqual(T_h, self.logic.Isotope['T_h'])
        self.logger.info('Setup Isotope passed!')
        slicer.app.processEvents()

        # act Resample CT
        self.logger.info('Resample started!')
        self.logic.resampleCT()

        # assert
        for folderID in folderIDs:
            nodes = Node.getFolderChildren(folderID)
            self.assertIn('CTRS', nodes)
            self.assertTrue(hasImageData(nodes['CTRS'].data))
            if 'CTCT' in nodes and '1HR' in nodes['CTCT'].name:
                referenceNode = nodes['CTCT']
                self.assertTrue(hasImageData(nodes['CTCT'].data))
        self.assertTrue(hasImageData(referenceNode.data))
        self.logger.info('Resample passed!')
        slicer.app.processEvents()

        # act Rescale
        self.logger.info('Rescale started!')
        calibration = {}
        calibration['CTCalibration'] = {
            "a0": 0.001, "b0": 1,
            "a1": 0.0005116346986394071, "b1": 1
        }
        calibration['SPECTSensitivity'] = {
            'Value': 4.53,
            'Units': 'counts/MBqs',
            'Time': 1800
        }
        self.logic.scaleValues(
            calibration=calibration,
            injectedActivity=dicomData.injectedActivity, 
            injectionTime=dicomData.injectionTime
        )

        # assert
        for folderID in folderIDs:
            nodes = Node.getFolderChildren(folderID)
            self.assertIn('DENM', nodes)
            self.assertTrue(hasImageData(nodes['DENM'].data))
            self.assertIn('ACTM', nodes)
            self.assertTrue(hasImageData(nodes['ACTM'].data))
        self.logger.info('Rescale passed!')
        slicer.app.processEvents()

        # act registration
        self.logger.info('Registration started!')
        # self.assertTrue(bool(moduleLoader)) # TODO Install Elastix by default and activate this test
        # TODO add statistics for registration
        self.logic.createTransforms(referenceNode.data)
        self.logger.info('Registration passed!')
        slicer.app.processEvents()

        # load segmentation
        self.logger.info('Segmentation started!')
        segNode = loadSegmentation()
        self.assertTrue(hasSegmentation(segNode.data))
        self.logic.Propagate(segNode.data)
        self.logger.info('Segmentation passed!')
        slicer.app.processEvents()

        self.logger.info(
            '\n******** ACTM workflow Tests started ***************\n')

        self.logger.info('create tables started!')
        self.logic.computeNewValues(mode='Activity')
        self.logger.info('create tables passed!')
        slicer.app.processEvents()

        self.logger.info('create plots started!')
        self.logic.processTimeIntegrationVariables(mode='Activity')
        self.logic.showTableandPlotTimeIntegrationVariables(
            mode='Activity', showlegend='False')
        self.logger.info('create plots passed!')
        slicer.app.processEvents()

        self.logger.info('integration started!')
        self.logic.setFittingClass(fittingClass="NUKFIT")
        self.logic.defaultFitting(
            mode='Activity',
            algorithm=''
        )
        self.logic.integrate(
            mode='Activity',
            algorithm=''
        )
        self.logger.info('integration passed!')
        slicer.app.processEvents()

        self.logger.info(
            '\n******** ACTM workflow Tests passed ***************\n')
        self.logger.info(
            '\n******** ADR workflow Tests started ***************\n')

        self.logger.info('Convolution started!')
        DRMode = 'FFT Convolution'
        self.logic.convolution(
            isotopeText='Lu-177',
            DVK_material='water',
            actThreshold=0.0,
            densThreshold=50,
            mode=DRMode,
            multithreaded=False
        )
        self.logger.info('Convolution passed!')
        slicer.app.processEvents()

        self.logger.info('create tables started!')
        self.logic.computeNewValues(mode='DoseRate', algorithm=DRMode)
        self.logger.info('create tables passed!')
        slicer.app.processEvents()

        self.logger.info('create plots started!')
        self.logic.processTimeIntegrationVariables(
            mode='DoseRate', algorithm=DRMode)
        self.logic.showTableandPlotTimeIntegrationVariables(
            mode='DoseRate', algorithm=DRMode, showlegend='False')
        self.logger.info('create plots passed!')
        slicer.app.processEvents()

        self.logger.info('integration started!')
        self.logic.setFittingClass(fittingClass="NUKFIT")
        self.logic.defaultFitting(
            mode='DoseRate',
            algorithm=DRMode,
        )
        self.logic.integrate(
            mode='DoseRate',
            algorithm=DRMode
        )
        self.logger.info('integration passed!')
        slicer.app.processEvents()

        self.logger.info(
            '\n******** ADR workflow Tests passed ***************\n')

        # TODO add statistics and output verification

        if checkTesting():
            self.tearDown()
        self.logger.info(f'Test {whoami()} passed!')
