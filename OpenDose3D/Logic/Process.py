import json
import pickle
import copy
import qt
import slicer


from Logic.logging import getLogger
from Logic.utils import getSystemCoresInfo
from slicer.ScriptedLoadableModule import ScriptedLoadableModuleLogic

#
# ProcessesLogic
#


class ProcessesLogic(ScriptedLoadableModuleLogic, qt.QObject):

    def __init__(self, parent=None, maximumRunningProcesses=None, completedCallback=lambda: None, windowTitle="Processing ..."):
        super().__init__(parent)
        self.logger = getLogger('OpenDose3D.Processes')
        self.results = {}
        if not maximumRunningProcesses:
            cpu_cores, _, _ = getSystemCoresInfo()
            self.maximumRunningProcesses = cpu_cores
        self.logger.info(
            f"Launching convolution in {self.maximumRunningProcesses} CPU cores")
        self.completedCallback = completedCallback

        self.QProcessStates = {0: 'NotRunning', 1: 'Starting', 2: 'Running', }

        self.processStates = ["Pending", "Running", "Completed"]
        self.__initializeProcessLists()

        self.canceled = False
        self.windowTitle = windowTitle

        if self.windowTitle:
            self.ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=100, windowTitle=self.windowTitle)
            labelText = "Processing ..."
            self.ProgressDialog.labelText = labelText
            self.ProgressDialog.show()
            slicer.app.processEvents()
        self.qtimer = qt.QTimer(self)
        self.qtimer.start(500)
        self.qtimer.timeout.connect(self.__checkCancel)

    def __initializeProcessLists(self):
        self.processLists = {}
        for processState in self.processStates:
            self.processLists[processState] = []

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        # This method must be defined but it is empty
        pass

    def __del__(self):
        self.qtimer.stop()
        if self.windowTitle:
            self.ProgressDialog.close()

    def __terminate(self):
        self.canceled = True
        if self.processLists["Running"]:
            for process in self.processLists["Running"]:
                process.terminate()
        self.__initializeProcessLists()

    def __checkFishished(self):
        if self.canceled:
            return
        if not self.processLists["Running"] and not self.processLists["Pending"]:
            for process in self.processLists["Completed"]:
                k, v = process.result[0], process.result[1]
                self.results[k] = v
            self.completedCallback()
        else:
            self.run()

    def waitForFinished(self):
        while self.processLists["Running"] or self.processLists["Pending"]:
            self.run()
            self.processLists["Running"][0].waitForFinished()
            self.__checkFishished()
            if self.canceled:
                break

    def setMaximumRunningProcesses(self, value):
        self.maximumRunningProcesses = value

    def saveState(self):
        state = {}
        for processState in self.processStates:
            state[processState] = [
                process.name for process in self.processLists[processState]]
        self.getParameterNode().SetAttribute("state", json.dumps(state))

    def state(self):
        return json.loads(self.getParameterNode().GetAttribute("state"))

    def addProcess(self, process):
        self.processLists["Pending"].append(process)
        if self.windowTitle:
            self.ProgressDialog.maximum = len(self.processLists["Pending"])

    def run(self):
        while self.processLists["Pending"]:
            if self.canceled or len(self.processLists["Running"]) >= self.maximumRunningProcesses:
                break
            process = self.processLists["Pending"].pop()
            process.run(self)
            self.processLists["Running"].append(process)
            if self.windowTitle:
                self.ProgressDialog.labelText = f"Processed {len(self.processLists['Completed'])}/{self.ProgressDialog.maximum - 1}..."
            self.saveState()
        slicer.app.processEvents()

    def __checkCancel(self):
        if self.windowTitle:
            if self.ProgressDialog.wasCanceled:
                self.__terminate()
                self.ProgressDialog.close()
                self.logger.error("Convolution cancelled")
                self.qtimer.stop()
                return
        self.run()

    def onProcessFinished(self, process):
        self.__checkCancel()
        if not self.canceled:
            self.processLists["Running"].remove(process)
            self.processLists["Completed"].append(process)
            if self.windowTitle:
                self.ProgressDialog.value += 1
            self.saveState()
        slicer.app.processEvents()
        self.__checkFishished()


class Process(qt.QProcess):

    def __init__(self, scriptPath, pickleFileName):
        super().__init__()
        self.name = "Process"
        self.processState = "Pending"
        self.scriptPath = scriptPath
        self.pickleFileName = pickleFileName
        self.debug = False
        self.result = []
        self.logger = getLogger("OpenDose3D.qprocess")

    def run(self, logic):
        self.stateChanged.connect(self.onStateChanged)
        self.started.connect(self.onStarted)

        def finishedSlot(exitCode, exitStatus):
            return self.onFinished(logic, exitCode, exitStatus)
        self.connect('finished(int,QProcess::ExitStatus)', finishedSlot)

        self.start("PythonSlicer", [self.scriptPath, self.pickleFileName])

    def onStateChanged(self, newState):
        self.logger.info('-'*40)
        self.logger.info(f'qprocess state code is: {self.state()}')
        self.logger.info(f'qprocess error code is: {self.error()}')

    def onStarted(self):
        self.logger.info("writing")
        if self.debug:
            with open("/tmp/pickledInput", "w") as fp:
                fp.buffer.write(self.pickledInput())
        self.write(self.pickledInput())
        self.closeWriteChannel()

    def onFinished(self, logic, exitCode, exitStatus):
        self.logger.info(f'finished, code {exitCode}, status {exitStatus}')
        stdout = self.readAllStandardOutput()
        # stderr = self.readAllStandardError()
        self.usePickledOutput(stdout.data())
        logic.onProcessFinished(self)


class ConvolutionProcess(Process):
    """This is an example of running a process to operate on model data"""

    def __init__(self, scriptPath, pickleFileName, initDict, iteration):
        super().__init__(scriptPath, pickleFileName)
        self.initDict = copy.deepcopy(initDict)
        self.iteration = iteration
        self.name = f"Iteration {iteration}"

    def pickledInput(self):
        return pickle.dumps(self.initDict)

    def usePickledOutput(self, pickledOutput):
        # output = np.frombuffer(pickledOutput, dtype=float)
        try:
            output = pickle.loads(pickledOutput)
            self.result = [self.iteration, output['data']]
        except Exception as e:
            print(e)
            print(pickledOutput)
