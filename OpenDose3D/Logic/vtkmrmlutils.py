from difflib import SequenceMatcher

import numpy as np
import SegmentStatistics
from numpy.core.defchararray import array
import vtk

import slicer

from Logic.logging import getLogger
from Logic.utils import checkTesting, whoami, getCurrentTime
from Logic.vtkutils import convertArray
from Logic.errors import IOError
from Logic.attributes import Attributes

logger = getLogger("OpenDose3D.vtkmrmlutils")


def isDICOM(itemID: str) -> bool:
    attribute = getItemDataNode(itemID).GetAttribute("DICOM.instanceUIDs")
    return bool(attribute)


def getSubjectHierarchyNode():
    """
    Gets the subject hierarchy node
    """
    return slicer.mrmlScene.GetSubjectHierarchyNode()


def getSubjectIDs() -> list:
    # NOTE assumes that: scene --> subject
    shNode = getSubjectHierarchyNode()
    sceneID = shNode.GetSceneItemID()
    subjectIDs = []

    rootNodesIDs = getAllFolderChildren(sceneID)
    for nodeID in rootNodesIDs:
        level = getItemAttributeValue(nodeID, "Level")
        if level == "Patient":
            subjectIDs.append(nodeID)

    return subjectIDs


def getStudyIDs() -> list:
    #  NOTE assumes that: scene --> subject --> study
    subjectIDs = getSubjectIDs()
    if subjectIDs:
        subjectID = subjectIDs[0]

        studyIDs = []
        nodeIDs = getAllFolderChildren(subjectID)
        for nodeID in nodeIDs:
            level = getItemAttributeValue(nodeID, "Level")
            if level == "Study":
                studyIDs.append(nodeID)

        return studyIDs
    else:
        return []


def getResultIDs() -> list:
    # NOTE assumes that: scene --> subject --> study --> results
    _ = getSubjectHierarchyNode()
    studyIDs = getStudyIDs()
    if studyIDs:
        studyID = studyIDs[0]

        resultIDs = []
        nodeIDs = getAllFolderChildren(studyID)
        for nodeID in nodeIDs:
            level = getItemAttributeValue(nodeID, "Level")
            if (level == "Folder") and (getItemAttributeValue(nodeID, "Results")):
                resultIDs.append(nodeID)

        return resultIDs
    else:
        return []


def getFolderIDs() -> list:
    # NOTE assumes that: scene --> subject --> study --> folder
    _ = getSubjectHierarchyNode()
    studyIDs = getStudyIDs()
    if studyIDs:
        studyID = studyIDs[0]

        folders = {}
        nodeIDs = getAllFolderChildren(studyID)
        for nodeID in nodeIDs:
            level = getItemAttributeValue(nodeID, "Level")
            if (level == "Folder") and not getItemAttributeValue(nodeID, "Results"):
                folderTime = getItemDataNodeName(nodeID)[:-2]
                if folderTime.isdigit():
                    folders[int(folderTime)] = nodeID
                elif checkTesting():
                    folders[nodeID] = nodeID
                else:
                    print(f"Error in the folder: {getItemDataNodeName(nodeID)}")
        folderIDs = [id[1] for id in sorted(folders.items())]
        return folderIDs
    else:
        return []


def getScalarVolumeNodes() -> list:
    """
    Gets all scalarVolumeNodes, no matter where in the hierarchy they exist
    """
    shNode = getSubjectHierarchyNode()
    nodes = slicer.util.getNodesByClass(Attributes().vtkMRMLScalarVolumeNode)
    itemIDs = []

    for node in nodes:
        nodeID = shNode.GetItemByDataNode(node)
        itemIDs.append(nodeID)

    return itemIDs


def getFirstNodeByName(name: str):
    shNode = getSubjectHierarchyNode()
    node = slicer.util.getFirstNodeByName(name)
    nodeID = shNode.GetItemByDataNode(node)
    return nodeID


def getItemDataNode(itemID: str):
    shNode = getSubjectHierarchyNode()
    node = shNode.GetItemDataNode(itemID)
    return node


def getItemDataNodeName(itemID: str) -> str:
    node = getItemDataNode(itemID)
    name = node.GetName()
    return name


def getItemID(node) -> int:
    """
    Gets itemID of a node
    """
    shNode = getSubjectHierarchyNode()
    try:
        return shNode.GetItemByDataNode(node)
    except:
        return 0


def getItemDataNodeAttributeNames(itemID: str) -> list:
    node = getItemDataNode(itemID)
    attributeNames = node.GetAttributeNames()
    return attributeNames


def hasItemDataNodeAttribute(itemID: str, attributeName: str) -> bool:
    attributeNames = getItemDataNodeAttributeNames(itemID)

    if attributeName in attributeNames:
        logger.debug(f"Item with id '{itemID}' has attribute '{attributeName}'")
        return True

    logger.info(f"Item with id '{itemID}' don't has attribute '{attributeName}'")
    return False


def getItemDataNodeAttributeValue(itemID: str, attributeName: str) -> str:
    node = getItemDataNode(itemID)
    return getDataNodeAttributeValue(node, attributeName)


def getDataNodeAttributeValue(node, attributeName: str) -> str:
    """
    Gets the value of an attribute
    """
    attributeValue = node.GetAttribute(attributeName)
    itemID = getItemID(node)

    if attributeValue is None:
        logger.info(f"Item with id '{itemID}' do not has attribute '{attributeName}'")
    else:
        logger.debug(f"Item with id '{itemID}' has attribute '{attributeName}'")

    return attributeValue


def setItemDataNodeAttribute(itemID: str, attributeName: str, attributeValue: str):
    node = getItemDataNode(itemID)

    try:
        node.SetAttribute(attributeName, str(attributeValue))
        logger.debug(
            f"Created attribute '{attributeName}' with value '{attributeValue}' for item data-node with id '{itemID}'"
        )
    except:
        logger.error(
            f"Failed to create attribute '{attributeName}' with value '{attributeValue}' for item data-node with id '{itemID}'"
        )


def getItemName(itemID: str) -> str:
    """
    Gets item name of a node
    """
    shNode = getSubjectHierarchyNode()

    return shNode.GetItemName(itemID)


def setItemName(itemID: str, name: str):
    """
    Sets item name of a node
    """
    shNode = getSubjectHierarchyNode()
    shNode.SetItemName(itemID, name)


def getItemAttributeNames(itemID: str) -> list:
    """
    Gets all attribute names of an item
    """
    shNode = getSubjectHierarchyNode()
    attributeNames = shNode.GetItemAttributeNames(itemID)

    if len(attributeNames) == 0:
        logger.error(f"Item with id '{itemID}' has no attributes")
    else:
        logger.debug(f"Item with id '{itemID}' has attributes '{attributeNames}'")

    return attributeNames


def getItemTransformNodeID(itemID: str) -> str:
    """
    Gets the transformation Node of an item
    """
    node = getItemDataNode(itemID)
    transformNode = node.GetParentTransformNode()
    return getItemID(transformNode)


def hasItemAttribute(itemID: str, attributeName: str) -> bool:
    """
    Checks if given attribute exists form an item
    """
    shNode = getSubjectHierarchyNode()
    attributeNames = shNode.GetItemAttributeNames(itemID)

    if attributeName in attributeNames:
        logger.debug(f"Item with id '{itemID}' has attribute '{attributeName}'")
        return True

    logger.debug(f"Item with id '{itemID}' has no attribute '{attributeName}'")
    return False


def getItemAttributeValue(itemID: str, attributeName: str) -> str:
    """
    Gets the value of an attribute
    """
    shNode = getSubjectHierarchyNode()
    attributeValue = shNode.GetItemAttribute(itemID, attributeName)

    if attributeValue == "":
        logger.info(f"Item with id '{itemID}' has no attribute '{attributeName}'")
    else:
        logger.debug(
            f"Item with id '{itemID}' has attribute '{attributeName}' with value '{attributeValue}'"
        )

    return attributeValue


def setItemAttribute(itemID: str, attributeName: str, attributeValue: str):
    """
    Sets the an attribute and a value for an item
    """
    shNode = getSubjectHierarchyNode()
    try:
        shNode.SetItemAttribute(itemID, attributeName, str(attributeValue))
        logger.debug(
            f"Created attribute '{attributeName}' with value '{attributeValue}' for item with id '{itemID}'"
        )
    except:
        logger.info(
            f"Failed to create attribute '{attributeName}' with value '{attributeValue}' for item with id '{itemID}'"
        )


def setItemFolder(itemID: str, folderID: str):
    shNode = getSubjectHierarchyNode()
    shNode.SetItemParent(itemID, folderID)


def setNodeFolder(dataNode, folderID: str):
    shNode = getSubjectHierarchyNode()
    shNode.CreateItem(folderID, dataNode)


def createFolder(parentID: str, name: str) -> str:
    shNode = getSubjectHierarchyNode()
    folderID = shNode.CreateFolderItem(parentID, name)

    pluginHandler = slicer.qSlicerSubjectHierarchyPluginHandler().instance()
    folderPlugin = pluginHandler.pluginByName("Folder")
    folderPlugin.setDisplayVisibility(folderID, 1)

    return folderID


def deleteFolder(folderID: str, removeDataNode=True, recursive=True):
    shNode = getSubjectHierarchyNode()
    shNode.RemoveItem(folderID, removeDataNode, recursive)


def createResultsFolder(parentID: str) -> str:
    ResultID = createFolder(parentID, "Results")
    setItemAttribute(ResultID, "Results", "1")

    return ResultID


def getFolderByName(name: str):
    shNode = getSubjectHierarchyNode()
    subjectID = getSubjectIDs()[0]

    studyIDs = getAllFolderChildren(subjectID)

    for studyID in studyIDs:
        folderIDs = getAllFolderChildren(studyID)

        for folderID in folderIDs:
            folderName = shNode.GetItemName(folderID)
            if name == folderName:
                return folderID

    return 0  # only if folder do not exists


def getFolderNames() -> list:
    folders = getFolderIDs()
    folderNames = []
    for folderID in folders:
        folderNames.append(getItemName(folderID))
    return folderNames


def getFolderID(nodeName: str) -> str:
    """
    Gets a specific folder
    NOTE if folder does not exists, returns 0
    """
    shNode = getSubjectHierarchyNode()
    referenceNode = slicer.util.getFirstNodeByClassByName(
        Attributes().vtkMRMLScalarVolumeNode, nodeName
    )
    nodeID = shNode.GetItemByDataNode(referenceNode)
    folderID = shNode.GetItemParent(nodeID)

    return folderID


def getParentFolder(itemID: str) -> str:
    shNode = getSubjectHierarchyNode()
    folderID = shNode.GetItemParent(itemID)

    return folderID


def getAllFolderChildren(folderID: str) -> list:
    shNode = getSubjectHierarchyNode()

    # empty list of node id"s gets filled
    items = vtk.vtkIdList()
    shNode.GetItemChildren(folderID, items)

    # move vtkList to list
    itemIDs = []
    for i in np.arange(items.GetNumberOfIds()):
        itemID = items.GetId(i)
        itemIDs.append(itemID)

    return itemIDs


def getFolderChildByName(folderID: str, name: str) -> str:
    shNode = getSubjectHierarchyNode()
    itemIDs = getAllFolderChildren(folderID)

    for itemID in itemIDs:
        itemName = shNode.GetItemName(itemID)
        if itemName == name:
            return itemID


def makeSlicerLinkedCompositeNodes():
    # Set linked slice views  in all existing slice composite nodes and in the default node
    sliceCompositeNodes = slicer.util.getNodesByClass(Attributes().vtkMRMLSliceCompositeNode)
    defaultSliceCompositeNode = slicer.mrmlScene.GetDefaultNodeByClass(
        Attributes().vtkMRMLSliceCompositeNode
    )
    if not defaultSliceCompositeNode:
        defaultSliceCompositeNode = slicer.mrmlScene.CreateNodeByClass(
            Attributes().vtkMRMLSliceCompositeNode
        )
        slicer.mrmlScene.AddDefaultNode(defaultSliceCompositeNode)
    # sliceCompositeNodes.append(defaultSliceCompositeNode)
    for sliceCompositeNode in sliceCompositeNodes:
        sliceCompositeNode.SetLinkedControl(True)
    defaultSliceCompositeNode.SetLinkedControl(True)


def setSlicerViews(backgroundID: str, foregroundID: str):
    if not checkTesting():
        makeSlicerLinkedCompositeNodes()

        slicer.util.setSliceViewerLayers(
            background=str(backgroundID),
            foreground=str(foregroundID),
            foregroundOpacity=0.5,
        )

        slicer.app.layoutManager().setLayout(
            slicer.vtkMRMLLayoutNode.SlicerLayoutFourUpView
        )
        slicer.util.resetSliceViews()
        slicer.app.processEvents()


def getFolderChildrenByPattern(folderID: str, pattern: str) -> list:
    """
    Gets all children of a specific folder with a name following the given pattern
    NOTE Could also return an empty array if pattern does not get matched for any children
    """
    # TODO refactor the return data structure to use the node class
    shNode = getSubjectHierarchyNode()
    children = vtk.vtkIdList()
    shNode.GetItemChildren(folderID, children)
    nodes = []

    for i in np.arange(children.GetNumberOfIds()):
        nodeID = children.GetId(i)

        if pattern in shNode.GetItemName(nodeID):
            nodes.append(nodeID)

    return nodes


def setAndObserveColorNode(itemID: str):
    """
    set observable in color node
    """
    node = getItemDataNode(itemID)
    name = getItemName(itemID)
    displayNode = node.GetScalarVolumeDisplayNode()

    if displayNode is not None:
        petID = getColorNodePETID()
        greyID = getColorNodeGreyID()

        if "acsc" in name.lower():
            displayNode.SetAndObserveColorNodeID(petID)
        elif "ctct" in name.lower() or "ctrs" in name.lower():
            displayNode.SetAndObserveColorNodeID(greyID)
        elif "ders" in name.lower():
            displayNode.SetAndObserveColorNodeID(greyID)
        elif "actm" in name or "adrm" in name.lower():
            displayNode.SetAndObserveColorNodeID(petID)

        # Refresh Window Level
        displayNode.AutoWindowLevelOff()
        displayNode.AutoWindowLevelOn()


def getColorNodePETID(ColorSpace="Magma"):
    # this color node is hidden, but initially instantiated at slicer startup
    # Variable ColorSpace can be one of "PET-Heat", "Magma", "Inferno", "PET-DICOM"
    return slicer.util.getFirstNodeByName(ColorSpace).GetID()


def getColorNodeGreyID():
    # this color node is hidden, but initially instantiated at slicer startup
    return slicer.util.getFirstNodeByName("Grey").GetID()


def getCommonNameSubString(itemName_0: str, itemName_1: str) -> str:
    # find the documentation: https://docs.python.org/2/library/difflib.html#difflib.SequenceMatcher.find_longest_match
    matcher = SequenceMatcher(None, itemName_0, itemName_1)
    sequence = matcher.find_longest_match(0, len(itemName_0), 0, len(itemName_1))
    match = itemName_1[sequence.b : sequence.b + sequence.size]
    return match


def hasImageData(volumeNode) -> bool:
    """
    This is an example logic method that
    returns true if the passed in volume
    node has valid image data
    """
    if not volumeNode:
        logger.info(f"{whoami()} failed: no volume node")
        return False
    if volumeNode.GetImageData() is None:
        logger.info(f"{whoami()} failed: no image data in volume node")
        return False
    return True


def hasSegmentation(segmentationNode) -> bool:
    """
    This is an example logic method that
    returns true if the passed in volume
    node has valid image data
    """
    if not segmentationNode:
        logger.info(f"{whoami()} failed: no segmentation node")
        return False
    segmnode = slicer.util.getFirstNodeByClassByName(
        Attributes().vtkMRMLSegmentationNode, segmentationNode.GetName()
    )
    if not segmnode:
        logger.info(f"{whoami()} failed: no segmentation in segmentation node")
        return False
    return True


def hasTable(TableNode) -> bool:
    """
    This is an example logic method that
    returns true if the passed in volume
    node has valid image data
    """
    if not TableNode:
        logger.info(f"{whoami()} failed: no table node")
        return False
    tablenode = slicer.util.getFirstNodeByClassByName(
        Attributes().vtkMRMLTableNode, TableNode.GetName()
    )
    if not tablenode:
        logger.info(f"{whoami()} failed: no table in tablenode")
        return False
    table = tablenode.GetTable()
    if table.GetNumberOfColumns() <= 0:
        logger.info(f"{whoami()} failed: tablenode is empty")
        return False
    return True


def GetTableAsArray(tableNode) -> dict:
    TableArray = {}
    table = tableNode.GetTable()
    for col in range(tableNode.GetNumberOfColumns()):
        column = table.GetColumn(col)
        TableArray[column.GetName()] = list(convertArray(column))
    return TableArray


def helperSegmentStatisticsTable(segmentationNode, masterVolumeNode, resultsTableNode):
    segnodeID = getItemID(segmentationNode)
    displayNode = segmentationNode.GetDisplayNode()
    if displayNode is not None:
        displayNode.SetVisibility(1)
    segStatLogic = SegmentStatistics.SegmentStatisticsLogic()
    parameters = segStatLogic.getParameterNode()
    parameters.SetParameter("visibleSegmentsOnly", "False")
    parameters.SetParameter("Segmentation", segmentationNode.GetID())
    parameters.SetParameter("ScalarVolume", masterVolumeNode.GetID())
    parameters.SetParameter("LabelmapSegmentStatisticsPlugin.enabled", "False")
    parameters.SetParameter("ScalarVolumeSegmentStatisticsPlugin.enabled", "True")
    parameters.SetParameter(
        "ScalarVolumeSegmentStatisticsPlugin.voxel_count.enabled", "True"
    )
    parameters.SetParameter(
        "ScalarVolumeSegmentStatisticsPlugin.volume_mm3.enabled", "False"
    )
    parameters.SetParameter("ScalarVolumeSegmentStatisticsPlugin.min.enabled", "False")
    parameters.SetParameter("ScalarVolumeSegmentStatisticsPlugin.max.enabled", "False")
    parameters.SetParameter("ScalarVolumeSegmentStatisticsPlugin.mean.enabled", "True")
    parameters.SetParameter(
        "ScalarVolumeSegmentStatisticsPlugin.median.enabled", "True"
    )
    parameters.SetParameter("ScalarVolumeSegmentStatisticsPlugin.std.enabled", "False")
    parameters.SetParameter("PETVolumeSegmentStatisticsPlugin.enabled", "False")
    segStatLogic.computeStatistics()
    segStatLogic.exportToTable(resultsTableNode)

    resultsTableNode.SetAttribute("Segmentation.Reference", str(segnodeID))
    resultsTableNode.SetAttribute("masterVolumeNode", str(getItemID(masterVolumeNode)))
    resultsTableNode.SetAttribute("DICOM.Study.Date", getCurrentTime())

    if not hasTable(resultsTableNode):
        raise IOError(f"Failed to create Table {resultsTableNode.GetName()}")

    if displayNode is not None:
        displayNode.SetVisibility(0)


def showTable(table):
    """
    Switch to a layout where tables are visible and show the selected table
    """
    if not checkTesting():
        layoutManager = slicer.app.layoutManager()
        currentLayout = layoutManager.layout
        layoutWithTable = slicer.modules.tables.logic().GetLayoutWithTable(
            currentLayout
        )
        layoutManager.setLayout(layoutWithTable)
        appLogic = slicer.app.applicationLogic()
        appLogic.GetSelectionNode().SetActiveTableID(table.GetID())
        appLogic.PropagateTableSelection()
        return

    # Test environment does not have layoutManager
    print("Can't display table")


def showChartinLayout(chart):
    """
    Switch to a layout where charts are visible and show the selected plot
    """
    if not checkTesting():
        slicer.modules.plots.logic().ShowChartInLayout(chart)
        return

    # Test environment does not have layoutManager
    print("Can't display chart")


def RefreshFolderView(folderItemID: str):
    if not checkTesting():
        pluginHandler = slicer.qSlicerSubjectHierarchyPluginHandler().instance()
        folderPlugin = pluginHandler.pluginByName("Folder")
        folderPlugin.setDisplayVisibility(folderItemID, 0)
        folderPlugin.setDisplayVisibility(folderItemID, 1)
        return

    # Test environment does not have layoutManager
    print("Can't refresh folder view")


def cloneNode(node, newName: str):
    if node.GetClassName() != Attributes().vtkMRMLScalarVolumeNode:
        return node

    shNode = getSubjectHierarchyNode()
    nodeID = shNode.GetItemByDataNode(node)
    newnode = slicer.util.getFirstNodeByClassByName(Attributes().vtkMRMLScalarVolumeNode, newName)

    if newnode:
        slicer.mrmlScene.RemoveNode(newnode)

    clonedNode = slicer.modules.volumes.logic().CloneVolume(node, newName)
    clonedItemID = getItemID(clonedNode)
    clonedNodeName = getItemDataNodeName(clonedItemID)
    if clonedNodeName != newName:
        shNode.SetItemName(clonedItemID, newName)
    spacing = node.GetSpacing()
    clonedNode.SetSpacing(spacing)

    parent = shNode.GetItemParent(nodeID)
    shNode.SetItemParent(clonedItemID, parent)

    transformNode = node.GetParentTransformNode()
    if transformNode is not None:
        clonedNode.SetAndObserveTransformNodeID(transformNode.GetID())

    return clonedNode


def reorient(node, orientation="LPS"):
    parameters = {
        "inputVolume1": node,
        "outputVolume": node,
        "orientation": orientation,
    }
    slicer.cli.run(
        slicer.modules.orientscalarvolume,
        None,
        parameters,
        wait_for_completion=True,
        update_display=False,
    )


def getAllowedFileExtensions(node) -> list:
    node.AddDefaultStorageNode()
    storageNode = node.GetStorageNode()
    fileTypes = storageNode.GetSupportedWriteFileTypes()
    fileExtensions = vtk.vtkStringArray()
    storageNode.GetFileExtensionsFromFileTypes(fileTypes, fileExtensions)
    return list(convertArray(fileExtensions))


def saveNode(node, fileName: str):
    slicer.util.saveNode(node, str(fileName), {"useCompression": False})


def saveNodeforGate(node, fileName: str):
    # Gate expects the orientation to be RAI, but doesn't reorient it itself

    # Prepare the node
    newNode = cloneNode(node, "reoriented")
    reorient(newNode, "RAI")
    newNode.SetOrigin(tuple([0, 0, 0]))

    # Remove transformation for saving
    newNode.SetAndObserveTransformNodeID(None)

    # Now save the node
    slicer.util.saveNode(newNode, str(fileName), {"useCompression": False})

    # Erase the temp Node
    slicer.mrmlScene.RemoveNode(newNode)


def changeNodeSpacing(node, newspacing=None):
    if newspacing is None:
        return

    # Change spacing
    ijkToRas = vtk.vtkMatrix4x4()
    node.GetIJKToRASMatrix(ijkToRas)
    for i in range(3):
        ijkToRas.SetElement(i, i, newspacing[i])
    node.SetIJKToRASMatrix(ijkToRas)
