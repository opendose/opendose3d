from datetime import datetime
import inspect
from pathlib import Path
import numpy as np
import qt
import vtk
from scipy import signal

import slicer
from slicer.ScriptedLoadableModule import ScriptedLoadableModuleLogic

import SegmentStatistics

from Logic.attributes import Attributes
from Logic.errors import Error, ConventionError
from Logic.logging import getLogger
from Logic.constants import Constants
from Logic import vtkmrmlutils
from Logic.utils import (
    getCurrentTime,
    extractMode,
    checkTesting,
    hounsfield2density,
    getScriptPath,
    writeTimeIntegFittingReport,
    isNumber,
)
from Logic.nodes import Node


class OpenDose3DLogic(ScriptedLoadableModuleLogic):
    """
    This class implements all the actual computation done by the module.
    The interface should be such, that other python code can import this class
    and make use of the functionality without requiring an instance of
    the Widget. Uses ScriptedLoadableModuleLogic base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def __init__(self, parent=None, isotopeText="Lu-177", compressed=True):
        super().__init__(parent=parent)
        self.logger = getLogger("OpenDose3D.Logic")

        # Variables
        self.parent = parent
        self.testing = checkTesting()
        self.data = {}
        self.tableheader = {}
        self.fittingVIOs = {}
        self.FitValues = None
        self.FIT_FUNCTIONS = None
        self.FIT_FUNCTIONS_DIC = {}
        self.Nukfit = False
        self.Report = {}

        self.forceZERO = {}
        self.segmentsColors = {}
        # self.CpuCores, self.CpuProcessors, self.system = getSystemCoresInfo()
        # self.logger.info(
        # f"Detected {self.CpuProcessors} processors with {self.CpuCores} physical cores")
        self.PETmode = False
        self.STPmode = False
        self.timePoints = []

        # Setup Variables
        self.PETnodeColor = slicer.util.getFirstNodeByName("PET-Heat")
        self.GreynodeColor = slicer.util.getFirstNodeByName("Grey")
        self.script_path = getScriptPath()
        self.logger.info(f"Loading data for isotope {isotopeText}")
        self.Isotope: dict = Constants().isotopes[isotopeText]
        self.Isotope["name"] = isotopeText
        self.logyaxis = False
        self.densityCorrection = True
        self.calculationMaterial = "Water"
        self.referenceFolderID = 0
        self.compressed = compressed
        self.OD3D_version = "v1.0"
        self.defaultFit = "auto-fit"
        self.Acronym_DENSITY_MAP = "DENM"

    def setFittingClass(self, fittingClass="LMFIT"):
        if fittingClass == "NUKFIT":
            from Logic.fit_values_NucmedUI import FitValues, fit_functions
            self.Nukfit = True
        else:
            from Logic.fit_values import FitValues, fit_functions

        self.FitValues = FitValues
        self.FIT_FUNCTIONS = fit_functions

    def standardise(self, injectedActivity=6848, injectionTime='2016-12-14 11:02:51'):
        """
        method to prepare raw data to be grouped by subject hierarchy and image name
        NOTE your data must have names according to the convention of either "[J][0-9]" or "\\d+h|h\\d+"
        TODO we might require to have searchSTR as input to allow flexibility
        """
        try:
            _, studyID = self.standardiseHierarchy()
        except ConventionError as err:
            self.showError(err.message)
            return

        try:
            self.standardiseNodes(studyID, float(injectedActivity), injectionTime)
        except ConventionError as err:
            self.showError(err.message)
            return

    def standardiseHierarchy(self):
        """
        Setup a default hierarchy or incorporate existing folder structure
        """
        shNode = vtkmrmlutils.getSubjectHierarchyNode()
        sceneItemID = shNode.GetSceneItemID()

        # define subject
        subjectIDs = vtkmrmlutils.getSubjectIDs()
        if len(subjectIDs) == 0:
            subjectID = shNode.CreateSubjectItem(sceneItemID, "Subject")
        elif len(subjectIDs) == 1:
            subjectID = subjectIDs[0]
        else:
            self.logger.error(
                "Failed to standardise hierarchy, multiple subjects exist"
            )
            raise ConventionError(
                f"Scene contains {len(subjectIDs)} subjects. Please remove all but one subject."
            )

        # get study
        studyIDs = vtkmrmlutils.getStudyIDs()
        if len(studyIDs) == 0:
            studyID = shNode.CreateStudyItem(subjectID, "Study")
        elif len(studyIDs) >= 1:
            studyID = studyIDs[0]
        else:
            self.logger.error("Failed to standardise hierarchy, multiple studies exist")
            raise ConventionError(
                f"Subject contains {len(studyIDs)} studies. Please remove all but one study."
            )

        # get/create results folder
        resultIDs = vtkmrmlutils.getResultIDs()
        if len(resultIDs) == 0:
            _ = vtkmrmlutils.createResultsFolder(studyID)
        elif len(resultIDs) == 1:
            _ = resultIDs[0]
        else:
            self.logger.error(
                "Failed to standardise hierarchy, multiple results folder exist"
            )
            raise ConventionError(
                f"Study contains {len(resultIDs)} results folder. Please remove all but one results folder."
            )

        return subjectID, studyID

    def standardiseNodes(
        self, studyID: str, injectedActivity: float, injectionTime: str
    ):
        """
        Standardise node naming and move nodes to correct folder
        """
        volumeNodeIDs = vtkmrmlutils.getScalarVolumeNodes()

        # setup progress bar
        if not self.testing:
            ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=len(volumeNodeIDs)
            )
        if not volumeNodeIDs:
            if not self.testing:
                ProgressDialog.close()
            self.showError("Please provide image data for renaming.")
            self.logger.error("Failed to rename files, no image data found")

        # process nodes (erase invalid ones)
        for volumeNodeID in volumeNodeIDs:
            try:
                node = Node.new(
                    volumeNodeID, self.Isotope["name"], injectedActivity
                )
            except:
                node = vtkmrmlutils.getItemDataNode(volumeNodeID)
                self.logger.error(f"Erasing non conforming node {node.GetName()}...")
                slicer.mrmlScene.RemoveNode(node)
                continue

            modality = node.getModality()
            if modality in ["NM", "PT"]:
                node.setTimeStamp(injectionTime)
                hours = round(
                    float(node.getTimeStamp()), 2
                )  # This step also sets time stamp
                if hours is None or hours < 0:
                    hours = 0
                elif hours < 12:  # One hours grace
                    hours = round(hours)
                else:  # 12 hours grace
                    hours = round(hours / 12) * 12

                if hours not in self.timePoints:
                    self.timePoints.append(hours)

        # create folder (named by hours) if not already exists inside study
        self.timePoints.sort()
        for hours in self.timePoints:
            folderName = f"{hours}HR"
            folderID = vtkmrmlutils.getFolderChildByName(studyID, folderName)
            if not folderID:
                folderID = vtkmrmlutils.createFolder(studyID, folderName)

        for index, volumeNodeID in enumerate(volumeNodeIDs):
            try:
                node = Node.new(
                    volumeNodeID, self.Isotope["name"], injectedActivity
                )
            except:
                node = vtkmrmlutils.getItemDataNode(volumeNodeID)
                self.logger.error(f"Erasing non conforming node {node.GetName()}...")
                slicer.mrmlScene.RemoveNode(node)
                continue

            modality = node.getModality()
            if modality in ["NM", "PT", "CT"]:
                vtkmrmlutils.setItemDataNodeAttribute(
                    node.nodeID, Attributes().injectionTime, injectionTime
                )
                vtkmrmlutils.setItemDataNodeAttribute(
                    node.nodeID, Attributes().timeStamp, ""
                )
                vtkmrmlutils.setItemDataNodeAttribute(
                    node.nodeID, Attributes().version, self.OD3D_version.strip()
                )

                if not self.testing:
                    ProgressDialog.labelText = f"Renaming {node.name}..."
                    ProgressDialog.setValue(index)
                    slicer.app.processEvents()
                    if ProgressDialog.wasCanceled:
                        return

                hours = round(float(node.getTimeStamp()), 2)
                diffTime = [abs(time - hours) for time in self.timePoints]
                closerIndex = diffTime.index(min(diffTime))
                node.rename(self.timePoints[closerIndex])
                vtkmrmlutils.setAndObserveColorNode(node.nodeID)

                folderID = vtkmrmlutils.getFolderChildByName(
                    studyID, f"{self.timePoints[closerIndex]}HR"
                )
                vtkmrmlutils.setItemFolder(node.nodeID, folderID)

        # Erase empty folders
        folderIDs = vtkmrmlutils.getFolderIDs()  # This omits Result folder already
        for folderID in folderIDs:
            childrenID = vtkmrmlutils.getAllFolderChildren(folderID)
            if not childrenID:
                vtkmrmlutils.deleteFolder(folderID)

        if not self.testing:
            ProgressDialog.close()

        self.referenceFolderID = self.getReferenceFolder()
        if self.referenceFolderID == 0:
            self.createWaterCT()
            self.referenceFolderID = self.getReferenceFolder()
            if self.referenceFolderID == 0:
                raise ConventionError("No CT study available, impossible to proceed")

        referencenodes = Node.getFolderChildren(self.referenceFolderID)
        if "CTCT" in referencenodes:
            minCTID = referencenodes["CTCT"].data.GetID()
        elif "CTRS" in referencenodes:
            minCTID = referencenodes["CTRS"].data.GetID()
        else:
            raise ConventionError(
                "No anatomical study available, impossible to proceed"
            )

        if "ACSC" in referencenodes:  # Covers NM and PT
            minSPECTID = referencenodes["ACSC"].data.GetID()
            self.spacing = referencenodes["ACSC"].getSpacing()
        else:
            raise ConventionError(
                "No functional study available, impossible to proceed"
            )

        vtkmrmlutils.setSlicerViews(minCTID, minSPECTID)

        self.fixManualTransformations()
        self.checkAttributes()

    def fixManualTransformations(self):
        # FIX manual transformations
        referencenodes = Node.getFolderChildren(self.referenceFolderID)
        folders = vtkmrmlutils.getFolderIDs()
        for index, folderID in enumerate(folders):
            nodes = Node.getFolderChildren(folderID)
            if "TRNF" in nodes:
                node = nodes["TRNF"].data
                if not vtkmrmlutils.hasItemDataNodeAttribute(
                    nodes["TRNF"].nodeID, Attributes().modality
                ):
                    if "CTCT" not in nodes:
                        node.SetAttribute(
                            Attributes().registrationVolume, str(nodes["ACSC"].nodeID)
                        )
                        node.SetAttribute(
                            Attributes().registrationReference,
                            str(referencenodes["ACSC"].nodeID),
                        )
                    else:
                        node.SetAttribute(
                            Attributes().registrationVolume, str(nodes["ACSC"].nodeID)
                        )
                        node.SetAttribute(
                            Attributes().registrationReference,
                            str(nodes["CTCT"].nodeID),
                        )
                    node.SetAttribute(Attributes().registrationSampling, str(0.02))
                    node.SetAttribute(Attributes().registrationMode, "useGeometryAlign")
                    node.SetAttribute(Attributes().registrationRigid, "True")
                    node.SetAttribute(Attributes().registrationAffine, "False")
                    node.SetAttribute(Attributes().registrationAlgorithm, "Manual")
                    node.SetAttribute(Attributes().studyCreation, getCurrentTime())
                    node.SetAttribute(Attributes().modality, "REG")

    def checkAttributes(self):
        """
        Runs sanity checks for the each node in each folder against a reference node
        and sets default attributes if they're missing.
        """
        folders = vtkmrmlutils.getFolderIDs()
        referenceFolderID = self.referenceFolderID

        try:
            referenceNodes = Node.getFolderChildren(referenceFolderID)
            referenceNode = referenceNodes["ACSC"]
        except:
            self.logger.error(
                f"Could not find a reference node in reference folder with id: {referenceFolderID}"
            )
            return False

        if not self.testing:
            ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=len(folders)
            )

        for index, folderID in enumerate(folders):
            folderName = vtkmrmlutils.getItemName(folderID)
            if not self.testing:
                ProgressDialog.labelText = (
                    f"Performing Sanity check on folder {folderName} ..."
                )
                ProgressDialog.value = index
                ProgressDialog.show()
                slicer.app.processEvents()
                if ProgressDialog.wasCanceled:
                    return

            # exclude the reference folder from checks
            if folderID != referenceFolderID:
                try:
                    nodes = Node.getFolderChildren(folderID)
                    node = nodes["ACSC"]
                except:
                    self.logger.error(
                        f"No node to compare to reference node in folder with id {folderID} found"
                    )
                    if not self.testing:
                        ProgressDialog.close()
                    return False

                if not node.compareAttributes(referenceNode):
                    self.showError(
                        "The attributes of the given nodes differ. Please check the integrity of the provided data."
                    )
                    if not self.testing:
                        ProgressDialog.close()
                    return False

        if not self.testing:
            ProgressDialog.close()
        self.logger.info("Sanity check successful!!")
        return True

    def showError(self, error: str):
        """
        displays an error as a feedback to the user
        """
        # TODO create a new class for all this frontend stuff
        errorPopup = qt.QErrorMessage()
        errorPopup.showMessage(error)
        errorPopup.exec_()

    def importMonteCarlo(self, directory: Path):
        """
        imports Gate monte carlo doses
        """
        # Check directory matches loaded Patient
        subdirectories = [f for f in directory.iterdir() if f.is_dir()]
        subdirectoriesStr = [d.name for d in subdirectories]
        shNode = vtkmrmlutils.getSubjectHierarchyNode()
        folders = vtkmrmlutils.getFolderIDs()
        folderkeys = {}
        success = True
        for index, folderID in enumerate(folders):
            folderName = vtkmrmlutils.getItemName(folderID)
            folderkeys[folderName] = folderID
            success = success and (folderName in subdirectoriesStr)
            if success:  # Erase old data if exists
                nodes = Node.getFolderChildren(folderID)
                for key, node in nodes.items():
                    if key == "ADRM":
                        for ikey, inode in node.items():
                            if "monte" in ikey.lower():
                                slicer.mrmlScene.RemoveNode(inode.data)

        if not success:
            raise IOError("loaded folder does not match patient data")

        # Load image and run data
        for subdirectory in subdirectories:
            executorFile = subdirectory / "mac" / "executor.mac"
            dname = subdirectory.name
            hour = float(dname[:-2])
            day = int(hour / 24)
            newName = f"J{day}:ADRM Monte Carlo {dname}"
            with executorFile.open("r") as f:
                lines = f.readlines()
                for line in lines:
                    if "setTotalNumberOfPrimaries" in line:
                        NumberOfPrimaries = int(float(line.strip().split()[-1]))
            simulationFile = subdirectory / "output" / "SimulationStatus.txt"
            with simulationFile.open("r") as f:
                lines = f.readlines()
                for line in lines:
                    if "NumberOfEvents" in line:
                        NumberOfEvents = int(float(line.strip().split()[-1]))
            doseFile = subdirectory / "output" / f"doseMonteCarlo{dname}-Dose.mhd"
            volumeNode = slicer.util.loadVolume(str(doseFile))
            volumeNodeID = vtkmrmlutils.getItemID(volumeNode)
            arr_data = slicer.util.arrayFromVolume(volumeNode).astype(float) * (
                NumberOfPrimaries / NumberOfEvents
            )
            slicer.util.updateVolumeFromArray(volumeNode, arr_data)
            # set attribute
            shNode.SetItemName(volumeNodeID, newName)
            vtkmrmlutils.setItemFolder(volumeNodeID, folderkeys[dname])

        # Standardise and fix positioning
        # self.standardise()
        for index, folderID in enumerate(folders):
            nodes = Node.getFolderChildren(folderID)

            for key, node in nodes.items():
                if key == "ADRM":
                    for ikey, inode in node.items():
                        if "monte" in ikey.lower():
                            # Apply folder transformation
                            if "TRNF" in nodes:
                                trnfNodeID = nodes["TRNF"].data.GetID()
                                inode.data.SetAndObserveTransformNodeID(trnfNodeID)
                            # Gate outputs in Gy/s, convert to mGy/h
                            vox_arr = inode.getArrayData() / Constants().units["mGy_h"]
                            # Fix overflows: >1000 mGy/h is nonsense and it happens because PVE in air
                            # vox_arr = np.where(vox_arr > 1000, 0, vox_arr)
                            vox_arr = np.flip(vox_arr, axis=(1, 2))  # flip Y, Z axis
                            inode.setArrayData(vox_arr)

                            # get ACSC node for data
                            origin = nodes["ACSC"].getOrigin()
                            spacing = nodes["ACSC"].getSpacing()
                            dim = nodes["ACSC"].getDimensions()
                            ijkToRas = nodes["ACSC"].getIJKtoRASMatrix()

                            # Modify ijkToRas matrix
                            for i in range(3):
                                if (
                                    ijkToRas.GetElement(i, i) < 0
                                ):  # We need to flip the direction
                                    ijkToRas.SetElement(
                                        i, i, -ijkToRas.GetElement(i, i)
                                    )
                                    origin[i] -= spacing[i] * (dim[i] - 1)

                            # Modify position and orientation
                            inode.setIJKtoRASMatrix(ijkToRas)
                            inode.setOrigin(origin)
                            inode.setSpacing(spacing)

                            # Convert to RAS orientation
                            inode.reorient("RAS")

                            # TODO: Fill missing attributes
                            inode.data.SetAttribute(Attributes().modality, "RTDOSE")
                            inode.data.SetAttribute(
                                Attributes().timeStamp, nodes["ACSC"].getTimeStamp()
                            )
                            inode.data.SetAttribute(
                                Attributes().isotope, str(self.Isotope["name"])
                            )
                            if "ACTM" in nodes:
                                inode.data.SetAttribute(
                                    Attributes().doseACTMVolume,
                                    str(nodes["ACTM"].nodeID),
                                )
                            else:
                                inode.data.SetAttribute(
                                    Attributes().doseACTMVolume,
                                    str(nodes["ACSC"].nodeID),
                                )
                            inode.data.SetAttribute(
                                Attributes().doseAlgorithm, "Monte Carlo"
                            )
                            inode.data.SetAttribute(
                                Attributes().studyCreation, getCurrentTime()
                            )

                            vtkmrmlutils.setAndObserveColorNode(
                                inode.nodeID
                            )  # refresh window
        slicer.util.resetSliceViews()

    def clean(self):
        """Cleans the setup from extra files and reinitializes all important data"""
        self.data = {}
        self.tableheader = {}
        self.timePoints = []
        self.fittingVIOs = {}
        self.forceZERO = {}
        self.segmentsColors = {}
        nodes = slicer.util.getNodesByClass(Attributes().vtkMRMLScalarVolumeNode)
        for node in nodes:
            name = node.GetName()
            if not any(ext in name for ext in ("ACSC", "CTCT", ": CT")):
                slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass(Attributes().vtkMRMLTransformNode)
        for node in nodes:
            slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass(Attributes().vtkMRMLTableNode)
        for node in nodes:
            slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass(Attributes().vtkMRMLSegmentationNode)
        for node in nodes:
            if "SEGM" in node.GetName():  # Only remove propagated segmentations
                slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass(Attributes().vtkMRMLPlotSeriesNode)
        for node in nodes:
            slicer.mrmlScene.RemoveNode(node)
        nodes = slicer.util.getNodesByClass(Attributes().vtkMRMLPlotChartNode)
        for node in nodes:
            slicer.mrmlScene.RemoveNode(node)

    def getReferenceFolder(self):
        folders = vtkmrmlutils.getFolderIDs()
        self.STPmode = len(folders) == 1

        for folderID in folders:
            nodes = Node.getFolderChildren(folderID)
            self.PETmode = "ACSC" in nodes and nodes["ACSC"].getModality() == "PT"
            if "TRNF" in nodes and vtkmrmlutils.hasItemDataNodeAttribute(
                nodes["TRNF"].nodeID, Attributes().registrationReferenceFolder
            ):
                referenceFolderName = vtkmrmlutils.getItemDataNodeAttributeValue(
                    nodes["TRNF"].nodeID, Attributes().registrationReferenceFolder
                )
                folderNode = slicer.util.getFirstNodeByClassByName(
                    Attributes().vtkMRMLFolderDisplayNode, referenceFolderName
                )
                referenceFolderID = (
                    vtkmrmlutils.getSubjectHierarchyNode().GetItemByDataNode(folderNode)
                )
                if not referenceFolderID:
                    continue
                return referenceFolderID

        minFolderID = 0
        hmin = 1000
        for folderID in folders:
            nodes = Node.getFolderChildren(folderID)

            if (
                "CTCT" in nodes and "ACSC" in nodes
            ):  # we need a folder with both CT and SPECT
                node = nodes["CTCT"]
            else:
                continue

            _, hours = node.extractTime()

            if hours <= hmin:
                hmin = hours
                minFolderID = folderID

        if minFolderID == 0:  # There are no CT, try with ACSC
            for folderID in folders:
                nodes = Node.getFolderChildren(folderID)

                if "ACSC" in nodes:
                    node = nodes["ACSC"]
                else:
                    continue

                _, hours = node.extractTime()

                if hours <= hmin:
                    hmin = hours
                    minFolderID = folderID

        return minFolderID

    def createWaterCT(self):
        folders = vtkmrmlutils.getFolderIDs()
        for folderID in folders:
            nodes = Node.getFolderChildren(folderID)
            if "CTCT" not in nodes or "CTRS" not in nodes:
                newName = nodes["ACSC"].name.replace("ACSC", "CTRS")
                CTRSNode = vtkmrmlutils.cloneNode(nodes["ACSC"].data, newName)
                CTRSNode.SetAttribute(Attributes().modality, "CTRS")
                ctrsItemID = vtkmrmlutils.getItemID(CTRSNode)
                CTRSNode = Node.new(ctrsItemID)

                arrayCTRS = CTRSNode.getArrayData()
                new_arr = np.zeros_like(arrayCTRS)
                CTRSNode.setArrayData(new_arr)

    def resampleCT(self):
        """
        resample all CT nodes to SPECT resolution, keep original data
        """
        folders = vtkmrmlutils.getFolderIDs()
        if not self.testing:
            ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=len(folders)
            )
        for index, folderID in enumerate(folders):
            folderName = vtkmrmlutils.getItemName(folderID)
            nodes = Node.getFolderChildren(folderID)
            if not self.testing:
                ProgressDialog.labelText = f"Resampling {folderName}..."
                ProgressDialog.value = index
                ProgressDialog.show()
                slicer.app.processEvents()
                if ProgressDialog.wasCanceled:
                    return

            # SPECT
            line = f"{folderName} -> {nodes.keys()}"
            self.logger.info(line)
            nodeSPECT = nodes["ACSC"]

            # CT
            if "CTCT" in nodes:
                nodeCT = nodes["CTCT"]
                newName = nodeCT.name.replace("CTCT", "CTRS")
            else:
                minNodes = Node.getFolderChildren(self.referenceFolderID)
                nodeCT = minNodes["CTCT"]
                newName = nodeSPECT.name.replace("ACSC", "CTRS")

            if "CTRS" in nodes:
                slicer.mrmlScene.RemoveNode(nodes["CTRS"].data)

            # Output Rescaled CT using SPECT sampling
            minCT = float(min(nodeCT.getArrayData().ravel()))
            CTRSNode = slicer.mrmlScene.AddNewNodeByClass(Attributes().vtkMRMLScalarVolumeNode)
            CTRSItemID = vtkmrmlutils.getItemID(CTRSNode)

            parameters = {
                "inputVolume": nodeCT.data,
                "referenceVolume": nodeSPECT.data,
                "outputVolume": CTRSNode,
                "interpolationMode": "Lanczos",
                "defaultValue": minCT,
            }

            slicer.cli.run(
                slicer.modules.brainsresample,
                None,
                parameters,
                wait_for_completion=True,
                update_display=False,
            )

            CTRSNode.SetAttribute(Attributes().patientID, nodeSPECT.getPatientID())
            CTRSNode.SetAttribute(
                Attributes().seriesInstanceUID, nodeCT.getSeriesInstanceUID()
            )
            CTRSNode.SetAttribute(
                Attributes().studyInstanceUID, nodeCT.getStudyInstanceUID()
            )
            CTRSNode.SetAttribute(
                Attributes().instanceUIDs, nodeSPECT.getInstanceUIDs()
            )
            CTRSNode.SetAttribute(Attributes().acquisition, nodeSPECT.getAcquisition())
            CTRSNode.SetAttribute(
                Attributes().acquisitionDuration, nodeSPECT.getAcquisitionDuration()
            )
            CTRSNode.SetAttribute(Attributes().patientName, nodeSPECT.getPatientName())
            CTRSNode.SetAttribute(Attributes().modality, "CTRS")
            CTRSNode.SetAttribute(Attributes().timeStamp, nodeSPECT.getTimeStamp())
            CTRSNode.SetAttribute(Attributes().resamplingMode, "Lanczos")
            CTRSNode.SetAttribute(Attributes().resamplingVolume, str(nodeCT.nodeID))
            CTRSNode.SetAttribute(
                Attributes().resamplingReference, str(nodeSPECT.nodeID)
            )
            CTRSNode.SetAttribute(Attributes().studyCreation, getCurrentTime())
            CTRSNode.SetAttribute(Attributes().version,self.OD3D_version.strip())
            CTRSNode.Modified()

            nodes["CTRS"] = Node.new(CTRSItemID)
            nodes["CTRS"].setVisualization()
            nodes["CTRS"].setNameParent(newName, folderID)

            self.logger.info(f"Folder {folderName} processed")

        if not self.testing:
            ProgressDialog.close()

    def scaleValues(
        self,
        calibration,
        injectedActivity: float,
        injectionTime: int,
        clinicalCenter="None",
    ):
        """method to perform image scaling by a factor
        passed as value of dictionnary
        TODO: rephrase
        """
        folders = vtkmrmlutils.getFolderIDs()
        cSensitivity = calibration["SPECTSensitivity"]["Value"]
        # from counts/Bq to Bq/counts
        if "counts/" in calibration["SPECTSensitivity"]["Units"]:
            cSensitivity = 1 / cSensitivity
        if "MBq" not in calibration["SPECTSensitivity"]["Units"]:  # to MBq/counts
            cSensitivity /= 1e6
        correctFactor = any(unit in calibration["SPECTSensitivity"]["Units"] for unit in ["Bqs", "Bq/ml"])
        if not correctFactor:
            cSensitivity *= calibration["SPECTSensitivity"]["Time"]

        ders = slicer.util.getNodes("*CTRS*")
        if len(ders) == 0:
            self.logger.error(
                "Resampled CT images not present, please create them before creating transforms"
            )
            return
        if not self.testing:
            ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=len(folders) * 3
            )

        for index, folderID in enumerate(folders):
            # Initialize folder
            folderName = vtkmrmlutils.getItemName(folderID)
            nodes = Node.getFolderChildren(folderID)
            if not self.testing:
                ProgressDialog.labelText = f"Rescaling {folderName}...Activity Map"
                ProgressDialog.value = 3 * index
                ProgressDialog.show()
                slicer.app.processEvents()
                if ProgressDialog.wasCanceled:
                    return

            # Scale SPECT to Activity Map
            ACSCNode: Node = nodes["ACSC"]
            newName = ACSCNode.name.replace("ACSC", "ACTM")
            
            if not self.PETmode:
                try:
                    duration = float(ACSCNode.getAcquisitionDuration())
                except:
                    self.logger.error("Acquisition duration not set in DICOM")
                    self.showError(
                        "Acquisition duration not set in DICOM\n"
                        + "Please fill the relevant data in the Node information"
                    )
                    if not self.testing:
                        ProgressDialog.close()
                    return

            try:
                localInjectedActivity = float(ACSCNode.getInjectedActivity())
            finally:
                if float(injectedActivity) != localInjectedActivity:
                    ACSCNode.data.SetAttribute(
                        Attributes().injectedActivity, str(injectedActivity)
                    )
                localInjectedActivity = float(injectedActivity)

            try:
                localInjectionTime = ACSCNode.getInjectionTime()
            finally:
                if injectionTime and injectionTime != localInjectionTime:
                    ACSCNode.data.SetAttribute(
                        Attributes().injectionTime, str(injectionTime)
                    )
                localInjectionTime = injectionTime

            try:
                instName = ACSCNode.getInstitutionName()
            finally:
                if instName and instName != clinicalCenter:
                    ACSCNode.data.SetAttribute(
                        Attributes().institutionName, clinicalCenter
                    )
                instName = clinicalCenter

            if "ACTM" in nodes:
                slicer.mrmlScene.RemoveNode(nodes["ACTM"].data)

            ACTMNode = vtkmrmlutils.cloneNode(ACSCNode.data, newName)
            actmItemID = vtkmrmlutils.getItemID(ACTMNode)
            ACTMNode.SetAttribute(
                Attributes().sensitivity, str(calibration["SPECTSensitivity"]["Value"])
            )
            ACTMNode.SetAttribute(
                Attributes().sensitivityUnits,
                str(calibration["SPECTSensitivity"]["Units"]),
            )
            if not self.PETmode:
                ACTMNode.SetAttribute(Attributes().acquisitionDuration, str(duration))
                ACTMNode.SetAttribute(
                    Attributes().calibrationDuration,
                    str(calibration["SPECTSensitivity"]["Time"]),
                )
            ACTMNode.SetAttribute(Attributes().isotope, str(self.Isotope["name"]))
            ACTMNode.SetAttribute(Attributes().rescalingVolume, str(ACSCNode.nodeID))
            ACTMNode.SetAttribute(Attributes().modality, "RWV")
            ACTMNode.SetAttribute(Attributes().studyCreation, getCurrentTime())
            ACTMNode.SetAttribute(
                Attributes().injectedActivity, str(localInjectedActivity)
            )

            vox_arr = ACSCNode.getArrayData()
            # to MBq, we consider MBq everywhere
            if ACSCNode.getModality() == "PT":  # PET images are expressed in Bq/ml
                voxelVolume = (
                    np.prod(ACSCNode.getSpacing())
                    * Constants().units["mm3"]
                    / Constants().units["cm3"]
                )
                # T_h: float = self.Isotope["T_h"] * Constants().units["hour"]
                # x = np.log(2) * duration / T_h
                # decayCorrection = (1 - np.exp(-x)) / x
                # vox_arr = voxelVolume * decayCorrection * vox_arr / 1e6
                vox_arr = voxelVolume * cSensitivity * vox_arr
            elif ACSCNode.getModality() == "NM":  # SPECT images are expressed in MBq
                lsensitivity = cSensitivity / duration
                vox_arr = lsensitivity * vox_arr  # TODO: Apply decay correction too?
            else:  # error
                raise IOError("activity node invalid, no modality present")

            vox_arr.astype(int)

            nodes["ACTM"] = Node.new(actmItemID)
            nodes["ACTM"].setArrayData(vox_arr)

            # Scale rescaled CT to density map
            if not self.testing:
                ProgressDialog.labelText = (
                    f"Rescaling {folderName}...CT density Map"
                )
                ProgressDialog.value = 3 * index + 1
                slicer.app.processEvents()
                if ProgressDialog.wasCanceled:
                    return
            if "CTRS" in nodes:
                ctrsnode = nodes["CTRS"]
            else:
                continue

            newName = ctrsnode.name.replace("CTRS", self.Acronym_DENSITY_MAP)
            if  self.Acronym_DENSITY_MAP in nodes:
                slicer.mrmlScene.RemoveNode(nodes[self.Acronym_DENSITY_MAP].data)

            DERSNode = vtkmrmlutils.cloneNode(ctrsnode.data, newName)
            dersItemID = vtkmrmlutils.getItemID(DERSNode)
            DERSNode.SetAttribute(Attributes().modality, "RWV")
            DERSNode.SetAttribute(Attributes().calibrationCT, str(calibration))
            DERSNode.SetAttribute(Attributes().rescalingVolume, str(ctrsnode.nodeID))
            DERSNode.SetAttribute(Attributes().studyCreation, getCurrentTime())

            # use same scale as original CT
            vox_arr = hounsfield2density(ctrsnode.getArrayData())

            nodes[self.Acronym_DENSITY_MAP] = Node.new(dersItemID)
            nodes[self.Acronym_DENSITY_MAP].setArrayData(vox_arr)

            self.logger.info(f"Folder {folderName} processed")

        if not self.testing:
            ProgressDialog.close()

    def createTransforms(self, reference: Node, preset="Rigid"):
        """Creates the transformations in all time points
        Performs initial COM registration
        Performs Elastix Automatic registration
        If there is a CT in the folder perform CT-CT elastic registration
        otherwise it performs a SPECT-SPECT rigid registration
        TODO: Affine registration is not working properly but it is the default when no elastix is found
        """
        try:
            import Elastix

            USE_ELASTIX = True
        except:
            USE_ELASTIX = False

        if checkTesting():  # TODO: Elastix is failing in tests
            USE_ELASTIX = False

        if USE_ELASTIX:
            elastixLogic = Elastix.ElastixLogic()
            try:
                # Attempt to access parameter filenames using the first method
                registrationPresets = elastixLogic.getRegistrationPresets()
                parameterFilenamesElastix = registrationPresets[0][Elastix.RegistrationPresets_ParameterFilenames]
                parameterFilenamesRigid = registrationPresets[1][Elastix.RegistrationPresets_ParameterFilenames]
            except (KeyError, AttributeError, TypeError) as e:
                self.logger.warning(f"Elastix primary method failed: {e}. Falling back to alternative method.")
                try:
                    # Fallback method to retrieve parameter files
                    parameterFilenamesElastix = registrationPresets[0].getParameterFiles()
                    parameterFilenamesRigid = registrationPresets[1].getParameterFiles()
                except Exception as e:
                    # Handle unexpected errors during fallback
                    self.logger.error(f"Elastix failed to retrieve parameter files in fallback method: {e}")
                    raise RuntimeError("Elastix unable to retrieve parameter filenames for Elastix registration.") from e

        folders = vtkmrmlutils.getFolderIDs()
        referenceName = reference.GetName()
        folderIDR = vtkmrmlutils.getFolderID(referenceName)
        self.referenceFolderID = folderIDR
        nodes = Node.getFolderChildren(folderIDR)
        self.forceZERO.clear()
        self.segmentsColors.clear()
        self.fittingVIOs.clear()

        if "ACTM" not in nodes:
            self.logger.error(
                "Scaling not done, please perform the step before creating transforms"
            )
            return

        ctctnode = nodes["CTCT"]
        actmRnode = nodes["ACTM"]
        originRef = ctctnode.getOrigin()
        spacingRef = ctctnode.getSpacing()
        positionRef = np.array(
            [
                originRef[0] * spacingRef[0],
                originRef[1] * spacingRef[1],
                originRef[2] * spacingRef[2],
            ]
        )
        if not self.testing:
            ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=len(folders)
            )

        for index, folderID in enumerate(folders):
            nodes = Node.getFolderChildren(folderID)

            # if "SEGM" in nodes:
            #     if self.referenceFolderID == folderID:
            #         segmMode = vtkmrmlutils.getItemDataNodeAttributeValue(
            #             vtkmrmlutils.getItemID(nodes["SEGM"].data), "Mode"
            #         )
            #         if not segmMode:
            #             slicer.mrmlScene.RemoveNode(nodes["SEGM"].data)
            #             del nodes["SEGM"]
            #     else:
            #         slicer.mrmlScene.RemoveNode(nodes["SEGM"].data)
            #         del nodes["SEGM"]

            if "TRNF" in nodes:
                slicer.mrmlScene.RemoveNode(nodes["TRNF"].data)
                del nodes["TRNF"]

            folderName = vtkmrmlutils.getItemName(folderID)
            if folderID == folderIDR:  # skip reference folder
                continue

            CTCTMode = "CTCT" in nodes and nodes["CTCT"]

            if CTCTMode:
                CTCTNode = nodes["CTCT"]
            ACTMNode = nodes["ACTM"]
            if CTCTMode:
                vtkmrmlutils.setSlicerViews(
                    ctctnode.data.GetID(), CTCTNode.data.GetID()
                )
            else:
                vtkmrmlutils.setSlicerViews(
                    actmRnode.data.GetID(), ACTMNode.data.GetID()
                )
            if not self.testing:
                if CTCTMode:
                    ProgressDialog.labelText = (
                        f"Registering {CTCTNode.name} to {ctctnode.name}..."
                    )
                else:
                    ProgressDialog.labelText = (
                        f"Registering {ACTMNode.name} to {actmRnode.name}..."
                    )
                ProgressDialog.value = index
                slicer.app.processEvents()
                if ProgressDialog.wasCanceled:
                    return

            # Create transformation
            if CTCTMode:
                transformName = CTCTNode.name.replace("CTCT", "TRNF").replace(
                    "CT", "LinearTransform"
                )
            else:
                transformName = (
                    ACTMNode.name.replace("ACTM", "TRNF")
                    .replace("SPECT", "LinearTransform")
                    .replace("PET", "LinearTransform")
                )
            if "TRNF" not in nodes:
                transformNode = slicer.mrmlScene.AddNewNodeByClass(
                    Attributes().vtkMRMLTransformNode
                )
                transformNode.SetName(transformName)
                transformID = vtkmrmlutils.getItemID(transformNode)
                # here it is the SH ID
                vtkmrmlutils.setNodeFolder(transformNode, folderID)

                # Get origin difference and apply as initial transform
                if CTCTMode:
                    originNode = CTCTNode.getOrigin()
                    spacingNode = CTCTNode.getSpacing()
                else:
                    originNode = ACTMNode.getOrigin()
                    spacingNode = ACTMNode.getSpacing()
                positionNode = np.array(
                    [
                        originNode[0] * spacingNode[0],
                        originNode[1] * spacingNode[1],
                        originNode[2] * spacingNode[2],
                    ]
                )
                TranslationVector = positionRef - positionNode
                vtransform = vtk.vtkTransform()
                vtransform.Translate(TranslationVector)
                transformNode.SetMatrixTransformToParent(vtransform.GetMatrix())
                useAffine = False
            else:
                transformNode = nodes["TRNF"].data
                useAffine = True
            # The procedures below require MRML ID
            transformID = transformNode.GetID()

            # Apply transformation to all volumes inside the folder
            for key, node in nodes.items():
                if key == "TABL" or key == "SEGM":
                    continue
                if key == "ADRM":
                    for inode in node.values():
                        inode.data.SetAndObserveTransformNodeID(transformID)
                else:
                    node.data.SetAndObserveTransformNodeID(transformID)

            # Perform the registration
            if not self.testing:
                ProgressDialog.labelText = f"Performing registration in {folderName}..."
                slicer.app.processEvents()

            if USE_ELASTIX:  # Try first elastix
                self.logger.info("Using Elastix")
                useAffine = False
                useRigid = False
                algorithm = "Automatic registration"
                if CTCTMode and "elastix" in preset.lower():
                    transformNode.SetName(
                        transformName.replace("LinearTransform", "ElastixTransform")
                    )
                    registrationMode = "Elastix"
                    elastixLogic.registerVolumes(
                        ctctnode.data,
                        CTCTNode.data,
                        parameterFilenames=parameterFilenamesElastix,
                        outputTransformNode=transformNode,
                        forceDisplacementFieldOutputTransform=False,
                    )
                elif CTCTMode and "rigid" in preset.lower():
                    transformNode.SetName(
                        transformName.replace("LinearTransform", "RigidTransform")
                    )
                    registrationMode = "Rigid"
                    elastixLogic.registerVolumes(
                        ctctnode.data,
                        CTCTNode.data,
                        parameterFilenames=parameterFilenamesRigid,
                        outputTransformNode=transformNode,
                        forceDisplacementFieldOutputTransform=False,
                    )
                else:
                    transformNode.SetName(
                        transformName.replace("LinearTransform", "RigidTransform")
                    )
                    registrationMode = "Rigid"
                    elastixLogic.registerVolumes(
                        actmRnode.data,
                        ACTMNode.data,
                        parameterFilenames=parameterFilenamesRigid,
                        outputTransformNode=transformNode,
                        forceDisplacementFieldOutputTransform=False,
                    )

            else:  # elastix does not worked, try brainsfit
                self.logger.info("Using Brains linear")
                transformNode.SetName(
                    transformName.replace("ElastixTransform", "LinearTransform")
                )
                registrationMode = "useGeometryAlign"
                algorithm = "Semi-automatic registration"
                useRigid = True
                parameters = {
                    "fixedVolume": ctctnode.data,
                    "movingVolume": CTCTNode.data,
                    "samplingPercentage": 0.02,
                    "linearTransform": transformID,
                    "initialTransform": transformID,
                    "initializeTransformMode": registrationMode,
                    "useRigid": useRigid,
                    "useAffine": useAffine,
                    "maximumStepLength": 0.5,
                }

                slicer.cli.run(
                    slicer.modules.brainsfit,
                    None,
                    parameters,
                    wait_for_completion=True,
                    update_display=False,
                )

            if CTCTMode:
                transformNode.SetAttribute(
                    Attributes().registrationVolume, str(CTCTNode.nodeID)
                )
                transformNode.SetAttribute(
                    Attributes().registrationReference, str(ctctnode.nodeID)
                )
            else:
                transformNode.SetAttribute(
                    Attributes().registrationVolume, str(ACTMNode.nodeID)
                )
                transformNode.SetAttribute(
                    Attributes().registrationReference, str(actmRnode.nodeID)
                )
            transformNode.SetAttribute(
                Attributes().registrationReferenceFolder,
                vtkmrmlutils.getItemName(folderIDR),
            )
            transformNode.SetAttribute(Attributes().registrationSampling, str(0.02))
            transformNode.SetAttribute(Attributes().registrationMode, registrationMode)
            transformNode.SetAttribute(Attributes().registrationRigid, str(useRigid))
            transformNode.SetAttribute(Attributes().registrationAffine, str(useAffine))
            transformNode.SetAttribute(Attributes().registrationAlgorithm, algorithm)
            transformNode.SetAttribute(Attributes().studyCreation, getCurrentTime())
            transformNode.SetAttribute(Attributes().modality, "REG")
            slicer.app.processEvents()

        if not self.testing:
            ProgressDialog.close()

    def o_LED(self, folderID, material, actThreshold, densThreshold):
        """Method to perform LED in one time point"""
        nodes = Node.getFolderChildren(folderID)
        if "ACTM" not in nodes:
            self.logger.error(Constants().strings["Scaling not Done"])
            return

        actNode = nodes["ACTM"]

        if self.Acronym_DENSITY_MAP not in nodes:
            minNodes = Node.getFolderChildren(self.minFolderID)

            if self.Acronym_DENSITY_MAP not in minNodes:
                self.logger.error(Constants().strings["Scaling not Done"])
                return

            DENSNode = minNodes[self.Acronym_DENSITY_MAP]
        else:
            DENSNode = nodes[self.Acronym_DENSITY_MAP]

        # Verify the consistency of the isotope used.
        if nodes["ACTM"].getIsotope() != self.Isotope["name"]:
            self.Isotope = Constants().isotopes[nodes["ACTM"].getIsotope()]

        self.material_density = Constants().materials[material]["density"]
        self.calculationMaterial = Constants().materials[material]["name"]
        self.logger.info(f"Reading node {actNode.name}")
        act_arr = actNode.getArrayData()
        # This threshold is to eliminate statistical noise
        th = max(act_arr.ravel()) * actThreshold
        # set threshold to avoid noise
        new_arr = np.where(act_arr > th, act_arr, 0)  # In this case we need Bq

        voxelVolume = np.prod(actNode.getSpacing()) * Constants().units["mm3"]

        self.logger.info(f"Reading node {DENSNode.name}")
        densityArray = DENSNode.getArrayData() * Constants().units["kg_m3"]
        correcteddensityArray = np.where(
            densityArray > densThreshold, densityArray, densThreshold
        )
        mass_arr = correcteddensityArray * voxelVolume  # get the mass per voxel (kg)
        mass_vox = self.material_density * voxelVolume
     
        if np.array_equal(np.shape(act_arr), np.shape(mass_arr)):
            # The following is in mGy/h
            if self.densityCorrection:
                new_arr = np.divide(new_arr, mass_arr)
            else:
                new_arr = new_arr / mass_vox
            new_arr = (
                new_arr
                * Constants().units["MBq"]
                * self.Isotope["energy"]
                / Constants().units["mGy_h"]
            )
            # create the dose volume
            lmode = "Local Energy Deposition"
            newName = (
                actNode.name.replace("ACTM", "ADRM")
                .replace("SPECT", lmode)
                .replace("PET", lmode)
            )
            if "ADRM" in nodes and lmode in nodes["ADRM"]:
                slicer.mrmlScene.RemoveNode(nodes["ADRM"][lmode].data)

            clonedNode = vtkmrmlutils.cloneNode(actNode.data, newName)
            clonedItemID = vtkmrmlutils.getItemID(clonedNode)
            clonedNode.SetAttribute(Attributes().modality, "RTDOSE")
            clonedNode.SetAttribute(Attributes().isotope, str(self.Isotope["name"]))
            clonedNode.SetAttribute(Attributes().doseACTMVolume, str(actNode.nodeID))
            clonedNode.SetAttribute(
                Attributes().doseAlgorithm, "Local Energy Deposition"
            )
            clonedNode.SetAttribute(Attributes().studyCreation, getCurrentTime())
            clonedNode.SetAttribute(
                Attributes().doseDensityCorrection, str(self.densityCorrection)
            )
            if not self.densityCorrection:
                clonedNode.SetAttribute(Attributes().doseMaterialDensity, str(round(self.material_density,2)))
                clonedNode.SetAttribute(Attributes().doseCalculationMaterial, self.calculationMaterial)

            DOSENode = Node.new(clonedItemID)
            DOSENode.setArrayData(new_arr)

            self.logger.info(f"Node {newName} created...")
            return True

        self.logger.error(
            f"Wrong imput: Images {actNode.name} and {DENSNode.name} don't have the same shape"
        )
        return False

    def LED(self, DVK_material="water", actThreshold=0.0, densThreshold=50):
        """method to perform local energy deposition in each time point prior to integration
        TODO: this shall be parallel, but is not working, it is fast anyway
        """

        self.logger.info("Performing local energy deposition")
        folders = vtkmrmlutils.getFolderIDs()
        for folderID in folders:
            self.o_LED(folderID, DVK_material, actThreshold, densThreshold)

    def __convolute(self, folderID, lmode, kernel, actThreshold, densThreshold, multithreaded, 
                    DVK_material = "water", kernelFilename="OpenDoseDVKData"):
        """convolution in the entire 3D matrix for one time point"""
        nodes = Node.getFolderChildren(folderID)

        if "ACTM" not in nodes:
            self.logger.error(Constants().strings["Scaling not Done"])
            return

        actNode = nodes["ACTM"]

        if self.Acronym_DENSITY_MAP not in nodes:
            minNodes = Node.getFolderChildren(self.minFolderID)

            if self.Acronym_DENSITY_MAP not in minNodes:
                self.logger.error(Constants().strings["Scaling not Done"])
                return

            DENSNode = minNodes[self.Acronym_DENSITY_MAP]
        else:
            DENSNode = nodes[self.Acronym_DENSITY_MAP]

        self.logger.info(f"Reading node {actNode.name}")
        act_arr = actNode.getArrayData()
        self.size = np.size(act_arr)

        self.logger.info(f"Reading node {DENSNode.name}")
        densityArray = DENSNode.getArrayData() * Constants().units["kg_m3"]
        correcteddensityArray = np.where(
            densityArray > densThreshold, densityArray, densThreshold
        )
        activityArray = act_arr * Constants().units["MBq"]

        # This threshold is to eliminate statistical noise
        act_ravel = activityArray.ravel()
        threshold = max(act_ravel) * actThreshold
        act_arr = np.where(  # Apply threshold for consistency
            activityArray > threshold, activityArray, 0
        )
        if np.array_equal(np.shape(activityArray), np.shape(densityArray)):
            if "FFT" in lmode:  # Fourier
                conv_arr = signal.convolve(in1=act_arr, in2=kernel, mode="same")

                if self.densityCorrection:  # Apply density correction
                    conv_arr = np.multiply(conv_arr, self.material_density)
                    conv_arr = np.divide(conv_arr, correcteddensityArray)

            # express in mGy/h
            new_arr = conv_arr / Constants().units["mGy_h"]

            newName = (
                actNode.name.replace("ACTM", "ADRM")
                .replace("SPECT", lmode)
                .replace("PET", lmode)
            )

            if "ADRM" in nodes and lmode in nodes["ADRM"]:
                slicer.mrmlScene.RemoveNode(nodes["ADRM"][lmode].data)

            clonedNode = vtkmrmlutils.cloneNode(actNode.data, newName)
            clonedItemID = vtkmrmlutils.getItemID(clonedNode)
            clonedNode.SetAttribute(Attributes().modality, "RTDOSE")
            clonedNode.SetAttribute(Attributes().isotope, str(self.Isotope["name"]))
            clonedNode.SetAttribute(Attributes().doseACTMVolume, str(actNode.nodeID))
            clonedNode.SetAttribute(Attributes().doseAlgorithm, "Convolution")
            # clonedNode.SetAttribute(Attributes().doseAlgorithmDescription, lmode)
            clonedNode.SetAttribute(Attributes().doseDensityCorrection, str(self.densityCorrection))
            if self.densityCorrection:
                clonedNode.SetAttribute(Attributes().doseMaterialDensity, str(round(self.material_density,2)))
            clonedNode.SetAttribute(Attributes().doseCalculationMaterial, self.calculationMaterial)
            clonedNode.SetAttribute(Attributes().doseKernelFilename, kernelFilename)
            clonedNode.SetAttribute(Attributes().studyCreation, getCurrentTime())

            DOSENode = Node.new(clonedItemID)
            DOSENode.setArrayData(new_arr)

            self.logger.info(f"Node {newName} created...")
            return True
        else:
            self.logger.error(
                f"Wrong imput: Images {actNode.name} and {DENSNode.name} don't have the same shape"
            )
            return False
    
    def listOfKernel(self, isotopeText: str, mat: str) -> dict[str, str]:
        referenceId = self.getReferenceFolder()
        refNodes = Node.getFolderChildren(referenceId)
        if not refNodes:
            return
        self.spacing = refNodes["ACSC"].getSpacing()
        spacing = round(self.spacing[0], 2)

        # Path to the directory containing the .raw files
        folder_path = Path(f"{self.script_path}/Resources/JSON/DVK/")

        # List all .raw files in the folder
        raw_files = list(folder_path.glob("*.raw"))

        kernels = {}
        for kernel in raw_files:
            kernelName = kernel.stem.split("_")
            isotope = kernelName[0]
            material = kernelName[1].lower()
            try:
                size = round(float(kernelName[2].split("mm")[0]), 2)
            except ValueError:
                continue
            
            if isotope == isotopeText \
                and mat.lower() in material \
                and size == spacing:
                kernels[kernelName[-1]] = kernel.stem
        
        return kernels

    def readKernel_raw(self, isotopeText, DVK_material, origin="OpenDoseDVKData"):
        kernels = self.listOfKernel(isotopeText, DVK_material)
        kernel_file = [kernels[file] for file in kernels.keys() if file == origin][0]
        
        # Try reading the .raw file
        raw_file_path = f"{self.script_path}/Resources/JSON/DVK/{kernel_file}.raw"
        
        # Extract the shape from the filename
        shape_str = kernel_file.split("_")[3].split("x")
        shape = tuple(int(shap) for shap in shape_str)
        
        try:
            # Attempt to read the kernel file as float32 and reshape
            kernel_fromfile = np.fromfile(raw_file_path, dtype=np.float32)
            reshapedKernel = np.reshape(kernel_fromfile, shape)
        except ValueError:
            # If reshaping fails, try reading it as float64 instead
            kernel_fromfile = np.fromfile(raw_file_path, dtype=np.float64)
            reshapedKernel = np.reshape(kernel_fromfile, shape)
        
        kernel = reshapedKernel * Constants().units["mGy_MBqs"]
        
        self.material_density = Constants().materials[DVK_material]["density"]
        self.calculationMaterial = Constants().materials[DVK_material]["name"]
        kernelFilename = f"{kernel_file}.raw"
        
        return kernel, kernelFilename
    
    def saveKernel_raw(self, kernel, isotopeText, DVK_material, mhd = False):
        size = round(self.DVK_size[0] / Constants().units["mm"], 2)
        save_kernel = kernel / Constants().units["mGy_MBqs"]
        shape = np.shape(kernel)
        shape2print = f"{shape[0]}x{shape[1]}x{shape[2]}"
        materialName = Constants().materials[DVK_material]["name"].replace(" ", "-")

        fileName = f"{isotopeText}_{materialName}_{size}mm_{shape2print}_OpenDoseDVKData"
        output_path = f"{self.script_path}/Resources/JSON/DVK/{fileName}"

        save_kernel.astype("float32").tofile(f"{output_path}.raw")

        if mhd:
            with open(f"{output_path}.mhd", "w") as file:
                file.writelines(inspect.cleandoc(f"""
                    Object = Image
                    NDims = 3
                    BinaryData = True
                    BinaryDataByteOrderMSB = False
                    CompressedData = False
                    TransformMatrix = 1 0 0 0 1 0 0 0 1
                    Offset = 0 0 0
                    CenterOfRotation = 0 0 0
                    AnatomicalOrientation = RAI
                    ElementSpacing = {size} {size} {size}
                    DimSize = {shape[0]} {shape[1]} {shape[2]}
                    ElementType = MET_FLOAT
                    ElementDataFile = {fileName}.raw
                """))
        
        kernelFilename = f"{fileName}.raw" 
        return kernelFilename

    def buildKernel(self):
        if not self.testing:
            ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=(2 * self.topx + 1)
            )
            ProgressDialog.labelText = " Building kernel..."

        # build kernel
        kernel = np.zeros((2 * self.topx + 1, 2 * self.topy + 1, 2 * self.topz + 1))
        for i in np.arange(-self.topx, self.topx + 1):
            if not self.testing:
                ProgressDialog.value = i + self.topx
                ProgressDialog.show()
                slicer.app.processEvents()
            for j in np.arange(-self.topy, self.topy + 1):
                for k in np.arange(-self.topz, self.topz + 1):
                    distance = int(round(np.sum(np.square([i, j, k]))))
                    identifier = int(abs(i) + abs(j) + abs(k))
                    kernel[i + self.topx, j + self.topy, k + self.topz] = self.DVK[
                        distance
                    ][identifier][0]

        if not self.testing:
            ProgressDialog.close()

        return kernel

    def readKernel(
        self, isotopeText, DVK_size=None, DVK_material="water", multithreaded=False
    ):
        import ast
        from .dbutils import readDVK

        referenceId = self.getReferenceFolder()
        refNodes = Node.getFolderChildren(referenceId)
        self.spacing = refNodes["ACSC"].getSpacing()
        self.DVK = {}
        if DVK_size is None:
            self.DVK_size = np.multiply(self.spacing, Constants().units["mm"])
            self.logger.info(
                f"Loading DVK {isotopeText} - {self.DVK_size/Constants().units['mm']} mm"
            )
        elif np.isscalar(DVK_size):
            self.DVK_size = np.array([DVK_size * Constants().units["mm"]] * 3)
        else:
            self.DVK_size = np.array(DVK_size * Constants().units["mm"])

        self.material_density = Constants().materials[DVK_material]["density"]
        self.calculationMaterial = Constants().materials[DVK_material]["name"]

        sizelow = np.floor(np.multiply(self.spacing, 10)) / 10 * Constants().units["mm"]
        sizehigh = np.ceil(np.multiply(self.spacing, 10)) / 10 * Constants().units["mm"]
        DVKlow = {}
        while not DVKlow:
            DVKlow = readDVK(
                isotopeText=isotopeText,
                material=DVK_material,
                DVK_size=sizelow,
                compressed=self.compressed,
            )
            if not DVKlow:
                sizelow -= 0.1 * Constants().units["mm"]
                if sizelow < 1.0 * Constants().units["mm"]:
                    raise IOError("Can't find lower bound data file")
        DVKhigh = {}
        while not DVKhigh:
            DVKhigh = readDVK(
                isotopeText=isotopeText,
                material=DVK_material,
                DVK_size=sizehigh,
                compressed=self.compressed,
            )
            if not DVKhigh:
                sizehigh += 0.1 * Constants().units["mm"]
                if sizehigh > 6.0 * Constants().units["mm"]:
                    raise IOError("Can't find higher bound data file")
        xs = [sizelow[0], sizehigh[0]]

        topdist = 0
        self.topx = 0
        self.topy = 0
        self.topz = 0

        if not self.testing:
            ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=len(DVKlow)
            )
            ProgressDialog.labelText = " Reading DVK..."

        for prog, coord in enumerate(DVKlow):
            l = ast.literal_eval(coord)
            distance = np.linalg.norm(l * self.DVK_size) / Constants().units["mm"]
            d1 = int(round(np.sum(np.square(l))))
            if d1 not in self.DVK:
                self.DVK[d1] = {}
            dvkvalue = np.interp(
                self.DVK_size[0], xs, [DVKlow[coord][0], DVKhigh[coord][0]]
            )
            dvkerror = dvkvalue * np.sqrt(
                (DVKlow[coord][1] / DVKlow[coord][0]) ** 2
                + (DVKhigh[coord][1] / DVKhigh[coord][0]) ** 2
            )
            # avoid overwrite non simmetrycal voxels at exact same euclidean distance
            identifier = int(np.sum(np.abs(l)))
            self.DVK[d1][identifier] = [
                dvkvalue * Constants().units["mGy_MBqs"],
                dvkerror * Constants().units["mGy_MBqs"],
            ]

            if distance > topdist:
                topdist = distance
            if l[0] > self.topx:
                self.topx = l[0]
            if l[1] > self.topy:
                self.topy = l[1]
            if l[2] > self.topz:
                self.topz = l[2]

            if not self.testing:
                ProgressDialog.value = prog
                ProgressDialog.show()
                slicer.app.processEvents()

        if not self.testing:
            ProgressDialog.close()

    def convolution(
        self,
        isotopeText,
        DVK_size=None,
        DVK_material="water",
        actThreshold=0.0,
        densThreshold=50, 
        mode="density_correction",
        multithreaded=False,
        kernelOrigin = "OpenDoseDVKData"
    ):
        try:
            # Attempt to read the kernel with the original parameters
            kernel, kernelFilename = self.readKernel_raw(isotopeText, DVK_material, kernelOrigin)
        except Exception as e:
            if kernelOrigin != "OpenDoseDVKData":
                slicer.util.warningDisplay(text=f"Error reading {kernelOrigin} kernel; calculation will continue with OpenDoseDVKData")
                self.logger.warning(f"{kernelOrigin} kernel {e}")
            try:
                kernel, kernelFilename = self.readKernel_raw(isotopeText, DVK_material, "OpenDoseDVKData")
            except Exception as e:
                kernel = self.readKernel(isotopeText, DVK_size, DVK_material)
                kernel = self.buildKernel()
                kernelFilename = self.saveKernel_raw(kernel, isotopeText, DVK_material)

        folders = vtkmrmlutils.getFolderIDs()
        if not self.testing:
            ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=len(folders)
            )

        for index, folderID in enumerate(folders):
            folderName = vtkmrmlutils.getItemName(folderID)
            if not self.testing:
                ProgressDialog.labelText = f"Processing {folderName}..."
                ProgressDialog.value = index
                ProgressDialog.show()
                slicer.app.processEvents()
                if ProgressDialog.wasCanceled:
                    return
            self.__convolute(
                folderID, 
                mode, 
                kernel, 
                actThreshold,
                densThreshold, 
                multithreaded,
                DVK_material, 
                kernelFilename)

        if not self.testing:
            ProgressDialog.close()

    def Propagate(self, segmentationNode):
        """Propagates reference segmentation in all time points
        TODO: make all segmenttion not visible by default
        """
        if not self.referenceFolderID:
            self.referenceFolderID = self.getReferenceFolder()
        nodes = Node.getFolderChildren(self.referenceFolderID)
        if self.Acronym_DENSITY_MAP not in nodes:
            self.logger.error(
                "Densities not present, please create them before propagating segmentations"
            )
            return

        folders = vtkmrmlutils.getFolderIDs()
        shNode = vtkmrmlutils.getSubjectHierarchyNode()

        oldsegmNode = segmentationNode.GetName()
        segnodeID = vtkmrmlutils.getItemID(segmentationNode)
        if not self.testing:
            ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=len(folders)
            )

        for index, folderID in enumerate(folders):
            nodes = Node.getFolderChildren(folderID)
            if not self.testing:
                ProgressDialog.labelText = f"Propagating {vtkmrmlutils.getItemName(self.referenceFolderID)} segmentation..."
                ProgressDialog.value = index
                slicer.app.processEvents()
                if ProgressDialog.wasCanceled:
                    return
            ACSCNode = nodes["ACSC"]
            newName = ACSCNode.name.replace("ACSC", "SEGM")
            if (
                newName == oldsegmNode
            ):  # This is already the reference, put it in the right place just in case
                vtkmrmlutils.setNodeFolder(segmentationNode, folderID)
                vtkmrmlutils.setItemName(segnodeID, newName)
                continue
            newsegmNode = slicer.util.getFirstNodeByClassByName(
                Attributes().vtkMRMLSegmentationNode, newName
            )
            while newsegmNode:
                slicer.mrmlScene.RemoveNode(newsegmNode)
                newsegmNode = slicer.util.getFirstNodeByClassByName(
                    Attributes().vtkMRMLSegmentationNode, newName
                )
            newsegmID = (
                slicer.modules.subjecthierarchy.logic().CloneSubjectHierarchyItem(
                    shNode, segnodeID, newName
                )
            )
            newsegmNode = shNode.GetItemDataNode(newsegmID)
            vtkmrmlutils.setNodeFolder(newsegmNode, folderID)
            newsegmNode.RemoveAttribute("Mode")

        if not self.STPmode:
            self.forceZERO.clear()
            self.segmentsColors.clear()
            self.fittingVIOs.clear()

        if not self.testing:
            ProgressDialog.close()

    def ResampleSegmentToSPECT(self):
        binaryLabelmapReprName = (
            slicer.vtkSegmentationConverter.GetBinaryLabelmapRepresentationName()
        )
        closedSurfaceReprName = (
            slicer.vtkSegmentationConverter.GetClosedSurfaceRepresentationName()
        )
        folders = vtkmrmlutils.getFolderIDs()

        if not self.testing:
            ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=len(folders)
            )

        for index, folderID in enumerate(folders):
            folderName = vtkmrmlutils.getItemName(folderID)
            nodes = Node.getFolderChildren(folderID)

            if "SEGM" in nodes:
                if not self.testing:
                    ProgressDialog.labelText = (
                        f"Resampling segments to SPECT resolution {folderName}..."
                    )
                    ProgressDialog.value = index
                    ProgressDialog.show()
                    slicer.app.processEvents()
                    if ProgressDialog.wasCanceled:
                        return

                segmName = nodes["SEGM"].name
                segmNode = slicer.util.getFirstNodeByClassByName(
                    Attributes().vtkMRMLSegmentationNode, segmName
                )
                segmResampled = vtkmrmlutils.getItemDataNodeAttributeValue(
                    vtkmrmlutils.getItemID(segmNode), "Resampled"
                )
                segmNode.GetDisplayNode().SetVisibility(0)

                if not segmResampled:  # Avoid repeat resampling
                    segmNode.CreateClosedSurfaceRepresentation()
                    if float(slicer.app.applicationVersion[:3]) < 5.3:
                        segmNode.GetSegmentation().SetMasterRepresentationName(
                            closedSurfaceReprName
                        )
                    else:
                        segmNode.GetSegmentation().SetSourceRepresentationName(
                            closedSurfaceReprName
                        )
                    segmNode.RemoveBinaryLabelmapRepresentation()
                    segmNode.SetReferenceImageGeometryParameterFromVolumeNode(
                        nodes["ACSC"].data
                    )
                    segmNode.CreateBinaryLabelmapRepresentation()
                    if float(slicer.app.applicationVersion[:3]) < 5.3:
                        segmNode.GetSegmentation().SetMasterRepresentationName(
                            binaryLabelmapReprName
                        )
                    else:
                        segmNode.GetSegmentation().SetSourceRepresentationName(
                            binaryLabelmapReprName
                        )
                    segmNode.CreateDefaultDisplayNodes()
                    segmNode.CreateClosedSurfaceRepresentation()
                    segmNode.SetAttribute(Attributes().modality, "RTSTRUCT")
                    segmNode.SetAttribute(Attributes().studyCreation, getCurrentTime())
                    segmNode.SetAttribute("Resampled", "True")

                else:
                    segmNode.SetReferenceImageGeometryParameterFromVolumeNode(
                        nodes["ACSC"].data
                    )

                if self.referenceFolderID == folderID:
                    segmNode.GetDisplayNode().SetVisibility(1)

                segmNode.Modified()
        if not self.testing:
            ProgressDialog.close()

    def boneMarrowSegmentation(self, minHU, maxHU, autoADR=False):
        folders = vtkmrmlutils.getFolderIDs()
        if not self.testing:
            ProgressDialog = slicer.util.createProgressDialog(parent=None, value=0)
            ProgressDialog.labelText = "Bone marrow segmentation..."
        for folderIndex, folderID in enumerate(folders):
            folderName = vtkmrmlutils.getItemName(folderID)
            nodes = Node.getFolderChildren(folderID)
            try:
                segmentNodeName = nodes["SEGM"].name
            except:
                continue
            segmentationNode = slicer.util.getFirstNodeByClassByName(
                Attributes().vtkMRMLSegmentationNode, segmentNodeName
            )

            ############
            masterVolumeNode = segmentationNode.GetNodeReference(
                segmentationNode.GetReferenceImageGeometryReferenceRole()
            )
            voxelsize = np.mean(masterVolumeNode.GetSpacing())

            # Create segment editor to get access to effects
            segmentEditorWidget = slicer.qMRMLSegmentEditorWidget()
            segmentEditorWidget.setMRMLScene(slicer.mrmlScene)
            segmentEditorNode = slicer.vtkMRMLSegmentEditorNode()
            slicer.mrmlScene.AddNode(segmentEditorNode)
            segmentEditorWidget.setMRMLSegmentEditorNode(segmentEditorNode)
            segmentEditorWidget.setSegmentationNode(segmentationNode)
            segmentEditorWidget.setSourceVolumeNode(masterVolumeNode)

            # Visible segments will be processed
            segmentIDs = segmentationNode.GetSegmentation().GetSegmentIDs()
            inputSegmentIDs = [seg for seg in segmentIDs if "vertebrae" in seg]

            if inputSegmentIDs:
                if not self.testing:
                    maxProgress = (
                        len(inputSegmentIDs) * len(folders)
                        if autoADR
                        else len(inputSegmentIDs)
                    )
                    ProgressDialog.setMaximum(maxProgress)
                # Create a segment that will contain all bone marrow
                boneMarrow = segmentationNode.GetSegmentation().AddEmptySegment(
                    "bone_marrow" , "bone marrow"
                )
                segmentEditorNode.SetOverwriteMode(
                    slicer.vtkMRMLSegmentEditorNode.OverwriteNone
                )

                for segIndex, segmentID in enumerate(inputSegmentIDs):
                    if not self.testing:
                        ProgressDialog.value = (
                            folderIndex * len(inputSegmentIDs) + segIndex
                            if autoADR
                            else segIndex
                        )
                        ProgressDialog.labelText = (
                            f"Bone marrow {segmentID.split('_')[-1]} at {folderName}..."
                        )
                        ProgressDialog.show()
                        slicer.app.processEvents()
                        if ProgressDialog.wasCanceled:
                            return
                    segmentEditorNode.SetSelectedSegmentID(segmentID)
                    # Do masking
                    segmentEditorNode.SetMaskSegmentID(segmentID)
                    segmentEditorNode.SetMaskMode(
                        slicer.vtkMRMLSegmentationNode.EditAllowedInsideSingleSegment
                    )
                    # Fill by thresholding
                    segmentEditorWidget.setActiveEffectByName("Threshold")
                    effect = segmentEditorWidget.activeEffect()
                    effect.setParameter("MinimumThreshold", str(minHU))
                    effect.setParameter("MaximumThreshold", str(maxHU))
                    effect.self().onApply()
                    # Unmask
                    segmentEditorNode.SetMaskMode(
                        slicer.vtkMRMLSegmentationNode.EditAllowedEverywhere
                    )
                    # Smooth
                    segmentEditorWidget.setActiveEffectByName("Smoothing")
                    effect = segmentEditorWidget.activeEffect()
                    effect.setParameter("SmoothingMethod", "GAUSSIAN")
                    effect.self().onApply()
                    # Remove noise
                    segmentEditorWidget.setActiveEffectByName("Islands")
                    effect = segmentEditorWidget.activeEffect()
                    effect.setParameter("Operation", "REMOVE_SMALL_ISLANDS")
                    effect.setParameter("MinimumSize", 500)
                    effect.self().onApply()
                    # Get voxel count
                    segStatLogic = SegmentStatistics.SegmentStatisticsLogic()
                    parameters = segStatLogic.getParameterNode()
                    parameters.SetParameter("Segmentation", segmentationNode.GetID())
                    parameters.SetParameter("ScalarVolume", masterVolumeNode.GetID())
                    parameters.SetParameter(
                        "ScalarVolumeSegmentStatisticsPlugin.enabled", "True"
                    )
                    parameters.SetParameter(
                        "ScalarVolumeSegmentStatisticsPlugin.voxel_count.enabled",
                        "True",
                    )
                    segStatLogic.computeStatistics()
                    stats = segStatLogic.getStatistics()
                    voxel_count = stats[
                        segmentID, "ScalarVolumeSegmentStatisticsPlugin.voxel_count"
                    ]
                    if voxel_count:
                        # Smooth
                        segmentEditorWidget.setActiveEffectByName("Smoothing")
                        effect = segmentEditorWidget.activeEffect()
                        effect.setParameter("SmoothingMethod", "GAUSSIAN")
                        effect.setParameter("KernelSizeMm", voxelsize)
                        effect.self().onApply()
                        # Increase margin
                        segmentEditorWidget.setActiveEffectByName("Margin")
                        effect = segmentEditorWidget.activeEffect()
                        effect.setParameter("MarginSizeMm", voxelsize * 2)
                        effect.self().onApply()
                        # Reduce margin
                        segmentEditorWidget.setActiveEffectByName("Margin")
                        effect = segmentEditorWidget.activeEffect()
                        effect.setParameter("MarginSizeMm", -voxelsize * 2)
                        effect.self().onApply()
                    # Add it to the boneMarrow
                    segmentEditorWidget.setCurrentSegmentID(boneMarrow)
                    segmentEditorWidget.setActiveEffectByName("Logical operators")
                    effect = segmentEditorWidget.activeEffect()
                    effect.setParameter("Operation", "UNION")
                    effect.setParameter("ModifierSegmentID", segmentID)
                    effect.self().onApply()
                    segmentationNode.RemoveSegment(segmentID)

            segmentEditorWidget.setActiveEffectByName("No editing")
            slicer.mrmlScene.RemoveNode(segmentEditorNode)
            del segmentEditorWidget

        if not self.testing:
            ProgressDialog.close()

    def autoSegmentation(self, auto=False, adr=False):

        from TotalSegmentator import TotalSegmentatorLogic
        from Logic.config import readAutoSegmentation

        autoSegments = readAutoSegmentation()
        self.forceZERO.clear()
        self.segmentsColors.clear()
        self.fittingVIOs.clear()
        nodes = slicer.util.getNodesByClass(Attributes().vtkMRMLSegmentationNode)
        for node in nodes:
            if "SEGM" in node.GetName():
                slicer.mrmlScene.RemoveNode(node)

        if self.Isotope["name"] in autoSegments:
            isotope = self.Isotope["name"]
        else:
            isotope = "AllOptions"  # Invalid isotope, use all options by default

        if auto and adr:
            folders = vtkmrmlutils.getFolderIDs()
            if not self.testing:
                ProgressDialog = slicer.util.createProgressDialog(
                    parent=None, value=0, maximum=len(folders) + 1
                )
        else:
            if not self.referenceFolderID:
                self.referenceFolderID = self.getReferenceFolder()
            referenceFolderID = self.referenceFolderID
            if not self.testing:
                ProgressDialog = slicer.util.createProgressDialog(
                    parent=None, value=0, maximum=2
                )
            folders = [referenceFolderID]

        for index, folderID in enumerate(folders):
            folderName = vtkmrmlutils.getItemName(folderID)
            nodes = Node.getFolderChildren(folderID)

            if not self.testing:
                ProgressDialog.labelText = f"Segmenting {folderName}..."
                ProgressDialog.value = index + 1
                ProgressDialog.show()
                slicer.app.processEvents()
                if ProgressDialog.wasCanceled:
                    return

            if "CTCT" in nodes and "ACSC" in nodes:
                ACSCNode = nodes["ACSC"]
                segmentationNode = ACSCNode.name.replace("ACSC", "SEGM")
                newsegmNode = slicer.util.getFirstNodeByClassByName(
                    Attributes().vtkMRMLSegmentationNode, segmentationNode
                )
                while newsegmNode:
                    slicer.mrmlScene.RemoveNode(newsegmNode)
                    newsegmNode = slicer.util.getFirstNodeByClassByName(
                        Attributes().vtkMRMLSegmentationNode, segmentationNode
                    )

                newsegmNode = slicer.mrmlScene.AddNewNodeByClass(
                    Attributes().vtkMRMLSegmentationNode, segmentationNode
                )
                CTCTNode = nodes["CTCT"]
                TotalSegmentatorLogic().process(
                    CTCTNode.data, newsegmNode
                )

                if isotope not in ["AllOptions"]:  # Only remove if valid isotope
                    for segment in newsegmNode.GetSegmentation().GetSegmentIDs():
                        if segment not in autoSegments[isotope]:
                            newsegmNode.GetSegmentation().RemoveSegment(segment)

                newsegmNode.SetAttribute("Mode", "Total Segmentator")

        if not self.testing:
            ProgressDialog.close()

    def setupModeAlgo(self, mode: str, algo: str):
        # TODO remove this method with a better approach
        if "dose" in mode.lower():
            if "FFT" in algo:
                lalgo = "FFT Convolution"
            elif "monte" in algo.lower():
                lalgo = "Monte Carlo"
            else:
                lalgo = "Local Energy Deposition"
        else:
            lalgo = "Activity"
        return lalgo

    def computeNewValues(self, mode: str, algorithm=""):
        """
        update tables once images are normalized
        """
        lalgo = self.setupModeAlgo(mode, algorithm)
        segmentationNodes = slicer.util.getNodesByClass(Attributes().vtkMRMLSegmentationNode)
        segmentationNodesNames = [
            segmentationNode.GetName() for segmentationNode in segmentationNodes
        ]
        if len(segmentationNodesNames) == 0:
            slicer.util.errorDisplay("No Segmentation found, please create one")
            self.logger.debug("No Segmentation found, please create one")
            return
        segmentationNode = segmentationNodes[0]
        folders = vtkmrmlutils.getFolderIDs()
        if not self.testing:
            ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=len(folders)
            )

        for index, folderID in enumerate(folders):
            folderName = vtkmrmlutils.getItemName(folderID)
            nodes = Node.getFolderChildren(folderID)
            if not self.testing:
                ProgressDialog.labelText = f"Processing {folderName}..."
                ProgressDialog.value = index
                slicer.app.processEvents()
                if ProgressDialog.wasCanceled:
                    return

            if "SEGM" in nodes:
                segmNode = nodes["SEGM"].data
            else:
                segmNode = segmentationNode

            if "dose" in mode.lower():
                masterNode = nodes["ADRM"][lalgo]
                newName = (
                    masterNode.name.replace(":ADRM", ":TABL")
                    .replace("Local Energy Deposition", "DoseRateLED")
                    .replace("Convolution", "DoseRateCONV")
                )
            else:
                masterNode = nodes["ACTM"]
                newName = (
                    masterNode.name.replace(":ACTM", ":TABL")
                    .replace("SPECT", "Activity")
                    .replace("PET", "Activity")
                )
            vtkmrmlutils.setSlicerViews(
                nodes[self.Acronym_DENSITY_MAP].data.GetID(), masterNode.data.GetID()
            )

            tmode = extractMode(newName)
            if "TABL" in nodes and tmode in nodes["TABL"]:
                slicer.mrmlScene.RemoveNode(nodes["TABL"][tmode].data)

            resultsTableNode = slicer.mrmlScene.AddNewNodeByClass(Attributes().vtkMRMLTableNode)
            resultsTableNode.SetName(newName)
            vtkmrmlutils.setNodeFolder(resultsTableNode, folderID)

            self.logger.info(
                f"Processing node {masterNode.name} with segmentation {segmNode.GetName()}"
            )
            vtkmrmlutils.helperSegmentStatisticsTable(
                segmNode, masterNode.data, resultsTableNode
            )
            resultsTableNode.SetAttribute(Attributes().tableCreationMode, tmode)
            if not vtkmrmlutils.hasTable(resultsTableNode):
                raise IOError(f"Failed to create Table {newName}")

            # Get the masses
            DERSNode = nodes[self.Acronym_DENSITY_MAP]
            newName = (
                DERSNode.name.replace(self.Acronym_DENSITY_MAP, "TABL")
                .replace("SPECT", "Densities")
                .replace("PET", "Densities")
                .replace("CT", "Densities")
            )
            tmode = extractMode(newName)
            if "TABL" in nodes and tmode in nodes["TABL"]:
                slicer.mrmlScene.RemoveNode(nodes["TABL"][tmode].data)

            resultsDTableNode = slicer.mrmlScene.AddNewNodeByClass(Attributes().vtkMRMLTableNode)
            resultsDTableNode.SetName(newName)
            vtkmrmlutils.setNodeFolder(resultsDTableNode, folderID)

            vtkmrmlutils.helperSegmentStatisticsTable(
                segmNode, DERSNode.data, resultsDTableNode
            )

            resultsDTableNode.SetAttribute(Attributes().tableCreationMode, tmode)

            if not vtkmrmlutils.hasTable(resultsDTableNode):
                raise IOError(f"Failed to create Table {newName}")

        if not self.testing:
            ProgressDialog.close()

    def processTimeIntegrationVariables(self, mode: str, algorithm=""):
        """
        Creation of the data object to generate plots and integration in time
        """
        lalgo = self.setupModeAlgo(mode, algorithm)
        if mode not in self.data:
            self.data[mode] = {}
            self.tableheader[mode] = {}
        self.tableheader[mode][lalgo] = []  # reinitialize the headers, always
        folders = vtkmrmlutils.getFolderIDs()
        if not self.testing:
            ProgressDialog = slicer.util.createProgressDialog(
                parent=None, value=0, maximum=len(folders)
            )
        data = {"mass": {}, "dose": {}}
        for index, folderID in enumerate(folders):
            folderName = vtkmrmlutils.getItemName(folderID)
            nodes = Node.getFolderChildren(folderID)
            if not self.testing:
                ProgressDialog.labelText = f"Processing {folderName}..."
                ProgressDialog.value = index
                slicer.app.processEvents()
                if ProgressDialog.wasCanceled:
                    return
            # Get time points in Hours
            timestamp = float(nodes["ACTM"].getTimeStamp())
            data["mass"][timestamp] = {}
            data["dose"][timestamp] = {}
            # Get segment mass in kg
            DERSNode = nodes[self.Acronym_DENSITY_MAP]
            newName = (
                DERSNode.name.replace(self.Acronym_DENSITY_MAP, "TABL")
                .replace("SPECT", "Densities")
                .replace("PET", "Densities")
                .replace("CT", "Densities")
            )
            tmode = extractMode(newName)
            tabledensitynode = vtkmrmlutils.getItemDataNode(nodes["TABL"][tmode].nodeID)

            for row in range(tabledensitynode.GetNumberOfRows()):
                segmentName = tabledensitynode.GetCellText(row, 0)
                if self.densityCorrection:
                    data["mass"][timestamp][segmentName] = float(tabledensitynode.GetCellText(row, 2)) \
                                                                * Constants().units["cm3"] \
                                                                * float(tabledensitynode.GetCellText(row, 3))
                else:
                    data["mass"][timestamp][segmentName] = float(tabledensitynode.GetCellText(row, 2)) \
                                                                * Constants().units["cm3"] \
                                                                * float(self.material_density)
                    
            # Get the Absorbed Dose Rate (Activity) in Gy/s (MBq)
            if "dose" in mode.lower():
                masterNode = nodes["ADRM"][lalgo]
                newName = (
                    masterNode.name.replace(":ADRM", ":TABL")
                    .replace("Local Energy Deposition", "DoseRateLED")
                    .replace("Convolution", "DoseRateCONV")
                )
            else:
                masterNode = nodes["ACTM"]
                newName = (
                    masterNode.name.replace(":ACTM", ":TABL")
                    .replace("SPECT", "Activity")
                    .replace("PET", "Activity")
                )

            tmode = extractMode(newName)
            resultsTableNode = nodes["TABL"][tmode]
            tableDOSENode = vtkmrmlutils.getItemDataNode(resultsTableNode.nodeID)

            self.tableheader[mode][lalgo].append(resultsTableNode.name)
            for row in range(tableDOSENode.GetNumberOfRows()):
                segmentName = tableDOSENode.GetCellText(row, 0)
                if "dose" in mode.lower():  # mean is what we want
                    data["dose"][timestamp][segmentName] = float(tableDOSENode.GetCellText(row, 3))
                else:  # We need the total, not the mean
                    data["dose"][timestamp][segmentName] = float(tableDOSENode.GetCellText(row, 1)) \
                                                            * float(tableDOSENode.GetCellText(row, 3))
                    
        # Extract values for both density and dose
        mmass = np.array(self.extract_listOfvalues(data, 'mass'))
        ddose = np.array(self.extract_listOfvalues(data, 'dose'))
        delay = np.array(sorted(data['dose'].keys()))

        self.data[mode][lalgo] = np.vstack([delay, mmass.T, ddose.T])

        if not self.testing:
            ProgressDialog.close()

    def extract_listOfvalues(self, data: dict, key: str) -> list:
        # Identify the minimum time point (earliest time point)
        min_time = min(data[key].keys())

        # Determine the organ order at the minimum time point
        organ_order_min_time = list(data[key][min_time].keys())

        values = []
        # Iterate over all time points, sorted to ensure order
        for time in sorted(data[key].keys()):
            # Get the values for the current time, based on organ order at min_time
            current_vector = [data[key][time].get(organ, None) for organ in organ_order_min_time]
            values.append(current_vector)
        return values

    def getTstack(self, data):
        tstack = []
        for elem in data.T:
            tstack.append(np.hstack(elem).tolist())

        sizeX = len(tstack)
        sizeY = len(tstack[0])
        res = np.zeros([sizeX, sizeY])
        for i in range(sizeX):
            for j in range(sizeY):
                res[i, j] = tstack[i][j]

        return res

    def getColors(self):
        import matplotlib._color_data as mcd

        ColorsHex = list(mcd.CSS4_COLORS.values())[10:]  # First 10 colors are too dark
        Colors = list(
            tuple(int(h[i : i + 2], 16) for i in (1, 3, 5)) for h in ColorsHex
        )
        del Colors[2]
        del Colors[10]
        return Colors

    def setColors(self):
        folders = vtkmrmlutils.getFolderIDs()
        Colors = self.getColors()
        for folderID in folders:
            nodes = Node.getFolderChildren(folderID)
            if "SEGM" in nodes:
                segmName = nodes["SEGM"].name
                segmNode = slicer.util.getFirstNodeByClassByName(
                    Attributes().vtkMRMLSegmentationNode, segmName
                )
                for i, segment in enumerate(segmNode.GetSegmentation().GetSegmentIDs()):
                    segmNode.GetSegmentation().GetSegment(segment).SetColor(
                        tuple(colr / 255.0 for colr in Colors[i])
                    )

    def showTableandPlotTimeIntegrationVariables(
        self, mode, algorithm="", showlegend=False
    ):
        lalgo = self.setupModeAlgo(mode, algorithm)
        name = f"T:TABL {lalgo}"
        tableNode = slicer.util.getFirstNodeByClassByName(Attributes().vtkMRMLTableNode, name)
        if tableNode:
            slicer.mrmlScene.RemoveNode(tableNode)

        tableNode = slicer.mrmlScene.AddNewNodeByClass(Attributes().vtkMRMLTableNode, name)

        resultsTableNodeID = vtkmrmlutils.getResultIDs()[0]
        vtkmrmlutils.setNodeFolder(tableNode, resultsTableNodeID)

        ltnode = slicer.util.getFirstNodeByClassByName(
            Attributes().vtkMRMLTableNode, self.tableheader[mode][lalgo][0]
        )
        lrows = ltnode.GetNumberOfRows()

        header_cells_mass = [f"M_{ltnode.GetCellText(i, 0)} (kg)" for i in range(lrows)]
        if "dose" in mode.lower():
            unit = " (mGy/h)"
            header_cells_doserate = [
                f"DR_{ltnode.GetCellText(i, 0)}{unit}" for i in range(lrows)
            ]
        else:
            unit = " (MBq)"
            header_cells_doserate = [
                f"CA_{ltnode.GetCellText(i, 0)}{unit}" for i in range(lrows)
            ]

        xname = "time (h)"
        columnNames = [xname] + header_cells_mass + header_cells_doserate

        slicer.util.updateTableFromArray(
            tableNode, self.getTstack(self.data[mode][lalgo]), columnNames
        )
        tableNode.SetAttribute(Attributes().studyCreation, getCurrentTime())
        tableNode.Modified()

        # Remove previous chart
        chartname = name.replace("TABL", "CHRT")
        plotChartNode = slicer.util.getFirstNodeByClassByName(
            Attributes().vtkMRMLPlotChartNode, chartname
        )
        if plotChartNode:
            slicer.mrmlScene.RemoveNode(plotChartNode)
        # now create the new chart and the plots
        plotChartNode = slicer.mrmlScene.AddNewNodeByClass(
            Attributes().vtkMRMLPlotChartNode, chartname
        )
        plotChartNode.SetTitle(chartname)
        plotChartNode.SetXAxisTitle(xname)
        plotChartNode.SetYAxisTitle(mode + unit)
        plotChartNode.SetYAxisLogScale(self.logyaxis)
        vtkmrmlutils.setNodeFolder(plotChartNode, resultsTableNodeID)
        Colors = self.getColors()

        for i, header in enumerate(header_cells_doserate):
            # Remove previous plots
            plotname = f"P:PLOT {lalgo} {header.split(unit)[0]}"
            plotSeriesNode = slicer.util.getFirstNodeByClassByName(
                Attributes().vtkMRMLPlotSeriesNode, plotname
            )
            while plotSeriesNode:
                slicer.mrmlScene.RemoveNode(plotSeriesNode)
                plotSeriesNode = slicer.util.getFirstNodeByClassByName(
                    Attributes().vtkMRMLPlotSeriesNode, plotname
                )

            # Now create the plots
            plotSeriesNode = slicer.mrmlScene.AddNewNodeByClass(
                Attributes().vtkMRMLPlotSeriesNode, plotname
            )

            plotSeriesNode.SetAndObserveTableNodeID(tableNode.GetID())
            plotSeriesNode.SetXColumnName(xname)
            plotSeriesNode.SetYColumnName(header)
            plotSeriesNode.SetPlotType(slicer.vtkMRMLPlotSeriesNode.PlotTypeScatter)
            plotSeriesNode.SetLineStyle(slicer.vtkMRMLPlotSeriesNode.LineStyleNone)
            plotSeriesNode.SetMarkerStyle(
                slicer.vtkMRMLPlotSeriesNode.MarkerStyleCircle
            )
            plotSeriesNode.SetColor(tuple(colr / 255.0 for colr in Colors[i]))
            vtkmrmlutils.setNodeFolder(
                plotSeriesNode, vtkmrmlutils.getItemID(plotChartNode)
            )
            # Add to chart
            plotChartNode.AddAndObservePlotSeriesNodeID(plotSeriesNode.GetID())

        plotChartNode.SetLegendVisibility(showlegend)
        plotChartNode.Modified()

        # Set slicer views
        if not self.referenceFolderID:
            self.referenceFolderID = self.getReferenceFolder()
        node = Node.getFolderChildren(self.referenceFolderID)
        segmentNodeName = node["SEGM"].name
        segmNode = slicer.util.getFirstNodeByClassByName(
            Attributes().vtkMRMLSegmentationNode, segmentNodeName
        )
        segmNode.GetDisplayNode().SetVisibility(1)
        frontID = node["CTCT"].data.GetID()
        if "activity" in lalgo.lower():
            backID = node["ACTM"].data.GetID()
        else:
            backID = node["ADRM"][lalgo].data.GetID()
        if backID and frontID:
            vtkmrmlutils.setSlicerViews(backID, frontID)
        # Show chart in layout
        vtkmrmlutils.showChartinLayout(plotChartNode)
        # Refresh Results folder view
        vtkmrmlutils.RefreshFolderView(resultsTableNodeID)

    def updateFittingSingleTP(self, mode, algorithm, segmentName, segmentRow, Tef):
        from numpy import log, exp

        lalgo = self.setupModeAlgo(mode, algorithm)
        x_values = self.data[mode][lalgo][0]
        ltnode = slicer.util.getFirstNodeByClassByName(
            Attributes().vtkMRMLTableNode, self.tableheader[mode][lalgo][0]
        )
        lrows = ltnode.GetNumberOfRows()

        if "dose" in mode.lower():
            unit = " (mGy/h)"
            name = f"Fit {lalgo} DR_{segmentName}_fit"
            fitname = f"DR_{segmentName}_{lalgo}"
        else:
            unit = " (MBq)"
            name = f"Fit {lalgo} CA_{segmentName}_fit"
            fitname = f"CA_{segmentName}_{lalgo}"

        tableNode = slicer.util.getFirstNodeByClassByName(Attributes().vtkMRMLTableNode, name)

        if not tableNode:
            raise IOError(f"table {name} was not found")

        dose_values = self.data[mode][lalgo][lrows + 1 :]
        xeff = x_values
        yeff = dose_values[segmentRow]

        lamda = np.log(2) / float(Tef)
        self.fittingVIOs[f"{fitname}_Tef"] = float(Tef)

        xeff = np.insert(xeff, 0, 0)
        yeff = np.insert(yeff, 0, yeff[0] * exp(lamda * xeff[1]))

        fit = self.FitValues(x=xeff, y=yeff, fit_type="single point", lamda=lamda)

        fit.fit()

        self.fittingVIOs[fitname] = fit

        xdataInc = np.linspace(0, xeff[-1], 200)
        xdataTail = np.linspace(xeff[-1], xeff[-1] * 10, 100)
        xdata = np.concatenate((xdataInc, xdataTail))

        lamda = np.log(2) / float(Tef)
        ydata = np.array([yeff[0] * exp(-lamda * x) for x in xdata])

        slicer.util.updateTableFromArray(
            tableNode, (xdata, ydata), columnNames=["time (h)", mode + unit]
        )
        tableNode.Modified()

    def updateFitting(
        self,
        mode,
        algorithm,
        segmentName,
        segmentRow,
        forceZero,
        integrationmode,
    ):
        import matplotlib._color_data as mcd
        from numpy import log, exp

        lalgo = self.setupModeAlgo(mode, algorithm)
        x_values = self.data[mode][lalgo][0]
        ltnode = slicer.util.getFirstNodeByClassByName(
            Attributes().vtkMRMLTableNode, self.tableheader[mode][lalgo][0]
        )
        lrows = ltnode.GetNumberOfRows()

        if "dose" in mode.lower():
            unit = " (mGy/h)"
            name = f"Fit {lalgo} DR_{segmentName}_fit"
            fitname = f"DR_{segmentName}_{lalgo}"
        else:
            unit = " (MBq)"
            name = f"Fit {lalgo} CA_{segmentName}_fit"
            fitname = f"CA_{segmentName}_{lalgo}"

        tableNode = slicer.util.getFirstNodeByClassByName(Attributes().vtkMRMLTableNode, name)

        if not tableNode:
            raise IOError(f"table {name} was not found")

        dose_values = self.data[mode][lalgo][lrows + 1 :]
        xeff = x_values
        yeff = dose_values[segmentRow]

        lamda = np.log(2) / self.Isotope["T_h"]

        if "trapezoid" in integrationmode.lower():
            # incorporation part
            if forceZero:
                xeff_trap = np.insert(x_values, 0, 0)
                yeff_trap = np.insert(yeff, 0, 0)
            else:
                xeff_trap = np.insert(x_values, 0, 0)
                yeff_trap = np.insert(yeff, 0, yeff[0])
            self.fittingVIOs[fitname] = self.FitValues(
                x=xeff_trap, y=yeff_trap, fit_type="trapezoid", lamda=lamda
            )
            try:
                fit  = self.FitValues(x=xeff_trap[-2:], y=yeff_trap[-2:], 
                                      fit_type="auto", lamda=lamda)
                fit.fit()
                T_ = list(fit.fitter.getParameters())[-1]
            except:
                T_ = 0 if self.Nukfit else self.Isotope["T_h"]

            lamda_ = (T_ + lamda) if self.Nukfit else np.log(2) / T_
            xdataTail = np.linspace(xeff_trap[-1], xeff_trap[-1] * 10, 100)
            ydataTail = np.array([yeff_trap[-1] * exp(-lamda_ *(x - xeff_trap[-1])) for x in xdataTail])
            
            ydata = np.concatenate((yeff_trap, ydataTail))
            xdata = np.concatenate((xeff_trap, xdataTail))

            Tef = np.log(2)/(T_ + lamda) if self.Nukfit else T_
            self.fittingVIOs[f"{fitname}_Tef"] = float(Tef)
        else:
            fit = self.FitValues(
                x=xeff, y=yeff, fit_type=integrationmode.lower(), \
                lamda=lamda, forceZeroModel=forceZero,
            )

            try:
                fit.fit()
                fit.fitter.AUC_SD()
            except:
                raise ValueError(
                    f"The {fit.fit_type} fitting fails with the given dataset."
                )

            self.fittingVIOs[fitname] = fit

            xdataInc = np.linspace(0, xeff[-1], 200)
            xdataTail = np.linspace(xeff[-1], xeff[-1] * 10, 100)
            xdata = np.concatenate((xdataInc, xdataTail))

            ydata = np.array([fit.fitter.plot(x) for x in xdata])

            self.Report['Fitting'][segmentName] = fit.fitting_summary
            self.Report['Model Selection'][segmentName] = fit.model_selection_report

            writeTimeIntegFittingReport("fitting_summary", self.Report['Fitting'])
            writeTimeIntegFittingReport("model_selection_report", self.Report['Model Selection'])

        self.forceZERO[fitname] = forceZero
        slicer.util.updateTableFromArray(
            tableNode, (xdata, ydata), columnNames=["time (h)", mode + unit]
        )
        tableNode.Modified()

    def defaultFitting(self, mode, algorithm):
        import matplotlib._color_data as mcd
        from numpy import log, exp

        lalgo = self.setupModeAlgo(mode, algorithm)
        x_values = self.data[mode][lalgo][0]
        ltnode = slicer.util.getFirstNodeByClassByName(
            Attributes().vtkMRMLTableNode, self.tableheader[mode][lalgo][0]
        )
        lrows = ltnode.GetNumberOfRows()
        Colors = self.getColors()

        # read values
        if "dose" in mode.lower():
            unit = " (mGy/h)"
            header_cells_doserate = [
                f"DR_{ltnode.GetCellText(i, 0)}" for i in range(lrows)
            ]
            dose_values = self.data[mode][lalgo][lrows + 1 :]
        else:
            unit = " (MBq)"
            header_cells_doserate = [
                f"CA_{ltnode.GetCellText(i, 0)}" for i in range(lrows)
            ]
            dose_values = self.data[mode][lalgo][lrows + 1 :]

        xname = "time (h)"

        # chart
        chartname = f"T:CHRT {lalgo}"
        plotChartNode = slicer.util.getFirstNodeByClassByName(
            Attributes().vtkMRMLPlotChartNode, chartname
        )
        if not plotChartNode:
            raise IOError(f"Can't find chart {chartname}")

        # table
        tablname = f"T:TABL {lalgo}"
        tablNode = slicer.util.getFirstNodeByClassByName(Attributes().vtkMRMLTableNode, tablname)
        if not tablNode:
            raise IOError(f"Can't find table {tablname}")

        xeff = x_values  # this in hours for fitting
        self.FIT_FUNCTIONS_DIC["ZERO"] = self.FIT_FUNCTIONS(len(xeff), True)
        self.FIT_FUNCTIONS_DIC["ALL"] = self.FIT_FUNCTIONS(len(xeff), False)   

        T_h = self.Isotope["T_h"]  # in hours
        self.logger.info(f"T1/2 = {T_h:.3e}h")
        lamda = np.log(2) / T_h

        self.Report['Fitting'] = {}
        self.Report['Model Selection'] = {}

        for i, dose in enumerate(dose_values):
            yeff = dose
            self.logger.info(f"Processing {header_cells_doserate[i]}")

            # Fitting funtion
            if self.STPmode:
                xeff = x_values
                xeff = np.insert(xeff, 0, 0)
                yeff = np.insert(yeff, 0, yeff[0] * exp(lamda * xeff[1]))
                fit = self.FitValues(
                    x=xeff, y=yeff, fit_type="single point", lamda=lamda
                )
                self.fittingVIOs[f"{header_cells_doserate[i]}_{lalgo}_Tef"] = T_h
            else:
                fit = self.FitValues(x=xeff, y=yeff, fit_type=self.defaultFit, lamda=lamda)
            fit.fit()

            self.fittingVIOs[f"{header_cells_doserate[i]}_{lalgo}"] = fit
            self.forceZERO[f"{header_cells_doserate[i]}_{lalgo}"] = False

            self.Report['Fitting'][header_cells_doserate[i][3:]] = fit.fitting_summary
            self.Report['Model Selection'][header_cells_doserate[i][3:]] = fit.model_selection_report

            # Table for fitting values
            name = f"Fit {lalgo} {header_cells_doserate[i]}_fit"
            TableNodes = slicer.util.getNodesByClass(Attributes().vtkMRMLTableNode)
            for node in TableNodes:
                if name == node.GetName():  # Table exists, erase it
                    slicer.mrmlScene.RemoveNode(node)

            plotTableNode = slicer.mrmlScene.AddNewNodeByClass(Attributes().vtkMRMLTableNode)
            plotTableNode.RemoveAllColumns()

            plotTableNode.SetName(name)
            plotTable = plotTableNode.GetTable()

            xColumn = vtk.vtkDoubleArray()
            xColumn.SetName(xname)
            plotTable.AddColumn(xColumn)

            yColumn = vtk.vtkDoubleArray()
            yColumn.SetName(mode + unit)
            plotTable.AddColumn(yColumn)

            if fit.fitter == "trapezoid":
                self.updateFitting( mode, 
                                    algorithm, 
                                    header_cells_doserate[i][3:], 
                                    i,
                                    False,
                                    "trapezoid",
                                    )
            else:
                xdataInc = np.linspace(0, xeff[-1], 200)
                xdataTail = np.linspace(xeff[-1], xeff[-1] * 10, 100)
                xdata = np.concatenate((xdataInc, xdataTail))

                plotTable.SetNumberOfRows(len(xdata))

                if self.STPmode:
                    for index, data in enumerate(xdata):
                        xColumn.SetValue(index, data)
                        yColumn.SetValue(index, yeff[0] * exp(-lamda * data))
                else:
                    for index, data in enumerate(xdata):
                        xColumn.SetValue(index, data)
                        yColumn.SetValue(index, fit.fitter.plot(data))

                xColumn.Modified()
                yColumn.Modified()

            # Remove previous plots
            plotname = f"P:PLOT {lalgo} {header_cells_doserate[i]}_fit"
            plotSeriesNode = slicer.util.getFirstNodeByClassByName(
                Attributes().vtkMRMLPlotSeriesNode, plotname
            )
            while plotSeriesNode:
                slicer.mrmlScene.RemoveNode(plotSeriesNode)
                plotSeriesNode = slicer.util.getFirstNodeByClassByName(
                    Attributes().vtkMRMLPlotSeriesNode, plotname
                )

            # Now create the plots
            plotSeriesNode = slicer.mrmlScene.AddNewNodeByClass(
                Attributes().vtkMRMLPlotSeriesNode, plotname
            )
            plotSeriesNode.SetAndObserveTableNodeID(plotTableNode.GetID())
            plotSeriesNode.SetXColumnName(xname)
            plotSeriesNode.SetYColumnName(mode + unit)
            plotSeriesNode.SetPlotType(slicer.vtkMRMLPlotSeriesNode.PlotTypeScatter)
            plotSeriesNode.SetLineStyle(slicer.vtkMRMLPlotSeriesNode.LineStyleSolid)
            plotSeriesNode.SetMarkerStyle(slicer.vtkMRMLPlotSeriesNode.MarkerStyleNone)
            plotSeriesNode.SetColor(tuple(colr / 255.0 for colr in Colors[i]))

            # Add to chart
            plotChartNode.AddAndObservePlotSeriesNodeID(plotSeriesNode.GetID())

            # Segment Color
            self.segmentsColors[ltnode.GetCellText(i, 0)] = Colors[i]

            vtkmrmlutils.setNodeFolder(plotTableNode, vtkmrmlutils.getItemID(tablNode))
            vtkmrmlutils.setNodeFolder(
                plotSeriesNode, vtkmrmlutils.getItemID(plotChartNode)
            )

        plotChartNode.SetLegendVisibility(False)
        if not self.STPmode:
            plotChartNode.SetXAxisRangeAuto(False)
            plotChartNode.SetXAxisRange(0, xeff[-1] * 1.10)

        if "local" in lalgo.lower() or "convolution" in lalgo.lower():
            referenceFolder = self.getReferenceFolder()
            nodes = Node.getFolderChildren(referenceFolder)
            ADRMNode = nodes["ADRM"][f"{lalgo}"]
            densityCorrection = vtkmrmlutils.getItemDataNodeAttributeValue(
                ADRMNode.nodeID, Attributes().doseDensityCorrection
            )
            material = vtkmrmlutils.getItemDataNodeAttributeValue(
                ADRMNode.nodeID, Attributes().doseCalculationMaterial
            )
            self.calculationMaterial = material or "Water" 

            if densityCorrection == "True":
                plotChartNode.SetTitle(f"{lalgo} (density correction ON)")
                self.densityCorrection = True
            else:
                plotChartNode.SetTitle(f"{lalgo} (density correction OFF)")
                self.densityCorrection = False
        else:
            plotChartNode.SetTitle(f"{lalgo}")

        plotChartNode.Modified()

        resultsTableNode = slicer.util.getFirstNodeByClassByName(
            Attributes().vtkMRMLTableNode, f"T:TABL {lalgo} integrated"
        )
        if resultsTableNode:
            slicer.mrmlScene.RemoveNode(resultsTableNode)

        writeTimeIntegFittingReport("fitting_summary", self.Report['Fitting'])
        writeTimeIntegFittingReport("model_selection_report", self.Report['Model Selection'])

    def integrate(self, mode: str, algorithm: str):
        lalgo = self.setupModeAlgo(mode, algorithm)

        if "dose" in mode.lower():
            lunit = "(Gy)"
            ltext = "Absorbed Dose"
        else:
            lunit = "(MBqh)"
            ltext = "TIA"

        ltnode = slicer.util.getFirstNodeByClassByName(
            Attributes().vtkMRMLTableNode, self.tableheader[mode][lalgo][0]
        )
        lrows = ltnode.GetNumberOfRows()

        header_cells_mass = [f"M_{ltnode.GetCellText(i, 0)}" for i in range(lrows)]

        if "dose" in mode.lower():
            header_cells_doserate = [
                f"DR_{ltnode.GetCellText(i, 0)}" for i in range(lrows)
            ]
        else:
            header_cells_doserate = [
                f"CA_{ltnode.GetCellText(i, 0)}" for i in range(lrows)
            ]

        mass_values = self.data[mode][lalgo][1 : len(header_cells_mass) + 1]
        x_values = self.data[mode][lalgo][0]

        name = f"T:TABL {lalgo} integrated"

        if not self.referenceFolderID:
            self.referenceFolderID = self.getReferenceFolder()
        nodes = Node.getFolderChildren(self.referenceFolderID)
        timePoint = float(nodes["ACSC"].getTimeStamp())
        indexReferenceFolder = int(np.nonzero(x_values == timePoint)[0])

        resultsTableNode = slicer.util.getFirstNodeByClassByName(
            Attributes().vtkMRMLTableNode, name
        )
        if resultsTableNode:
            table = resultsTableNode.GetTable()
        else:
            # prepare clean table
            resultsTableNode = slicer.mrmlScene.AddNewNodeByClass(Attributes().vtkMRMLTableNode)
            resultsTableNode.RemoveAllColumns()
            resultsTableNodeID = vtkmrmlutils.getResultIDs()[0]
            vtkmrmlutils.setNodeFolder(resultsTableNode, resultsTableNodeID)

            resultsTableNode.SetName(name)
            table = resultsTableNode.GetTable()

            segmentColumnValue = vtk.vtkStringArray()
            segmentColumnValue.SetName("VOI")
            table.AddColumn(segmentColumnValue)

            segmentColumnValue = vtk.vtkStringArray()
            segmentColumnValue.SetName("Mass (kg)")
            table.AddColumn(segmentColumnValue)

            TotalColumnValue = vtk.vtkStringArray()
            TotalColumnValue.SetName(f"{ltext} {lunit}")
            table.AddColumn(TotalColumnValue)

            if "dose" not in mode.lower():
                doseColumnValue = vtk.vtkStringArray()
                doseColumnValue.SetName("Absorbed Dose by LED (Gy)")
                table.AddColumn(doseColumnValue)

            differenceColumnValue = vtk.vtkStringArray()
            differenceColumnValue.SetName("StdDev (Gy)")
            table.AddColumn(differenceColumnValue)

            FitColumnValue = vtk.vtkStringArray()
            fittingClass = ' (NUKFIT)' if self.Nukfit else ' (LMFIT)'
            fittingClass = '' if self.STPmode else fittingClass
            FitColumnValue.SetName("Fit function" + fittingClass)
            table.AddColumn(FitColumnValue)

            trapintColumnValue = vtk.vtkStringArray()
            trapintColumnValue.SetName("Extrapolation (%)")
            table.AddColumn(trapintColumnValue)

            table.SetNumberOfRows(len(header_cells_doserate))

        T_h = self.Isotope["T_h"]  # in hours
        self.Report['Integration'] = {}

        for i, mass in enumerate(mass_values):
            fitVIO = self.fittingVIOs[f"{header_cells_doserate[i]}_{lalgo}"]
            T_ = self.fittingVIOs[f"{header_cells_doserate[i]}_{lalgo}_Tef"] \
                  if self.STPmode or 'trapezoid' in fitVIO.fit_type else T_h
            (
                data_integral,
                data_integral_error,
                total_integral,
                fit_function,
            ) = fitVIO.integrate(T_)

            self.Report['Integration'][header_cells_doserate[i].split("_")[1]] = fitVIO.integration_summary

            # To display the equations change to info
            amass = mass[indexReferenceFolder]
            table.SetValue(i, 0, header_cells_doserate[i][3:])
            table.SetValue(i, 1, f"{amass:.3f}" if amass > 0.100 else f"{amass:.2e}" ) # scientific notation if mass less than 100 grams 
            if "dose" in mode.lower():  # units mGy/h * h -> mGy
                # output is in Gy
                if isNumber(data_integral_error):
                    integral_error = data_integral_error * Constants().units['mGy']
                    data_integral_error = f"{integral_error:.2f}" if integral_error > 5e-2 else f"{integral_error:.2e}"
                    self.logger.info(
                        f"AD to {header_cells_doserate[i]} = ({total_integral * Constants().units['mGy']:.2f} +/- {integral_error:.2e}) Gy"
                    )
                adr_doses = total_integral * Constants().units['mGy']
                table.SetValue(i, 2, f"{adr_doses:.2f}") if adr_doses > 5e-1 else table.SetValue(i, 2, f"{adr_doses:.2e}")
                table.SetValue(i, 3, data_integral_error)
                table.SetValue(i, 4, fit_function)
                table.SetValue(i, 5, f"{(1 - data_integral/total_integral)*100:.1f}")
            else:  # units MBq * h -> MBqh
                table.SetValue(i, 2, f"{total_integral:.3e}")
                ldose = (
                    total_integral
                    * Constants().units["MBqh"]
                    * self.Isotope["energy"]
                    / amass
                )  
                # output is in Gy
                if isNumber(data_integral_error):
                    integral_error = data_integral_error * ldose / total_integral
                    data_integral_error = f"{integral_error:.2f}" if integral_error > 5e-2 else f"{integral_error:.2e}"
                    self.logger.info(
                        f"AD to {header_cells_doserate[i]} = ({ldose:.3e} +/- {integral_error:.2e}) Gy"
                    )
                table.SetValue(i, 3, f"{ldose:.2f}") if ldose > 5e-1 else table.SetValue(i, 3, f"{ldose:.2e}")
                table.SetValue(i, 4, data_integral_error)
                table.SetValue(i, 5, fit_function)
                table.SetValue(i, 6, f"{(1 - data_integral/total_integral)*100:.1f}")  # output in MBqh
        writeTimeIntegFittingReport("integration_summary", self.Report['Integration'])
        resultsTableNode.Modified()
        # Show the Table
        vtkmrmlutils.showTable(resultsTableNode)
