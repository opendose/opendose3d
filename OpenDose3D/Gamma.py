import logging
import importlib
import numpy as np

import vtk
import qt
import ctk
import slicer
from slicer.ScriptedLoadableModule import *

from Logic import utils, vtkmrmlutils
from Logic.attributes import Attributes

__package__ = "Gamma"

from pymedphys import gamma
import skimage.metrics as compare

#
# Gamma
#


class Gamma(ScriptedLoadableModule):
    """Uses ScriptedLoadableModule base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def __init__(self, parent):
        super().__init__(parent)
        # TODO: make this more human readable by adding spaces
        self.parent.title = "OpenDose ADR Comparator"
        # TODO: set categories (folders where the module shows up in the module selector)
        self.parent.categories = ["Radiotherapy"]
        # TODO: add here list of module names that this module requires
        self.parent.dependencies = []
        # TODO: replace with "Firstname Lastname (Organization)"
        self.parent.contributors = [
            "Alex Vergara Gil (INSERM, France)",
            "Gan Quan (INSERM, France)",
        ]
        self.parent.helpText = """
This module allows the comparison of Absorbed Dose Rate volumes. 
It implements gamma comparison aswell as other metrics.
"""  # TODO: update with short description of the module
        self.parent.helpText += (
            self.getDefaultModuleDocumentationLink()
        )  # TODO: verify that the default URL is correct or change it to the actual documentation
        self.parent.acknowledgementText = """
This software was originally developed by Alex Vergara Gil (INSERM, France)
"""  # TODO: replace with organization, grant and thanks.

    def runTest(self, msec=100, **kwargs):
        """
        :param msec: delay to associate with :func:`ScriptedLoadableModuleTest.delayDisplay()`.
        """
        # test GammaTest
        # name of the test case class is expected to be <ModuleName>Test
        module = importlib.import_module(self.__module__)
        className = self.moduleName + "Test"
        try:
            TestCaseClass = getattr(module, className)
        except AttributeError:
            # Treat missing test case class as a failure; provide useful error message
            raise AssertionError(
                f"Test case class not found: {self.__module__}.{className} "
            )

        testCase = TestCaseClass()
        testCase.messageDelay = msec
        testCase.runTest(**kwargs)


#
# GammaWidget
#


class GammaWidget(ScriptedLoadableModuleWidget):
    """Uses ScriptedLoadableModuleWidget base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def setup(self):
        """
        Called when the user opens the module the first time and the widget is initialized.
        """
        super().setup()

        ParametersCollapsibleButton = ctk.ctkCollapsibleButton()
        ParametersCollapsibleButton.text = "Parameters"
        self.layout.addWidget(ParametersCollapsibleButton)
        ParameterLayout = qt.QHBoxLayout(ParametersCollapsibleButton)
        ParametersWidget = qt.QWidget()
        ParameterLayout.addWidget(ParametersWidget)
        ParametersFormLayout = qt.QFormLayout(ParametersWidget)

        # distanceThreshold=1, interpolationFraction=10, doseThreshold=1, lowerDoseCutoff=20
        self.distanceThreshold = qt.QDoubleSpinBox()
        self.distanceThreshold.setDecimals(1)
        self.distanceThreshold.setMinimum(0.1)
        self.distanceThreshold.setMaximum(50)
        self.distanceThreshold.value = 1
        self.distanceThreshold.setToolTip(
            "This is the distance (mm) to do gamma comparisons"
        )

        self.interpolationFactor = qt.QSpinBox()
        self.interpolationFactor.setMinimum(1)
        self.interpolationFactor.setMaximum(30)
        self.interpolationFactor.value = 10
        self.interpolationFactor.setToolTip(
            "This is the factor to divide the grid for interpolations"
        )

        self.doseThreshold = qt.QDoubleSpinBox()
        self.doseThreshold.setDecimals(1)
        self.doseThreshold.setMinimum(0.1)
        self.doseThreshold.setMaximum(10)
        self.doseThreshold.value = 1
        self.doseThreshold.setToolTip(
            "This is the dose threshold (%) to accept the gamma comparison"
        )

        self.lowerDoseCutoff = qt.QDoubleSpinBox()
        self.lowerDoseCutoff.setDecimals(1)
        self.lowerDoseCutoff.setMinimum(0.1)
        self.lowerDoseCutoff.setMaximum(20)
        self.lowerDoseCutoff.value = 1
        self.lowerDoseCutoff.setToolTip(
            "This is the dose cutoff (%) to reject dose values"
        )

        ParametersFormLayout.addRow("Distance Threshold (mm)", self.distanceThreshold)
        ParametersFormLayout.addRow("Interpolation Factor", self.interpolationFactor)
        ParametersFormLayout.addRow("Dose Threshold (%)", self.doseThreshold)
        ParametersFormLayout.addRow("Lower Dose Cutoff (%)", self.lowerDoseCutoff)

        InputCollapsibleButton = ctk.ctkCollapsibleButton()
        InputCollapsibleButton.text = "Input Volumes"
        self.layout.addWidget(InputCollapsibleButton)
        InputLayout = qt.QHBoxLayout(InputCollapsibleButton)
        InputWidget = qt.QWidget()
        InputLayout.addWidget(InputWidget)
        InputFormLayout = qt.QFormLayout(InputWidget)

        # input volume selector
        self.Image1 = slicer.qMRMLNodeComboBox()
        self.Image1.nodeTypes = [Attributes().vtkMRMLScalarVolumeNode]
        self.Image1.addAttribute(
            Attributes().vtkMRMLScalarVolumeNode,
            Attributes().modality,
            "RTDOSE",
        )
        self.Image1.selectNodeUponCreation = True
        self.Image1.addEnabled = False
        self.Image1.removeEnabled = False
        self.Image1.noneEnabled = False
        self.Image1.showHidden = False
        self.Image1.showChildNodeTypes = False
        self.Image1.setMRMLScene(slicer.mrmlScene)
        self.Image1.setToolTip("Select absorbed dose rate (ADR) Image 1 (reference).")
        InputFormLayout.addRow("ADR Image 1: ", self.Image1)

        # input volume selector
        self.Image2 = slicer.qMRMLNodeComboBox()
        self.Image2.nodeTypes = [Attributes().vtkMRMLScalarVolumeNode]
        self.Image2.addAttribute(
            Attributes().vtkMRMLScalarVolumeNode,
            Attributes().modality,
            "RTDOSE",
        )
        self.Image2.selectNodeUponCreation = True
        self.Image2.addEnabled = False
        self.Image2.removeEnabled = False
        self.Image2.noneEnabled = False
        self.Image2.showHidden = False
        self.Image2.showChildNodeTypes = False
        self.Image2.setMRMLScene(slicer.mrmlScene)
        self.Image2.setToolTip(
            "Select absorbed dose rate (ADR) Image 2 (experimental)."
        )
        InputFormLayout.addRow("ADR Image 2: ", self.Image2)

        self.ApplyButton = qt.QPushButton("Calculate Gamma")
        self.ApplyButton.toolTip = "Calculates Gamma image."
        InputFormLayout.addRow(self.ApplyButton)

        self.ApplyButton.clicked.connect(self.onApplyButton)

        # Add vertical spacer
        self.layout.addStretch(1)

        # Create a new parameterNode
        # This parameterNode stores all user choices in parameter values, node selections, etc.
        # so that when the scene is saved and reloaded, these settings are restored.
        self.logic = GammaLogic()

    def onApplyButton(self):
        """
        Run processing when user clicks "Apply" button.
        """
        distanceThreshold = self.distanceThreshold.value
        interpolationFraction = self.interpolationFactor.value
        doseThreshold = self.doseThreshold.value
        lowerDoseCutoff = self.lowerDoseCutoff.value
        self.logic.run(
            inputVolume1=self.Image1.currentNode(),
            inputVolume2=self.Image2.currentNode(),
            distanceThreshold=distanceThreshold,
            interpolationFraction=interpolationFraction,
            doseThreshold=doseThreshold,
            lowerDoseCutoff=lowerDoseCutoff,
        )


#
# GammaLogic
#


class GammaLogic(ScriptedLoadableModuleLogic):

    def skimage_metrics(self, im1, im2):
        # Metric 1: MSE (Mean Squared Error)
        mse = compare.mean_squared_error(im1, im2)
        # Metric 1.1: NRMSE (Normalized Root Mean Squared Error)
        nrmse = compare.normalized_root_mse(im1, im2, normalization="euclidean")
        # Metric 2: SSIM (Structural Similarity Image Matrix)
        ssim = compare.structural_similarity(im1, im2, data_range=im2.max() - im2.min())
        # Metric 3: Peak Signal-to-Noise ratio
        if mse > 0:
            psnr = compare.peak_signal_noise_ratio(
                im1, im2, data_range=im2.max() - im2.min()
            )
        else:
            psnr = -1

        return mse, nrmse, ssim, psnr

    def GammaIndex(
        self,
        im1,
        im2,
        imageThreshold,
        distanceThreshold,
        interpolationFraction,
        doseThreshold,
        lowerDoseCutoff,
    ):
        shape = np.shape(im1)
        xgrid = np.linspace(1, shape[0] * imageThreshold[0], shape[0])
        ygrid = np.linspace(1, shape[1] * imageThreshold[1], shape[1])
        zgrid = np.linspace(1, shape[2] * imageThreshold[2], shape[2])
        coords1 = (xgrid, ygrid, zgrid)
        shape = np.shape(im2)
        xgrid = np.linspace(1, shape[0] * imageThreshold[0], shape[0])
        ygrid = np.linspace(1, shape[1] * imageThreshold[1], shape[1])
        zgrid = np.linspace(1, shape[2] * imageThreshold[2], shape[2])
        coords2 = (xgrid, ygrid, zgrid)
        # Gamma index parameters
        gammaOptions = {
            "dose_percent_threshold": doseThreshold,
            "distance_mm_threshold": distanceThreshold,
            "lower_percent_dose_cutoff": lowerDoseCutoff,
            # Should be 10 or more for more accurate results
            "interp_fraction": interpolationFraction,
            "max_gamma": 2,
            "random_subset": None,
            "local_gamma": False,
            "ram_available": 2**29,  # 1/2 GB
        }

        gammaMatrix = gamma(coords1, im1, coords2, im2, **gammaOptions)
        validGammaMatrix = np.ma.masked_invalid(gammaMatrix)
        validGammaMatrix = validGammaMatrix[~validGammaMatrix.mask]
        size = len(validGammaMatrix)
        if size < 1:
            size = 1
        gammaIndex = np.sum(validGammaMatrix <= 1) / size
        return gammaIndex, gammaMatrix

    def run(
        self,
        inputVolume1,
        inputVolume2,
        distanceThreshold=1,
        interpolationFraction=10,
        doseThreshold=1,
        lowerDoseCutoff=20,
    ):
        im1 = slicer.util.arrayFromVolume(inputVolume1).astype(float)
        im2 = slicer.util.arrayFromVolume(inputVolume2).astype(float)
        GammaImage = vtkmrmlutils.cloneNode(inputVolume1, "Gamma Image")
        imageThreshold = inputVolume1.GetSpacing()
        GammaIndex, GammaMatrix = self.GammaIndex(
            im1,
            im2,
            imageThreshold,
            distanceThreshold,
            interpolationFraction,
            doseThreshold,
            lowerDoseCutoff,
        )
        slicer.util.updateVolumeFromArray(GammaImage, GammaMatrix)
        mse_const, nrmse_const, ssim_const, psnr_const = self.skimage_metrics(im1, im2)
        TableValues = {
            "Gamma Index": GammaIndex,
            "Mean Square Error (MSE)": mse_const,
            "Normalized Root MSE (%)": nrmse_const * 100,
            "Structural Similarity Index": ssim_const,
            "Peak Signal to Noise Ratio": psnr_const,
        }

        if not utils.checkTesting():

            displayNode = GammaImage.GetScalarVolumeDisplayNode()

            if displayNode is not None:
                colorID = slicer.util.getFirstNodeByName("DivergingBlueRed").GetID()
                displayNode.SetAndObserveColorNodeID(colorID)
                displayNode.AutoWindowLevelOff()
                displayNode.AutoWindowLevelOn()

            TableNodes = slicer.util.getNodesByClass(Attributes().vtkMRMLTableNode)
            name = "T:TABL Comparison"
            for node in TableNodes:
                if name == node.GetName():  # Table exists, erase it
                    slicer.mrmlScene.RemoveNode(node)

            # prepare clean table
            resultsTableNode = slicer.mrmlScene.AddNewNodeByClass(Attributes().vtkMRMLTableNode)
            resultsTableNode.RemoveAllColumns()
            shNode = slicer.vtkMRMLSubjectHierarchyNode.GetSubjectHierarchyNode(
                slicer.mrmlScene
            )
            nodeID = shNode.GetItemByDataNode(inputVolume1)
            folderID = shNode.GetItemParent(nodeID)
            shNode.CreateItem(folderID, resultsTableNode)
            resultsTableNode.SetName(name)
            table = resultsTableNode.GetTable()

            segmentColumnValue = vtk.vtkStringArray()
            segmentColumnValue.SetName("Metric")
            table.AddColumn(segmentColumnValue)

            segmentColumnValue = vtk.vtkStringArray()
            segmentColumnValue.SetName("Value")
            table.AddColumn(segmentColumnValue)

            table.SetNumberOfRows(len(TableValues.keys()))

            for i, (metric, value) in enumerate(TableValues.items()):
                table.SetValue(i, 0, metric)
                table.SetValue(i, 1, value)

            resultsTableNode.Modified()
            vtkmrmlutils.showTable(resultsTableNode)

            backgroundID = inputVolume1.GetID()
            foregroundID = GammaImage.GetID()

            slicer.util.setSliceViewerLayers(
                background=str(backgroundID),
                foreground=str(foregroundID),
                foregroundOpacity=0.5,
            )

        return TableValues  # for testing


#
# GammaTest
#


class GammaTest(ScriptedLoadableModuleTest):
    """
    This is the test case for your scripted module.
    Uses ScriptedLoadableModuleTest base class, available at:
    https://github.com/Slicer/Slicer/blob/master/Base/Python/slicer/ScriptedLoadableModule.py
    """

    def setUp(self):
        """Do whatever is needed to reset the state - typically a scene clear will be enough."""
        slicer.mrmlScene.Clear(0)
        slicer.app.processEvents()

    def runTest(self):
        """Run as few or as many tests as needed here."""
        self.setUp()
        self.test_Gamma1()

    def test_Gamma1(self):
        """Ideally you should have several levels of tests.  At the lowest level
        tests should exercise the functionality of the logic with different inputs
        (both valid and invalid).  At higher levels your tests should emulate the
        way the user would interact with your code and confirm that it still works
        the way you intended.
        One of the most important features of the tests is that it should alert other
        developers when their changes will have an impact on the behavior of your
        module.  For example, if a developer removes a feature that you depend on,
        your test should break so they know that the feature is needed.
        """

        self.delayDisplay("Starting the test")
        slicer.app.aboutToQuit.connect(self.setUp)

        # Get/create input data

        import SampleData

        inputVolume = SampleData.downloadFromURL(
            nodeNames="MRHead",
            fileNames="MR-Head.nrrd",
            uris="https://github.com/Slicer/SlicerTestingData/releases/download/MD5/39b01631b7b38232a220007230624c8e",
            checksums="MD5:39b01631b7b38232a220007230624c8e",
        )[0]
        self.delayDisplay("Finished with download and loading")

        inputScalarRange = inputVolume.GetImageData().GetScalarRange()
        self.assertEqual(inputScalarRange[0], 0)
        self.assertEqual(inputScalarRange[1], 279)

        # Test the module logic
        logic = GammaLogic()

        # Test algorithm
        values = logic.run(
            inputVolume,
            inputVolume,
            distanceThreshold=1,
            interpolationFraction=10,
            doseThreshold=1,
            lowerDoseCutoff=1,
        )
        logging.info("%s", values)
        OutputNode = slicer.util.getFirstNodeByClassByName(
            Attributes().vtkMRMLScalarVolumeNode, "Gamma Image"
        )

        self.assertEqual(values["Gamma Index"], 1)
        self.assertEqual(values["Normalized Root MSE (%)"], 0)
        self.assertEqual(values["Structural Similarity Index"], 1)
        self.assertTrue(vtkmrmlutils.hasImageData(OutputNode))

        self.delayDisplay("Test passed")
