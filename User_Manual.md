# Table of Contents
- [Table of Contents](#table-of-contents)
  - [Presentation](#presentation)
  - [Installation](#installation)
    - [Testing the installation](#testing-the-installation)
  - [My first dosimetry with OpenDose3D](#my-first-dosimetry-with-opendose3d)
    - [Initialization](#initialization)
    - [Registration](#registration)
    - [Absorbed dose rate calculation](#absorbed-dose-rate-calculation)
    - [Segmentation](#segmentation)
    - [Time integration](#time-integration)
  - [Understanding intermediary data](#understanding-intermediary-data)
  - [Workflows](#workflows)
    - [1. Activity (ACT)](#1-activity-act)
      - [Automatic ACT](#automatic-act)
    - [2. Absorbed dose rate (ADR)](#2-absorbed-dose-rate-adr)
      - [Automatic ADR](#automatic-adr)
  - [Description of OpenDose3D panels](#description-of-opendose3d-panels)
    - [1. Parameters](#1-parameters)
    - [2. Registration](#2-registration)
    - [3. Absorbed Dose Rate Calculation](#3-absorbed-dose-rate-calculation)
    - [4. Segmentation](#4-segmentation)
    - [5. Time integration](#5-time-integration)
      - [NUKFIT Models](#nukfit-models)
      - [LMFIT Models](#lmfit-models)
    - [6. Utilities](#6-utilities)
    - [7. Settings](#7-settings)
    - [8. Data Probe](#8-data-probe)
  - [Calibration module](#calibration-module)
    - [Sensitivity](#sensitivity)
    - [Recovery coefficient](#recovery-coefficient)
  - [Voxel S Values generation](#voxel-s-values-generation)
    - [1. DPKgenerator.py](#1-dpkgeneratorpy)
    - [2. DPKprocess.py](#2-dpkprocesspy)
    - [3. DPK.py](#3-dpkpy)
    - [4. DVK.py](#4-dvkpy)
  - [Q\&A:](#qa)
  - [Roadmap:](#roadmap)
  - [References](#references)

## Presentation
  
  This module allows performing image-based dosimetry of Molecular RadioTherapy (MRT). It considers reconstructed SPECT/CT or PET/CT images, with one or several time points. Different Clinical Dosimetry Workflows (CDW) are implemented, with several utilities to test the dosimetry process.
  The **_OpenDose3D_** module can be used in conjunction with its companion **_Calibration Module_**, but this is not mandatory.

  DISCLAIMER: This plugin is for academic purposes only and is not recommended for use in a clinical environment.


## Installation
  
  The installation requires access to internet, and permissions to install software on your computer.
  You should install the latest stable release of 3D Slicer (5.6.2 in Nov 2024). 
  Launch 3D Slicer. Open the _Extensions manager_

  ![install1](images/mbInstall1.png)

  Go to the _Install Extensions_ tab.
  - In the _Radiotherapy_ Category, select **_OpenDose3D_**
  - In the _Segmentation_ Category, select **_TotalSegmentator_**
  
  These Extensions have dependencies:
  - **_OpenDose3D_** will automatically install **_SlicerElastix_** 
  - **_TotalSegmentator_** will automatically install **_PyTorch_** 
  
  There are additional modules that can be usefull. Just browse available Extensions and choose...
  
  We recommend at least this one:
  - In the _Radiotherapy_ Category, select **_SlicerRT_**
  
  It will allow defining isodoses and absorbed dose volume histograms on your absorbed dose maps... 

  ![install2](images/mbInstall2.png)

  In the _Manage Extensions_ tab, check that you have installed all required extensions. 
  Restart 3D Slicer (bottom right button).

  ![install3](images/mbInstall3.png)

  The **_OpenDose3D_** module should preferably be installed automatically from 3D Slicer _Extensions manager_.  Yet, it can also be downloaded or cloned  from [https://gitlab.com/opendose/opendose3d](https://gitlab.com/opendose/opendose3d). This should be only be done by experienced users. 


### Testing the installation

  Once 3D Slicer is restarted, the **_OpenDose3D_** module should be available from *All Modules* → *Radiotherapy* → *OpenDose3D*, together with the **_OpenDose Calibration_** module (for calculating and storing various calibration factors).
  - In 3D Slicer Edit/Application/Modules Settings menu, it is possible to put **_OpenDose3D_** in the favourite module toolbar.

  ![test1](images/mbTest1.png)

  - In the main **_OpenDose3D_** panel, notice the version number of **_OpenDose3D_**. It is important to keep track of the version to ensure traceability.
  - At the bottom of the **_OpenDose3D_** panel, click on _Utilities_, and then _Run Test_. 

  ![test2](images/mbTest2.png)

  This will test successively the activity workflow and absorbed dose rate workflow, on a test dataset thereby testing that the installation of **_OpenDose3D_** is operational.
  At the end of the process, you should have a final window that looks like below (mind that the displayed results are not clinically relevant).

  ![test3](images/mbTest3.png)


## My first dosimetry with OpenDose3D
  
  The first step is to **load the clinical dataset**.
  We will use a patient dataset generated for the [SNMMI dosimetry challenge](https://sites.snmmi.org/Therapy/SNMMI-THERAPY/Dosimetry_Challenge.aspx).
  The University of Michigan is proposing several clinical datasets that can be downloaded freely and used to practice clinical dosimetry.
  For the following example, we will use Patient 4 from the [Lu-177 DOTATATE Anonymized Patient Datasets: Multi-Time Point Lu-177 SPECT/CT Scans ](https://deepblue.lib.umich.edu/data/concern/data_sets/dn39x173n?locale=en).

  Once downloaded, open 3D Slicer. 
  - go to *File* → *DICOM* (or use the button ![dcm](images/dcm.png) )
  ![patSelect](images/mbPatSelect.png)
  - mark the *Advanced* modality
  - click *Examine* then *Load*.
  Your dataset is now loaded...

   ![patInit](images/mbPatInit.png)

  Optional: You can play with 3D Slicer display options if you wish, then:

  Open the **_OpenDose3D_** module.

  ### Initialization

  The first panel should look like this:
   ![dosi1](images/mbDosi1.png)

  Note for the first utilisation, we do not have a clinical centre and default calibration factors. This may come after (see the manual for the **_OpenDose Calibration_** module. Yet, the information on the radionuclide (177Lu) is correctly displayed.
  Leave the default workflow (_Absorbed Dose Rate_) and click on _Initialization_.
  The software will read DICOM headers and try to find Injected activity, Injection time, etc.

   ![dosi2](images/mbDosi2.png)

  For that dataset, the software will correctly identify "quantitative data" and change the calibration factor accordingly.
  The Injection time will also be found, but not the Injected activity. A warning indicating the issue will be displayed. Input the administered activity (7210 MBq in the example) and click again on _Initialization_.
  The initialization process should then run smoothly:
   ![dosi3](images/mbDosi3.png)
  Initialization consists in:
  - resampling CT at the spatial sampling of SPECT
  - generating and activity-indexed SPECT image
  - generating a density-indexed CT image

  At the end of the process, it is possible to proceed to the 

  ### Registration
  
  ![dosi4](images/mbDosi4.png)

  For this example, select _Rigid_, and the 24h CT image.
  The **_Elastix_** module of 3D Slicer is performing the rigid registration on the whole field of view. 
  It is highly recommended to check the registration (by clicking the _Check Transforms_ button ), but for the sake of the example, let's assume it's OK, and move to the 

  ### Absorbed dose rate calculation

  ![dosi5](images/mbDosi5.png)

  Select _FFT Convolution_ and _Density correction_ and click on _Calculate Absorbed Dose Rates_. 
  The first calculation may take a while since the kernel needs to be built before the first run.

  Once done, it is possible to move to the segmentation step.

  ### Segmentation

  ![dosi6](images/mbDosi6.png)

  The segmentation can be performed on the 3D Slicer segmentation module, by clicking on _VOI segmentation_. For the example, we will select _OAR auto segmentation_. This will call **_TotalSegmentator_**, and a preset number of OAR (Organs At Risk) will be segmented on the reference CT selected during the **Registration** step (24h in the example):

  ![dosi6](images/mbDosi6.png)

  The Segmentation by default for 177Lu datasets considers Liver, Right and Left Kidneys, Spleen and L2, L3 L4 vertebrae. This can be changed in a settings file.

  When the segmentation is completed on the _reference image_, it is time to _Propagate_ the segmentation to other images, using the transformations computed during the **Registration** step. This will allow moving the  

  ### Time integration
  
  ![dosi7](images/mbDosi7.png)

  Clicking on _Absorbed Dose Rate Tables_ will generate the ADR-time tables, fit and display the curves on the top right panel. The fitting algorithm (by default NUKFIT) will generate the best fit, but it is possible to change the fit manually.
  ![dosi8](images/mbDosi8.png)

  At last, clicking on _Integrate Absorbed Dose Rates_ will generate the final results:
  ![dosi9](images/mbDosi9.png)

_**Congratulations, you have now completed your first clinical dosimetry on a patient dataset!**_

## Understanding intermediary data 
  **_OpenDose3D_** creates several intermediary files during the processing. We'll go through these, step by step, using the previous example (My first dosimetry with **_OpenDose3D_**). Identifying the files that are created (or not) is a convenient way to follow the CDW, and make sure all files are correctly created. It also helps in assessing _where_ things went wrong - when they do...

  When uploaded, the original dataset looks like that:
  ![mbData1](images/mbData1.png)
  There are 4 SPECT and 4 CT. Passing the mouse on the name of each file displays volume information dimension/spacing.


  During the Initialization step, 2 types of operations are performed: original files are renamed and reordered, and extra files are created:
  Every time point has a specific folder (4HR 24HR, 108HR and 120HR in our example). Inside each folder, the original images are copied and renamed, with 4-letter formatting:
  - **_J0:CTCT CT 4HR_** for the original CT at 4hr, and
  - **_J0:ACSC PET 4HR_** for the original SPECT at 4hr. 

  Mind that these are _quantified_ SPECT images, labelled as "PET", even though these really are SPECT images!
  "ACSC" comes from "attenuation corrected scatter corrected" images.
  Then 3 extra files are created:
  - **_J0:CTRS CT 4HR_** is the resampled CT (sampling is now that of SPECT images),
  - **_J0:ACTM PET 4HR_** is an activity-indexed SPECT, after integration of the calibration factor, and 
  - **_J0:DENM CT 4HR_** is a density-indexed CT, after conversion of HU units in density.
  These 2 datasets will be used for next steps.
  ![mbData2](images/mbData2.png)


  After registration, "transforms" are created at each time point (except the _reference_ time point):
  **_J0:TRNF RigidTransform 4HR_** 

  Here we can see that the reference time point is 24h, since no "transform" is created. Also the name of the transform file indicates which transform was performed (Rigid here):
  ![mbData3](images/mbData3.png)


  During the absorbed dose rate calculation, an absorbed dose rate map is created at each time point:
  - **_J0:ADRM FFT Convolution 4HR_** 

  The type of file (ADRM: absorbed dose rate map), type of calculation (FFT Convolution) and time (4HR) help tracing what was done:
  ![mbData4](images/mbData4.png)


  The first phase of the segmentation (automatic here, but this would be equivalent for manual or semi-automatic segmentation of tumours for example) is generating volumes of interest (VOI) in the _reference_ image, i.e. that used for the registration (24h in our example). The VOIs (spleen, right kidney, etc.) are in a file: 
  - **_J1:SEGM PET 24HR_** 

  ![mbData5](images/mbData5.png)


  After propagation, a segmentation (SEGM) file is created at each time point:
  ![mbData6](images/mbData6.png)


  The last steps generate absorbed dose rate tables and densities at each time point:
  - **_J0:TABL FFT DoseRateCONV 4HR_** 
  - **_J0:TABL Densities 4HR_**

  In addition, in the results folder, tables (TABL) and plots (CHRT) are generated for each VOI, and the final table of results is generated:
  - **_T:TABL FFT Convolution integrated_** 

  These files are all part of the study and can be saved at the end for further retrieval and analysis:
  ![mbData7](images/mbData7.png)

  In order for the module to work properly, all the images must be included in the same *study* folder of the same *subject* folder. Check if this condition is fulfilled looking at *Data* -> *Subject hierarchy* -> *Node*; otherwise move manually all the images inside a single *subject* and *study*.

## Workflows
  The Parameters panel allows selecting different workflows. Below is a quick description of the sequence of operations for each CDW, and the impact on **_OpenDose3D_**  interface and order of the differnt panels.

  ### 1. Activity (ACT)

   This workflow was considered mostly as it allows assessing pharmacokinetics. The first steps (Parameter and Registration) are the same than for the ADR workflow. Then the segmentation step is performed (no ADR calculation), and time-activity curves are calculated, fitted and integrated to provide the cumulated activity (time-integrated activity) in each VOI. The absorbed dose is calculated for each VOI, assuming local energy deposition for electrons and beta particles. This assumption may be valid for $^{177}Lu$ but should be taken with care for radionuclides with a high gamma emission component ($^{131}I$).


   #### Automatic ACT

   When this option is activated, a simplified ACT workflow is implemented.

   The main features are:
   - Elastic registration based on the first time point
   - Automatic segmentation of organs at risk with TotalSegmentator
   - Fitting based on the NUKFIT algorithm

  ### 2. Absorbed dose rate (ADR)

   This is the default workflow in **_OpenDose3D_**. 
   The ADR workflow considers a reference time point (image) used for registration. ADR is computed voxel-based at each time point. Segmentation is performed on the reference time point (image), and average ADR is computed for each VOI. Fit and integration of ADR-time curves is performed, and mean absorbed doses are calculated for each VOI.

   ####  Automatic ADR

   When this option is activated, a simplified ADR workflow is implemented. The underlying hypothesis are that the absorbed dose rate is less sensitive to registration than the activity. Since a segmentation at each time point is becoming more and more feasible thanks to artificial intelligence algorithms (like TotalSegmentator), computing the ADR for VOIs defined at each time point is appealing.

   The main features are:
   - No registration is performed
   - Absorbed dose rate (voxel-based) calculation using Local Energy Deposition algorithm with density correction.
   - Automatic segmentation of organs at risk with TotalSegmentator at each time point
   - Fitting based on the NUKFIT algorithm
   - The mass displayed in the results panel corresponds to the VOIs in the _reference image_.

**NB:** The results obtained with different CDW may differ. Studies are ongoing to assess the impact of CDW selection.


## Description of OpenDose3D panels

  The structure of **_OpenDose3D_** is modular. This reflects the different steps that need to be executed to complete a dosimetry study.
  The arrangement of the steps depends on the clinical dosimetry workflow selected. This will be reflected in the different panels present in **_OpenDose3D_**.
  Normally, the sequence of operation is simple, as one panel can only be activated when the previous step is completed.

  Mind that the panel sequence is impacted by the dosimetry workflow selected: **_OpenDose3D_** interface will not be exactly the same for ACT and ADR workflows. 

  **What follows describes the default CDW (ADR)**. More information on ACT steps and interface is given in the [Workflow](#workflows) section. 

  ### 1. Parameters  
     
   This first panel contains important information (**_OpenDose3D_** version for example), and allows entering global parameter needed for the study.

   The _Clinical centre_ and _Calibration date_ are optional.
   The **_OpenDose Calibration_** module allows generating sensitivity and recovery coefficient for each centre. This means that, when informed, the required calibration factors will be automatically attached to the study. Otherwise, the user should enter the _Camera Sensitivity_ and its unit.

   Some information may be directly extracted from the DICOM headers:
   - _Radionuclide_,
   - _Injected activity_,
   - _Injection time_.

   In case the information is not present in the header, a warning message will appear when launching the _Initialization_ process, to ask the user to inform these variables.

   The user will then choose the clinical dosimetry workflow (CDW).
   There are 2 broad CDW in **_OpenDose3D_**:
   - _Activity_ 
   - _Absorbed Dose Rate_

   For each [Workflow](#workflows), the activation of _auto_ allows launching a series of operations that will generate a fast/automatic approximation of the absorbed doses to the organs at risk (OAR). 
   Clicking on _Initialization_ then starts **_OpenDose3D_**.
   The _Initialization_ consists in:
   - resampling CT (CTRS) at the spatial sampling of SPECT
   - generating an activity-map (ACTM) from SPECT image
   - generating a density-map (DENM) form resampling CT image

  ### 2. Registration

   This step allows selecting a _reference image_ defined by the user, and then spatially register the image series on that reference. 
   The user should  select _Rigid_ or _Elastic_ transformations, then select the _Reference Image_ and _Execute_.

   This module is a pre-existing module of 3D Slicer, and the user will find documentation on image registration on 3D Slicer on the net ([elastix](https://elastix.dev/index.php)).
   It uses the already available registration methods in slicer (rigid + affine).
      * The user is expected to review the results in the transformation module, correct manually important deviations, and repeat the registration process.
      * Each registration is applied to all volumes (field of view) of a given time point.
      * Repeating the execution after manually adjusting for broad deviations refine the registration.

  ### 3. Absorbed Dose Rate Calculation

   This step is activated when the user selects the ADR workflow. It allows selecting the ADR calculation algorithm, between:

   - Local Energy Deposition, 
   - FFT convolution,
   - Monte Carlo.

   By default the first 2 options are performed in water. The density correction option allows using the HU-derived voxel density in the calculation. For the FFT convolution, the correction is just a scaling of the energy absorbed in a voxel by the density ratio.

   Monte Carlo modelling is using both activity and density maps. Selecting the option generates the input file for GATE ([Sarrut et al. 2014](https://aapm.onlinelibrary.wiley.com/doi/full/10.1118/1.4871617)), that must be installed and run independently by the user. At the end of them process, OpenDose3D can read the output of GATE.

  ### 4. Segmentation
     
   This step allows defining or importing user defined volumes of interest (VOI).
   - _Select a Segmentation_ allows importing an already defined/save segmentation.
   - _VOI segmentation_ opens the 3D Slicer module for defining VOIs. Several tutorials are available on internet to learn using the (not so simple) module (search _Slicer segmentation tutorial_ for example). 
   - _OAR auto segmentation_ launches TotalSegmentator. This 3D Slicer external module will generate a list of VOIs based on predefined settings.

   VOIs are defined on the same _reference image_ than that defined at the **registration** step.
   At the end of the segmentation process, the segmentation should be propagated to all images, by clicking on _Propagate Segmentation_.

  ### 5. Time integration

   This major step has a very simple interface. Clicking on  _Absorbed Dose Rate Tables_ will first extract the absorbed dose rates in each VOI, at each time point. The time-ADR curves will then be fitted and integrated. The fitting module offers two options: [LMFIT](#lmfit-models) and [NUKFIT](#nukfit-models) models. By default, this process employs a NUKFIT derived algorithm with the "auto-fit" feature, which uses the corrected Akaike Information Criterion (AICc) to select the best-fitting model from those functions that satisfy the goodness-of-fit criteria. Yet, it is possible for the user to select other equations to fit independently each VOI, and to force the first point to zero when relevant. This modifies the plot displayed in the top right window.

   At the end, the user should  click on _Integrate Absorbed Dose Rate_ to generate the final results.
   In the final table, for each VOI the following information is displayed:
   - _VOI name_
   - _VOI mass (kg)_: shows the masses of the VOIs in the _reference image_
   - _Absorbed dose (Gy)_
   - _Standard Deviation (Gy)_: Std of the area under the curve (AUC) during the fitting procedure (Std_AUC). If no value is provided ("--"), it indicates that the Std_AUC exceeds the absorbed dose value, suggesting that the solution does not meet the quality criteria.
   - _Fit function and parameters_: The algorithm used for ADR-time curve fitting (by default NUKFIT) is also displayed.
    
  #### NUKFIT Models

  NUKFIT offers six Sum of Exponentials Functions (SOEF) for fitting time Activity/ADR values. These functions are based on recommendations from the literature ([Kletting et al. 2013](https://onlinelibrary.wiley.com/doi/abs/10.1118/1.4820367); [Hardiansyah et al. 2024](https://jnm.snmjournals.org/content/early/2024/02/29/jnumed.123.266268)):

  1. $f_2(t) = A_1 e^{-(\lambda_1 + \lambda_{phys})t}$

  2. $f_3(t) = A_1 e^{-(\lambda_1 + \lambda_{phys})t} - A_1 e^{-(\lambda_2 + \lambda_{phys})t}$ 

  3. $f_{3a}(t) = A_1 \alpha e^{-(\lambda_1 + \lambda_{phys})t} + A_1 (1 - \alpha) e^{-(\lambda_{phys})t}$

  4. $f_{4a}(t) = A_1 e^{-(\lambda_1 + \lambda_{phys})t} + A_2 e^{-(\lambda_2 + \lambda_{phys})t}$

  5. $f_{4b}(t) = A_1 e^{-(\lambda_1 + \lambda_{phys})t} - A_2 e^{-(\lambda_2 + \lambda_{phys})t} - (A_1 - A_2)e^{-(\lambda_{bc} + \lambda_{phys})t}$ 

  6. $f_{4c}(t) = A_1 e^{-(\lambda_1 + \lambda_{phys})t} + A_2 e^{-(\lambda_2 + \lambda_{phys})t} - (A_1 + A_2)e^{-(\lambda_{bc} + \lambda_{phys})t}$ 

  Where:
  -  $f_i(t)$: Fit function, with $i$ being the total number of estimated parameters.
  -  $A_i$ ($i = 1$ or $2$): Coefficients of the respective exponential terms.
  -  $\lambda_i$ ($i = 1$ or $2$): Biologic uptake or clearance rates of the radiopharmaceutical.
  -  $\alpha$: Exponential fraction, with values between 0 and 1.
  -  $\lambda_{bc}$: Blood circulation rate, set to $\lambda_{bc} = \frac{\ln(2)}{1\, \text{min}}$.
  -  $\lambda_{phys}$: Physical decay constant of the radionuclide.

  Equation (1) represents a mono-exponential function, which, along with Equations (3) and (4), describes the clearance-only model. In contrast, Equations (2), (5), and (6) account for both uptake and clearance, with the function value being zero at time zero.

 #### LMFIT Models

 LMFIT provides three SOEF for fitting the time Activity/ADR values. The provided SOEF are:

 1. _mono-exponential_: $A_1 e^{- \ln(2) \frac{t}{T_1}}$

 2. _bi-exponential_: $A_1 \left(e^{- \ln(2) \frac{t}{T_1}} - e^{- \ln(2) \frac{t}{T_2}}\right)$

 3. _tri-exponential_: $A_1 e^{-\ln(2) \frac{t}{T_1}} - A_2 e^{-\ln(2) \frac{t}{T_2}} - (A_1 - A_2) e^{-\ln(2) \frac{t}{T_{\text{phys}}}}$

 Where:

 * $A_i$ ​($i = 1$ or $2$): Coefficients of the respective exponential terms.
 * $T_i$ ($i = 1$ or $2$)​: Effective half-life ($T_{\text{biol}} + T_{\text{phys}}$) of uptake or clearance component of the radiopharmaceutical. 
 * $T_{\text{phys}}​$: Physical half-life

 _mono-exponential_ function, describes only a clearance model, whereas _bi-exponential_ and _tri-exponential_ account for both uptake and clearance, with the function value being zero at time zero.

  ### 6. Utilities 
   - Export buttons (_Export XML:ACTM_ and _Export XML:ADRM_) allow exporting variables.
   - _Decompress Database_ unzip the Dose Voxel Kernel (DVK) (also known as voxel S values (VSV)) data from [OpenDoseDVKdata](https://gitlab.com/opendose/opendosedvkdata/-/tree/master/DVK?ref_type=heads).
   - _Reload_ reset the variables of OpenDose3D and reload the current status of the module.
   - _Run Test_ downloads a test dataset, and performs automatic workflows (ACT and ADR), to ensure that the installation is correct and operational.

  ### 7. Settings

   These settings impact **_OpenDose3D_** processing or display.
   - _Show Legend in graphs_ and _In y axis_ are self explanatory.
   - _Bone marrow extraction_ will allow extracting bone marrow from the segmented vertebrae, based on thresholding on Hounsfield Units (_min HU_ and _max HU_).  
   - _Select Kernel_ allows selecting different Kernel datasets for the convolution algorithm. By default [OpenDoseDVKdata](https://gitlab.com/opendose/opendosedvkdata/-/tree/master/DVK?ref_type=heads) are used, but user-provided kernels can be imported if need be.
   - _Set logging level_ allows selecting the verbosity of process reporting.

  ### 8. Data Probe

   This panel is a 3D Slicer panel that allows interrogating the content of images displayed.

## Calibration module
  A calibration factor (sensitivity) is needed to convert the reconstructed images indexed in counts or counts per second into activity-indexed images ([Dewaraja et al. 2024](https://www.iaea.org/publications/15002/dosimetry-for-radiopharmaceutical-therapy)). This may not be the case when “quantitative reconstruction” is implemented in the reconstruction process, an option proposed now by most manufacturers. 

  In clinical dosimetry software the calibration factor can be inserted at various CDW stages, either at the start of the procedure or during the absorbed dose calculation. In addition, as the calibration factor can be expressed in different ways, this is a source of human error when entering this variable. This was one of the motivations for developing a specific calibration module. Ideally, this calibration module should 1) identify and keep track of the relevant datasets used for the calibration procedure, and 2) store and pass the relevant variables to the dosimetry module without operator intervention.

  The calibration module is not limited to sensitivity (counts per second/MBq) and may also consider other calibration procedures, for example recovery coefficients, dead-time correction, or CT density calibration.

  To date, only sensitivity and recovery coefficient are considered:

  ### Sensitivity
    
  The sensitivity calibration factor requires SPECT/CT images of a cylindrical or Jaszczak phantom without inserts, filled with a known homogeneous activity (Figure below). Radionuclide name, acquisition date/time, acquisition duration, initial and residual activity in the syringe, and date and time of these measurements are the required parameters.
  Automated segmentation is then performed to determine a sensitivity factor with three approaches: (1) total field of view, (2) the exact SPECT-based source volume, or (3) extended source volume, as recommended by [Tran-Gia et al. 2021](https://doi.org/10.1186/s40658-021-00397-0). For approaches (2) and (3), the phantom volume and diameter are required, respectively. The resulting sensitivity factors are presented. Users can select and store their preferred option for the clinical center. For each center, the calibrated radionuclide, calibration date and time, and used parameters are stored in a database file called SPECTSensitivity.json. This database allows the precise tracking of calibration conditions.

  ![Sensitivity](images/Sensitivity.png)
  The figure illustrates the sensitivity calibration factor assessed using SPECT/CT images of a Jaszczak phantom filled with a known activity. The module implements three distinct approaches to calculate sensitivity.
    
  ### Recovery coefficient
    
  The module requires SPECT/CT images of a NEMA phantom that contains six spheres as input (Figure below) . To calculate the recovery coefficients, the same parameters needed for sensitivity calibration are required and also additional parameters, such as camera sensitivity and vial volume. If the sensitivity calibration data have already been saved, the module will automatically use this information. The vial volume is used to determine the activity concentration used to fill the spheres. The volume of the six spheres is then entered, and a point should be placed approximately at the center of each sphere in the image. These points are used as seeds for the initial segmentation iteration, which can be performed on the CT or/and SPECT images. A logistic function is used to fit the recovery coefficient values. These values can be used, but are not currently automatically transmitted to OD3D. Discussions are currently focused on how to integrate automatically these values in the CDW.

  ![RecoveryCoefficient](images/RecoveryCoefficient.png)
  The figure shows the recovery coefficients obtained from SPECT/CT images of a NEMA phantom containing six spheres. The accompanying graph shows the results obtained by fitting the calculated values (displayed in the table) using a logistic function. Calculations can be performed from CT (blue curve) and SPECT (red curve) image segmentations.


## Voxel S Values generation

  To generate Voxel S Values (VSV), four Python scripts must be executed sequentially. Each script depends on the output of the previous one, thus it is essential to wait for each script to complete before proceeding to the next step. The workflow is as follows:

  1. **`DPKgenerator.py`**: Generate a GATE macro file for Monte Carlo simulation.

  2. **`DPKprocess.py`**: Processes the outputs from the GATE simulation.

  3. **`DPK.py`**: Calculates effective Dose Point Kernel (DPK) values and interpolates the results to produce a refined dataset.

  4. **`DVK.py`**: Utilizes the pre-calculated DPK data to compute 3D absorbed dose distributions and store them in voxelized formats (VSV) ([Franquiz, Chigurupati, and Kandagatla 2003](https://onlinelibrary.wiley.com/doi/abs/10.1118/1.1573204)).

  ### 1. DPKgenerator.py

  1. **Input Parameters**:
     - The script expects three command-line arguments:
       - Atomic Symbol (e.g., Lu for Lutetium).
       - Atomic Number (Z).
       - Atomic Mass (A).
     - Example usage:  
       ```bash
       python3 1-DPKgenerator.py Lu 71 177
       ```

  2. **Geometry and Materials**:
     - Defines a world material (water by default).
     - Defines concentric spherical shells around a central point with logarithmically spaced radii.

  3. **Physics**:
     - Uses the emlivermore physics list for detailed electromagnetic interactions.

  4. **Source Definition**:
     - Defines a radioactive ion source at the central point of the geometry.

  5. **Simulation Initialization**:
     - Sets the total number of primary particles for the simulation (10⁸ by default).

  6. **Output**:
     - Creates a directory named `output-{RadioNuclide}` (e.g., `output-Lu-177`) in the user's `Documents/DVK` folder.
     - Creates a simulation statistics file `{RadioNuclide}-stat.txt` every 1 hour.
     - Scores the energy deposition (in MeV) and uncertainty for each spherical shell.

  ### 2. DPKprocess.py

   1. **Input**:
      - Uses the same directory and radioisotope parameters as defined in `1-DPKgenerator.py`.
      - Looks for simulation results in the `output-{RadioNuclide}` folder (e.g., `output-Lu-177`).

   2. **Number of Primary Particles Extraction**:
      - Reads the number of primary particles simulated (NPrimaries) from the statistics file (`{RadioNuclide}-stat.txt`), ensuring correct normalization of energy deposition values.

   3. **Processing Edep Data**:
      - Iterates through all files containing energy deposition (`*Edep.txt`) and uncertainties (`*Edep-Uncertainty.txt`).
      - Extracts energy deposition values and uncertainties for each spherical shell from the respective files.
      - Normalized energy deposition values (MeV/Bq) are calculated.

   4. **Output File Generation**:
      - Creates a file named `DPK-{RadioNuclide}.txt` (e.g., `DPK-F-18.txt`) in the same directory.
      - The output file contains:
        - Shell number.
        - Radius of the shell (in mm).
        - Energy deposited per unit activity (MeV/Bq).
        - Uncertainty.

  ### 3. DPK.py

  1. **Input**:
     - Reads the DPK file (`DPK-{RadioNuclide}.txt`) created by `2-DPKprocess.py`.
     - Extracts shell distances (outer radii in mm), deposited energy (Edep in MeV), and uncertainties.

  2. **Effective Radius Calculation**:
     - Computes the effective radius for each shell based on a geometric approach ([Janicki and Seuntjens 2004](https://onlinelibrary.wiley.com/doi/abs/10.1118/1.1668393)).

  3. **Absorbed Dose Calculation**:
     - Calculates the mass of each spherical shell (volume × density).
     - Calculates the absorbed dose to each shell per unit of activity. Dividing the deposited energy per unit activity (Edep/Bq) by the shell mass gives the absorbed dose per unit activity (Gy/Bq).

  4. **Interpolation**:
     - Uses cubic spline interpolation to refine DPK data points.

  5. **Output**:
     - Creates an output file (`DPK_{RadioNuclide}_fine.txt`), containing:
       - Distance from the source (in mm).
       - Dose Point Kernel (in Gy/Bq).
       - Uncertainty.

  ### 4. DVK.py

   1. **Input**:
      - RadioNuclide: Used to select the corresponding DPK data.
      - Material (media): Adjusts voxel density and relative attenuation for specific tissue types (e.g., bone, adipose tissue, lung).
      - Voxel size (vdim): Default voxel dimensions are set to 3×3×3 mm.

   2. **Reading DPK Data**:
      - Reads from the refined DPK file (`DPK_{RadioNuclide}_fine.txt`) generated by `3-DPK.py`.

   3. **Kernel Size Setup**:
      - Creates a matrix of target voxels around the source voxel.
      - Limits the number of voxels based on the DPK distance data and user-defined maximum voxel count (default 99x99x99).

   4. **Monte Carlo Simulation**:
      - Random point pairs are sampled within the target voxel and the central source voxel. The mean absorbed dose to the target voxel is calculated based on the distances between these points. The uncertainty of the method decreases with more simulated points (default: 10⁸).
      - Supports GPU acceleration when available; otherwise, defaults to CPU-based calculations.

   5. **Output**:
      - Outputs the S value (mGy/MBq), uncertainty, and mean distance.
      - **Text File**: `results_{RadioNuclide}_{voxel_dim}_{media}.txt` with tabulated results containing voxel coordinates, S value, uncertainty, and mean distance.
      - **JSON File**: `DVK_{RadioNuclide}_{voxel_dim}_{media}.json` with hierarchical structure containing S values for easy integration into OpenDose3D.

## Q&A:

  Questions and answers related to the project could be accessed through both [open](https://gitlab.com/opendose/opendose3d/-/issues/?sort=created_date&state=opened&first_page_size=20) and [closed](https://gitlab.com/opendose/opendose3d/-/issues/?sort=created_date&state=closed&first_page_size=20)  GitLab repositories. The closed repository includes all resolved questions and is the best place to check if your query has already been addressed. If your question isn’t covered or you encounter a new issue, you can submit it by clicking on **New issue** in the open repository. Our team will respond as quickly as possible.

## Roadmap:

  1. The already available features include:
      -	Sensitivity calibration module and transparent transfer to the OD3D module.
      -	Contrast recovery calibration module.
      -	Step-by-step archiving of intermediate results for retrieval and analysis.
      -	Sanity check to verify that all imported SPECT/CT images belong to the same patient and study, and time sampling evaluation through the percentage of the extrapolated part of the time integral relative to the total.
      -	Automated mode to obtain a first-order dosimetric result for organs at risk within minutes on average computers.
      -	Uncertainty calculations for time-dependent variable integration.

  2. Short term developments:
     - More calibration module options, like deadtime, CT density, or recommended [EARL-compliant calibrations](https://eanm.org/the-eanm-community/initiatives/earl/), and their integration in OD3D.
     - Enhanced sanity checks (consistency of patient acquisition and reconstruction protocols across time points, impact of cross-irradiation based on the CONV/LED ratio).
     - Single-time-point CDW for simplified dosimetry.
     - Reporting CDW steps and results.
     - Full voxel level dosimetry.
  
  3. Middle term developments:
     - Possible integration of a reconstruction module.
     - Surrogate emitter dosimetry (e.g., ß+ß-, Selective Internal Radiotherapy).
     - Radiobiology variable integration.
     - Gate 10 compatibility.
     - Uncertainty calculation for the whole CDW, including uncertainty propagation for time-integrated variables and absorbed dose calculation algorithms.

This wish list will ultimately depend on the resources assigned by the project members.


## References

  Dewaraja, Yuni K, Mark Madsen, Michael Ljungberg, and Heribert Hänscheid. 2024. “QUANTIFICATION OF ACTIVITY.” In Dosimetry for Radiopharmaceutical Therapy, Non-serial Publications, Vienna: INTERNATIONAL ATOMIC ENERGY AGENCY. https://www.iaea.org/publications/15002/dosimetry-for-radiopharmaceutical-therapy.
  
  Franquiz, J. M., S. Chigurupati, and K. Kandagatla. 2003. “Beta Voxel S Values for Internal Emitter Dosimetry.” Medical Physics 30(6): 1030–32. doi:10.1118/1.1573204.
  
  Hardiansyah, Deni, Elham Yousefzadeh-Nowshahr, Felix Kind, Ambros J. Beer, Juri Ruf, Gerhard Glatting, and Michael Mix. 2024. “Single-Time-Point 
  Renal Dosimetry Using Nonlinear Mixed-Effects Modeling and Population-Based Model Selection in [177Lu]Lu-PSMA-617 Therapy.” Journal of Nuclear Medicine. doi:10.2967/jnumed.123.266268.
  
  Janicki, Christian, and Jan Seuntjens. 2004. “Accurate Determination of Dose-Point-Kernel Functions Close to the Origin Using Monte Carlo 
  Simulations.” Medical Physics 31(4): 814–18. doi:10.1118/1.1668393.
  
  Kletting, P., S. Schimmel, H. A. Kestler, H. Hänscheid, M. Luster, M. Fernández, J. H. Bröer, et al. 2013. “Molecular Radiotherapy: The NUKFIT Software for Calculating the Time-Integrated Activity Coefficient.” Medical Physics 40(10): 102504. doi:10.1118/1.4820367.
  
  Sarrut, David, Manuel Bardiès, Nicolas Boussion, Nicolas Freud, Sébastien Jan, Jean-Michel Létang, George Loudos, et al. 2014. “A Review of the Use and Potential of the GATE Monte Carlo Simulation Code for Radiation Therapy and Dosimetry Applications.” Medical Physics 41(6Part1): 064301. doi:10.1118/1.4871617.
  
  Tran-Gia, Johannes, Ana M. Denis-Bacelar, Kelley M. Ferreira, Andrew P. Robinson, Nicholas Calvert, Andrew J. Fenwick, Domenico Finocchiaro, et al. 2021. “A Multicentre and Multi-National Evaluation of the Accuracy of Quantitative Lu-177 SPECT/CT Imaging Performed within the MRTDosimetry Project.” EJNMMI Physics 8(1): 55. doi:10.1186/s40658-021-00397-0.
